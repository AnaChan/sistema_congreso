<?php

class UsuariosCongresosController  extends BaseController {
	
	/**
	 * Muestra Las configuraciones actuales del congreso : datos generales, Fechas importantes y Mecanismo de Activacion/Desactivacion del mismo 
	 *
	 * @return View::HTML
	 */	
	public function editarCongreso($id)
	{
		$permiso = DB::table('detalle_congreso')->join(
													'congreso',
													'congreso.idCongreso',
													'=',
													'detalle_congreso.idCongreso'
												)
												->join(
													'usuariorol_x_congreso',
													'usuariorol_x_congreso.idCongreso',
													'=',
													'detalle_congreso.idCongreso'
												)
												->join(
													'estado_congreso',
													'estado_congreso.idEstadoCongreso',
													'=',
													'congreso.idEstadoCongreso'
												)
												->join(
													'rol',
													'rol.idRol',
													'=',
													'usuariorol_x_congreso.idRol'
												)
												->Where(
													'usuariorol_x_congreso.idUsuario',
													'=',
													Auth::user()->idUsuario
												)
												->Where(
													'usuariorol_x_congreso.idCongreso',
													'=',
													$id
												)      
												->get();
		
		if (count($permiso)==0 || $permiso[0]->nomRol != 'Chair') return Redirect::to('home');

		$congreso=$this->inicializarConfiguracionCongreso($id);
		$this->layout->content = View::make('congresos/ConfigurarCongreso')->with('info_congreso',$congreso)
																			->with('idCongreso',$id)
																			->with('estadoCong',$permiso[0]->valorEstado);
	}

	/**
	 * Muestra Las configuraciones actuales del congreso : Categorias, tematicas, formatos , comite 
	 *
	 * @return View::HTML
	 */	
	public function editarPostCongreso($id)
	{
		$permiso = DB::table('detalle_congreso')->join(
													'congreso',
													'congreso.idCongreso',
													'=',
													'detalle_congreso.idCongreso'
												)
												->join(
													'usuariorol_x_congreso',
													'usuariorol_x_congreso.idCongreso',
													'=',
													'detalle_congreso.idCongreso'
												)
												->join(
													'estado_congreso',
													'estado_congreso.idEstadoCongreso',
													'=',
													'congreso.idEstadoCongreso'
												)
												->join(
													'rol',
													'rol.idRol',
													'=',
													'usuariorol_x_congreso.idRol'
												)
												->Where(
													'usuariorol_x_congreso.idUsuario',
													'=',
													Auth::user()->idUsuario
												)
												->Where(
													'usuariorol_x_congreso.idCongreso',
													'=',
													$id
												)      
												->get();
		
		if (!$permiso || $permiso[0]->nomRol != 'Chair') return Redirect::to('home');

		$congreso=$this->inicializarConfiguracionPostCongreso($id);
		$this->layout->content = View::make('congresos/ConfigurarPostCongreso')->with('info_congreso',$congreso)
											->with('idCongreso',$id);
	}

	/**
	 * Muestra todos los usuarios elegidos para ser parte del equipo de revisores para una tematica
	 * El Chair puede aprobar o rechazar estas peticiones
	 * @return View::HTML
	 */	
	public function editarRevisoresPorPC($id)
	{
		try{
			$revisores = DB::table('pc_x_revisor_area')
												->join(
													'pc_x_tematica',
													'pc_x_tematica.idPcTematica',
													'=',
													'pc_x_revisor_area.idPcRevisor'
												)
												->join(
													'usuariorol_x_congreso as urcPC',
													'urcPC.idUsuariorolXCongreso',
													'=',
													'pc_x_tematica.idUsuariorolXCongreso'
												)
												->join(
													'tematica',
													'tematica.idTematica',
													'=',
													'pc_x_tematica.idTematica'
												)	
												->join(
													'rol',
													'rol.idRol',
													'=',
													'urcPC.idRol'
												)
												->join('estado_urc as estadoPC',
														'estadoPC.idEstadoURC',
														'=',
														'urcPC.idEstadoURC'

												)
												->join(
													'usuarios',
													'usuarios.idUsuario',
													'=',
													'pc_x_revisor_area.idUsuario'
												)
												->Where('estadoPC.valorEstado',
														'=',
														'ACT'
												)
												->Where(
													'urcPC.idCongreso',
													'=',
													$id
												)
												->join(
													'usuariorol_x_congreso as urcRevisor',
													'urcRevisor.idUsuario',
													'=',
													'usuarios.idUsuario'
												)
												->join('estado_urc as estadoRevisor',
														'estadoRevisor.idEstadoURC',
														'=',
														'urcRevisor.idEstadoURC'

												)
												->WhereIn('estadoRevisor.valorEstado',
														['SOL','POR']
												)
												->Where(
													'urcRevisor.idCongreso',
													'=',
													$id
												)
												->select('usuarios.idUsuario as idUsuario',
													'tematica.idTematica as idTematica',
													DB::Raw('group_concat(nomTematica) as nomTematica'),
													'pc_x_tematica.idPcTematica',
													'estadoRevisor.nombreEstado as nombreEstado')
												->groupBy('usuarios.idUsuario')
												->get();
			$this->layout->content = View::make('congresos/GestionarRevisoresTematicas')
											->with('revisores',$revisores)
											->with('idCongreso',$id);
		}catch(Exception $e)
		{
			Session::flash('message',  $e->getMessage());
			return Redirect::action('HomeController@inicio');
		}	
	}

	/**
	 * Muestra todos los usuarios elegibles para ser asignados Revisores para un PC y su tematica.
	 *
	 * @return View::HTML
	 */	
	public function invitarRevisoresPorPC($id,$idUsuario)
	{
		$tematica=null;
		$revisores=array();
		try
		{
			$UsuarioRXC=DB::table('usuariorol_x_congreso')
								->join(
									'rol',
									'rol.idRol',
									'=',
									'usuariorol_x_congreso.idRol'
								)
								->join(
									'estado_urc',
									'estado_urc.idEstadoURC',
									'=',
									'usuariorol_x_congreso.idEstadoURC'
								)
								->Where(
									'usuariorol_x_congreso.idUsuario',
									'=',
									$idUsuario
								)
								->Where(
									'usuariorol_x_congreso.idCongreso',
									'=',
									$id
								)
								->WhereIn(
									'rol.nomRol',
									['PC','Chair']
								)
								->Where(
									'estado_urc.valorEstado',
									'=',
									'ACT'
								)
								->get();
			if(count($UsuarioRXC) == 0) throw new Exception("Usted no se encuentra registrado en ninguna temática");
			$tematica=DB::table('pc_x_tematica')
							->Where(
								'pc_x_tematica.idUsuariorolXCongreso',
								'=',
								$UsuarioRXC[0]->idUsuariorolXCongreso
							)
							->get();
			if(count($tematica) == 0) throw new Exception("Usted no encuentra registrado en ninguna temática");
			$revisores = DB::table('pc_x_revisor_area')
													->join(
														'usuariorol_x_congreso',
														'usuariorol_x_congreso.idUsuario',
														'=',
														'pc_x_revisor_area.idUsuario'
													)
													->join(
														'usuarios',
														'usuarios.idUsuario',
														'=',
														'pc_x_revisor_area.idUsuario'
													)
													->join(
														'congreso',
														'congreso.idCongreso',
														'=',
														'usuariorol_x_congreso.idCongreso'
													)
													->join(
														'tematica',
														'tematica.idTematica',
														'=',
														'pc_x_revisor_area.idTematica'
													)												
													->join(
														'estado_urc',
														'estado_urc.idEstadoURC',
														'=',
														'usuariorol_x_congreso.idEstadoURC'
													)
													->join(
														'rol',
														'rol.idRol',
														'=',
														'usuariorol_x_congreso.idRol'
													)
													->Where(
														'usuariorol_x_congreso.idCongreso',
														'=',
														$id
													)
													->Where(
														'idPcRevisor',
														'=',
														$tematica[0]->idPcTematica
													)
													->WhereIn(
														'estado_urc.valorEstado',
														['POR','INV','ACT','SOL']
													)
													->get();
		}catch(Exception $e)
		{
			Session::flash('message',  $e->getMessage());
			return Redirect::action('UsuariosCongresosController@establecerCongreso',array($id));
		}	

		$this->layout->content = View::make('congresos/InvitarRevisores')
														->with('revisores',$revisores)
														->with('idCongreso',$id)
														->with('tematica',$tematica);
	}

	/**
	 * Determina el Rol y los permisos del usuario solicitante para establecer su interfaz respectiva
	 *
	 * @return View::HTML
	 */	
	public function establecerCongreso($idCongreso)
	{

		$permiso = DB::table('detalle_congreso')
		->join(
			'congreso',
			'congreso.idCongreso',
			'=',
			'detalle_congreso.idCongreso'
		)
		->join(
			'usuariorol_x_congreso',
			'usuariorol_x_congreso.idCongreso',
			'=',
			'detalle_congreso.idCongreso'
		)
		->join(
			'estado_congreso',
			'estado_congreso.idEstadoCongreso',
			'=',
			'congreso.idEstadoCongreso'
		)
		->join(
			'rol',
			'rol.idRol',
			'=',
			'usuariorol_x_congreso.idRol'
		)
		->join('estado_urc',
				'estado_urc.idEstadoURC',
				'=',
				'usuariorol_x_congreso.idEstadoURC'
			)
		->Where(
			'usuariorol_x_congreso.idUsuario',
			'=',
			Auth::user()->idUsuario
		)
		->Where(
			'usuariorol_x_congreso.idCongreso',
			'=',
			$idCongreso
		)
		->Where('estado_urc.valorEstado',
				'=',
				'ACT'
		)
		->orWhere('rol.nomRol',
				'=',
				'Autor'
		)
		->WhereIn('estado_urc.valorEstado',
				['POR','INV','SOL']
		)
		->select('estado_congreso.valorEstado as valorEstado','detalle_congreso.idDetalleCongreso as idDetalleCongreso','rol.nomRol as nomRol')
		->get();
		if (count($permiso)==0){
			Session::flash('message',  'Su cuenta se encuentra inactiva o used ha recibido una invitación para cumplir un rol diferente para este congreso. Asegurese de revisar su correo');
			return Redirect::action('HomeController@inicio');
		}
		switch ($permiso[0]->valorEstado) {
			
			case 'SNC':
				if ($permiso[0]->nomRol == 'Chair') {
					return Redirect::action('UsuariosCongresosController@editarCongreso', array($idCongreso));
				}
				else return Redirect::action('HomeController@inicio');
				break;
			case 'CER':

				$fechas = DB::table('fecha_importante')
				->join(
					'tipo_fecha',
					'tipo_fecha.idTipoFecha',
					'=',
					'fecha_importante.idTipoFecha'
				)
				->join(
					'detalle_congreso',
					'detalle_congreso.idDetalleCongreso',
					'=',
					'fecha_importante.idDetalleCongreso'
				)
				->where(
					'detalle_congreso.idCongreso',
					'=',
					$idCongreso
				)
				->orderBy(
					'fecInicio',
					'asc'
				)
				->get();

				$personasInscritas = DB::table('usuariorol_x_congreso')
				->where(
					'usuariorol_x_congreso.idCongreso',
					$idCongreso
				)
				->where(
					'usuariorol_x_congreso.idUsuario',
					'!=',
					Auth::user()->idUsuario
				)
				->count();

				$pcInscritos = DB::table('usuariorol_x_congreso')
				->join(
					'rol',
					'rol.idRol',
					'=',
					'usuariorol_x_congreso.idRol'
				)
				->where(
					'usuariorol_x_congreso.idCongreso',
					$idCongreso
				)
				->where(
					'usuariorol_x_congreso.idUsuario',
					'!=',
					Auth::user()->idUsuario
				)
				->where(
					'rol.nomRol',
					'PC'
				)
				->count();

				$revisoresInscritos = DB::table('usuariorol_x_congreso')
				->join(
					'rol',
					'rol.idRol',
					'=',
					'usuariorol_x_congreso.idRol'
				)
				->where(
					'usuariorol_x_congreso.idCongreso',
					$idCongreso
				)
				->where(
					'usuariorol_x_congreso.idUsuario',
					'!=',
					Auth::user()->idUsuario
				)
				->where(
					'rol.nomRol',
					'Revisor'
				)
				->count();

				$autoresInscritos = DB::table('usuariorol_x_congreso')
				->join(
					'rol',
					'rol.idRol',
					'=',
					'usuariorol_x_congreso.idRol'
				)
				->where(
					'usuariorol_x_congreso.idCongreso',
					$idCongreso
				)
				->where(
					'usuariorol_x_congreso.idUsuario',
					'!=',
					Auth::user()->idUsuario
				)
				->where(
					'rol.nomRol',
					'Autor'
				)
				->count();

				$fichasInscritas = DB::table('ficha')
				->where(
					'ficha.idDetalleCongreso',
					$permiso[0]->idDetalleCongreso
				)
				->count();

				$estado1 = EstadoFicha::where('nombreEstado','=','APROBADA')->first();
				$fichasAceptadas = DB::table('ficha')
				->where(
					'ficha.idDetalleCongreso',
					$permiso[0]->idDetalleCongreso
				)
				->where('ficha.idEstadoFicha',$estado1)
				->count();

				$estado2 = EstadoFicha::where('nombreEstado','=','PENDIENTE')->first();
				$fichasSinRevisar = DB::table('ficha')
				->where(
					'ficha.idDetalleCongreso',
					$permiso[0]->idDetalleCongreso
				)
				->where('ficha.idEstadoFicha',$estado2)
				->count();

				$nombreCongreso = DB::table('congreso')
				->where(
					'congreso.idCongreso',
					$idCongreso
				)
				->select('nomCongreso')
				->first();

				$acronimoCongreso = DB::table('congreso')
				->where(
					'congreso.idCongreso',
					$idCongreso
				)
				->select('acronimoCongreso')
				->first();

				$bannerCongreso = DB::table('detalle_congreso')
				->where(
					'detalle_congreso.idCongreso',
					$idCongreso
				)
				->select('banner')
				->first();

				$ListaFichas = DB::table('ficha')
								->join('paper','paper.idFicha','=','ficha.idFicha')
								->join('estado_paper','estado_paper.idEstadoPaper','=','paper.idEstadoPaper')
								->where('ficha.idDetalleCongreso', '=', $permiso[0]->idDetalleCongreso)
								->get();


				$TodasFichas = array();
				foreach ($ListaFichas as $value) {
					$autores = DB::select("SELECT GROUP_CONCAT(usuarios.nombreUsuario,' ',usuarios.apelUsuario SEPARATOR ', ') autores
											FROM autor_x_ficha
											inner join usuarios on usuarios.idUsuario=autor_x_ficha.idUsuario
											where idFicha=(?)", array($value->idFicha));
					
					$arreglo = array(
						'idFicha' =>$value->idFicha,
						'idPaper'=>$value->idPaper,
						'tituloPaper'=>$value->tituloPaper,
						'nombreEstado'=>$value->nombreEstado,
						'autores'=>$autores[0]->autores,
						'rutaFicha'=>$value->rutaFicha,
						'rutaPaper'=>$value->rutaPaper
						);

					array_push($TodasFichas, $arreglo);

				}


				if(strcmp($permiso[0]->nomRol,'Chair')!=0)
					$this->layout->mostrar_barra=False;
				else
					$this->layout->mostrar_barra=True;
				$this->layout->content = View::make('congresos.congresoCerrado')
				->with('nomRol', $permiso[0]->nomRol)
				->with('fechasImportantes', $fechas)
				->with('idCongreso', $idCongreso)
				->with('personasInscritas',$personasInscritas)
				->with('pcInscritos',$pcInscritos)
				->with('revisoresInscritos',$revisoresInscritos)
				->with('autoresInscritos',$autoresInscritos)
				->with('fichasInscritas',$fichasInscritas)
				->with('fichasAceptadas',$fichasAceptadas)
				->with('fichasSinRevisar',$fichasSinRevisar)
				->with('nombreCongreso',$nombreCongreso->nomCongreso)
				->with('acronimoCongreso',$acronimoCongreso->acronimoCongreso)
				->with('listaArticulos',$TodasFichas)
				->with('banner',URL::asset('banners/'.$bannerCongreso->banner));
				break;
			case 'ACT':
				$fechas = DB::table('fecha_importante')
				->join(
					'tipo_fecha',
					'tipo_fecha.idTipoFecha',
					'=',
					'fecha_importante.idTipoFecha'
				)
				->join(
					'detalle_congreso',
					'detalle_congreso.idDetalleCongreso',
					'=',
					'fecha_importante.idDetalleCongreso'
				)
				->where(
					'detalle_congreso.idCongreso',
					'=',
					$idCongreso
				)
				->orderBy(
					'fecInicio',
					'asc'
				)
				->get();

				$personasInscritas = DB::table('usuariorol_x_congreso')
				->where(
					'usuariorol_x_congreso.idCongreso',
					$idCongreso
				)
				->where(
					'usuariorol_x_congreso.idUsuario',
					'!=',
					Auth::user()->idUsuario
				)
				->count();

				$pcInscritos = DB::table('usuariorol_x_congreso')
				->join(
					'rol',
					'rol.idRol',
					'=',
					'usuariorol_x_congreso.idRol'
				)
				->where(
					'usuariorol_x_congreso.idCongreso',
					$idCongreso
				)
				->where(
					'usuariorol_x_congreso.idUsuario',
					'!=',
					Auth::user()->idUsuario
				)
				->where(
					'rol.nomRol',
					'PC'
				)
				->count();

				$revisoresInscritos = DB::table('usuariorol_x_congreso')
				->join(
					'rol',
					'rol.idRol',
					'=',
					'usuariorol_x_congreso.idRol'
				)
				->where(
					'usuariorol_x_congreso.idCongreso',
					$idCongreso
				)
				->where(
					'usuariorol_x_congreso.idUsuario',
					'!=',
					Auth::user()->idUsuario
				)
				->where(
					'rol.nomRol',
					'Revisor'
				)
				->count();

				$autoresInscritos = DB::table('usuariorol_x_congreso')
				->join(
					'rol',
					'rol.idRol',
					'=',
					'usuariorol_x_congreso.idRol'
				)
				->where(
					'usuariorol_x_congreso.idCongreso',
					$idCongreso
				)
				->where(
					'usuariorol_x_congreso.idUsuario',
					'!=',
					Auth::user()->idUsuario
				)
				->where(
					'rol.nomRol',
					'Autor'
				)
				->count();

				$fichasInscritas = DB::table('ficha')
				->where(
					'ficha.idDetalleCongreso',
					$permiso[0]->idDetalleCongreso
				)
				->count();

				$estado1 = EstadoFicha::where('nombreEstado','=','APROBADA')->first();
				$fichasAceptadas = DB::table('ficha')
				->where(
					'ficha.idDetalleCongreso',
					$permiso[0]->idDetalleCongreso
				)
				->where('ficha.idEstadoFicha','=',$estado1->idEstadoFicha)
				->count();

				$estado2 = EstadoFicha::where('nombreEstado','=','PENDIENTE')->first();
				$fichasSinRevisar = DB::table('ficha')
				->where(
					'ficha.idDetalleCongreso',
					$permiso[0]->idDetalleCongreso
				)
				->where('ficha.idEstadoFicha',$estado2->idEstadoFicha)
				->count();

				$nombreCongreso = DB::table('congreso')
				->where(
					'congreso.idCongreso',
					$idCongreso
				)
				->select('nomCongreso')
				->first();

				$acronimoCongreso = DB::table('congreso')
				->where(
					'congreso.idCongreso',
					$idCongreso
				)
				->select('acronimoCongreso')
				->first();

				$bannerCongreso = DB::table('detalle_congreso')
				->where(
					'detalle_congreso.idCongreso',
					$idCongreso
				)
				->select('banner')
				->first();


				$ListaFichas = DB::table('ficha')
								->join('estado_ficha','estado_ficha.idEstadoFicha','=','ficha.idEstadoFicha')
								->where('ficha.idDetalleCongreso', '=', $permiso[0]->idDetalleCongreso)
								->get();


				$TodasFichas = array();
				foreach ($ListaFichas as $value) {
					$autores = DB::select("SELECT GROUP_CONCAT(usuarios.nombreUsuario,' ',usuarios.apelUsuario SEPARATOR ', ') autores
											FROM autor_x_ficha
											inner join usuarios on usuarios.idUsuario=autor_x_ficha.idUsuario
											where idFicha=(?)", array($value->idFicha));
					
					$tematicaFicha = Tematica::where('idTematica','=',$value->idTematica)->first(); 
					$responsableFicha = DB::table('autor_x_ficha')
										->where('autor_x_ficha.idFicha','=',$value->idFicha)
										->where('autor_x_ficha.responsable','=',1)
										->get();


					$arreglo = array(
						'idFicha' =>$value->idFicha,
						'Tematica' =>$tematicaFicha->nomTematica,
						'tituloPaper'=>$value->tituloPaper,
						'nombreEstado'=>$value->nombreEstado,
						'autores'=>$autores[0]->autores,
						'rutaFicha'=>$value->rutaFicha
						);

					array_push($TodasFichas, $arreglo);

				}


				$this->layout->content = View::make('congresos.home')
				->with('nomRol', $permiso[0]->nomRol)
				->with('fechasImportantes', $fechas)
				->with('idCongreso', $idCongreso)
				->with('personasInscritas',$personasInscritas)
				->with('pcInscritos',$pcInscritos)
				->with('revisoresInscritos',$revisoresInscritos)
				->with('autoresInscritos',$autoresInscritos)
				->with('fichasAceptadas',$fichasAceptadas)
				->with('fichasSinRevisar',$fichasSinRevisar)
				->with('fichasInscritas',$fichasInscritas)
				->with('listaArticulos',$TodasFichas)
				->with('nombreCongreso',$nombreCongreso->nomCongreso)
				->with('acronimoCongreso',$acronimoCongreso->acronimoCongreso)
				->with('banner',URL::asset('banners/'.$bannerCongreso->banner));
				break;
			case 'REC':
				App::abort(404);
				break;
			case 'INS':
				return Redirect::action('HomeController@inicio');
				break;
		}
	}

	/**
	 * Muestra todos los usuarios elegibles para ser asignados como Miembros del comite (PC) HTML.
	 *
	 * @return HTML
	 */	
	public function usuariosDisponiblesPC()
	{
		$id= Input::get('idCongreso');
		$usuariosEnLista= json_decode(Input::get('idRepetidos'));

		$usuarios=DB::table('usuariorol_x_congreso') //trae los usuarios que ya estan como PC para este congreso
							->join(
								'rol',
								'rol.idRol',
								'=',
								'usuariorol_x_congreso.idRol'
							)
							->join(
								'usuarios',
								'usuarios.idUsuario',
								'=',
								'usuariorol_x_congreso.idUsuario'
							)
							->join(
								'estado_usuario',
								'estado_usuario.idEstadoUsuario',
								'=',
								'usuarios.idEstadoUsuario'
							)
							->join('pc_x_tematica',
									'pc_x_tematica.idUsuariorolXCongreso',
									'=',
									'usuariorol_x_congreso.idUsuariorolXCongreso'
							)
							->Where(
								'usuariorol_x_congreso.idCongreso',
								'=',
								$id)
							->select('usuariorol_x_congreso.idUsuario')
							->lists('usuarios.idUsuario');	

		$revisores=DB::table('usuariorol_x_congreso')//trae a los revisores de este congreso para evitar quitarle revisores a otros PC
						->join('rol',
								'rol.idRol',
								'=',
								'usuariorol_x_congreso.idRol'
							)
						->where('rol.nomRol',
								'=',
								'Revisor'
							)
						->where('usuariorol_x_congreso.idCongreso',
								'=',
								$id
							)
						->lists('usuariorol_x_congreso.idUsuario');

		$chair=DB::table('usuariorol_x_congreso')//trae al admin del congreso (CHAIR)
						->join('rol',
								'rol.idRol',
								'=',
								'usuariorol_x_congreso.idRol'
							)
						->where('rol.nomRol',
								'=',
								'Chair'
							)
						->where('usuariorol_x_congreso.idCongreso',
								'=',
								$id
							)
						->lists('usuariorol_x_congreso.idUsuario');

		$admin=DB::table('usuarios') //trae al admin
					->join('estado_usuario',
							'estado_usuario.idEstadoUsuario',
							'=',
							'usuarios.idEstadoUsuario'
						)
					->where('estado_usuario.valorEstado','=','ADM')
					->lists('usuarios.idUsuario');

		$usuariosNuevo=array_merge($usuarios,$usuariosEnLista,$revisores,$admin,$chair);
		$filtrados = DB::table('usuarios')
					->whereNotIn('usuarios.idUsuario',$usuariosNuevo)
					->get();

		return $this->generarHTML($filtrados);	
	}

	/**
	 * Muestra todos los usuarios elegibles para ser asignados como Revisores HTML.
	 *
	 * @return HTML
	 */	
	public function usuariosDisponiblesRevisores()
	{
		$id= Input::get('idCongreso');
		$idPcRevisor=Input::get('idPcRevisor');
		$idTematica= Input::get('idTematica');
		$usuariosEnLista= json_decode(Input::get('idRepetidos'));

		$usuarios=DB::table('pc_x_revisor_area')
							->join(
								'usuariorol_x_congreso',
								'usuariorol_x_congreso.idUsuario',
								'=',
								'pc_x_revisor_area.idUsuario'
							)
							->join(
								'rol',
								'rol.idRol',
								'=',
								'usuariorol_x_congreso.idRol'
							)
							->join(
								'estado_urc',
								'estado_urc.idEstadoURC',
								'=',
								'usuariorol_x_congreso.idEstadoURC'
							)
							->Where(
								'pc_x_revisor_area.idPcRevisor',
								'=',
								$idPcRevisor
							)
							->WhereIn(
								'estado_urc.valorEstado',
								['ACT','POR','INV','SOL'] //hmmm que pasa si esta inactivo, mas bien ¿Se dará ese escenario en alguna ocasión?
							)
							->select('pc_x_revisor_area.idUsuario')
							->lists('idUsuario');


		$usuarios2=DB::table('usuariorol_x_congreso')
							->join(
								'rol',
								'rol.idRol',
								'=',
								'usuariorol_x_congreso.idRol'
							)
							->join(
								'estado_urc',
								'estado_urc.idEstadoURC',
								'=',
								'usuariorol_x_congreso.idEstadoURC'
							)
							->WhereIn('rol.nomRol',
								['PC','Chair']
							)
							->Where(
								'usuariorol_x_congreso.idCongreso',
								'=',
								$id
							)
							->WhereIn(
								'estado_urc.valorEstado',
								['ACT','POR','INV'] //¿Que pasa si esta inactivo? mas bien ¿Se dará ese escenario en alguna ocasión?
							)
							->select('usuariorol_x_congreso.idUsuario')
							->lists('idUsuario');

		$admin=DB::table('usuarios') //trae al admin
					->join('estado_usuario',
							'estado_usuario.idEstadoUsuario',
							'=',
							'usuarios.idEstadoUsuario'
						)
					->where('estado_usuario.valorEstado','=','ADM')
					->lists('usuarios.idUsuario');
					
		$usuariosNuevo=array_merge($usuarios,$usuariosEnLista,$admin,$usuarios2);

		$filtrados = DB::table('usuarios')
					->whereNotIn('usuarios.idUsuario',$usuariosNuevo)
					->get();
		return $this->generarHTMLRev($filtrados);	
	}

	/**
	 * Muestra la información general(Nombre, Acronimo, fechas importantes,etc) configurada para un congreso especifico.
	 *
	 * @return Array
	 */	
	public function inicializarConfiguracionCongreso($id)
	{
		$congreso_detalle=DB::table('detalle_congreso')
			->join('congreso','congreso.idCongreso','=','detalle_congreso.idCongreso')
			->Where('detalle_congreso.idCongreso','=',  $id)      
			->get();

		$fechas_importantes=DB::table('detalle_congreso')
			->join('fecha_importante','fecha_importante.idDetalleCongreso','=','detalle_congreso.idDetalleCongreso')
			->join('tipo_fecha','tipo_fecha.idTipoFecha','=','fecha_importante.idTipoFecha')
			->Where('detalle_congreso.idCongreso','=',  $id)
			->get();


		$tipo_fecha=DB::table('tipo_fecha')
			->get();

		$info_congreso['general'] = $congreso_detalle;
		$info_congreso['fechas_importantes'] = $fechas_importantes;
		$info_congreso['tipo_fecha'] = $tipo_fecha;

		return $info_congreso;

	}

	/**
	 * Muestra la información secundaria (tematicas,categorias,miembros del comite) configurada para un congreso especifico.
	 *
	 * @return Array
	 */	

	public function inicializarConfiguracionPostCongreso($id)
	{
		$congreso_detalle=DB::table('detalle_congreso')
			->join('congreso','congreso.idCongreso','=','detalle_congreso.idCongreso')
			->Where('detalle_congreso.idCongreso','=',  $id)      
			->get();

		$fechas_importantes=DB::table('detalle_congreso')
			->join('fecha_importante','fecha_importante.idDetalleCongreso','=','detalle_congreso.idDetalleCongreso')
			->Where('detalle_congreso.idCongreso','=',  $id)
			->get();

		$categorias= DB::select( DB::raw("SELECT dcc.idCategoria,dcc.nomCategoria,IFNULL(idCongreso,'NULL') idCongreso,filtrado.idDetalleCongreso idDetalleCongreso
													FROM categoria dcc
													LEFT OUTER JOIN (SELECT idCategoria,idDetalleCongreso,idCongreso FROM categoria cat
													JOIN detallecongreso_x_categoria dcc using(idCategoria)
													LEFT OUTER JOIN detalle_congreso dc using(idDetalleCongreso) 
													WHERE dc.idCongreso='$id') filtrado using(idCategoria)
													"));	

		$extensiones=DB::select( DB::raw("SELECT ext.idExtensionDocumento idExtensionDocumento,filtrado.idDetalleCongreso idDetalleCongreso,ext.nomExtension nomExtension, IFNULL(idCongreso,'NULL') idCongreso 
										FROM extension_documento ext		
										LEFT OUTER JOIN (select idDetalleCongreso,idExtensionDocumento,idCongreso
										FROM extension_documento ext
										JOIN detallecongreso_x_extension dext using(idExtensionDocumento)
										LEFT OUTER JOIN detalle_congreso dc using(idDetalleCongreso) 
										WHERE dc.idCongreso='$id') filtrado   using(idExtensionDocumento)"));

		$tematicas=DB::select( DB::raw("SELECT tematica.idTematica idTematica,tematica.nomTematica nomTematica,IFNULL(idCongreso,'NULL') idCongreso from tematica
										left outer join (select * from tematica te
														JOIN congreso_x_tematica cxt using(idTematica)
														inner join congreso c using(idCongreso)
														where c.idCongreso='$id') filtrado using(idTematica)"));

		$miembrosPC=DB::table('detalle_congreso')
			->join('usuariorol_x_congreso','usuariorol_x_congreso.idCongreso','=','detalle_congreso.idCongreso')
			->join('pc_x_tematica','pc_x_tematica.idUsuariorolXCongreso','=','usuariorol_x_congreso.idUsuariorolXCongreso')
			->Where('detalle_congreso.idCongreso','=',  $id)		
			->get();

		$info_congreso['general'] = $congreso_detalle;
		$info_congreso['categorias'] = $categorias;
		$info_congreso['extensiones'] = $extensiones;
		$info_congreso['tematicas'] = $tematicas;
		$info_congreso['comitePC'] = $miembrosPC;

		return $info_congreso;

	}

	/**
	 * Elimina un registro de cualquiera tipo en la base 
	 *
	 * @return JSON
	 */	
	public function eliminarRegistro()
	{
		return DB::transaction(function()
		{
			DB::beginTransaction();
			$id= Input::get('idRegistro');
			$tabla= Input::get('nomTabla');
			try {
				if(strcmp ($tabla,'tablaFechas')==0)
				{				
					$idDetalleCongreso= Input::get('idDetalleCongreso');
					$Congreso=DetalleCongreso::Where("idDetalleCongreso","=",$idDetalleCongreso)->first();
					$idCongreso=$Congreso->idCongreso;
					$verif=FechaImportante::whereRaw('idTipoFecha=? and idDetalleCongreso=?', array($id,$idDetalleCongreso))->delete();
					if(count($verif)==0)throw new Exception('Al actualizar las fechas importantes');
				}

				if(strcmp ($tabla,'tablaPC')==0)
				{
					$idDetalleCongreso= Input::get('idDetalleCongreso');
					$Congreso=DetalleCongreso::Where("idDetalleCongreso","=",$idDetalleCongreso)->first();
					if(count($Congreso)==0)throw new Exception('miembros PC');
					$idCongreso=$Congreso->idCongreso;
					$idUsuarioPCXT=Input::get('idPcTematica');
					$idUsr=Input::get('idUsr');

					$revisoresPCXT=DB::table('pc_x_revisor_area')
										->Where('pc_x_revisor_area.idPcRevisor',
												'=',
												$idUsuarioPCXT
											)->first();

					if(count($revisoresPCXT)!=0)
						throw new Exception("No se puede eliminar un miembro del comité que contiene revisores asignados para su temática.");

					$delPCXT=PcXTematica::whereRaw('idPcTematica=?',array($idUsuarioPCXT))->delete();
					if(count($delPCXT)==0)throw new Exception('miembro del comité');

					$idRol = Rol::where('nomRol', '=', 'Autor')->first();
					if(count($idRol)==0)throw new Exception('miembro del comité');

					$checkRol=DB::table('usuariorol_x_congreso')
									->join('rol',
											'rol.idRol',
											'=',
											'usuariorol_x_congreso.idRol'
										)
									->where('usuariorol_x_congreso.idUsuario',
											'=',
											$idUsr
											)
									->where('usuariorol_x_congreso.idCongreso',
											'=',
											$idCongreso
										)
									->where('rol.nomRol',
											'=',
											'Chair'
										)->first();
					
					if(count($checkRol)==0){//Cambiar rol solo si su rango es menor a chair
						$verif=UsuariorolXCongreso::whereRaw('idUsuario=? and idCongreso=?', array($idUsr,$idCongreso))->update(array('idRol' => $idRol->idRol));
						if(count($verif)==0)throw new Exception('Al cambiar rol miembro del comité');
					}
				}

				if(strcmp ($tabla,'tiposarchivo')==0)
				{
					$idDetalleCongreso= Input::get('idDetalleCongreso');
					$Congreso=DetalleCongreso::Where("idDetalleCongreso","=",$idDetalleCongreso)->first();
					$verif=DetallecongresoXExtension::whereRaw('idDetalleCongreso=? and idExtensionDocumento=?', array($idDetalleCongreso,$id))->delete();
					if(count($verif)==0)throw new Exception('Al actualizar los tipos de archivo');
				}	
				
				if(strcmp ($tabla,'categoria')==0)
				{
					$idDetalleCongreso= Input::get('idDetalleCongreso');
					$Congreso=DetalleCongreso::Where("idDetalleCongreso","=",$idDetalleCongreso)->first();	
					$verif=DetallecongresoXCategoria::whereRaw('idDetalleCongreso=? and idCategoria=?', array($idDetalleCongreso,$id))->delete();
					if(count($verif)==0)throw new Exception('Al actualizar las categorias');
				}
				
				if(strcmp ($tabla,'tematica')==0)
				{
					$idDetalleCongreso= Input::get('idDetalleCongreso');
					$Congreso=DetalleCongreso::Where("idDetalleCongreso","=",$idDetalleCongreso)->first();
					$idCongreso=$Congreso->idCongreso;
					$verif=CongresoXTematica::whereRaw('idCongreso=? and idTematica=?', array($idCongreso,$id))->delete();
					if(count($verif)==0)throw new Exception('Al actualizar las tematicas');
				}

				if(strcmp ($tabla,'revisores')==0)
				{
					$idRolI = Rol::where('nomRol', '=', 'Revisor')->first()->idRol;
					$idURC = EstadoURC::where('valorEstado', '=', 'ACT')->first()->idEstadoURC;
					$idC=Input::get('idCongreso');
					$idPcRevisor=Input::get('idPcTematica');
					$idTematica=Input::get("idTematica");
					$id=Input::get("idRegistro");
					$idRol = Rol::where('nomRol', '=', 'Autor')->first(); //valirdar si esta inscrito en otra tematica como revisor antes de cambiar el rol
					if(count($idRol)==0)throw new Exception('Al actualizar revisores');
					$verif2=PcXRevisorArea::whereRaw('idPcRevisor=? and idTematica=? and idUsuario=?', array($idPcRevisor,$idTematica,$id))->delete();					
					if(count($verif2)==0)throw new ExceptionException('Al actualizar los revisores');
					$solicitudes=DB::table('pc_x_revisor_area')
											->join('pc_x_tematica',
													'pc_x_tematica.idPcTematica',
													'=',
													'pc_x_revisor_area.idPcRevisor'
												)
											->join('tematica',
													'tematica.idTematica',
													'=',
													'pc_x_tematica.idTematica'
												)
											->join('usuariorol_x_congreso',
													'usuariorol_x_congreso.idUsuariorolXCongreso',
													'=',
													'pc_x_tematica.idUsuariorolXCongreso'
												)
											->where('pc_x_revisor_area.idUsuario',
													'=',
													$id
												)
											->where('usuariorol_x_congreso.idCongreso',
													'=',
													$idC
												)
											->get();
					if(count($solicitudes)==0){ //Si no es revisor en otra tematica entonces se le cambia el rol a Autor
						$verif=UsuariorolXCongreso::whereRaw('idCongreso=? and idUsuario=?', array($idC,$id))->update(array('idRol' => $idRol->idRol,'idEstadoURC'=>$idURC));
						if(count($verif)==0)throw new ExceptionException('Al actualizar los revisores');
					}
					
				}
				if(strcmp ($tabla,'revisoresGrupo')==0)
				{
					$idURC = EstadoURC::where('valorEstado', '=', 'ACT')->first()->idEstadoURC;
					$idC=Input::get('idCongreso');
					$idRol = Rol::where('nomRol', '=', 'Autor')->first(); //valirdar si esta inscrito en otra tematica como revisor antes de cambiar el rol
					if(count($idRol)==0)throw new Exception('Al actualizar revisores');

					$id=Input::get("idRegistro");

					$solicitudes=DB::table('pc_x_revisor_area')
											->join('pc_x_tematica',
													'pc_x_tematica.idPcTematica',
													'=',
													'pc_x_revisor_area.idPcRevisor'
												)
											->join('tematica',
													'tematica.idTematica',
													'=',
													'pc_x_tematica.idTematica'
												)
											->join('usuariorol_x_congreso',
													'usuariorol_x_congreso.idUsuariorolXCongreso',
													'=',
													'pc_x_tematica.idUsuariorolXCongreso'
												)
											->where('pc_x_revisor_area.idUsuario',
													'=',
													$id
												)
											->where('usuariorol_x_congreso.idCongreso',
													'=',
													$idC
												)
											->get();
					foreach($solicitudes as $solicitud)
					{
						$verif2=PcXRevisorArea::whereRaw('idPcRevisor=? and idTematica=? and idUsuario=?', array($solicitud->idPcRevisor,$solicitud->idTematica,$id))->delete();					
						if(count($verif2)==0)throw new ExceptionException('Al actualizar los revisores');
					}
					$verif=UsuariorolXCongreso::whereRaw('idCongreso=? and idUsuario=?', array($idC,$id))->update(array('idRol' => $idRol->idRol,'idEstadoURC'=>$idURC));
				}
				DB::commit();
				return Response::json(array('error' => False, 'mensaje' => 'La fecha ha sido eliminada exitosamente.'));
			} catch(Exception $e)
			{
				DB::rollback();
				return Response::json(array('error' => True, 'mensaje' => 'ERROR: '.$e->getMessage()));
			}
		});

	}

	/**
	 * Aprueba la petición de un chair para un revisor en un congreso
	 *
	 * @return JSON
	 */	
	public function aprobarRegistroRevisor()
	{
		return DB::transaction(function()
		{
			DB::beginTransaction();
			try{
				$id= Input::get('idUsuario');
				$idC=Input::get('idCongreso');
				$leEstadoURC=DB::table('estado_urc')->Where('valorEstado','=','INV')->first();
				$verif=UsuariorolXCongreso::whereRaw('idCongreso=? and idUsuario=?', array($idC,$id))->update(array('idEstadoURC' => $leEstadoURC->idEstadoURC));
				if(count($verif)==0 || count($leEstadoURC)==0)throw new Exception("ERROR: Al actualizar el registro.");
				$usr=Usuario::where('idUsuario', '=', $id)->first();
				$congreso=Congreso::where('idCongreso','=',$idC)->first();
				$dc=DetalleCongreso::where('idCongreso','=',$idC)->first();
				$codConf=UsuariorolXCongreso::whereRaw('idCongreso=? and idUsuario=?', array($idC,$id))->first();
				$banner = URL::asset('banners/'.$dc->banner);

				$solicitudes=DB::table('pc_x_revisor_area')
										->join('pc_x_tematica',
												'pc_x_tematica.idPcTematica',
												'=',
												'pc_x_revisor_area.idPcRevisor'
											)
										->join('tematica',
												'tematica.idTematica',
												'=',
												'pc_x_tematica.idTematica'
											)
										->join('usuariorol_x_congreso',
												'usuariorol_x_congreso.idUsuariorolXCongreso',
												'=',
												'pc_x_tematica.idUsuariorolXCongreso'
											)
										->where('pc_x_revisor_area.idUsuario',
												'=',
												$id
											)
										->where('usuariorol_x_congreso.idCongreso',
												'=',
												$idC
											)
										->get();

				foreach($solicitudes as $solicitud)
				{
					$idPcTematica=$solicitud->idPcTematica;
					$idTematica=$solicitud->idTematica;
					$sent=Mail::send(
						'emails.registroCongresoConfirmar', 
						array(
							'nombreSistema'	=>	$congreso->acronimoCongreso,
							'nombreSistemaCompleto'	=>	$congreso->nomCongreso,
							'correo'		=>	$usr->emailUsuario,
							'nombres'		=>	$usr->nombreUsuario,
							'apellidos'		=>	$usr->apelUsuario,
							'nomTematica'	=>	$solicitud->nomTematica,
							'anio'			=>	date("Y"),
							'rolSistema'	=> 'Revisor',
							'URLConfirmar'	=> URL::action('UsuariosCongresosController@aceptarRolRevisor', 
															array('idCongreso'=>$idC,'codConf'=>$codConf->idUsuariorolXCongreso)),
							'banner_email' 	=>	$banner,
							'URLSitio'		=> URL::action('UsuariosCongresosController@rechazarRolRevisor', 
															array('c0'=>$idC,'ccnf'=>$codConf->idUsuariorolXCongreso,
																'pta'=>$idPcTematica,'tca'=>$idTematica,
																'hsu'=>$id)),
						), 
						function($mensaje) use ($usr)
						{
							$mensaje->to($usr->emailUsuario, $usr->nombreUsuario . ' ' . $usr->apelUsuario)
									->subject('CONUCA: Notificación de Invitacion en Congreso');
						}
					);
					if($sent==0)
						throw new Exception("ERROR: al enviar el correo de confirmación, por favor intente nuevamente.");
				}
				DB::commit();
				return Response::json(array('error' => False, 'mensaje' => 'Registro aprobado exitosamente.'));
			} catch(Exception $e)
			{
				DB::rollback();
				return Response::json(array('error' => True, 'mensaje' => 'ERROR: al aprobar el registro. '.$e->getMessage()));
			}
		});
	}

	/**
	 * Funcion que permite que un usuario acepte la invitación enviada por el PC para que sea revisor de un congreso/tematica
	 *
	 * @return JSON
	 */	
	public function aceptarRolRevisor()
	{
		return DB::transaction(function()
		{
			DB::beginTransaction();
			try{
				
				$id=Input::get("codConf");
				$leEstadoURC=DB::table('estado_urc')->Where('valorEstado','=','ACT')->get()[0];
				$idRol = Rol::where('nomRol', '=', 'Revisor')->first()->idRol;
				$verif=UsuariorolXCongreso::whereRaw('idUsuariorolXCongreso=?', array($id))->update(array('idEstadoURC' => $leEstadoURC->idEstadoURC,
																											'idRol' => $idRol));
				if(count($verif)==0 || !$leEstadoURC)throw new Exception("ERROR: Al actualizar el registro. ");
				$idC=UsuariorolXCongreso::whereRaw('idUsuariorolXCongreso=?', array($id))->first();
				$usr=Usuario::where('idUsuario', '=', $idC->idUsuario)->first();
				$congreso=Congreso::where('idCongreso','=',$idC->idCongreso)->first();
				$dc=DetalleCongreso::where('idCongreso','=',$idC->idCongreso)->first();
				if(count($usr)==0 || count($congreso)==0)throw new Exception("ERROR: Al actualizar el registro. ".$congreso);
				$banner = URL::asset('banners/'.$dc->banner);
				Mail::send(
					'emails.notificacionUsuario', 
					array(
						'nombreSistema'	=>	$congreso->acronimoCongreso,
						'nombreSistemaCompleto'	=>	$congreso->nomCongreso,
						'correo'		=>	$usr->emailUsuario,
						'nombres'		=>	$usr->nombreUsuario,
						'apellidos'		=>	$usr->apelUsuario,
						'anio'			=>	date("Y"),
						'rolSistema'	=> 'Revisor',
						'URLConfirmar'	=> '',
						'banner_email' 	=>	$banner,
						'URLSitio'		=> URL::action('UsuariosCongresosController@establecerCongreso', 
															array($idC->idCongreso))
					),
					function($mensaje) use ($usr)
					{
						$mensaje->to($usr->emailUsuario, $usr->nombreUsuario . ' ' . $usr->apelUsuario)
								->subject('CONUCA: Gracias por confirmar su participacion');
					}
				);
				DB::commit();
				return Redirect::action('LoginController@index');
				
			} catch(Exception $e)
			{
				DB::rollback();
				return Response::json(array('error' => True, 'mensaje' => 'ERROR: al confirmar el registro '.$e->getMessage()));
			}
		});

	}

	/**
	 * Funcion que permite que un usuario rechaze la invitación enviada por el PC para que sea revisor de un congreso/tematica
	 *
	 * @return JSON
	 */	
	public function rechazarRolRevisor()
	{
		return DB::transaction(function()
		{
			DB::beginTransaction();
			try{

				$id=Input::get("ccnf");
				$idPcTematica=Input::get("pta");
				$idUsuario=Input::get("hsu");
				$idTematica=Input::get("tca");
				$idRol = Rol::where('nomRol', '=', 'Autor')->first()->idRol;
				$leEstadoURC=DB::table('estado_urc')->Where('valorEstado','=','ACT')->get()[0];
				if(!$leEstadoURC)throw new Exception("ERROR: Al actualizar el registro. ".$id);

				$idC=UsuariorolXCongreso::whereRaw('idUsuariorolXCongreso=?', array($id))->first();
				$usr=Usuario::where('idUsuario', '=', $idC->idUsuario)->first();
				$congreso=Congreso::where('idCongreso','=',$idC->idCongreso)->first();
				$pcxrevisor=PcXRevisorArea::whereRaw('idPcRevisor=? and idUsuario=? and idTematica=?',array($idPcTematica,$idUsuario,$idTematica))->delete();
				if(count($pcxrevisor)==0 ||count($usr)==0 || count($congreso)==0)throw new Exception("ERROR: Al actualizar el registro. ".$congreso);

				$solicitudes=DB::table('pc_x_revisor_area')
										->join('pc_x_tematica',
												'pc_x_tematica.idPcTematica',
												'=',
												'pc_x_revisor_area.idPcRevisor'
											)
										->join('tematica',
												'tematica.idTematica',
												'=',
												'pc_x_tematica.idTematica'
											)
										->join('usuariorol_x_congreso',
												'usuariorol_x_congreso.idUsuariorolXCongreso',
												'=',
												'pc_x_tematica.idUsuariorolXCongreso'
											)
										->where('pc_x_revisor_area.idUsuario',
												'=',
												$idC->idUsuario
											)
										->where('usuariorol_x_congreso.idCongreso',
												'=',
												$idC->idCongreso
											)
										->get();
				if(count($solicitudes)==0) //Si no es revisor en otra tematica entonces se le cambia el rol a Autor
					$verif=UsuariorolXCongreso::whereRaw('idUsuariorolXCongreso=?', array($id))->update(array('idEstadoURC' => $leEstadoURC->idEstadoURC,'idRol' => $idRol));

				DB::commit();
				return Redirect::action('LoginController@index');
				
			} catch(Exception $e)
			{
				DB::rollback();
				return Response::json(array('error' => True, 'mensaje' => 'ERROR: al anular el registro '.$e->getMessage()));
			}
		});

	}

	/**
	 * Funcion que genera una salida en HTML para la vista
	 *
	 * @return HTML
	 */	
	public function generarHTML($usuarios)
	{
		if(count($usuarios)==0)return "";
		$cade="";
		$cade.='<select data-placeholder="Seleccione un usuario" class="chosen" id="listaUsuarios">';
		foreach($usuarios as $usuario)
		{
			$cade.='<option value='.$usuario->idUsuario.'>'.$usuario->nombreUsuario.' '.$usuario->apelUsuario.' ('.$usuario->emailUsuario.')</option>';
		}
		$cade.='</select>';
		return $cade;
	}

	/**
	 * Funcion que genera una salida en HTML para la vista
	 *
	 * @return HTML
	 */	
	public function generarHTMLRev($usuarios)
	{
		if(count($usuarios)==0)return "";
		$cade="";
		$cade.='<select data-placeholder="Seleccione un usuario" class="chosen" id="listaRevisores">';
		foreach($usuarios as $usuario)
		{
			$cade.='<option value='.$usuario->idUsuario.'>'.$usuario->nombreUsuario.' '.$usuario->apelUsuario.' ('.$usuario->emailUsuario.')</option>';
		}
		$cade.='</select>';
		return $cade;
	}

	/**
	 * Actualiza si existe algun cambio en la configuración general de un congreso
	 *
	 * @return JSON
	 */	
	public function actualizarCongreso()
	{
		return DB::transaction(function()
		{		
			DB::beginTransaction();
			try {
				$id= Input::get('idCongreso');
				$nombreCongreso=Input::get('nombreCongreso');
				$acronimo=Input::get('acronimo');
				$sitioWeb=Input::get('sitioWeb');
				$ciudad=Input::get('ciudad');
				$pais=Input::get('pais');
				$fechaInicio=Input::get('fechaInicio');
				$fechaFin=Input::get('fechaFin');
				$email=Input::get('email');
				$cad=Input::get('cad');
				$banner=Input::get('banner');
				$estadoCongreso=Input::get('estadoCongreso');
				$fechasImportantes= json_decode(Input::get('fechasImportantes'));
				$fechasImportantesIni= json_decode(Input::get('fechasImportantesIni'));
				$fechasImportantesFin= json_decode(Input::get('fechasImportantesFin'));

				$estadoCon=DB::table('estado_congreso')->Where('valorEstado','=',$estadoCongreso)->first();

				$afectadas = Congreso::where('idCongreso', '=', $id)
							->update(array('nomCongreso' => $nombreCongreso, 
											'acronimoCongreso' => $acronimo,
											'webCongreso' => $sitioWeb,
											'ciudad' => $ciudad,
											'pais' => $pais,
											'emailOrgCongreso' => $email,
											'fecIniCongreso' => $fechaInicio,
											'fecFinCongreso' => $fechaFin,
											'idEstadoCongreso' => $estadoCon->idEstadoCongreso // cambiar por estadoCongreso 
											));
				if(count($afectadas)==0)							
					throw new Exception("ERROR: Al actualizar información general.");

				if(strcmp($estadoCongreso,"ACT")==0)				
					$afectadas = Congreso::where('idCongreso', '=', $id)
								->update(array('comentarioCongreso' => ''));

				if(count($afectadas)==0)							
					throw new Exception("ERROR: Al cancelar solicitud de eliminación.");

				$Congreso=DetalleCongreso::Where("idCongreso","=",$id)->get();
				$idDetalleCongreso=$Congreso->first()->idDetalleCongreso;
			
				$length = count($fechasImportantes);
				for ($i = 0; $i < $length; $i++) {
					FechaImportante::create(array('idTipoFecha'	=> $fechasImportantes[$i],
												'idDetalleCongreso'	=>$idDetalleCongreso,
												'fecInicio' => $fechasImportantesIni[$i],
												'fecFin' => $fechasImportantesFin[$i]
												)
											);
				}
				DB::commit();
				return Response::json(array('error' => False, 'mensaje' => 'Los datos del congreso han sido actualizados'));
			} catch(Exception $e)
			{
				DB::rollback();
				return Response::json(array('error' => True, 'mensaje' => 'Error al actualizar datos generales del congreso'));
			}
		});
	}

	/**
	 * Actualiza si existe algun cambio en la configuración secundaria de un congreso
	 *
	 * @return JSON
	 */	
	public function actualizarCongresoPreEnvio()
	{
		return DB::transaction(function()
		{
			DB::beginTransaction();
			try {
				$id= Input::get('idCongreso');
				$categorias= json_decode(Input::get('categorias'));
				$tematicas= json_decode(Input::get('tematicas'));
				$extensiones= json_decode(Input::get('extensiones'));
				$tammaxarchivo= Input::get('tammaxarchivo');
				$longmax= Input::get('longmax');
				$mostrarNRev=Input::get('mostrarNRev');

				$Congreso=DetalleCongreso::Where("idCongreso","=",$id)->get();
				$idDetalleCongreso=$Congreso->first()->idDetalleCongreso;

				$length = count($categorias);
				for ($i = 0; $i < $length; $i++) {
					DetallecongresoXCategoria::create(array('idCategoria'=> $categorias[$i],'idDetalleCongreso'	=>$idDetalleCongreso));
				}

				$length = count($tematicas);
				for ($i = 0; $i < $length; $i++) {
					CongresoXTematica::create(array('idTematica'=> $tematicas[$i],'idCongreso'	=>$id));
				}

				$length = count($extensiones);
				for ($i = 0; $i < $length; $i++) {
					DetallecongresoXExtension::create(array('idExtensionDocumento'=> $extensiones[$i],'idDetalleCongreso'=>$idDetalleCongreso));
				}

				DetalleCongreso::Where('idCongreso', '=', $id)
									->update(array('tamMaxArchivo' => $tammaxarchivo,
													'longitudMaxResumenPaper' => $longmax,
													'nomRevisorVisible' => $mostrarNRev
										));
				DB::commit();
				return Response::json(array('error' => False, 'mensaje' => 'Los datos de pre-envio del congreso han sido actualizados'));
			} catch(ValidationException $e)
			{
				DB::rollback();
				return Response::json(array('error' => True, 'mensaje' => 'Error al actualizar datos generales del congreso'));
			}
		});
	}

	/**
	 * Actualiza si existe algun cambio en el comite (PC) de un congreso 
	 *
	 * @return JSON
	 */	
	public function actualizarCongresoComite()
	{
		return DB::transaction(function()
			{
			DB::beginTransaction();
			try {
				$id= Input::get('idCongreso');
				$comiteUSR=  json_decode(Input::get('comiteUSR'));
				$comiteTEM= json_decode(Input::get('comiteTEM'));

				$length = count($comiteUSR);
				$leRol=DB::table('rol')->Where('nomRol','=','PC')->get()[0];
				$autorRol=DB::table('rol')->Where('nomRol','=','Autor')->get()[0];
				$leEstadoURC=DB::table('estado_urc')->Where('nombreEstado','=','ACTIVO')->get()[0];
				
				if(!$leRol || !$leEstadoURC )
					throw new Exception('Error');
				
				for ($i = 0; $i < $length; $i++){

					$existe=UsuariorolXCongreso::WhereRaw('idCongreso=? and idUsuario=?',array($id,$comiteUSR[$i]))->first();

					if(count($existe)>0) //Quiere decir que es autor , se actualiza su rol y se crea una entrada en PCxTematica
					{

						$checkRol=DB::table('usuariorol_x_congreso')
										->join('rol',
												'rol.idRol',
												'=',
												'usuariorol_x_congreso.idRol'
											)
										->where('usuariorol_x_congreso.idUsuario',
												'=',
												$comiteUSR[$i]
												)
										->where('usuariorol_x_congreso.idCongreso',
												'=',
												$id
											)
										->where('rol.nomRol',
												'=',
												'Chair'
											)->first();
						
						if (count($checkRol)==0)//Cambiar rol solo si su rango es menor a chair
							$rt=UsuariorolXCongreso::WhereRaw('idCongreso=? and idUsuario=?',array($id,$comiteUSR[$i]))
													->update(array('idRol' => $leRol->idRol,'idEstadoURC' => $leEstadoURC->idEstadoURC)
													);

						$res1=UsuariorolXCongreso::WhereRaw("idCongreso=? and idUsuario=?",array($id,$comiteUSR[$i]))->first();
						$idURC=$res1->idUsuariorolXCongreso;
						$res2=PcXTematica::create(array('idUsuariorolXCongreso'=> $idURC,'idTematica'	=>$comiteTEM[$i]));

						if(count($res1)==0||count($res2)==0)
						{
							DB::rollback();
							throw new Exception('Error');
						}
					}
					else
					{
						$res0=UsuariorolXCongreso::Create(array('idUsuario' => $comiteUSR[$i], 
																'idRol'=>$leRol->idRol,
																'idCongreso'=>$id,
																'idEstadoURC'=>$leEstadoURC->idEstadoURC));

						$res1=UsuariorolXCongreso::WhereRaw("idCongreso=? and idUsuario=?",array($id,$comiteUSR[$i]))->first();
						$idURC=$res1->idUsuariorolXCongreso;
						$res2=PcXTematica::create(array('idUsuariorolXCongreso'=> $idURC,'idTematica'	=>$comiteTEM[$i]));

						if(count($res0)==0||count($res1)==0||count($res2)==0)
						{
							DB::rollback();
							throw new Exception('Error');
						}	
					}
					
				};
				$congreso=Congreso::where('idCongreso','=',$id)->first();
				$dc=DetalleCongreso::where('idCongreso','=',$id)->first();
				$banner = URL::asset('banners/'.$dc->banner);
				for ($i = 0; $i < $length; $i++){
					$usr=Usuario::where('idUsuario', '=', $comiteUSR[$i])->first();
					Mail::send(
						'emails.notificacionUsuario', 
						array(
							'nombreSistema'	=>	$congreso->acronimoCongreso,
							'nombreSistemaCompleto'	=>	$congreso->nomCongreso,
							'correo'		=>	$usr->emailUsuario,
							'nombres'		=>	$usr->nombreUsuario,
							'apellidos'		=>	$usr->apelUsuario,
							'anio'			=>	date("Y"),
							'rolSistema'	=> 'Miembro del Comité ',
							'URLConfirmar'	=> URL::action('UsuariosCongresosController@establecerCongreso', 
															array(Input::get('idCongreso'))),
							'banner_email' 	=>	$banner,
							'URLSitio'		=> URL::action('UsuariosCongresosController@establecerCongreso', 
															array(Input::get('idCongreso')))
						), 
						function($mensaje) use ($usr, $comiteUSR)
						{
							$mensaje->to($usr->emailUsuario, $usr->nombreUsuario . ' ' . $usr->apelUsuario)
									->subject('CONUCA: Notificación de Invitacion en Congreso');
						}
					);
				}
				DB::commit();
				return Response::json(array('error' => False, 'mensaje' => 'Los datos de pre-envio del congreso han sido actualizados'));
			} catch(ValidationException $e)
			{
				DB::rollback();
				return Response::json(array('error' => True, 'mensaje' => 'ERROR: al actualizar datos generales del congreso'));
			}
		});

	}

	/**
	 * Actualiza si existe algun cambio en los revisores para una temática
	 *
	 * @return JSON
	 */	
	public function actualizarRevisores()
	{
		return DB::transaction(function()
		{
			DB::beginTransaction();
			try {
				$id= Input::get('idCongreso');
				$idPcTematica=Input::get('idPcTematica');
				$idTematica=PcXTematica::Where('idPcTematica','=',$idPcTematica)->first()->idTematica;
				$listaUsuarios= json_decode(Input::get('listaUsuarios'));
				$solicitado=Input::get('solicitado');

				$idRol = Rol::where('nomRol', '=', 'Autor')->first()->idRol; //Seguira siendo autor - solo se creara una entrada en PCXREVArea
				$autorRol=DB::table('rol')->Where('nomRol','=','Autor')->get()[0];
				$idEstadoURC =null;
				if($solicitado != 1){
					$idEstadoURC = EstadoURC::where('valorEstado', '=', 'POR')->first()->idEstadoURC;
				}
				else{
					$idEstadoURC = EstadoURC::where('valorEstado', '=', 'SOL')->first()->idEstadoURC;
				}

				$length = count($listaUsuarios);
				for ($i = 0; $i < $length; $i++) {
					$existe=UsuariorolXCongreso::WhereRaw('idCongreso=? and idUsuario=?',array($id,$listaUsuarios[$i]))->first();
					if(count($existe)>0) //Quiere decir que es autor , se actualiza su estado usuario a SOLICITANTE y se crea una entrada en PCxTematica
					{
						UsuariorolXCongreso::WhereRaw('idCongreso=? and idUsuario=?',array($id,$listaUsuarios[$i]))
													->update(
													array('idEstadoURC' => $idEstadoURC)
													);
						PcXRevisorArea::create(array('idPcRevisor'	=> $idPcTematica,
													'idTematica'	=>$idTematica,
													'idUsuario' => $listaUsuarios[$i]
													)
												);
					}
					else
					{
						$usuarioxc = new UsuariorolXCongreso;
						$usuarioxc->idUsuario =$listaUsuarios[$i];
						$usuarioxc->idCongreso= $id;
						$usuarioxc->idRol= $idRol;
						$usuarioxc->idEstadoURC = $idEstadoURC;
						$usuarioxc->save();

						PcXRevisorArea::create(array('idPcRevisor'	=> $idPcTematica,
													'idTematica'	=>$idTematica,
													'idUsuario' => $listaUsuarios[$i]
													)
												);
					}
					DB::commit();
					$datosPC=DB::table('usuariorol_x_congreso')
							->join('usuarios',
									'usuarios.idUsuario',
									'=',
									'usuariorol_x_congreso.idUsuario'
							)
							->join('pc_x_tematica',
									'pc_x_tematica.idUsuariorolXCongreso',
									'=',
									'usuariorol_x_congreso.idUsuariorolXCongreso'
							)
							->where('pc_x_tematica.idPcTematica',
									'=',
									$idPcTematica
								)->first();

					$usr=DB::table('usuarios')
								->where('usuarios.idUsuario',
										'=',
										$listaUsuarios[$i]
									)->first();

					$tematica=DB::table('tematica')
										->where('tematica.idTematica',
												'=',
												$datosPC->idTematica
												)->first();

					$congreso=DB::table('congreso')
									->where('congreso.idCongreso',
											'=',
											$id
											)->first();

					$chair=DB::table('usuarios')
									->where('usuarios.idUsuario',
											'=',
											$congreso->idCreador
											)->first();


					$dc=DetalleCongreso::where('idCongreso','=',$id)->first();
					$banner = URL::asset('banners/'.$dc->banner);					
					Mail::send(
						'emails.notificacionAdminCongreso', 
						array(
							'nombreSistema'	=>	$congreso->acronimoCongreso,
							'nombreSistemaCompleto'	=>	$congreso->nomCongreso,
							'correo'		=>	$usr->emailUsuario,
							'nombres'		=>	$usr->nombreUsuario,
							'apellidos'		=>	$usr->apelUsuario,
							'tematica'		=> 	$tematica->nomTematica,
							'anio'			=>	date("Y"),
							'rolSistema'	=> 'Administrador de Congreso',
							'URLConfirmar'	=> '',
							'banner_email' => $banner,
							'URLSitio'		=> URL::action('HomeController@inicio'),
						), 
						function($mensaje) use ($usr,$chair)
						{
							$mensaje->to($chair->emailUsuario, $usr->nombreUsuario . ' ' . $usr->apelUsuario)
									->subject('CONUCA: Auto-Mensaje , Congreso requiere respuesta.');
						}
					);
				}
				return Response::json(array('error' => False, 'mensaje' => 'Los revisores han sido actualizados.'));
			} catch(Exception $e)
			{
				DB::rollback();
				return Response::json(array('error' => True, 'mensaje' => 'ERROR: al actualizar el listado de revisores. '.$e->getMessage()));
			}
		});
	}

	/**
	 * Muestra todos las fichas enviadas por los usuarios al congreso
	 * El Chair puede aprobar o rechazar estas peticiones
	 * @return View::HTML
	 */	
	public function listarFichas($id)
	{
		try{
			if(!$this->verificarFechaRecurso($id,'Confirmación de aceptación de ponencias')) //quiere decir que la fecha esta fuera de rango
				return Redirect::action('UsuariosCongresosController@establecerCongreso',array($id));

			$fichas = DB::table('ficha')
								->join(
									'detalle_congreso',
									'detalle_congreso.idDetalleCongreso',
									'=',
									'ficha.idDetalleCongreso'
								)		
								->join(
									'tematica',
									'tematica.idTematica',
									'=',
									'ficha.idTematica'
								)
								->join(
									'categoria',
									'categoria.idCategoria',
									'=',
									'ficha.idCategoria'
								)
								->join(
									'estado_ficha',
									'estado_ficha.idEstadoFicha',
									'=',
									'ficha.idEstadoFicha'
								)
								->Where(
									'estado_ficha.nombreEstado',
									'=',
									'PENDIENTE'
								)
								->Where(
									'detalle_congreso.idCongreso',
									'=',
									$id
								)
								->get();

			$maxSumaArray = DB::table('criterio_evaluacion')
			->join(
				'detalle_congreso',
				'detalle_congreso.idDetalleCongreso',
				'=',
				'criterio_evaluacion.idDetalleCongreso'
			)
			->join(
				'congreso',
				'congreso.idCongreso',
				'=',
				'detalle_congreso.idCongreso'
			)
			->join(
				'puntaje',
				'puntaje.idCriterioEvaluacion',
				'=',
				'criterio_evaluacion.idCriterioEvaluacion'
			)
			->where(
				'congreso.idCongreso',
				'=',
				$id
			)
			->where(
				'indicadorFicha',
				'=',
				'F'
			)
			->whereRaw(
				'((criterio_evaluacion.idCriterioEvaluacion = criterio_evaluacion.idCriterioEvaluacionPadre and criterio_evaluacion.criterioGrupal = 0) or (criterio_evaluacion.idCriterioEvaluacion != criterio_evaluacion.idCriterioEvaluacionPadre))'
			)
			->groupBy('criterio_evaluacion.idCriterioEvaluacion')
			->select(DB::raw('max(valorPuntaje) valorPuntaje'))
			->get();

			$maxSuma = 0;
			foreach ($maxSumaArray as $maxSumaOb)
			{
				$maxSuma = $maxSumaOb->valorPuntaje + $maxSuma;
			}

			foreach ($fichas as $ficha) {
				$revisionesQuery = DB::table('revision_ficha_x_criterio')
				->join(
					'revision_ficha',
					'revision_ficha.idRevisionFicha',
					'=',
					'revision_ficha_x_criterio.idRevisionFicha'
				)
				->join(
					'puntaje',
					'puntaje.idPuntaje',
					'=',
					'revision_ficha_x_criterio.idPuntaje'
				)
				->where(
					'revision_ficha.idFicha',
					'=',
					$ficha->idFicha
				);

				$promedios = $revisionesQuery
				->groupBy('idCriterioEvaluacion')
				->select(DB::raw('avg(valorPuntaje) valorPuntaje, idCriterioEvaluacion'))
				->get();

				$ficha->revisadas = DB::table('revision_ficha')
				->join(
					'usuarios',
					'usuarios.idUsuario',
					'=',
					'revision_ficha.idUsuario'
				)
				->where(
					'idFicha',
					'=',
					$ficha->idFicha
				)		
				->whereExists(function($query)
				{
					$query
					->select(DB::raw(1))
					->from('revision_ficha_x_criterio')
					->whereRaw('revision_ficha_x_criterio.idRevisionFicha = revision_ficha.idRevisionFicha');
				})
				->count();

				$ficha->revisiones = DB::table('revision_ficha')
				->join(
					'usuarios',
					'usuarios.idUsuario',
					'=',
					'revision_ficha.idUsuario'
				)
				->where(
					'idFicha',
					'=',
					$ficha->idFicha
				)
				->count();

				$suma = 0;
				foreach ($promedios as $promedio) {
					$suma = $suma + $promedio->valorPuntaje;
				}
				if($maxSuma==0)
					$ficha->porcentaje = 0;
				else
					$ficha->porcentaje = round($suma/$maxSuma,2)*100;	
			}

			$this->layout->content = View::make('congresos/GestionarFichas')
											->with('fichas',$fichas)
											->with('idCongreso',$id);
		}
		catch(Exception $e)
		{
			Session::flash('message',  $e->getMessage());
			return Redirect::action('HomeController@inicio');
		}	
	}

	/**
	 * Muestra los datos de una ficha especifica
	 * @return View::HTML
	 */	
	public function listarDetalleFicha($idCongreso,$idFicha)
	{
		if(!$this->verificarFechaRecurso($idCongreso,'Confirmación de aceptación de Artículos')) //quiere decir que la fecha esta fuera de rango
			return Redirect::action('UsuariosCongresosController@establecerCongreso',array($idCongreso));
		$ficha = DB::table('ficha')
		->join(
			'detalle_congreso',
			'detalle_congreso.idDetalleCongreso',
			'=',
			'ficha.idDetalleCongreso'
		)		
		->join(
			'tematica',
			'tematica.idTematica',
			'=',
			'ficha.idTematica'
		)
		->join(
			'categoria',
			'categoria.idCategoria',
			'=',
			'ficha.idCategoria'
		)
		->join(
			'estado_ficha',
			'estado_ficha.idEstadoFicha',
			'=',
			'ficha.idEstadoFicha'
		)
		->Where(
			'estado_ficha.nombreEstado',
			'=',
			'PENDIENTE'
		)
		->Where(
			'ficha.idFicha',
			'=',
			$idFicha
		)
		->first();

		$criterios = DB::table('criterio_evaluacion')
		->join(
			'detalle_congreso',
			'detalle_congreso.idDetalleCongreso',
			'=',
			'criterio_evaluacion.idDetalleCongreso'
		)
		->join(
			'congreso',
			'congreso.idCongreso',
			'=',
			'detalle_congreso.idCongreso'
		)
		->where(
			'congreso.idCongreso',
			'=',
			$idCongreso
		)
		->where(
			'indicadorFicha',
			'=',
			'F'
		)
		->whereRaw(
			'criterio_evaluacion.idCriterioEvaluacion = criterio_evaluacion.idCriterioEvaluacionPadre'
		)
		->get();

		foreach ($criterios as $criterio)
		{
			$criterio->hijos = DB::table('criterio_evaluacion')
			->where(
				'idCriterioEvaluacionPadre',
				'=',
				$criterio->idCriterioEvaluacion
			)
			->whereRaw(
				'criterio_evaluacion.idCriterioEvaluacion != criterio_evaluacion.idCriterioEvaluacionPadre'
			)->get();

			foreach($criterio->hijos as $hijo)
			{
				$hijo->puntajes = DB::table('puntaje')
				->where(
					'idCriterioEvaluacion',
					'=',
					$hijo->idCriterioEvaluacion
				)
				->select('nomPuntaje', 'valorPuntaje', 'idPuntaje')
				->orderBy('valorPuntaje','asc')
				->get();
				$hijo->puntajesMax = count($hijo->puntajes);
			}

			$criterio->puntajes = DB::table('puntaje')
			->where(
				'idCriterioEvaluacion',
				'=',
				$criterio->idCriterioEvaluacion
			)
			->select('nomPuntaje', 'valorPuntaje', 'idPuntaje')
			->orderBy('valorPuntaje','asc')
			->get();

			$criterio->puntajesMax = count($criterio->puntajes);
		}

		//promedio de todas las revisiones
		$revisionesQuery = DB::table('revision_ficha_x_criterio')
		->join(
			'revision_ficha',
			'revision_ficha.idRevisionFicha',
			'=',
			'revision_ficha_x_criterio.idRevisionFicha'
		)
		->join(
			'puntaje',
			'puntaje.idPuntaje',
			'=',
			'revision_ficha_x_criterio.idPuntaje'
		)
		->where(
			'revision_ficha.idFicha',
			'=',
			$idFicha
		);

		$promedios = $revisionesQuery
		->groupBy('idCriterioEvaluacion')
		->select(DB::raw('avg(valorPuntaje) valorPuntaje, idCriterioEvaluacion'))
		->get();

		$revisiones = $revisionesQuery
		->select(array('revision_ficha.idRevisionFicha', 'puntaje.idCriterioEvaluacion', 'puntaje.valorPuntaje'))
		->get();

		$suma = 0;
		foreach ($promedios as $promedio) {
			$suma = $suma + $promedio->valorPuntaje;
		}

		$maxSumaArray = DB::table('criterio_evaluacion')
		->join(
			'detalle_congreso',
			'detalle_congreso.idDetalleCongreso',
			'=',
			'criterio_evaluacion.idDetalleCongreso'
		)
		->join(
			'congreso',
			'congreso.idCongreso',
			'=',
			'detalle_congreso.idCongreso'
		)
		->join(
			'puntaje',
			'puntaje.idCriterioEvaluacion',
			'=',
			'criterio_evaluacion.idCriterioEvaluacion'
		)
		->where(
			'congreso.idCongreso',
			'=',
			$idCongreso
		)
		->where(
			'indicadorFicha',
			'=',
			'F'
		)
		->whereRaw(
			'((criterio_evaluacion.idCriterioEvaluacion = criterio_evaluacion.idCriterioEvaluacionPadre and criterio_evaluacion.criterioGrupal = 0) or (criterio_evaluacion.idCriterioEvaluacion != criterio_evaluacion.idCriterioEvaluacionPadre))'
		)
		->groupBy('criterio_evaluacion.idCriterioEvaluacion')
		->select(DB::raw('max(valorPuntaje) valorPuntaje'))
		->get();

		$maxSuma = 0;
		foreach ($maxSumaArray as $maxSumaOb)
		{
			$maxSuma = $maxSumaOb->valorPuntaje + $maxSuma;
		}

		$usuariosSinRev = DB::table('revision_ficha')
		->join(
			'usuarios',
			'usuarios.idUsuario',
			'=',
			'revision_ficha.idUsuario'
		)
		->where(
			'idFicha',
			'=',
			$idFicha
		)
		->whereNotExists(function($query)
		{
			$query
			->select(DB::raw(1))
			->from('revision_ficha_x_criterio')
			->whereRaw('revision_ficha_x_criterio.idRevisionFicha = revision_ficha.idRevisionFicha');
		})
		->select(array('nombreUsuario', 'apelUsuario', 'emailUsuario'))
		->get();

		$usuariosConRev = DB::table('revision_ficha')
		->join(
			'usuarios',
			'usuarios.idUsuario',
			'=',
			'revision_ficha.idUsuario'
		)
		->where(
			'idFicha',
			'=',
			$idFicha
		)		
		->whereExists(function($query)
		{
			$query
			->select(DB::raw(1))
			->from('revision_ficha_x_criterio')
			->whereRaw('revision_ficha_x_criterio.idRevisionFicha = revision_ficha.idRevisionFicha');
		})
		->select(array('nombreUsuario', 'apelUsuario', 'emailUsuario', 'idRevisionFicha'))
		->get();

		foreach ($usuariosConRev as $usuarioConRev) {
			$usuarioConRev->revisiones = DB::table('revision_ficha_x_criterio')
			->join(
				'puntaje',
				'puntaje.idPuntaje',
				'=',
				'revision_ficha_x_criterio.idPuntaje'
			)
			->where(
				'idRevisionFicha',
				'=',
				$usuarioConRev->idRevisionFicha
			)
			->select(array('valorPuntaje', 'idRevisionFicha', 'idCriterioEvaluacion'))
			->get();

			$usuarioConRev->suma = 0;
			foreach ($usuarioConRev->revisiones as $rev) {
				$usuarioConRev->suma = $usuarioConRev->suma + $rev->valorPuntaje;
			}

			if($maxSuma==0)
				$usuarioConRev->porcentaje = 0;
			else
				$usuarioConRev->porcentaje = round($usuarioConRev->suma/$maxSuma,2)*100;
		}

		$autores = DB::select("SELECT GROUP_CONCAT(usuarios.nombreUsuario,' ',usuarios.apelUsuario SEPARATOR ', ') autores
										FROM autor_x_ficha
										inner join usuarios on usuarios.idUsuario=autor_x_ficha.idUsuario
										where idFicha=(?)", array($idFicha));

		$this->layout->content = View::make('congresos/DetalleFicha')
		->with('ficha',$ficha)
		->with('autores',$autores)
		->with('idCongreso',$idCongreso)
		->with('criterios',$criterios)
		->with('promedios', $promedios)
		->with('revisiones',$revisiones)
		->with('usuariosConRev',$usuariosConRev)
		->with('usuariosSinRev',$usuariosSinRev)
		->with('suma',$suma)
		->with('max',$maxSuma);
	}

	/**
	 * Muestra todos las papers enviados por los usuarios al congreso
	 * El Chair puede aprobar o rechazar estas peticiones
	 * @return View::HTML
	 */	
	public function listarPapers($id)
	{
		try{
			if(!$this->verificarFechaRecurso($id,'Confirmación de aceptación de Artículos')) //quiere decir que la fecha esta fuera de rango
				return Redirect::action('UsuariosCongresosController@establecerCongreso',array($id));
			$papers = DB::table('paper')
								->join('ficha',
									'ficha.idFicha',
									'=',
									'paper.idFicha'
								)
								->join(
									'detalle_congreso',
									'detalle_congreso.idDetalleCongreso',
									'=',
									'ficha.idDetalleCongreso'
								)		
								->join(
									'tematica',
									'tematica.idTematica',
									'=',
									'ficha.idTematica'
								)
								->join(
									'estado_paper',
									'estado_paper.idEstadoPaper',
									'=',
									'paper.idEstadoPaper'
								)
								->join(
									'categoria',
									'categoria.idCategoria',
									'=',
									'ficha.idCategoria'
								)
								->Where(
									'estado_paper.nombreEstado',
									'=',
									'PENDIENTE'
								)
								->Where(
									'detalle_congreso.idCongreso',
									'=',
									$id
								)
								->get();

			$maxSumaArray = DB::table('criterio_evaluacion')
			->join(
				'detalle_congreso',
				'detalle_congreso.idDetalleCongreso',
				'=',
				'criterio_evaluacion.idDetalleCongreso'
			)
			->join(
				'congreso',
				'congreso.idCongreso',
				'=',
				'detalle_congreso.idCongreso'
			)
			->join(
				'puntaje',
				'puntaje.idCriterioEvaluacion',
				'=',
				'criterio_evaluacion.idCriterioEvaluacion'
			)
			->where(
				'congreso.idCongreso',
				'=',
				$id
			)
			->where(
				'indicadorFicha',
				'=',
				'P'
			)
			->whereRaw(
				'((criterio_evaluacion.idCriterioEvaluacion = criterio_evaluacion.idCriterioEvaluacionPadre and criterio_evaluacion.criterioGrupal = 0) or (criterio_evaluacion.idCriterioEvaluacion != criterio_evaluacion.idCriterioEvaluacionPadre))'
			)
			->groupBy('criterio_evaluacion.idCriterioEvaluacion')
			->select(DB::raw('max(valorPuntaje) valorPuntaje'))
			->get();

			$maxSuma = 0;
			foreach ($maxSumaArray as $maxSumaOb)
			{
				$maxSuma = $maxSumaOb->valorPuntaje + $maxSuma;
			}

			foreach ($papers as $paper) {
				$revisionesQuery = DB::table('revision_paper_x_criterio')
				->join(
					'revision_paper',
					'revision_paper.idRevisionPaper',
					'=',
					'revision_paper_x_criterio.idRevisionPaper'
				)
				->join(
					'puntaje',
					'puntaje.idPuntaje',
					'=',
					'revision_paper_x_criterio.idPuntaje'
				)
				->where(
					'revision_paper.idPaper',
					'=',
					$paper->idPaper
				);

				$promedios = $revisionesQuery
				->groupBy('idCriterioEvaluacion')
				->select(DB::raw('avg(valorPuntaje) valorPuntaje, idCriterioEvaluacion'))
				->get();

				$paper->revisadas = DB::table('revision_paper')
				->join(
					'usuarios',
					'usuarios.idUsuario',
					'=',
					'revision_paper.idUsuario'
				)
				->where(
					'idPaper',
					'=',
					$paper->idPaper
				)		
				->whereExists(function($query)
				{
					$query
					->select(DB::raw(1))
					->from('revision_paper_x_criterio')
					->whereRaw('revision_paper_x_criterio.idRevisionPaper = revision_paper.idRevisionPaper');
				})
				->count();

				$paper->revisiones = DB::table('revision_paper')
				->join(
					'usuarios',
					'usuarios.idUsuario',
					'=',
					'revision_paper.idUsuario'
				)
				->where(
					'idPaper',
					'=',
					$paper->idPaper
				)		
				->whereExists(function($query)
				{
					$query
					->select(DB::raw(1))
					->from('revision_paper_x_criterio')
					->whereRaw('revision_paper_x_criterio.idRevisionPaper = revision_paper.idRevisionPaper');
				})
				->count();
				
				$suma = 0;
				foreach ($promedios as $promedio) {
					$suma = $suma + $promedio->valorPuntaje;
				}
				if($maxSuma==0)
					$paper->porcentaje = 0;
				else
					$paper->porcentaje = round($suma/$maxSuma,2)*100;	
			}

			$this->layout->content = View::make('congresos/GestionarPapers')
											->with('papers',$papers)
											->with('idCongreso',$id);
		}
		catch(Exception $e)
		{
			Session::flash('message',  $e->getMessage());
			return Redirect::action('HomeController@inicio');
		}	
	}

	/**
	 * Muestra los datos de una ficha especifica
	 * @return View::HTML
	 */	
	public function listarDetallePaper($idCongreso,$idPaper)
	{
		if(!$this->verificarFechaRecurso($idCongreso,'Confirmación de aceptación de Artículos')) //quiere decir que la fecha esta fuera de rango
			return Redirect::action('UsuariosCongresosController@establecerCongreso',array($idCongreso));

		$paper = DB::table('paper')
		->join('ficha',
			'ficha.idFicha',
			'=',
			'paper.idFicha'
		)
		->join(
			'detalle_congreso',
			'detalle_congreso.idDetalleCongreso',
			'=',
			'ficha.idDetalleCongreso'
		)		
		->join(
			'tematica',
			'tematica.idTematica',
			'=',
			'ficha.idTematica'
		)
		->join(
			'categoria',
			'categoria.idCategoria',
			'=',
			'ficha.idCategoria'
		)
		->join(
			'estado_paper',
			'estado_paper.idEstadoPaper',
			'=',
			'paper.idEstadoPaper'
		)
		->Where(
			'estado_paper.nombreEstado',
			'=',
			'PENDIENTE'
		)
		->Where(
			'paper.idPaper',
			'=',
			$idPaper
		)
		->first();

		$criterios = DB::table('criterio_evaluacion')
		->join(
			'detalle_congreso',
			'detalle_congreso.idDetalleCongreso',
			'=',
			'criterio_evaluacion.idDetalleCongreso'
		)
		->join(
			'congreso',
			'congreso.idCongreso',
			'=',
			'detalle_congreso.idCongreso'
		)
		->where(
			'congreso.idCongreso',
			'=',
			$idCongreso
		)
		->where(
			'indicadorFicha',
			'=',
			'P'
		)
		->whereRaw(
			'criterio_evaluacion.idCriterioEvaluacion = criterio_evaluacion.idCriterioEvaluacionPadre'
		)
		->get();

		foreach ($criterios as $criterio)
		{
			$criterio->hijos = DB::table('criterio_evaluacion')
			->where(
				'idCriterioEvaluacionPadre',
				'=',
				$criterio->idCriterioEvaluacion
			)
			->whereRaw(
				'criterio_evaluacion.idCriterioEvaluacion != criterio_evaluacion.idCriterioEvaluacionPadre'
			)->get();

			foreach($criterio->hijos as $hijo)
			{
				$hijo->puntajes = DB::table('puntaje')
				->where(
					'idCriterioEvaluacion',
					'=',
					$hijo->idCriterioEvaluacion
				)
				->select('nomPuntaje', 'valorPuntaje', 'idPuntaje')
				->orderBy('valorPuntaje','asc')
				->get();
				$hijo->puntajesMax = count($hijo->puntajes);
			}

			$criterio->puntajes = DB::table('puntaje')
			->where(
				'idCriterioEvaluacion',
				'=',
				$criterio->idCriterioEvaluacion
			)
			->select('nomPuntaje', 'valorPuntaje', 'idPuntaje')
			->orderBy('valorPuntaje','asc')
			->get();

			$criterio->puntajesMax = count($criterio->puntajes);
		}

		//promedio de todas las revisiones
		$revisionesQuery = DB::table('revision_paper_x_criterio')
		->join(
			'revision_paper',
			'revision_paper.idRevisionPaper',
			'=',
			'revision_paper_x_criterio.idRevisionPaper'
		)
		->join(
			'puntaje',
			'puntaje.idPuntaje',
			'=',
			'revision_paper_x_criterio.idPuntaje'
		)
		->where(
			'revision_paper.idPaper',
			'=',
			$idPaper
		);

		$promedios = $revisionesQuery
		->groupBy('idCriterioEvaluacion')
		->select(DB::raw('avg(valorPuntaje) valorPuntaje, idCriterioEvaluacion'))
		->get();

		$revisiones = $revisionesQuery
		->select(array('revision_paper.idRevisionPaper', 'puntaje.idCriterioEvaluacion', 'puntaje.valorPuntaje'))
		->get();

		$suma = 0;
		foreach ($promedios as $promedio) {
			$suma = $suma + $promedio->valorPuntaje;
		}

		$maxSumaArray = DB::table('criterio_evaluacion')
		->join(
			'detalle_congreso',
			'detalle_congreso.idDetalleCongreso',
			'=',
			'criterio_evaluacion.idDetalleCongreso'
		)
		->join(
			'congreso',
			'congreso.idCongreso',
			'=',
			'detalle_congreso.idCongreso'
		)
		->join(
			'puntaje',
			'puntaje.idCriterioEvaluacion',
			'=',
			'criterio_evaluacion.idCriterioEvaluacion'
		)
		->where(
			'congreso.idCongreso',
			'=',
			$idCongreso
		)
		->where(
			'indicadorFicha',
			'=',
			'F'
		)
		->whereRaw(
			'((criterio_evaluacion.idCriterioEvaluacion = criterio_evaluacion.idCriterioEvaluacionPadre and criterio_evaluacion.criterioGrupal = 0) or (criterio_evaluacion.idCriterioEvaluacion != criterio_evaluacion.idCriterioEvaluacionPadre))'
		)
		->groupBy('criterio_evaluacion.idCriterioEvaluacion')
		->select(DB::raw('max(valorPuntaje) valorPuntaje'))
		->get();

		$maxSuma = 0;
		foreach ($maxSumaArray as $maxSumaOb)
		{
			$maxSuma = $maxSumaOb->valorPuntaje + $maxSuma;
		}
		
		$usuariosSinRev = DB::table('revision_paper')
		->join(
			'usuarios',
			'usuarios.idUsuario',
			'=',
			'revision_paper.idUsuario'
		)
		->where(
			'idPaper',
			'=',
			$idPaper
		)
		->whereNotExists(function($query)
		{
			$query
			->select(DB::raw(1))
			->from('revision_paper_x_criterio')
			->whereRaw('revision_paper_x_criterio.idRevisionPaper = revision_paper.idRevisionPaper');
		})
		->select(array('nombreUsuario', 'apelUsuario', 'emailUsuario'))
		->get();

		$usuariosConRev = DB::table('revision_paper')
		->join(
			'usuarios',
			'usuarios.idUsuario',
			'=',
			'revision_paper.idUsuario'
		)
		->where(
			'idPaper',
			'=',
			$idPaper
		)		
		->whereExists(function($query)
		{
			$query
			->select(DB::raw(1))
			->from('revision_paper_x_criterio')
			->whereRaw('revision_paper_x_criterio.idRevisionPaper = revision_paper.idRevisionPaper');
		})
		->select(array('nombreUsuario', 'apelUsuario', 'emailUsuario', 'idRevisionPaper'))
		->get();

		foreach ($usuariosConRev as $usuarioConRev) {
			$usuarioConRev->revisiones = DB::table('revision_paper_x_criterio')
			->join(
				'puntaje',
				'puntaje.idPuntaje',
				'=',
				'revision_paper_x_criterio.idPuntaje'
			)
			->where(
				'idRevisionPaper',
				'=',
				$usuarioConRev->idRevisionPaper
			)
			->select(array('valorPuntaje', 'idRevisionPaper', 'idCriterioEvaluacion'))
			->get();

			$usuarioConRev->suma = 0;
			foreach ($usuarioConRev->revisiones as $rev) {
				$usuarioConRev->suma = $usuarioConRev->suma + $rev->valorPuntaje;
			}
			if($maxSuma==0)
				$usuarioConRev->porcentaje = 0;
			else
				$usuarioConRev->porcentaje = round($usuarioConRev->suma/$maxSuma,2)*100;
		}

		$autores = DB::select("SELECT GROUP_CONCAT(usuarios.nombreUsuario,' ',usuarios.apelUsuario SEPARATOR ', ') autores FROM autor_x_ficha inner join usuarios on usuarios.idUsuario=autor_x_ficha.idUsuario inner join ficha on autor_x_ficha.idFicha = ficha.idFicha inner join paper on ficha.idFicha = paper.idFicha WHERE idPaper =(?)", array($idPaper));

		$this->layout->content = View::make('congresos/DetallePaper')
		->with('paper',$paper)
		->with('autores',$autores)
		->with('idCongreso',$idCongreso)
		->with('criterios',$criterios)
		->with('promedios', $promedios)
		->with('revisiones',$revisiones)
		->with('usuariosConRev',$usuariosConRev)
		->with('usuariosSinRev',$usuariosSinRev)
		->with('suma',$suma)
		->with('max',$maxSuma);
	}

	/**
	 * Aprueba o rechaza fichas/papers
	 * @return JSON
	 */	
	public function aprobarRechazarFP()
	{
		return DB::transaction(function()
				{
					DB::beginTransaction();
					try{
						$idRegistro=Input::get('idRegistro');
						$tabla=Input::get('tabla');
						$accion=Input::get('accion');
						$idEstadoFP=null;
						if(strcmp($tabla,"ficha")==0)
							$idEstadoFP=DB::table('estado_ficha')->Where('nombreEstado','=',$accion)->first()->idEstadoFicha;
						else
							$idEstadoFP=DB::table('estado_paper')->Where('nombreEstado','=',$accion)->first()->idEstadoPaper;

						if($idEstadoFP==null)throw new Exception("al actualizar ".$tabla);
						
						$verif=null;

						if(strcmp($tabla,"ficha")==0)
							$verif=Ficha::whereRaw('idFicha=?', array($idRegistro))->update(array('idEstadoFicha' => $idEstadoFP));
						else
							$verif=Paper::whereRaw('idPaper=?', array($idRegistro))->update(array('idEstadoPaper' => $idEstadoFP));

						if(count($verif)==0)throw new Exception("Al actualizar registro en ".$tabla);
						DB::commit();
						if(strcmp($accion,"APROBADA")==0) //Notifica si se aprueba la ficha o paper
						{
							$reg=null;
							if(strcmp($tabla,"ficha")==0)
								$reg=Ficha::WhereRaw('idFicha=?',array($idRegistro))->first();
							else
								$reg=DB::table('paper')
											->join('ficha',
													'ficha.idFicha',
													'=',
													'paper.idFicha'
												)
											->Where('paper.idPaper',
													'=',
													$idRegistro
												)->first();

							$congreso=DB::table('detalle_congreso')
										->join('congreso',
												'congreso.idCongreso',
												'=',
												'detalle_congreso.idCongreso'
											)
										->Where('detalle_congreso.idDetalleCongreso',
												'=',
												$reg->idDetalleCongreso
											)->first();

							$autores=null;
							if(strcmp($tabla,"ficha")==0){
								$autores=DB::table('autor_x_ficha')
													->join('usuarios',
															'usuarios.idUsuario',
															'=',
															'autor_x_ficha.idUsuario'
														)
													->where('idFicha',
															'=',
															$idRegistro
															)->get();
							}
							else{
								$autores=DB::table('autor_x_ficha')
										->join('paper',
												'paper.idFicha',
												'=',
												'autor_x_ficha.idFicha'
										)
										->join('usuarios',
												'usuarios.idUsuario',
												'=',
												'autor_x_ficha.idUsuario'
											)
										->where('idPaper',
												'=',
												$idRegistro
												)->get();
							}

							if(count($autores)==0)throw new Exception("Al notificar a los autores");
							$length = count($autores);
							for ($i = 0; $i < $length; $i++) {	
								$usr=$autores[$i];
								$banner = URL::asset('banners/'.$congreso->banner);
								Mail::send(
									'emails.notificacionUsuarioFP', 
									array(
										'nombreSistema'	=>	$congreso->acronimoCongreso,
										'nombreSistemaCompleto'	=>	$congreso->nomCongreso,
										'correo'		=>	$usr->emailUsuario,
										'nombres'		=>	$usr->nombreUsuario,
										'apellidos'		=>	$usr->apelUsuario,
										'tabla'			=> 	$tabla,
										'tituloFP'		=>	$reg->tituloPaper,
										'anio'			=>	date("Y"),
										'rolSistema'	=> 'Revisor',
										'URLConfirmar'	=> '',
										'banner_email' 	=>	$banner,
										'URLSitio'		=> URL::action('HomeController@inicio')
									),
									function($mensaje) use ($usr,$tabla)
									{
										$mensaje->to($usr->emailUsuario, $usr->nombreUsuario . ' ' . $usr->apelUsuario)
												->subject('CONUCA: Felicitaciones su envio de '.$tabla.' ha sido APROBADO!');
									}
								);
							}
						}


						return Response::json(array('error' => False, 'mensaje' => 'Solicitud aprobada exitosamente'));						
					} catch(Exception $e)
					{
						DB::rollback();
						return Response::json(array('error' => True, 'mensaje' => $e->getMessage()));
					}
				});		
	}

	public function rechazarTodasFP()
	{
		return DB::transaction(function()
				{
					DB::beginTransaction();
					try{
						$ids=json_decode(Input::get('arrayRegistros'));
						$tabla=Input::get('tabla');
						$accion=Input::get('accion');
						$idEstadoFP=null;
						if(strcmp($tabla,"ficha")==0)
							$idEstadoFP=DB::table('estado_ficha')->Where('nombreEstado','=',$accion)->first()->idEstadoFicha;
						else
							$idEstadoFP=DB::table('estado_paper')->Where('nombreEstado','=',$accion)->first()->idEstadoPaper;

						if($idEstadoFP==null)throw new Exception("al actualizar ".$tabla);
						
						$verif=null;

						if(strcmp($tabla,"ficha")==0){
							$length = count($ids);
							for ($i = 0; $i < $length; $i++) {
								$verif=Ficha::whereRaw('idFicha=?', array($ids[$i]))->update(array('idEstadoFicha' => $idEstadoFP));
								if(count($verif)==0)throw new Exception("Al actualizar registro en ".$tabla);
							}
						}
						else{
							$length = count($ids);
							for ($i = 0; $i < $length; $i++) {
								$verif=Paper::whereRaw('idPaper=?', array($ids[$i]))->update(array('idEstadoPaper' => $idEstadoFP));
								if(count($verif)==0)throw new Exception("Al actualizar registro en ".$tabla);
							}
							
						}
						DB::commit();

						return Response::json(array('error' => False, 'mensaje' => 'Solicitud aprobada exitosamente'));						
					} catch(Exception $e)
					{
						DB::rollback();
						return Response::json(array('error' => True, 'mensaje' => $e->getMessage()));
					}
				});		
	}
}
