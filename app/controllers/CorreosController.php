<?php 

class CorreosController extends BaseController {
	
	public function index($id)
	{
		if (Auth::guest()) return Redirect::guest('login');
		$admin = DB::table('usuarios')
			->where('emailUsuario','=', 'conuca@uca.edu.sv')
			->get();
        $usuarios = DB::table('usuariorol_x_congreso')
			->join('usuarios','usuariorol_x_congreso.idUsuario','=','usuarios.idUsuario')
			->join('congreso','usuariorol_x_congreso.idCongreso','=','congreso.idCongreso')
			->join('rol','usuariorol_x_congreso.idRol','=','rol.idRol')
			->Where('usuariorol_x_congreso.idCongreso','=',  $id)      
			->Where('usuarios.idUsuario','<>',Auth::user()->idUsuario)
			->get();
		$detalleCongreso = DB::table('detalle_congreso')
			->where('idCongreso','=', $id)
			->first();
        $idDetalleCongreso = $detalleCongreso->idDetalleCongreso;
		$investigaciones = DB::table('autor_x_ficha')
			->join('usuarios','autor_x_ficha.idUsuario','=','usuarios.idUsuario')
			->join('ficha','autor_x_ficha.idFicha','=','ficha.idFicha')
			->Where('ficha.idDetalleCongreso','=',  $idDetalleCongreso)
			->Where('autor_x_ficha.responsable','=',  '1')
			//->Where('usuarios.idUsuario','<>',Auth::user()->idUsuario)
			->get();
		$remitente = Usuario::where('idUsuario', '=',Auth::user()->idUsuario)->first();
		$this->layout->content = View::make('congresos.envioCorreos' , array('idCongreso' => $id, 'usuarios' => $usuarios, 'remitente' => $remitente, 'investigaciones' => $investigaciones, 'admin' => $admin));
	}

	public function adminIndex()
	{
		if (Auth::guest()) return Redirect::guest('login');
		$usuarios = DB::table('usuarios')
			->where('emailUsuario','<>', 'conuca@uca.edu.sv')
			->get();
		$remitente = Usuario::where('idUsuario', '=',Auth::user()->idUsuario)->first();
		$this->layout->content = View::make('adminEnvioCorreos' , array('usuarios' => $usuarios, 'remitente' => $remitente));
	}

	public function sendMail()
	{
		try {
    		if(Request::ajax())
			{
				$idC = Input::get('congreso');
				$remit = Auth::user()->idUsuario;
				$destin = "";
				$ord = 1;

				$banner = URL::asset('imagenes/banner_mail_conuca.png');
				$cuerpo = Input::get('cuerpo');
				$destinatarios = Input::get('destinatarios');
 				foreach ($destinatarios as $usuario) {
					$usr =Usuario::where('idUsuario', '=',substr($usuario, 0, strpos($usuario, "_")))->first();
					$cuerpoTemp = $cuerpo;
					$cuerpoTemp1 = "-".$cuerpoTemp;

					if (strpos($cuerpoTemp1, "[*PNombre*]") != false) {
						if (strpos($usr->nombreUsuario, " ") != false) {
							$pos = strpos($usr->nombreUsuario, " ");
							$PNombre = substr($usr->nombreUsuario, 0, $pos);
							$cuerpoTemp = str_replace("[*PNombre*]", $PNombre, $cuerpoTemp);
						}else{
							$cuerpoTemp = str_replace("[*PNombre*]", $usr->nombreUsuario, $cuerpoTemp);
						}
					}
					if (strpos($cuerpoTemp1, "[*PApellido*]") != false) {
						if (strpos($usr->nombreUsuario, " ") != false) {
							$pos = strpos($usr->apelUsuario, " ");
							$PApellido = substr($usr->apelUsuario, 0, $pos);
							$cuerpoTemp = str_replace("[*PApellido*]", $PApellido, $cuerpoTemp);
						}else{
							$cuerpoTemp = str_replace("[*PApellido*]", $usr->apelUsuario, $cuerpoTemp);
						}
					}
					if (strpos($cuerpoTemp1, "[*Nombre_Completo*]") != false) {
						$NombreComp = $usr->nombreUsuario." ".$usr->apelUsuario;
						$cuerpoTemp = str_replace("[*Nombre_Completo*]", $NombreComp, $cuerpoTemp);
					}
					if (strpos($cuerpoTemp1, "[*Acronimo_Congreso*]") != false) {
						$congresoTemp = Congreso::where('idCongreso','=', $idC)->first();
						$cuerpoTemp = str_replace("[*Acronimo_Congreso*]", $congresoTemp->acronimoCongreso, $cuerpoTemp);
					}
					if (strpos($cuerpoTemp1, "[*Titulo_Congreso*]") != false) {
						$congresoTemp = Congreso::where('idCongreso','=', $idC)->first();
						$cuerpoTemp = str_replace("[*Titulo_Congreso*]", "\"".$congresoTemp->nomCongreso."\"", $cuerpoTemp);
					}
					if (strpos($cuerpoTemp1, "[*Titulo_Investigacion*]") != false) {
						$invest = Ficha::where('idFicha','=', substr($usuario, strpos($usuario, "_")+1))->first();
						$cuerpoTemp = str_replace("[*Titulo_Investigacion*]", "\"".$invest->tituloPaper."\"", $cuerpoTemp);
					}
					$cuerpoTemp = str_replace("[b]", "<b>", $cuerpoTemp);
					$cuerpoTemp = str_replace("[/b]", "</b>", $cuerpoTemp);
					$cuerpoTemp = str_replace("[i]", "<i>", $cuerpoTemp);
					$cuerpoTemp = str_replace("[/i]", "</i>", $cuerpoTemp);
					$cuerpoTemp = str_replace("[u]", "<u>", $cuerpoTemp);
					$cuerpoTemp = str_replace("[/u]", "</u>", $cuerpoTemp);
					$cuerpoTemp = str_replace("[u]", "<u>", $cuerpoTemp);
					$cuerpoTemp = str_replace("[/u]", "</u>", $cuerpoTemp);
					$cuerpoTemp = str_replace("[t]", "<h3>", $cuerpoTemp);
					$cuerpoTemp = str_replace("[/t]", "</h3>", $cuerpoTemp);

					$cuerpoTemp = nl2br($cuerpoTemp);

					$sent=Mail::send(
						'emails.correosCongreso', 
						array(
							'correo'		=>	$usr->emailUsuario,
							'remitente'		=>	Input::get('remitente'),
							'anio'			=>	date("Y"),
							'banner_email'	=>	$banner,
							'asunto'		=>	Input::get('asunto'),
							'cuerpo'		=>	$cuerpoTemp
						), 
						function($mensaje) use ($usr)
						{
							$mensaje->to($usr->emailUsuario, $usr->nombreUsuario . ' ' . $usr->apelUsuario)
									->subject(Input::get('asunto'));
						}
					);
					if($sent==0) throw new Exception('No se pudo enviar el correo de notificación , favor intentelo mas tarde.');
					$destin .= $usr->nombreUsuario." ".$usr->apelUsuario." (".$usr->emailUsuario."); ";
				}
				
				$fileName = 'Correo_'.$remit.'.xml';
				$fileRoot = public_path().'/correos/'.$fileName;
				$mappedPath = public_path().'/correos'; 
				File::makeDirectory($mappedPath, $mode = 0777, true, true); 

				$documento = new DOMDocument("1.0");

				if(!(file_exists($fileRoot))){
					$correo = $documento->createElement('correo');
					//$mail = $documento->createElement('mail'); // este no deberia estar aqui
				}else{
					$documento->load($fileRoot);
					$correo = $documento->getElementsByTagName('correo')->item(0); 
					$ord += $documento->getElementsByTagName('mail')->length;
				}

				$mail = $documento->createElement('mail');

				$orden = $documento->createElement('orden', $ord);
				$congreso = $documento->createElement('congreso', $idC);
				$remitente = $documento->createElement('fecha', date('d-m-Y'));
				$destinatarios = $documento->createElement('destinatarios', $destin);
				$asunto = $documento->createElement('asunto', Input::get('asunto'));
				$mensaje = $documento->createElement('mensaje', $cuerpo);
				$mail->appendChild($orden);
				$mail->appendChild($congreso);
				$mail->appendChild($remitente);
				$mail->appendChild($destinatarios);
				$mail->appendChild($asunto);
				$mail->appendChild($mensaje);

				$correo->appendChild($mail);
				$documento->appendChild($correo);
				$documento->save($fileRoot);

				return Response::json(array('error' => False, 'mensaje' => 'La solicitud ha sido enviada, recibirá un correo de notificación.'));
			}	
    	} catch (Exception $e) {
    		return Response::json(array('error' => True, 'mensaje' => $e->getMessage()));
		}		
	}

	public function adminSendMail()
	{
		try {
    		if(Request::ajax())
			{
				$remit = Auth::user()->idUsuario;
				$destin = "";
				$ord = 1;

				$banner = URL::asset('imagenes/banner_mail_conuca.png');
				$cuerpo = Input::get('cuerpo');
				$destinatarios = Input::get('destinatarios');
 				foreach ($destinatarios as $usuario) {
					$usr =Usuario::where('idUsuario', '=',$usuario)->first();
					$cuerpoTemp = $cuerpo;
					$cuerpoTemp1 = "-".$cuerpoTemp;

					if (strpos($cuerpoTemp1, "[*PNombre*]") != false) {
						if (strpos($usr->nombreUsuario, " ") != false) {
							$pos = strpos($usr->nombreUsuario, " ");
							$PNombre = substr($usr->nombreUsuario, 0, $pos);
							$cuerpoTemp = str_replace("[*PNombre*]", $PNombre, $cuerpoTemp);
						}else{
							$cuerpoTemp = str_replace("[*PNombre*]", $usr->nombreUsuario, $cuerpoTemp);
						}
					}
					if (strpos($cuerpoTemp1, "[*PApellido*]") != false) {
						if (strpos($usr->nombreUsuario, " ") != false) {
							$pos = strpos($usr->apelUsuario, " ");
							$PApellido = substr($usr->apelUsuario, 0, $pos);
							$cuerpoTemp = str_replace("[*PApellido*]", $PApellido, $cuerpoTemp);
						}else{
							$cuerpoTemp = str_replace("[*PApellido*]", $usr->apelUsuario, $cuerpoTemp);
						}
					}
					if (strpos($cuerpoTemp1, "[*Nombre_Completo*]") != false) {
						$NombreComp = $usr->nombreUsuario." ".$usr->apelUsuario;
						$cuerpoTemp = str_replace("[*Nombre_Completo*]", $NombreComp, $cuerpoTemp);
					}

					$cuerpoTemp = str_replace("[b]", "<b>", $cuerpoTemp);
					$cuerpoTemp = str_replace("[/b]", "</b>", $cuerpoTemp);
					$cuerpoTemp = str_replace("[i]", "<i>", $cuerpoTemp);
					$cuerpoTemp = str_replace("[/i]", "</i>", $cuerpoTemp);
					$cuerpoTemp = str_replace("[u]", "<u>", $cuerpoTemp);
					$cuerpoTemp = str_replace("[/u]", "</u>", $cuerpoTemp);
					$cuerpoTemp = str_replace("[u]", "<u>", $cuerpoTemp);
					$cuerpoTemp = str_replace("[/u]", "</u>", $cuerpoTemp);
					$cuerpoTemp = str_replace("[t]", "<h3>", $cuerpoTemp);
					$cuerpoTemp = str_replace("[/t]", "</h3>", $cuerpoTemp);

					$cuerpoTemp = nl2br($cuerpoTemp);

					$sent=Mail::send(
						'emails.correosCongreso', 
						array(
							'correo'		=>	$usr->emailUsuario,
							'remitente'		=>	Input::get('remitente'),
							'anio'			=>	date("Y"),
							'banner_email'	=>	$banner,
							'asunto'		=>	Input::get('asunto'),
							'cuerpo'		=>	$cuerpoTemp
						), 
						function($mensaje) use ($usr)
						{
							$mensaje->to($usr->emailUsuario, $usr->nombreUsuario . ' ' . $usr->apelUsuario)
									->subject(Input::get('asunto'));
						}
					);
					if($sent==0) throw new Exception('No se pudo enviar el correo de notificación , favor intentelo mas tarde.');
					$destin .= $usr->nombreUsuario." ".$usr->apelUsuario." (".$usr->emailUsuario."); ";
				}
				
				$fileName = 'Correo_'.$remit.'.xml';
				$fileRoot = public_path().'/correos/'.$fileName;
				$mappedPath = public_path().'/correos'; 
				File::makeDirectory($mappedPath, $mode = 0777, true, true); 

				$documento = new DOMDocument("1.0");

				if(!(file_exists($fileRoot))){
					$correo = $documento->createElement('correo');
					//$mail = $documento->createElement('mail'); // este no deberia estar aqui
				}else{
					$documento->load($fileRoot);
					$correo = $documento->getElementsByTagName('correo')->item(0); 
					$ord += $documento->getElementsByTagName('mail')->length;
				}

				$mail = $documento->createElement('mail');

				$orden = $documento->createElement('orden', $ord);
				$remitente = $documento->createElement('fecha', date('d-m-Y'));
				$destinatarios = $documento->createElement('destinatarios', $destin);
				$asunto = $documento->createElement('asunto', Input::get('asunto'));
				$mensaje = $documento->createElement('mensaje', $cuerpo);
				$mail->appendChild($orden);
				$mail->appendChild($remitente);
				$mail->appendChild($destinatarios);
				$mail->appendChild($asunto);
				$mail->appendChild($mensaje);

				$correo->appendChild($mail);
				$documento->appendChild($correo);
				$documento->save($fileRoot);

				return Response::json(array('error' => False, 'mensaje' => 'La solicitud ha sido enviada, recibirá un correo de notificación.'));
			}	
    	} catch (Exception $e) {
    		return Response::json(array('error' => True, 'mensaje' => $e->getMessage()));
		}		
	}

	public function mostrarMail($id)
	{
		$idU = Auth::user()->idUsuario;
		$idC = $id;
		$correos = array();
		$mail = array();

		$fileName = 'Correo_'.$idU.'.xml';
		$fileRoot = public_path().'/correos/'.$fileName;
		if(!(file_exists($fileRoot))){
		}else{
			$documento = new DOMDocument();
			$documento->load($fileRoot);

			$cantidad_de_mensajes = $documento->getElementsByTagName('mail')->length;

			for($i = 0; $i < $cantidad_de_mensajes; $i++){
				if ($documento->getElementsByTagName('congreso')->item($i)->nodeValue == $idC) {
					$mail['orden'] = $documento->getElementsByTagName('orden')->item($i)->nodeValue;
					$mail['fecha'] = $documento->getElementsByTagName('fecha')->item($i)->nodeValue;
					$mail['destinatarios'] = $documento->getElementsByTagName('destinatarios')->item($i)->nodeValue;
					$mail['asunto'] = $documento->getElementsByTagName('asunto')->item($i)->nodeValue;
					$mail['mensaje'] = $documento->getElementsByTagName('mensaje')->item($i)->nodeValue;
					$correos[] = $mail;
				}
			}
		}

		$this->layout->content = View::make('congresos.historialCorreos' , array('idCongreso' => $id, 'correos' => $correos));
	}

	public function adminMostrarMail()
	{
		$idU = Auth::user()->idUsuario;
		$correos = array();
		$mail = array();

		$fileName = 'Correo_'.$idU.'.xml';
		$fileRoot = public_path().'/correos/'.$fileName;
		if(!(file_exists($fileRoot))){
		}else{
			$documento = new DOMDocument();
			$documento->load($fileRoot);

			$cantidad_de_mensajes = $documento->getElementsByTagName('mail')->length;

			for($i = 0; $i < $cantidad_de_mensajes; $i++){
				$mail['orden'] = $documento->getElementsByTagName('orden')->item($i)->nodeValue;
				$mail['fecha'] = $documento->getElementsByTagName('fecha')->item($i)->nodeValue;
				$mail['destinatarios'] = $documento->getElementsByTagName('destinatarios')->item($i)->nodeValue;
				$mail['asunto'] = $documento->getElementsByTagName('asunto')->item($i)->nodeValue;
				$mail['mensaje'] = $documento->getElementsByTagName('mensaje')->item($i)->nodeValue;
				$correos[] = $mail;
			}
		}

		$this->layout->content = View::make('adminHistorialCorreos' , array('correos' => $correos));
	}

	public function verMail($id, $ord)
	{
		$idU = Auth::user()->idUsuario;
		$correos = array();
		$mail = array();

		$fileName = 'Correo_'.$idU.'.xml';
		$fileRoot = public_path().'/correos/'.$fileName;
		if(!(file_exists($fileRoot))){	
		}else{
			$documento = new DOMDocument();
			$documento->load($fileRoot);

			$cantidad_de_mensajes = $documento->getElementsByTagName('mail')->length;

			for($i = 0; $i < $cantidad_de_mensajes; $i++){
				if ($documento->getElementsByTagName('orden')->item($i)->nodeValue == $ord) {
					$mail['fecha'] = $documento->getElementsByTagName('fecha')->item($i)->nodeValue;
					$mail['destinatarios'] = $documento->getElementsByTagName('destinatarios')->item($i)->nodeValue;
					$mail['asunto'] = $documento->getElementsByTagName('asunto')->item($i)->nodeValue;
					$mail['mensaje'] = $documento->getElementsByTagName('mensaje')->item($i)->nodeValue;
					$correos[] = $mail;
				}
			}
		}

		$this->layout->content = View::make('congresos.verCorreos' , array('idCongreso' => $id, 'correos' => $correos));
	}

	public function adminVerMail($ord)
	{
		$idU = Auth::user()->idUsuario;
		$correos = array();
		$mail = array();

		$fileName = 'Correo_'.$idU.'.xml';
		$fileRoot = public_path().'/correos/'.$fileName;
		if(!(file_exists($fileRoot))){	
		}else{
			$documento = new DOMDocument();
			$documento->load($fileRoot);

			$cantidad_de_mensajes = $documento->getElementsByTagName('mail')->length;

			for($i = 0; $i < $cantidad_de_mensajes; $i++){
				if ($documento->getElementsByTagName('orden')->item($i)->nodeValue == $ord) {
					$mail['fecha'] = $documento->getElementsByTagName('fecha')->item($i)->nodeValue;
					$mail['destinatarios'] = $documento->getElementsByTagName('destinatarios')->item($i)->nodeValue;
					$mail['asunto'] = $documento->getElementsByTagName('asunto')->item($i)->nodeValue;
					$mail['mensaje'] = $documento->getElementsByTagName('mensaje')->item($i)->nodeValue;
					$correos[] = $mail;
				}
			}
		}

		$this->layout->content = View::make('adminVerCorreos' , array('correos' => $correos));
	}

}