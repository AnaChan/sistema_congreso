<?php

class RevisarPaperController extends \BaseController {

	/**
	 * Variable que indica si se quieren solo articulos sin revisiones.
	 *
	 */
	protected $PAPER_SIN_REV = 0;

	/**
	 * Variable que indica si se quieren solo articulos con revisiones.
	 *
	 */
	protected $PAPER_CON_REV = 1;

	/**
	 * Muestra la lista de articulos asociadas al usuario dependiendo de la temática asignada a el.
	 *
	 * @return HTML
	 */
	public function listarPaper($idCongreso)
	{

		if(!$this->verificarFechaRecurso($idCongreso,'Confirmacion de aceptacion de ponencias')) //quiere decir que la fecha esta fuera de rango
		return Redirect::action('UsuariosCongresosController@establecerCongreso',array($idCongreso));

		$papers = $this->paperFiltroQuery($idCongreso, $this->PAPER_SIN_REV);

		$criterios = DB::table('criterio_evaluacion')
		->join(
			'detalle_congreso',
			'detalle_congreso.idDetalleCongreso',
			'=',
			'criterio_evaluacion.idDetalleCongreso'
			)
		->join(
			'congreso',
			'congreso.idCongreso',
			'=',
			'detalle_congreso.idCongreso'
			)
		->where(
			'congreso.idCongreso',
			'=',
			$idCongreso
			)
		->where(
			'indicadorFicha',
			'=',
			'P'
			)
		->whereRaw(
			'criterio_evaluacion.idCriterioEvaluacion = criterio_evaluacion.idCriterioEvaluacionPadre'
			)
		->get();

		foreach ($criterios as $criterio)
		{
			$criterio->hijos = DB::table('criterio_evaluacion')
			->where(
				'idCriterioEvaluacionPadre',
				'=',
				$criterio->idCriterioEvaluacion
				)
			->whereRaw(
				'criterio_evaluacion.idCriterioEvaluacion != criterio_evaluacion.idCriterioEvaluacionPadre'
				)->get();

			foreach($criterio->hijos as $hijo)
			{
				$hijo->puntajes = DB::table('puntaje')
				->where(
					'idCriterioEvaluacion',
					'=',
					$hijo->idCriterioEvaluacion
					)
				->select('nomPuntaje', 'valorPuntaje', 'idPuntaje')
				->orderBy('valorPuntaje','asc')
				->get();
				$hijo->puntajesMax = count($hijo->puntajes);
			}

			$criterio->puntajes = DB::table('puntaje')
			->where(
				'idCriterioEvaluacion',
				'=',
				$criterio->idCriterioEvaluacion
				)
			->select('nomPuntaje', 'valorPuntaje', 'idPuntaje')
			->orderBy('valorPuntaje','asc')
			->get();

			$criterio->puntajesMax = count($criterio->puntajes);
		}

		$this->layout->content = View::Make('congresos.articulos.revisarPaper')->withPapers($papers)->withCriterios($criterios)->with('idCongreso', $idCongreso);
	}


	/**
	 * retorna la lista de fichas segun el filtro seleccionado.
	 *
	 * @return Json listado de todas las fichas 
	 */
	public function paperFiltro($idCongreso)
	{

		if(Request::ajax())
		{
			$rules = array(
				'filtro' =>	'required|integer|in:0,1,2'
				);

			$validador = Validator::make(Input::all(), $rules);

			if($validador->fails()) {
				$errores = $validador->messages();
				return Response::json(array('error' => True, 'mensaje' => $errores->first()));
			}
			else
			{
				$papers = $this->paperFiltroQuery($idCongreso, Input::get('filtro'));

				foreach ($papers as $paper)
				{
					$paper->esPDF = pathinfo($paper->rutaPaper, PATHINFO_EXTENSION) == "pdf";
					$paper->rutaArchivo = URL::action('ArchivoController@servirArchivo', array($idCongreso,$paper->rutaPaper));
					$paper->nombreArchivo = pathinfo($paper->rutaPaper, PATHINFO_BASENAME);
					$paper->rechazarUrl = URL::action('RevisarPaperController@rechazarPaper', array($idCongreso,$paper->idPaper));
					$paper->revisarUrl =  URL::action('RevisarPaperController@evaluarPaper', array($idCongreso,$paper->idPaper));
					$paper->revisiones = DB::table('revision_paper_x_criterio')
					->join(
						'puntaje',
						'puntaje.idPuntaje',
						'=',
						'revision_paper_x_criterio.idPuntaje'
						)
					->where(
						'revision_paper_x_criterio.idRevisionPaper',
						'=',
						$paper->idRevisionPaper
						)
					->select('idCriterioEvaluacion', 'valorPuntaje', 'idRevisionPaper')
					->get();
					$paper->cantidadRevs = count($paper->revisiones);
				}

				return Response::json(array('error' => False, 'mensaje' => '','papers' => $papers));
			}
		}
	}

	/**
	 * Rechaza la revision de la ficha.
	 *
	 * @param integer $idCongreso id del congreso al que pertenece la ficha.
	 *
	 * @param integer $idFicha id de la ficha a rechazar.
	 *
	 * @return Json 
	 */
	public function rechazarPaper($idCongreso,$idPaper)
	{
		$paperQuery = DB::Table('paper')
		->join(
			'ficha',
			'ficha.idFicha',
			'=',
			'paper.idFicha'
			)
		->join(
			'detalle_congreso',
			'detalle_congreso.idDetalleCongreso',
			'=',
			'ficha.idDetalleCongreso'
			)
		->join(
			'congreso',
			'congreso.idCongreso',
			'=',
			'detalle_congreso.idCongreso'
			)
		->join(
			'autor_x_ficha',
			'autor_x_ficha.idFicha',
			'=',
			'ficha.idFicha'
			)
		->join(
			'revision_paper',
			'revision_paper.idPaper',
			'=',
			'paper.idPaper'
			)
		->where(
			'congreso.idCongreso',
			'=',
			$idCongreso
			)
		->where(
			'autor_x_ficha.idUsuario',
			'!=',
			Auth::user()->idUsuario
			)
		->where(
			'revision_paper.idUsuario',
			'=',
			Auth::user()->idUsuario
			)
		->where(
			'paper.idPaper',
			'=',
			$idPaper
			);

		$paper = $paperQuery->first();
		$revisiones = $paperQuery
		->join(
			'revision_paper_x_criterio',
			'revision_paper_x_criterio.idRevisionPaper',
			'=',
			'revision_paper.idRevisionPaper'
			)->get();

		if(count($paper) == 0)
		{
			return Response::json(array('error' => True, 'mensaje' => 'No puedes eliminar un articulo del cual eres autor o que no se te haya asignado revisar o que no pertenezca al congreso.'));
		}

		if(count($revisiones) != 0)
		{
			return Response::json(array('error' => True, 'mensaje' => 'No puedes rechazar la revision de un articulo que ya se ha revisado.'));
		}

		try {
			DB::beginTransaction();
			RevisionPaper::find($paper->idRevisionPaper)->delete();
			//TODO: Enviar correo de rechazo a PC
			DB::commit();
			return Response::json(array('error' => False, 'mensaje' => 'Se ha rechazado exitosamente.'));
		} catch (\Exception $e) {
			DB::rollback();
			return Response::json(array('error' => True, 'mensaje' => 'No se pudo guardar el registro o no se pudo enviar el correo.'));
		}
	}

	/**
	 * Rechaza la revision de la ficha.
	 *
	 * @param integer $idCongreso id del congreso al que pertenece la ficha.
	 *
	 * @param integer $idFicha id de la ficha a rechazar.
	 *
	 * @return Json 
	 */
	public function evaluarPaper($idCongreso,$idPaper)
	{

		$rules = array(
			'puntajes' =>	'required|array' 
		);

		$validador = Validator::make(Input::all(), $rules);

		if ($validador->fails()) {
			$errores = $validador->messages();
			return Response::json(array('error' => True, 'mensaje' => $errores->first()));
		}
		else
		{
			$fichaQuery = DB::Table('paper')
			->join(
				'ficha',
				'ficha.idFicha',
				'=',
				'paper.idFicha'
				)
			->join(
				'detalle_congreso',
				'detalle_congreso.idDetalleCongreso',
				'=',
				'ficha.idDetalleCongreso'
				)
			->join(
				'congreso',
				'congreso.idCongreso',
				'=',
				'detalle_congreso.idCongreso'
				)
			->join(
				'autor_x_ficha',
				'autor_x_ficha.idFicha',
				'=',
				'ficha.idFicha'
				)
			->join(
				'revision_paper',
				'revision_paper.idPaper',
				'=',
				'paper.idPaper'
				)
			->where(
				'congreso.idCongreso',
				'=',
				$idCongreso
				)
			->where(
				'autor_x_ficha.idUsuario',
				'!=',
				Auth::user()->idUsuario
				)
			->where(
				'revision_paper.idUsuario',
				'=',
				Auth::user()->idUsuario
				)
			->where(
				'paper.idPaper',
				'=',
				$idPaper
				);

			$ficha = $fichaQuery->first();
			$revisiones = $fichaQuery
			->join(
				'revision_paper_x_criterio',
				'revision_paper_x_criterio.idRevisionPaper',
				'=',
				'revision_paper.idRevisionPaper'
				)->get();

			if(count($ficha) == 0)
			{
				return Response::json(array('error' => True, 'mensaje' => 'No puedes evaluar un articulo del cual eres autor o que no se te haya asignado revisar o que no pertenezca al congreso.'));
			}

			if(count($revisiones) != 0)
			{
				return Response::json(array('error' => True, 'mensaje' => 'No puedes evaluar un articulo que ya se ha evaluada.'));
			}

			try {
				DB::beginTransaction();
				$puntajes = Input::get('puntajes');

				foreach ($puntajes as $puntaje)
				{
					$rev = array('idRevisionPaper' => $ficha->idRevisionPaper, 'idPuntaje' => $puntaje);
					DB::table('revision_paper_x_criterio')->insert(
						$rev
					);
				}
				$fichaData=DB::table('ficha')
								->join('paper',
										'paper.idFicha',
										'=',
										'ficha.idFicha'
									)
								->where('paper.idPaper','=',$idPaper)->first();

				$cong = Congreso::where('idCongreso', '=',$idCongreso)->first();
				$dc=DetalleCongreso::where('idCongreso', '=',$idCongreso)->first();
				$usr = DB::table('pc_x_tematica')
								->join('tematica',
										'pc_x_tematica.idTematica',	
										'=',
										'tematica.idTematica'
									)
								->join('usuariorol_x_congreso',
										'usuariorol_x_congreso.idUsuariorolXCongreso',
										'=',
										'pc_x_tematica.idUsuariorolXCongreso'
									)
								->join('usuarios',
										'usuarios.idUsuario',
										'=',
										'usuariorol_x_congreso.idUsuario'
									)
								->where('pc_x_tematica.idTematica',
										'=',
										$fichaData->idTematica
									)
								->where('usuariorol_x_congreso.idCongreso',
										'=',
										$idCongreso
									)->first();

				$usrChair = DB::table('usuarios')
								->join('congreso',
										'congreso.idCreador',	
										'=',
										'usuarios.idUsuario'
									)
								->where('congreso.idCongreso',
										'=',
										$idCongreso
									)->first();
				$banner = URL::asset('banners/'.$dc->banner);
				Mail::send(
					'emails.notificacionPCRevisionFicha', 
					array(
						'nombreSistema'	=>	$cong->acronimoCongreso,
						'nombreSistemaCompleto'	=>	$cong->nomCongreso,
						'correo'		=>	$usrChair->emailUsuario,
						'nombres'		=>	$usrChair->nombreUsuario,
						'apellidos'		=>	$usrChair->apelUsuario,
						'anio'			=>	date("Y"),
						'tematica'		=> $usr->nomTematica,
						'tabla'			=> 'Paper',
						'tituloFP'		=> $fichaData->tituloPaper,
						'rolSistema'	=> 'Administrador de Congreso',
						'banner_email' 	=>	$banner,
						'URLConfirmar'	=> '',
						'URLSitio'		=> URL::action('HomeController@inicio'),
					), 
					function($mensaje) use ($usrChair)
					{
						$mensaje->to($usrChair->emailUsuario, $usrChair->nombreUsuario . ' ' . $usrChair->apelUsuario)
								->subject('CONUCA: Una asignación requiere de su aprobación.');
					}
				);
				DB::commit();
				return Response::json(array('error' => False, 'mensaje' => 'Se ha evaluado exitosamente.'));
			} catch (\Exception $e) {
				DB::rollback();
				return Response::json(array('error' => True, 'mensaje' => 'No se pudo guardar el registro o no se pudo enviar el correo.' . $e->getMessage()));
			}
		}
	}

	/**
	 * Funcion que regresa los articulo que no han sido revisadas.
	 *
	 * @param $idCongreso id del congreso al que hay que sacar la lista de articulos.
	 *
	 * @param $flag Determina el filtro a utilizar. Ocupa los valores de banderas seteados al inicio de la clase
	 *
	 * @return array Lista de objetos articulos no revisadas.
	 *
	 */
	protected function paperFiltroQuery($idCongreso, $flag)
	{
		$fichasQuery = DB::Table('paper')
		->join(
			'ficha',
			'ficha.idFicha',
			'=',
			'paper.idFicha'
			)
		->join(
			'detalle_congreso',
			'detalle_congreso.idDetalleCongreso',
			'=',
			'ficha.idDetalleCongreso'
			)
		->join(
			'congreso',
			'congreso.idCongreso',
			'=',
			'detalle_congreso.idCongreso'
			)
		->join(
			'estado_paper',
			'estado_paper.idEstadoPaper',
			'=',
			'paper.idEstadoPaper'
			)
		->join(
			'autor_x_ficha',
			'autor_x_ficha.idFicha',
			'=',
			'paper.idFicha'
			)
		->join(
			'usuarios',
			'usuarios.idUsuario',
			'=',
			'autor_x_ficha.idUsuario'
			)
		->join(
			'revision_paper',
			'revision_paper.idPaper',
			'=',
			'paper.idPaper'
			)
		->where(
			'congreso.idCongreso',
			'=',
			$idCongreso
			)
		->where(
			'autor_x_ficha.idUsuario',
			'!=',
			Auth::user()->idUsuario
			)
		->where(
			'autor_x_ficha.responsable',
			'=',
			True
			)
		->where(
			'revision_paper.idUsuario',
			'=',
			Auth::user()->idUsuario
			);

		if($flag == $this->PAPER_SIN_REV) 
			return $fichasQuery
			->whereNotExists(function($query)
			{
				$query
				->select(DB::raw(1))
				->from('revision_paper_x_criterio')
				->whereRaw('revision_paper_x_criterio.idRevisionPaper = revision_paper.idRevisionPaper');
			})
			->get();
		elseif($flag == $this->PAPER_CON_REV)
			return $fichasQuery
			->whereExists(function($query)
			{
				$query
				->select(DB::raw(1))
				->from('revision_paper_x_criterio')
				->whereRaw('revision_paper_x_criterio.idRevisionPaper = revision_paper.idRevisionPaper');
			})
			->get();
		else
			return $fichasQuery->get();
	}
}