<?php

class AdminusuariosController extends \BaseController {
	
	public function index()
	{
		$modeloUsuario = new Usuario;
		$usuarios = $modeloUsuario::all();
		$this->layout->content = View::make('adminUsuariosIndex', array('message' => '', 
									'usuarios' => $usuarios ));
	}

	public function create()
	{
		$estadosUser = EstadoUsuario::orderBy('nombreEstado', 'asc')->lists('nombreEstado','idEstadoUsuario');
		$this->layout->content = View::make('adminUsuariosCreate', array('estados' => $estadosUser));
	}
		
	public function store()
	{
		try {
			$validEmail = Usuario::where("emailUsuario", "=", Input::get('emailUsuario'))
									->count();
									
			if($validEmail > 0)
			{
				return Redirect::back()
					->with('message', 'Correo Electr&oacute;nico ya existe, introduzca otra direcci&oacute;n de correo')
					->withInput();
			}
			
			$setValores	= Input::all();
			$setValores['passwordUsuario']	= Hash::make((Input::get('passwordUsuario')));
			Usuario::create($setValores);
			return Redirect::to('inicio/adminusuarios');
		} catch (Exception $e) {
			return Redirect::back()
					->with('message', $e->getMessage())
					->withInput();
		}
	}
	
	public function edit($id)
	{
		$estadosUser = EstadoUsuario::orderBy('nombreEstado', 'asc')->lists('nombreEstado','idEstadoUsuario');
		$this->layout->content = View::make('adminUsuariosEdit', array('idUsuario' => $id, 'estados' => $estadosUser));
	}

	public function update($id)
	{
		try {
			
			$validEmail = Usuario::where("emailUsuario", "=", Input::get('emailUsuario'))
									->where("idUsuario", "<>", $id)
									->count();
									
			if($validEmail > 0)
			{
				return Redirect::back()
					->with('message', 'Correo Electr&oacute;nico ya existe, introduzca otra direcci&oacute;n de correo')
					->withInput();
			}
			
			$user	= Usuario::find($id);
			$inputs	= Input::all();
			
			if(Input::get('nocambiar') == '1')
				$inputs['passwordUsuario']	= $user['passwordUsuario'];
			else
				$inputs['passwordUsuario']	= Hash::make((Input::get('passwordUsuario')));

			$inputs['fecModUsuario'] = date('Y-m-d H:i:s');
			 
			if (!$user->update($inputs)) {
				return Redirect::back()
						->with('message', 'Hubo algunos problemas para actulizar este usuario')
						->withInput();
			}
			return Redirect::to('inicio/adminusuarios');
		} catch (Exception $e) {
			return Redirect::back()
					->with('message', $e->getMessage())
					->withInput();
		}
	}

	public function destroy($id)
	{
		$currentId = $id;
		Usuario::destroy($currentId);
		return Response::json(array('error' => False, 'mensaje' => 'Usuario Eliminado, espere un momento mientras se refresca la pantalla'));
	}

}