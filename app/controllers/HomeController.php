<?php

class HomeController extends BaseController {
	/**
	 * Ventana principal de la aplicación.
	 *
	 * @return HTML
	 */
	public function inicio()
	{
		$idUsuario = Auth::check()?Auth::user()->idUsuario:0;
		if($idUsuario==0){
				$congresos = DB::table('detalle_congreso')
				->join(
					'congreso',
					'congreso.idCongreso',
					'=',
					'detalle_congreso.idCongreso'
					)
				->join(
					'estado_congreso',
					'estado_congreso.idEstadoCongreso',
					'=',
					'congreso.idEstadoCongreso'
					)
				->Where(
					'estado_congreso.valorEstado',
					'=',
					'ACT'
					)
				->orderBy('congreso.fecCreaCongreso', 'desc')
		                    ->groupBy('congreso.fecCreaCongreso')
				->get();
		}
		else{
			$isAdmin=DB::table('usuarios')
						->join('estado_usuario',
								'estado_usuario.idEstadoUsuario',
								'=',
								'usuarios.idEstadoUsuario'
							)
						->Where('estado_usuario.valorEstado','=','ADM')
						->Where('usuarios.idUsuario','=',$idUsuario)
						->first();
			$congresos=null;
			if(count($isAdmin)!=0)
			{
				$congresos = DB::table('detalle_congreso')
				->join(
					'congreso',
					'congreso.idCongreso',
					'=',
					'detalle_congreso.idCongreso'
					)
				->join(
					'estado_congreso',
					'estado_congreso.idEstadoCongreso',
					'=',
					'congreso.idEstadoCongreso'
					)
				->WhereIn(
					'estado_congreso.valorEstado',
					['ACT','SNC']
					)
				->get();
			}
			else
			{
				$congresos = DB::table('detalle_congreso')
				->join(
					'congreso',
					'congreso.idCongreso',
					'=',
					'detalle_congreso.idCongreso'
					)
				->join(
					'estado_congreso',
					'estado_congreso.idEstadoCongreso',
					'=',
					'congreso.idEstadoCongreso'
					)
				->join('usuariorol_x_congreso',
						'usuariorol_x_congreso.idCongreso',
						'=',
						'congreso.idCongreso'
					)
				->Where('usuariorol_x_congreso.idUsuario',
						'=',
						$idUsuario
						)
				->WhereIn(
					'estado_congreso.valorEstado',
					['ACT', 'SNC','CER']
					)
				->orWhere(
					'congreso.idCreador',
					'=',
					$idUsuario
					)
				->WhereIn(
					'estado_congreso.valorEstado',
					['ACT', 'SNC','CER']
					)
				->orderBy('congreso.fecCreaCongreso', 'desc')
		                    ->groupBy('congreso.fecCreaCongreso')
				->get();
			}
		}
		Session::forget('rol_barra');
		$this->layout->content = View::make('inicio.Home');
		$this->layout->content->congresos = $congresos;
		$this->layout->content->idUsuario = $idUsuario;
	}

	/**
	 * Verifica si existe el usuario actualmente loggeado en el congreso enviado (JSON).
	 *
	 * @param integer idCongreso
	 *
	 * @return JSON
	 */
	public function verificarExistente()
	{
		if(Request::ajax())
		{

			$rules = array(
				'idCongreso'	=>	'required|integer'
			);

			$validador = Validator::make(Input::all(), $rules);

			if($validador->fails()) {
				$errores = $validador->messages();
				return Response::json(array('error' => True, 'mensaje' => $errores->first()));
			}
			else
			{
				$admin=DB::table('usuarios')
								->join('estado_usuario',
										'estado_usuario.idEstadoUsuario',
										'=',
										'usuarios.idEstadoUsuario'
									)
								->Where(
									'usuarios.idUsuario',
									'=',
									Auth::user()->idUsuario
								)
								->where('estado_usuario.valorEstado',
										'=',
										'ADM'
									)
								->first();

				if(count($admin)>0)
					return Response::json(array('error' => True, 'mensaje' => 'El administrador del sistema no puede participar en ningún congreso.', 'existe' => False));

				$actuales = DB::table('detalle_congreso')
				->join(
					'usuariorol_x_congreso',
					'usuariorol_x_congreso.idCongreso',
					'=',
					'detalle_congreso.idCongreso'
				)
				->join(
					'congreso',
					'congreso.idCongreso',
					'=',
					'detalle_congreso.idCongreso'
				)
				->join(
					'estado_urc',
					'usuariorol_x_congreso.idEstadoURC',
					'=',
					'estado_urc.idEstadoURC'
				)
				->Where(
					'usuariorol_x_congreso.idUsuario',
					Auth::user()->idUsuario
				)
				->Where(
					'usuariorol_x_congreso.idCongreso',
					Input::get('idCongreso')
				)->first();


				if(count($actuales) > 0)
					if(strcmp($actuales->valorEstado,'ACT')==0)
						return Response::json(array('error' => False, 'mensaje' => 'Ingresando al congreso...', 'existe' => True, 'urlCongreso' => URL::action('UsuariosCongresosController@establecerCongreso', array(Input::get('idCongreso')))));
					else
						return Response::json(array('error' => False, 'mensaje' => 'Su cuenta esta en proceso de confirmación para este congreso, Revise su bandeja de entrada o contacte al Administrador', 'existe' => True,'urlCongreso' => URL::action('UsuariosCongresosController@establecerCongreso', array(Input::get('idCongreso')))));
				return Response::json(array('error' => False, 'mensaje' => '', 'existe' => False));
			}
		}
	}

	/**
	 * Sirve para inscribir el usuario actualmente loggeado el congreso enviado (JSON).
	 *
	 * @param integer idCongreso
	 *
	 * @return JSON
	 */
	public function inscribirUsuario()
	{
		$rules = array(
			'idCongreso'	=>	'required|integer'
		);

		$validador = Validator::make(Input::all(), $rules);

		if($validador->fails()) {
			$errores = $validador->messages();
			return Response::json(array('b'=>'b', 	'error' => True, 'mensaje' => $errores->first()));
		}
		else
		{
			$actuales = DB::table('detalle_congreso')
			->join(
				'usuariorol_x_congreso',
				'usuariorol_x_congreso.idCongreso',
				'=',
				'detalle_congreso.idCongreso'
			)
			->join(
				'congreso',
				'congreso.idCongreso',
				'=',
				'detalle_congreso.idCongreso'
			)
			->Where(
				'usuariorol_x_congreso.idUsuario',
				Auth::user()->idUsuario
			)
			->Where(
				'usuariorol_x_congreso.idCongreso',
				Input::get('idCongreso')
			)
			->select(
				'detalle_congreso.idCongreso'
			)
			->lists(
				'idCongreso'
			);
			if(count($actuales) > 0)
				return Response::json(array('error' => False, 'mensaje' => 'Ingresando al congreso...', 'urlCongreso' => URL::action('UsuariosCongresosController@establecerCongreso', array(Input::get('idCongreso')))));
			
			return DB::transaction(function()
			{
				DB::beginTransaction();
				try
				{
					//TODO: mejorar el manejo de excepcion para hacer rollback en caso falle enviar un correo.
					$idRol = Rol::where('nomRol', '=', 'Autor')->first()->idRol;
					$idEstadoURC = EstadoURC::where('valorEstado', '=', 'ACT')->first()->idEstadoURC;

					$usuarioxc = new UsuariorolXCongreso;
					$usuarioxc->idUsuario = auth::user()->idUsuario;
					$usuarioxc->idCongreso= Input::get('idCongreso');
					$usuarioxc->idRol= $idRol;
					$usuarioxc->idEstadoURC = $idEstadoURC;
					$usuarioxc->save();

					$congreso= Congreso::WhereRaw('idCongreso=?',array(Input::get('idCongreso')))->first();
					$dc=DetalleCongreso::WhereRaw('idCongreso=?',array(Input::get('idCongreso')))->first();
					$banner = URL::asset('banners/'.$dc->banner);
					Mail::send(
						'emails.notificacionUsuario', 
						array(
							'nombreSistema'	=>	$congreso->acronimoCongreso,
							'nombreSistemaCompleto'	=>	$congreso->nomCongreso,
							'correo'		=>	auth::user()->emailUsuario,
							'nombres'		=>	auth::user()->nombreUsuario,
							'apellidos'		=>	auth::user()->apelUsuario,
							'anio'			=>	date("Y"),
							'rolSistema'	=> 'Autor',
							'banner_email' 	=>	$banner,
							'URLSitio'		=> URL::action('UsuariosCongresosController@establecerCongreso', 
															array(Input::get('idCongreso')))
						), 
						function($mensaje)
						{
							$mensaje->to(auth::user()->emailUsuario, auth::user()->nombreUsuario . ' ' . auth::user()->apelUsuario)
									->subject('CONUCA: Notificación de Registro en Congreso');
						}
					);
					DB::commit();
					return Response::json(array('error' => False, 'mensaje' => 'Usted ha sido inscrito en el congreso, en breve recibirá un correo de notificación. <br> Ingresando....', 'urlCongreso' => URL::action('UsuariosCongresosController@establecerCongreso', array(Input::get('idCongreso')))));
				}
				catch(Exception $e)
				{
					DB::Rollback();
					return Response::json(array('error' => True, 'mensaje' => $e->getMessage()));
				}
			});
		}
	}

	public function filtrar()
	{ 
		$filtro= Input::get('filtro');
		if($filtro==1)
			return $this->mostrarTodos();
		if($filtro==2)
			return $this->congresosComoAutor();
		if($filtro==3)
			return $this->misCongresosRevisor();
		if($filtro==4)
			return $this->misCongresosChair();
		if($filtro==5)
			return $this->misCongresosPC();
		if($filtro==6)
			return $this->congresosSinInscribir();
		if($filtro==7)
			return $this->mostrarTodosCerrados();
		if($filtro==8)
			return $this->mostrarTodosMisCongresosSNC();
		if($filtro==9)
			return $this->mostrarTodosMisCongresosDESC();
		if($filtro==10)
			return $this->mostrarTodosMisCongresosASC();
	}

	/**
	 * Muestra todos los congresos activos o todos los congreso activos, sin configurar o cerrados propios del usuario(JSON).
	 *
	 * @return JSON
	 */	
	public function mostrarTodos()
	{
		$idUsuario = Auth::check()?Auth::user()->idUsuario:0;
		$congresos = DB::table('detalle_congreso')
		->join(
			'congreso',
			'congreso.idCongreso',
			'=',
			'detalle_congreso.idCongreso'
			)
		->join(
			'estado_congreso',
			'estado_congreso.idEstadoCongreso',
			'=',
			'congreso.idEstadoCongreso'
			)
		->Where(
			'estado_congreso.valorEstado',
			'=',
			'ACT'
			)
		->orWhere(
			'congreso.idCreador',
			'=',
			$idUsuario
			)
		->WhereIn(
			'estado_congreso.valorEstado',
			['ACT']
			)
		->get();

		return Response::json(array('error' => False, 'mensaje' => '', 'congresos' => $congresos));
	}


	/**
	 * Muestra todos los congresos creados por el usuario(JSON).
	 * de manera descendente
	 * @return JSON
	 */	
	public function mostrarTodosMisCongresosDESC()
	{
		$idUsuario = Auth::check()?Auth::user()->idUsuario:0;
		$congresos = DB::table('detalle_congreso')
				->join(
					'congreso',
					'congreso.idCongreso',
					'=',
					'detalle_congreso.idCongreso'
					)
				->join(
					'estado_congreso',
					'estado_congreso.idEstadoCongreso',
					'=',
					'congreso.idEstadoCongreso'
					)
				->join('usuariorol_x_congreso',
						'usuariorol_x_congreso.idCongreso',
						'=',
						'congreso.idCongreso'
					)
				->Where('usuariorol_x_congreso.idUsuario',
						'=',
						$idUsuario
						)
				->WhereIn(
					'estado_congreso.valorEstado',
					['ACT', 'SNC','CER']
					)
				->orWhere(
					'congreso.idCreador',
					'=',
					$idUsuario
					)
				->WhereIn(
					'estado_congreso.valorEstado',
					['ACT', 'SNC','CER']
					)
				->orderBy('congreso.fecCreaCongreso', 'desc')
		                    ->groupBy('congreso.fecCreaCongreso')
				->get();

		return Response::json(array('error' => False, 'mensaje' => '', 'congresos' => $congresos));
	}

		/**
	 * Muestra todos los congresos creados por el usuario(JSON).
	 * de manera ascendente
	 * @return JSON
	 */	
	public function mostrarTodosMisCongresosASC()
	{
		$idUsuario = Auth::check()?Auth::user()->idUsuario:0;
		$congresos = DB::table('detalle_congreso')
				->join(
					'congreso',
					'congreso.idCongreso',
					'=',
					'detalle_congreso.idCongreso'
					)
				->join(
					'estado_congreso',
					'estado_congreso.idEstadoCongreso',
					'=',
					'congreso.idEstadoCongreso'
					)
				->join('usuariorol_x_congreso',
						'usuariorol_x_congreso.idCongreso',
						'=',
						'congreso.idCongreso'
					)
				->Where('usuariorol_x_congreso.idUsuario',
						'=',
						$idUsuario
						)
				->WhereIn(
					'estado_congreso.valorEstado',
					['ACT', 'SNC','CER']
					)
				->orWhere(
					'congreso.idCreador',
					'=',
					$idUsuario
					)
				->WhereIn(
					'estado_congreso.valorEstado',
					['ACT', 'SNC','CER']
					)
				->orderBy('congreso.fecCreaCongreso', 'asc')
		                    ->groupBy('congreso.fecCreaCongreso')
				->get();

		return Response::json(array('error' => False, 'mensaje' => '', 'congresos' => $congresos));
	}

	/**
	 * Muestra todos los congresos creados por el usuario(JSON).
	 * Sin configurar
	 * @return JSON
	 */	
	public function mostrarTodosMisCongresosSNC()
	{
		$idUsuario = Auth::check()?Auth::user()->idUsuario:0;
		$congresos = DB::table('detalle_congreso')
		->join(
			'congreso',
			'congreso.idCongreso',
			'=',
			'detalle_congreso.idCongreso'
			)
		->join(
			'estado_congreso',
			'estado_congreso.idEstadoCongreso',
			'=',
			'congreso.idEstadoCongreso'
			)
		->Where(
			'congreso.idCreador',
			'=',
			$idUsuario
			)
		->WhereIn(
			'estado_congreso.valorEstado',
			['SNC']
			)
		->get();

		return Response::json(array('error' => False, 'mensaje' => '', 'congresos' => $congresos));
	}

	/**
	 * Muestra todos los congresos cerrados.
	 *
	 * @return JSON
	 */	
	public function mostrarTodosCerrados()
	{
		$idUsuario = Auth::check()?Auth::user()->idUsuario:0;
		$congresos = DB::table('detalle_congreso')
		->join(
			'congreso',
			'congreso.idCongreso',
			'=',
			'detalle_congreso.idCongreso'
			)
		->join(
			'estado_congreso',
			'estado_congreso.idEstadoCongreso',
			'=',
			'congreso.idEstadoCongreso'
			)
		->Where(
			'estado_congreso.valorEstado',
			'=',
			'CER'
			)
		->get();

		return Response::json(array('error' => False, 'mensaje' => '', 'congresos' => $congresos));
	}

	/**
	 * Muestra todos los congresos donde el usuario es chair (JSON).
	 *
	 * @return JSON
	 */	
	public function misCongresosChair()
	{
		$idUsuario = Auth::check()?Auth::user()->idUsuario:0;

		$congresos=DB::table('detalle_congreso')
		->join(
			'congreso',
			'congreso.idCongreso',
			'=',
			'detalle_congreso.idCongreso'
			)
		->join(
			'usuariorol_x_congreso',
			'usuariorol_x_congreso.idCongreso',
			'=',
			'detalle_congreso.idCongreso'
			)
		->join(
			'rol',
			'rol.idRol',
			'=',
			'usuariorol_x_congreso.idRol'
			)
		->join(
			'estado_congreso',
			'estado_congreso.idEstadoCongreso',
			'=',
			'congreso.idEstadoCongreso'
			)
		->join(
			'estado_urc',
			'estado_urc.idEstadoURC',
			'=',
			'usuariorol_x_congreso.idEstadoURC'
		)
		->Where(
			'usuariorol_x_congreso.idUsuario',
			'=',
			$idUsuario
			)      
		->Where(
			'rol.nomRol',
			'=',
			'Chair'
			)
		->WhereIn( 
			'estado_congreso.valorEstado',
			['ACT']
			)
		->get();

		return Response::json(array('error' => False, 'mensaje' => '', 'congresos' => $congresos));
	}

	/**
	 * Muestra todos los congresos donde el usuario es actua como PC (JSON).
	 *
	 * @return JSON
	 */	
	public function misCongresosPC()
	{
		$idUsuario = Auth::check()?Auth::user()->idUsuario:0;

		$congresos=DB::table('detalle_congreso')
		->join(
			'congreso',
			'congreso.idCongreso',
			'=',
			'detalle_congreso.idCongreso'
			)
		->join(
			'usuariorol_x_congreso',
			'usuariorol_x_congreso.idCongreso',
			'=',
			'detalle_congreso.idCongreso'
			)
		->join(
			'rol',
			'rol.idRol',
			'=',
			'usuariorol_x_congreso.idRol'
			)
		->join(
			'estado_congreso',
			'estado_congreso.idEstadoCongreso',
			'=',
			'congreso.idEstadoCongreso'
			)
		->join(
			'estado_urc',
			'estado_urc.idEstadoURC',
			'=',
			'usuariorol_x_congreso.idEstadoURC'
		)
		->Where(
			'usuariorol_x_congreso.idUsuario',
			'=',
			$idUsuario
			)      
		->Where(
			'rol.nomRol',
			'=',
			'PC'
			)
		->Where(
			'estado_urc.valorEstado',
			'=',
			'ACT'
			)
		->whereIn(
			'estado_congreso.valorEstado',
			['ACT']
			)
		->get();

		return Response::json(array('error' => False, 'mensaje' => '', 'congresos' => $congresos));
	}

	/**
	 * Muestra todos los congresos donde el usuario es actua como Revisor (JSON).
	 *
	 * @return JSON
	 */	
	public function misCongresosRevisor()
	{
		$idUsuario = Auth::check()?Auth::user()->idUsuario:0;

		$congresos=DB::table('detalle_congreso')
		->join(
			'congreso',
			'congreso.idCongreso',
			'=',
			'detalle_congreso.idCongreso'
			)
		->join(
			'usuariorol_x_congreso',
			'usuariorol_x_congreso.idCongreso',
			'=',
			'detalle_congreso.idCongreso'
			)
		->join(
			'rol',
			'rol.idRol',
			'=',
			'usuariorol_x_congreso.idRol'
			)
		->join(
			'estado_congreso',
			'estado_congreso.idEstadoCongreso',
			'=',
			'congreso.idEstadoCongreso'
			)
		->join(
			'estado_urc',
			'estado_urc.idEstadoURC',
			'=',
			'usuariorol_x_congreso.idEstadoURC'
		)
		->Where(
			'usuariorol_x_congreso.idUsuario',
			$idUsuario
			)      
		->Where(
			'estado_urc.valorEstado',
			'=',
			'ACT'
			)
		->Where(
			'rol.nomRol',
			'=',
			'Revisor'
			)
		->Where(
			'estado_congreso.valorEstado',
			'=',
			'ACT'
			)
		->get();

		return Response::json(array('error' => False, 'mensaje' => '', 'congresos' => $congresos));
	}

	/**
	 * Muestra todos los congresos donde el usuario aun no se ha inscrito (JSON).
	 *
	 * @return JSON
	 */	
	public function congresosSinInscribir() 
	{
		$idUsuario = Auth::check()?Auth::user()->idUsuario:0;

		$congresos = DB::table('detalle_congreso')
		->join(
			'congreso',
			'congreso.idCongreso',
			'=',
			'detalle_congreso.idCongreso'
		)
		->join(
			'usuariorol_x_congreso',
			'usuariorol_x_congreso.idCongreso',
			'=',
			'detalle_congreso.idCongreso'
		)
		->Where(
			'usuariorol_x_congreso.idUsuario',
			$idUsuario
		)
		->select(
			'detalle_congreso.idCongreso'
		)
		->lists(
			'idCongreso'
		);

		$filtrados = DB::table('detalle_congreso')
		->join(
			'congreso',
			'congreso.idCongreso',
			'=',
			'detalle_congreso.idCongreso'
		)
		->join(
			'estado_congreso',
			'estado_congreso.idEstadoCongreso',
			'=',
			'congreso.idEstadoCongreso'
		)
		->Where(
			'estado_congreso.valorEstado',
			'ACT'
		);

		if(count($congresos) > 0)
			$filtrados->whereNotIn('detalle_congreso.idCongreso',$congresos);
		$filtrados = $filtrados->get();

		return Response::json(array('error' => False, 'mensaje' => '', 'congresos' => $filtrados));
	}

	/**
	 * Muestra todos los congresos donde el usuario actua como autor (JSON).
	 *
	 * @return JSON
	 */
	 public function congresosComoAutor() 
	{
		$idUsuario = Auth::check()?Auth::user()->idUsuario:0;

		$congresos = DB::table('detalle_congreso')
		->join(
			'congreso',
			'congreso.idCongreso',
			'=',
			'detalle_congreso.idCongreso'
		)
		->join(
			'usuariorol_x_congreso',
			'usuariorol_x_congreso.idCongreso',
			'=',
			'detalle_congreso.idCongreso'
		)
		->join(
			'rol',
			'rol.idRol',
			'=',
			'usuariorol_x_congreso.idRol'
			)
		->join(
			'estado_urc',
			'estado_urc.idEstadoURC',
			'=',
			'usuariorol_x_congreso.idEstadoURC'
		)
		->join(
			'estado_congreso',
			'estado_congreso.idEstadoCongreso',
			'=',
			'congreso.idEstadoCongreso'
			)
		->Where(
			'usuariorol_x_congreso.idUsuario',
			$idUsuario
		)
		->Where(
			'rol.nomRol',
			'=',
			'Autor'
		)
		->WhereIn(
			'estado_congreso.valorEstado',
			['ACT','POR','INV','SOL']
		)
		->get();

		return Response::json(array('error' => False, 'mensaje' => '', 'congresos' => $congresos));
	}

}

