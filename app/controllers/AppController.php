<?php

//use BaseController;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface;

class AppController extends BaseController {

    protected $GridEncoder;

    public function __construct(RequestedDataInterface $GridEncoder)
    {
        $this->GridEncoder = $GridEncoder;
    }

    public function getIndex()
    {
        $misUsuarios = Usuario::all();
        //var_dump(expression)($misUsuarios);
        $dataTable = '';
        $i = 0;
        while ($i < count($misUsuarios)) {

            $dataTable3[] =  array(
                                    'id' =>$misUsuarios[$i]->idUsuario,
                                    'invdate'=>'2010-05-24',
                                    'name'=>$misUsuarios[$i]->NombreUsuario,
                                    'note'=>$misUsuarios[$i]->idUsuario,
                                    'tax'=>$misUsuarios[$i]->idUsuario,
                                    'total'=>$misUsuarios[$i]->idUsuario 
                                    );
            $i++;
        }

        $dataTable3 = json_encode($dataTable3);
        //var_dump($dataTable3);

        $this->layout->content = View::make('gridTest',
            array('usuariosData' =>$dataTable3));

    }

    public function postGridData()
    {
        $this->GridEncoder->encodeRequestedData(new ExampleRepository(), Input::all());
    }

    public function getGrid()
    {

        $misUsuarios = Usuario::all();
        //var_dump(expression)($misUsuarios);
        $dataTable = '';
        $i = 0;
        while ($i < count($misUsuarios)) {

            $dataTable3[] =  array(
                                    'id' =>$misUsuarios[$i]->idUsuario,
                                    'invdate'=>'2010-05-24',
                                    'name'=>$misUsuarios[$i]->NombreUsuario,
                                    'note'=>$misUsuarios[$i]->idUsuario,
                                    'tax'=>$misUsuarios[$i]->idUsuario,
                                    'total'=>$misUsuarios[$i]->idUsuario 
                                );
            $i++;
        }

        $dataTable3 = json_encode($dataTable3);
        return $dataTable3;
    }

}