<?php

class EditProfileController extends \BaseController {
	
	public function index()
	{
		if (Auth::guest()) return Redirect::guest('login');
		
		$userAreasAux	= AreaInteres::where('idUsuario','=', Auth::user()->idUsuario)->get(array('idTematica'));
		$i = 0;
		$userIdAreas = array();
		$userIdAreasTEMP = '';
        while ($i < count($userAreasAux)) {
            $userIdAreas[] =  $userAreasAux[$i]->idTematica;
            $userIdAreasTEMP = $userIdAreasTEMP . '-' . $userAreasAux[$i]->idTematica;
            $i++;
        }
		if(count(array_filter($userIdAreas))==0)
		{
			$userAreas		= Tematica::where('idTematica', -1)->get();
			$todasOtrasAreas= Tematica::orderBy('nomTematica')->get();
		}
		else
		{
			$userAreas		= Tematica::whereIn('idTematica', $userIdAreas)->get();
			$todasOtrasAreas= Tematica::whereNotIn('idTematica', $userIdAreas)->orderBy('nomTematica')->get();
		}

		$this->layout->content=View::make('editUsuarioProfile', array('mensajeController' => '', 'areas1' => $userAreas, 'otrasAreas' => $todasOtrasAreas ));
	}

	public function create()
	{
	}
		
	public function store()
	{
	}

	public function show($id)
	{
	}

	public function edit($id)
	{
	}

	public function update($id)
	{
		try {
			$validEmail = Usuario::where("emailUsuario", "=", Input::get('emailUsuario'))
									->where("idUsuario", "<>", $id)
									->count();
									
			if($validEmail > 0)
			{
				return Redirect::back()
					->with('message', 'Email de Usuario ya existe, introduzca otra direcci&oacute;n de correo')
					->withInput();
			}
		
			$user	= Usuario::find($id);
			$inputs	= Input::all();
			
			//**********************
			if(Input::get('nocambiar') == '1')
				$inputs['passwordUsuario']	= $user['passwordUsuario'];
			else
				$inputs['passwordUsuario']	= Hash::make((Input::get('passwordUsuario')));
			//**********************
			$inputs['fecModUsuario'] = date('Y-m-d H:i:s');
			 
			if (!$user->update($inputs)) {
				return Redirect::back()
						->with('message', 'Hubo algunos problemas para actulizar este usuario')
						->withInput();
			}
			
			/*Actualizando areas de investigacion*/
			//Algoritmo para agregar mi SET de AreasXUsuario
			$borrarActuales	= AreaInteres::where('idUsuario','=', Auth::user()->idUsuario)->delete();
			$seleccionAreas	= Input::get('listaMisSelecciones');
			if(trim($seleccionAreas) != '')
			{
				$arrayAreas		= explode(',', $seleccionAreas);
				foreach ($arrayAreas as $valor) {
					$UserXCat = new AreaInteres;
					$UserXCat->idTematica	= $valor;
					$UserXCat->idUsuario	= Auth::user()->idUsuario;
					$UserXCat->save();
				}
			}
			
			return Redirect::route('editprofile.index')->with('message', 'Informaci&oacute;n actualizada con exit&oacute;');
		}catch (Exception $e)
		{
			return Redirect::back()
					->with('message', $e->getMessage())
					->withInput();
		}
	}

	public function destroy($id)
	{
	}
}