<?php

class CongresoAulasController extends BaseController {

	public function getIndex($id)
	{
		$nombreDeCongreso = DB::table('congreso')
					->where('idCongreso',$id)
                    ->first();

        $nombreCongreso = '<< error >>';
        $congresoOwner	= -1;
        $idDetCongreso 	= -1;
        if($nombreDeCongreso != null)
		{
			$nombreCongreso = $nombreDeCongreso->nomCongreso;
			if(Auth::user()->idUsuario == $nombreDeCongreso->idCreador)
				$congresoOwner	= 1;

			$detalleCongreso = DB::table('detalle_congreso')->where('idCongreso','=', $id)->first();
        	$idDetCongreso 	 = $detalleCongreso->idDetalleCongreso;
		}
		//*************************************************************************
		$aulasXCongreso = DB::table('aula')
            ->where('idDetalleCongreso','=',$idDetCongreso)
            ->get();
		
		$this->layout->content = View::make('congresos.ConfigurarAulas',array('idCongreso' => $id,'aulasCongreso' => $aulasXCongreso,'nombreCongreso' => $nombreCongreso , 'congresoOwner' => $congresoOwner));
	}
	
	public function actualizarData()
	{
		try {
    		if(Request::ajax())
			{
				$detalleCongreso = DB::table('detalle_congreso')->where('idCongreso','=', Input::get('idDC'))->first();
	        	$idDetCongreso = $detalleCongreso->idDetalleCongreso;

				$action = Input::get('action');
				//$idDC 	= Input::get('idDC');
				$idDC = $idDetCongreso;
				$error	= false;
				$returnMSG = 'Actualizado';

				switch ($action) {
				    case "ADD":
						$checkExists = DB::table('aula')
            							->where('idDetalleCongreso','=',$idDC)->where('nomAula','=',Input::get('nomAula'))->count();

									
						if($checkExists == 0)
						{
							$objeto = new Aula;
							$objeto->nomAula	= Input::get('nomAula');
							$objeto->tipoAula	= Input::get('tipoAula');
							$objeto->idDetalleCongreso	= $idDC;
							$objeto->save();
							$returnMSG = 'Agregado';
						}else{
							$error	= true;
							$returnMSG = 'Aula ya esta creada para este congreso';
						}
				        break;
				    case "UPDATE":
					    $validRecord = Aula::where("nomAula", "=", Input::get('nomAula'))
										->where("idAula", "<>", Input::get('currentId'))
										->where('idDetalleCongreso',$idDC)
										->count();
						if($validRecord > 0){
							$error	= true;
							$returnMSG = 'Aula ya esta creada, no se puede modificar';
						}else{
							$objeto = Aula::find(Input::get('currentId'));
							$currentLabel = $objeto->nomAula;
							$objeto->nomAula	= Input::get('nomAula');
							$objeto->tipoAula	= Input::get('tipoAula');
							$objeto->save();
							DB::table('agenda')->where('aulaLabel',$currentLabel)->update(array('aulaLabel' => $objeto->nomAula));
						}
				        break;
				    case "DELETE":
				     	$validDelete = DB::table('agenda')
							            ->where('idCongreso',Input::get('idDC'))->where('idAula',Input::get('currentId'))
							            ->count();

						if($validDelete == 0){
				        	Aula::destroy(Input::get('currentId'));
				        	$returnMSG = 'Eliminado';
				        }else{
							$error	= true;
							$returnMSG = 'Aula en USO, no se puede eliminar';
				        }
				        break;
				}
				if($error)
					return Response::json(array('error' => True, 'mensaje' => $returnMSG ));
				else
					return Response::json(array('error' => False, 'mensaje' => $returnMSG . ' con &eacute;xito, refrescando...'));
			}
		}catch(Exception $e)
		{
			return Response::json(array('error' => True, 'mensaje' => 'Problemas para actualizar: '  . $e->getMessage()));
		}

	}
}