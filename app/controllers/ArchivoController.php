<?php

class ArchivoController extends \BaseController {

	/*
	 * Funcion que valida si el usuario actual tiene permisos de ver un archivo.
	 *
	 * @return File
	*/ 
	public function servirArchivo($idCongreso,$nombreArchivo)
	{
		$file = base_path() . "/archivos/" . $nombreArchivo;
		$file = str_replace("..", "", $file);
		if (!file_exists($file)) App::abort(404);
		return Response::download($file, pathinfo($file, PATHINFO_BASENAME));
	}

	public function previsualizarArchivo($idCongreso,$nombreArchivo)
	{
		$file = base_path() . "/archivos/" . $nombreArchivo;
		$file = str_replace("..", "", $file);
		if (!file_exists($file)) App::abort(404);
		$content = file_get_contents($file);
	    return Response::make($content, 200, array('content-type'=>'application/pdf'));
	}
}