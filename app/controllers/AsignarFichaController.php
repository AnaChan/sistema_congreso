<?php 


class AsignarFichaController extends BaseController {

	/**
	 * Cargar vista de asignar Fichas.
	 *
	 * @return View('SolicitarCongreso')
	 */
	public function index($id){
		if(Auth::check())
		{
			if(!$this->verificarFechaRecurso($id,'Confirmación de aceptación de ponencias')) //quiere decir que la fecha esta fuera de rango
				return Redirect::action('UsuariosCongresosController@establecerCongreso',array($id));
			$PC = DB::table('usuariorol_x_congreso')
			->join(
				'rol',
				'rol.idRol',
				'=',
				'usuariorol_x_congreso.idRol'
			)
			->join(
				'pc_x_tematica',
				'pc_x_tematica.idUsuariorolXCongreso',
				'=',
				'usuariorol_x_congreso.idUsuariorolXCongreso'
			)
			->where(
				"idUsuario", 
				"=",
				Auth::user()->idUsuario
			)
			->where('usuariorol_x_congreso.idCongreso', '=',$id)
			->where(function($query)
			{
				$query
				->Where('rol.nomRol','PC')
				->orWhere('rol.nomRol','Chair');
			})
			->first();

			$detalleCongreso = DetalleCongreso::where("idCongreso","=",$id)->first();
			//se va a validar si el usuario es un pc
			//Se filtraran sus revisores gracias al PC
			//$revisores = PCXRevisorArea::where("idPcrevisor","=",$PC->idPcTematica)->get();
			

			if(count($PC))
			{
				$estado = EstadoURC::where('nombreEstado','=','ACTIVO')->first();
			
				//$revisores = DB::table('pc_x_revisor_area')
				//->join('usuarios','usuarios.idUsuario','=','pc_x_revisor_area.idUsuario')
				//->join('usuariorol_x_congreso','usuarios.idUsuario','=','usuariorol_x_congreso.idUsuario')
				//->where('idPcRevisor','=',$PC->idPcTematica)
				//->where(
				//	'usuariorol_x_congreso.idEstadoURC',
			//		'=',
			//		$estado->idEstadoURC
				//)
				//->get();


				$revisores = DB::table('pc_x_revisor_area')
					->join('usuarios','usuarios.idUsuario','=','pc_x_revisor_area.idUsuario')
					->join('pc_x_tematica','pc_x_tematica.idPcTematica','=','pc_x_revisor_area.idPcRevisor')
					->join('usuariorol_x_congreso','usuariorol_x_congreso.idUsuariorolXCongreso','=','pc_x_tematica.idUsuariorolXCongreso')
					->where('usuariorol_x_congreso.idCongreso','=',$id)
					->where('pc_x_revisor_area.idPcRevisor','=',$PC->idPcTematica)
					->where(
					'usuariorol_x_congreso.idEstadoURC',
					'=',
					$estado->idEstadoURC
					)
					->get(array('pc_x_revisor_area.idUsuario','usuarios.nombreUsuario','usuarios.apelUsuario'));

				$fichas = DB::table('ficha')->where("idTematica", "=", $PC->idTematica)
										->where("idDetalleCongreso", "=",$detalleCongreso->idDetalleCongreso)
										->join(DB::raw('(select COALESCE(count(rf.idFicha),0) cantRev, f.idFicha
															from revision_ficha rf 
															right join ficha f on f.idFicha=rf.idFicha
															group by f.idFicha) Contadores'), function($join)
				        																		{
				            																		$join->on('ficha.idFicha', '=', 'Contadores.idFicha');
				        																		})->get();
				$tematica = DB::table('tematica')->where('idTematica', "=", $PC->idTematica)->first();

				
			}else
			{
				$revisores = [];
				$fichas = [];
				$tematica = [];
			}
			$info['tematica'] = $tematica;
			$info['fichas'] = $fichas;
			$info['revisores'] = $revisores;
			$info['pc'] = $PC;
			$this->layout->content = View::make('sumision/AsignarFicha')->with('info',$info)
																		->with('idCongreso',$id);
		
		}

	}


	/**
	 * Cargar vista de asignar articulos a los revisores.
	 *
	 * @return View('SolicitarCongreso')
	 */
	public function indexPaper($id)
	{
		if(Auth::check())
		{
			if(!$this->verificarFechaRecurso($id,'Confirmación de aceptación de Artículos')) //quiere decir que la fecha esta fuera de rango
				return Redirect::action('UsuariosCongresosController@establecerCongreso',array($id));
			$PC = DB::table('usuariorol_x_congreso')
			->join(
				'rol',
				'rol.idRol',
				'=',
				'usuariorol_x_congreso.idRol'
			)
			->join(
				'pc_x_tematica',
				'pc_x_tematica.idUsuariorolXCongreso',
				'=',
				'usuariorol_x_congreso.idUsuariorolXCongreso'
			)
			->where('usuariorol_x_congreso.idCongreso', '=',$id)
			->where(
				"idUsuario", 
				"=",
				Auth::user()->idUsuario
			)
			->where(function($query)
			{
				$query
				->Where('rol.nomRol','PC')
				->orWhere('rol.nomRol','Chair');
			})
			->first();


			$detalleCongreso = DetalleCongreso::where("idCongreso","=",$id)->first();
			//se va a validar si el usuario es un pc
			if(count($PC))
			{
				//Se filtraran sus revisores gracias al PC
				//$revisores = PCXRevisorArea::where("idPcrevisor","=",$PC->idPcTematica)->get();
				$estado = EstadoURC::where('nombreEstado','=','ACTIVO')->first();
				$revisores = DB::table('pc_x_revisor_area')
					->join('usuarios','usuarios.idUsuario','=','pc_x_revisor_area.idUsuario')
					->join('pc_x_tematica','pc_x_tematica.idPcTematica','=','pc_x_revisor_area.idPcRevisor')
					->join('usuariorol_x_congreso','usuariorol_x_congreso.idUsuariorolXCongreso','=','pc_x_tematica.idUsuariorolXCongreso')
					->where('usuariorol_x_congreso.idCongreso','=',$id)
					->where('pc_x_revisor_area.idPcRevisor','=',$PC->idPcTematica)
					->where(
					'usuariorol_x_congreso.idEstadoURC',
					'=',
					$estado->idEstadoURC
					)
					->get(array('pc_x_revisor_area.idUsuario','usuarios.nombreUsuario','usuarios.apelUsuario'));


				//Se sacan las fichas filtradas por las tematicas
				$fichas = DB::table('ficha')->join("paper", 'paper.idFicha', '=','ficha.idFicha')
											->where("idTematica", "=", $PC->idTematica)
											->where("idDetalleCongreso", "=",$detalleCongreso->idDetalleCongreso)
											->join(DB::raw('(select COALESCE(count(rf.idPaper),0) cantRev, f.idPaper
																from revision_paper rf 
																right join paper f on f.idPaper=rf.idPaper
																group by f.idPaper) Contadores'), function($join)
					        																		{
					            																		$join->on('paper.idPaper', '=', 'Contadores.idPaper');
					        																		})
											->get();
				//$tematica = $PC->idPcTematica;
				$tematica = DB::table('tematica')->where('idTematica', "=", $PC->idTematica)->first();
				//$tematica = Tematica::where("idTematica","=",$PC->idTematica);
				
			}
			else
			{
				$revisores = [];
				$fichas = [];
				$tematica = [];		
			}
				$info['tematica'] = $tematica;
				$info['fichas'] = $fichas;
				$info['revisores'] = $revisores;
				$info['pc'] = $PC;
				
				$this->layout->content = View::make('sumision/AsignarPaper')->with('info',$info)
																			->with('idCongreso',$id);
		}
	}


	
	/**
	*Guarda la información de quienes seran los encargados de revisar las fichas
	*/
	public function guardarRevisores()
	{
		if(Request::ajax())
		{
			return DB::transaction(function()
			{
				DB::beginTransaction();
				try {
					$idFicha = Input::get('fichaEditar');
					$arregloRevisores = json_decode(Input::get('arregloIDRevisores'));

					for($j=0; $j<count($arregloRevisores);$j++)
					{
						$fichaRevisor = RevisionFicha::create(
							array(
								'idFicha'	=>  $idFicha,
								'idUsuario'	=> $arregloRevisores[$j],
								'comentarioFicha'	=> ''
							)
						);
						if(!$fichaRevisor) throw new Exception("Al asignar revisores");
					}
					DB::commit();
					return Response::json(array('error' => False, 'mensaje' => 'Se han asignado a los revisores con éxito.'));
				}
				catch (Exception $e)
				{
					DB::rollback();
					return Response::json(array('error' => True, 'mensaje' => "ERROR: ".$e->getMessage()));
				}		
			});
		}
	}

	public function guardarRevisoresPaper()
	{
		if(Request::ajax())
		{
			return DB::transaction(function()
			{
				DB::beginTransaction();
				try {
					$idPaper = Input::get('fichaEditar');
					$arregloRevisores = json_decode(Input::get('arregloIDRevisores'));

					for($j=0; $j<count($arregloRevisores);$j++)
					{

						$fichaRevisor = RevisionPaper::create(
							array(
								'idPaper' =>  $idPaper,
								'idUsuario'	=> $arregloRevisores[$j],
								'comentario' => ''
							)
						);
						if(!$fichaRevisor) throw new Exception("Al asignar revisores");
					}
					DB::commit();
					return Response::json(array('error' => False, 'mensaje' => 'Se han asignado a los revisores con éxito.'));
				}
				catch (Exception $e)
				{
					DB::rollback();	
					return Response::json(array('error' => True, 'mensaje' => "ERROR: ".$e->getMessage()));
				}		
			});
		}
	}

/**
 * Elimina una asignacion de un revisor a la ficha dada
 *
 * @return View('SolicitarCongreso')
 */
public function eliminarRevisorFicha()
	{
		if(Request::ajax())
		{
			return DB::transaction(function()
			{
				DB::beginTransaction();
				try {
					$idFicha = Input::get('fichaEditar');
					$idUsuario = Input::get('idUsuario');

					$row=RevisionFicha::WhereRaw('idUsuario=? and idFicha=?',array($idUsuario,$idFicha))->delete();
					if(count($row) ==0)throw new Exception("Al eliminar un revisor de la ficha");
					DB::commit();
					return Response::json(array('error' => False, 'mensaje' => 'Revisor eliminado con éxito.'));
				}
				catch (Exception $e)
				{
					DB::rollback();
					return Response::json(array('error' => True, 'mensaje' => "ERROR: ".$e->getMessage()));
				}		
			});
		}
	}

	public function eliminarRevisorPaper()
	{
		if(Request::ajax())
		{
			return DB::transaction(function()
			{
				DB::beginTransaction();
				try {
					$idPaper = Input::get('fichaEditar');
					$idUsuario = Input::get('idUsuario');

					$row=RevisionPaper::WhereRaw('idUsuario=? and idPaper=?',array($idUsuario,$idPaper))->delete();
					if(count($row) ==0)throw new Exception("Al eliminar un revisor de el articulo");
					DB::commit();
					return Response::json(array('error' => False, 'mensaje' => 'Revisor eliminado con éxito.'));
				}
				catch (Exception $e)
				{
					DB::rollback();
					return Response::json(array('error' => True, 'mensaje' => "ERROR: ".$e->getMessage()));
				}		
			});
		}
	}

	/**
	 * Muestra los revisores de una ficha especifica
	 * @return View::HTML
	 */	
	public function revisoresFicha()
	{
		try{
			$idFicha=Input::get('fichaEditar');
			$revisores = DB::table('revision_ficha')
								->Where(
									'revision_ficha.idFicha',
									'=',
									$idFicha
								)
								->lists('idUsuario');

			return Response::json(array('error' => False, 'mensaje' => 'Exito' ,'agregados'=>$revisores));
		}
		catch(Exception $e)
		{
			return Response::json(array('error' => True, 'mensaje' => 'ERROR: al extraer los revisores asignados a esta ficha '.$e->getMessage()));
		}	
	}

	public function revisoresPaper()
	{
		try{
			$idPaper=Input::get('fichaEditar');
			$revisores = DB::table('revision_paper')
								->Where(
									'revision_paper.idPaper',
									'=',
									$idPaper
								)
								->lists('idUsuario');

			return Response::json(array('error' => False, 'mensaje' => 'Exito' ,'agregados'=>$revisores));
		}
		catch(Exception $e)
		{
			return Response::json(array('error' => True, 'mensaje' => 'ERROR: al extraer los revisores asignados a esta ficha '.$e->getMessage()));
		}	
	}
}
