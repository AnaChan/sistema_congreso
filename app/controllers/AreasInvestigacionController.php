<?php 

class AreasInvestigacionController extends BaseController {
	
	public function index()
	{
		$areas = AreaInvestigacion::orderBy('nomAreaInvestigacion')->get();
		$this->layout->content = View::make('adminAreasInvestigacion')->with('areas',$areas);
	}

	public function create()
	{
		try {
			$validRecord = AreaInvestigacion::where("nomAreaInvestigacion", "=", Input::get('nomArea'))
							->count();
			if($validRecord > 0)
			{
				return Response::json(array('error' => True, 'mensaje' => 'Area de Investigaci&oacute;n ya existente, imposible agregar'));
			}					
			$miModelo = new AreaInvestigacion;
			$miModelo->nomAreaInvestigacion = Input::get('nomArea');
			$miModelo->save();
			return Response::json(array('error' => False, 'mensaje' => 'Guardando... espere un momento mientras se refresca la pantalla'));
		} catch (Exception $e) {return Response::json(array('error' => True, 'mensaje' => 'Problemas para modificar area'));}
	}

	public function edit($id)
	{
		if(Request::ajax())
		{
			if(Input::get('accion') == 'edit')
			{
				try {

					$validRecord = AreaInvestigacion::where("nomAreaInvestigacion", "=", Input::get('nomArea'))
									->where("idAreaInvestigacion", "<>", $id)
									->count();
					if($validRecord > 0)
					{
						return Response::json(array('error' => True, 'mensaje' => 'Area de Investigaci&oacute;n ya existente, no se puede modificar'));
					}

					$miModelo	= AreaInvestigacion::find($id);
					$miModelo->nomAreaInvestigacion = Input::get('nomArea' );
					$miModelo->fecModAreaInvestigacion = date('Y-m-d H:i:s');
					if (!$miModelo->save()) {
						return Response::json(array('error' => True, 'mensaje' => 'Problemas para modificar area'));
					}
					return Response::json(array('error' => False, 'mensaje' => 'Actualizando... espere un momento mientras se refresca la pantalla'));
				} catch (Exception $e) {
					return Response::json(array('error' => True, 'mensaje' => 'Problemas para modificar area'));
				}
			}
			
			if(Input::get('accion') == 'delete')
			{
				try {
					$currentId = $id;
					AreaInvestigacion::destroy($currentId);
					return Response::json(array('error' => False, 'mensaje' => 'Area Investigaci&oacute;n eliminada, espere un momento mientras se refresca la pantalla'));
				} catch (Exception $e) {
					return Response::json(array('error' => True, 'mensaje' => 'ALTO! &Aacute;rea en uso, no se puede eliminar'));
				}
			}
			
		}
	}

}