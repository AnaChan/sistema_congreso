<?php

class ConfigurarEvaluacionController extends \BaseController {


	/**
	 *
	 * Variable que indica si se quieren los criterios de la ficha
	 */
	protected $CRITERIO_FICHA = 'F';

	/**
	 *
	 * Variable que indica si se quieren los criterios del paper
	 */
	protected $CRITERIO_PAPER = 'P';

	/*
	 * Retorna la vista de la configuracion de la matriz de evaluacion.
	 *
	 * @return HTML
	*/
	public function configurarEvaluacion($idCongreso)
	{
		$criteriosFicha = $this->cargarCriterios($idCongreso,$this->CRITERIO_FICHA);
		$criteriosPaper = $this->cargarCriterios($idCongreso,$this->CRITERIO_PAPER);
		$this->layout->content = View::make("congresos.configurarEvaluacion");
		$this->layout->content->criteriosFicha = $criteriosFicha;
		$this->layout->content->criteriosPaper = $criteriosPaper;
		$this->layout->content->idCongreso = $idCongreso;
	}


	/*
	 * Funcion para almacenarla informacón de la matriz de evaluación
	 *
	*/
	public function guardarConfigurarEval($idCongreso)
	{
		if(Request::ajax())
		{
			$rules = array(
				'criterios' =>	'required|array' 
			);

			$validador = Validator::make(Input::all(), $rules);

			if ($validador->fails()) {
				$errores = $validador->messages();
				if($errores->has('criterios'))
					return Response::json(array('error' => True, 'mensaje' => 'No se puede guardar un matriz que no contenga elementos.'));
				return Response::json(array('error' => True, 'mensaje' => $errores->first()));
			}
			else
			{
				$idDetalleCongreso = DB::table('detalle_congreso')
				->join(
					'congreso',
					'congreso.idCongreso',
					'=',
					'detalle_congreso.idCongreso'
				)
				->where(
					'congreso.idCongreso',
					'=',
					$idCongreso
				)
				->first()->idDetalleCongreso;
				$criterios = Input::get('criterios');

				try {
					DB::beginTransaction();
					$criteriosAgregados = array();
					$criteriosPadres = array();
					$tipo = '';
	 				foreach ($criterios as $criterio)
					{
						$tipo = $criterio['indicadorFicha'];
						$criterio['idDetalleCongreso'] = $idDetalleCongreso;
						$puntajes = $criterio['puntajes'];
						$idTemp = isset($criterio['idTemp']) ? $criterio['idTemp'] : null;
						$idTempPadre = isset($criterio['idTempPadre']) ? $criterio['idTempPadre'] : null;
						unset($criterio['puntajes']);
						unset($criterio['idTemp']);
						unset($criterio['idTempPadre']);

						if(array_key_exists('idCriterioEvaluacion', $criterio))
						{
							CriterioEvaluacion::findOrFail($criterio['idCriterioEvaluacion']);
						}
						else
						{
							$criterio['idCriterioEvaluacion'] = DB::table('criterio_evaluacion')
							->insertGetId($criterio);
							$criterio['idCriterioEvaluacionPadre'] = $criterio['idCriterioEvaluacion'];

							if(isset($idTemp)) $criteriosPadres[$idTemp] = $criterio['idCriterioEvaluacionPadre'];
							else $criterio['idCriterioEvaluacionPadre'] = $criteriosPadres[$idTempPadre];
						}
						DB::table('criterio_evaluacion')
						->where('idCriterioEvaluacion', $criterio['idCriterioEvaluacion'])
						->update($criterio);

						$puntajesAgregados = array();
						foreach ($puntajes as $puntaje)
						{
							unset($puntaje['idPuntaje']);
	
							$puntajeDB = DB::table('puntaje')
							->where(
								'valorPuntaje',
								'=',
								$puntaje['valorPuntaje']
							)
							->where(
								'idCriterioEvaluacion',
								'=',
								$criterio['idCriterioEvaluacion']
							)
							->first();

							if(!is_null($puntajeDB))
							{
								$puntaje['idPuntaje'] = $puntajeDB->idPuntaje;
								DB::table('puntaje')
								->where('idPuntaje', $puntaje['idPuntaje'])
								->update($puntaje);
							}
							else
							{
								$puntaje['idCriterioEvaluacion'] = $criterio['idCriterioEvaluacion'];
								$puntaje['idPuntaje'] = DB::table('puntaje')->insertGetId($puntaje);
							}
							$puntajesAgregados[] = $puntaje['idPuntaje'];
						}
						if (count($puntajesAgregados) > 0)
							DB::table('puntaje')
							->whereNotIn('idPuntaje', $puntajesAgregados)
							->where('idCriterioEvaluacion','=',$criterio['idCriterioEvaluacion'])
							->delete();

						$criteriosAgregados[] = $criterio['idCriterioEvaluacion'];
					}

					if (count($criteriosAgregados) > 0)
						DB::table('criterio_evaluacion')
						->whereNotIn('idCriterioEvaluacion', $criteriosAgregados)
						->where('idDetalleCongreso','=',$idDetalleCongreso)
						->where('indicadorFicha','=',$tipo)
						->delete();
					DB::commit();
					return Response::json(array('error' => False, 'mensaje' => 'Se guardo la matriz exitosamente'));
				} catch (Exception $e) {
					DB::rollback();	
					return Response::json(array('error' => True, 'mensaje' => 'Hubo un error al crear la matriz. ' . $e->getMessage()));
				}
			}
		}

	}


	/**
	 *
	 * Funcion qeu retorna todos los criterios 
	 *
	 * @param $idCongreso integer id del congreso a cargar los criterios
	 *
	 * @param $indicadorFicha integer indica el tipo de criterio a cargar
	 */
	public function cargarCriterios($idCongreso,$indicadorFicha)
	{
		$criterios = DB::table('criterio_evaluacion')
		->join(
			'detalle_congreso',
			'detalle_congreso.idDetalleCongreso',
			'=',
			'criterio_evaluacion.idDetalleCongreso'
			)
		->join(
			'congreso',
			'congreso.idCongreso',
			'=',
			'detalle_congreso.idCongreso'
			)
		->where(
			'congreso.idCongreso',
			'=',
			$idCongreso
			)
		->where(
			'indicadorFicha',
			'=',
			$indicadorFicha
			)
		->whereRaw(
			'criterio_evaluacion.idCriterioEvaluacion = criterio_evaluacion.idCriterioEvaluacionPadre'
			)
		->get();

		foreach ($criterios as $criterio)
		{
			$criterio->hijos = DB::table('criterio_evaluacion')
			->where(
				'idCriterioEvaluacionPadre',
				'=',
				$criterio->idCriterioEvaluacion
				)
			->whereRaw(
				'criterio_evaluacion.idCriterioEvaluacion != criterio_evaluacion.idCriterioEvaluacionPadre'
				)->get();

			$criterio->cantidadHijos = count($criterio->hijos);

			foreach($criterio->hijos as $hijo)
			{
				$hijo->puntajes = DB::table('puntaje')
				->where(
					'idCriterioEvaluacion',
					'=',
					$hijo->idCriterioEvaluacion
					)
				->select('nomPuntaje', 'valorPuntaje', 'idPuntaje')
				->orderBy('valorPuntaje','asc')
				->get();
				$hijo->puntajesMax = count($hijo->puntajes);
			}

			$criterio->puntajes = DB::table('puntaje')
			->where(
				'idCriterioEvaluacion',
				'=',
				$criterio->idCriterioEvaluacion
				)
			->select('nomPuntaje', 'valorPuntaje', 'idPuntaje')
			->orderBy('valorPuntaje','asc')
			->get();

			$criterio->puntajesMax = count($criterio->puntajes);
		}

		return $criterios;
	}
}