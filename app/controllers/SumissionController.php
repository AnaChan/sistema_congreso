<?php 

class SumissionController extends BaseController {

	/**
	 * Cargar vista principal del login.
	 *
	 * @return View('login')
	 */
	public function index($id)
	{
		if(Auth::check())
		{
			if(!$this->verificarFechaRecurso($id,'Llamado a ponencias')) //quiere decir que la fecha esta fuera de rango
				return Redirect::action('UsuariosCongresosController@establecerCongreso',array($id));



			$detalleCongreso = DetalleCongreso::where('idCongreso','=',$id)->firstOrFail();
		
			$categorias = DB::table('detallecongreso_x_categoria')
			->join('detalle_congreso','detalle_congreso.idDetalleCongreso','=','detallecongreso_x_categoria.idDetalleCongreso')
			->join('categoria','categoria.idCategoria','=','detallecongreso_x_categoria.idCategoria')
			->Where('detallecongreso_x_categoria.idDetalleCongreso','=',  $detalleCongreso->idDetalleCongreso)      
			->get();

			$tematicas = DB::table('congreso_x_tematica')
			->join('congreso','congreso.idCongreso','=','congreso_x_tematica.idCongreso')
			->join('tematica','tematica.idTematica','=','congreso_x_tematica.idTematica')
			->Where('congreso_x_tematica.idCongreso','=',  $id)      
			->get();

			$usuarios = DB::table('usuariorol_x_congreso')
			->join('usuarios','usuariorol_x_congreso.idUsuario','=','usuarios.idUsuario')
			->join('congreso','usuariorol_x_congreso.idCongreso','=','congreso.idCongreso')
			->join('rol','usuariorol_x_congreso.idRol','=','rol.idRol')
			->Where('usuariorol_x_congreso.idCongreso','=',  $id)      
			->WhereIn('nomRol',['Autor','Revisor','Chair','PC'])
			->get();

			$info['idcongreso'] = $id;
			$info['detalle'] = $detalleCongreso->idDetalleCongreso;
			$info['longitudMaxima'] = $detalleCongreso->longitudMaxResumenPaper;

			$info['categorias'] = $categorias;
			$info['tematicas'] = $tematicas;
			$info['usuarios'] = $usuarios;
			$info['ficha'] = [];

			//<antonio>
			$idUsuarioLogueado = Auth::user()->idUsuario;
			$presentaciones = DB::select( DB::raw("select distinct a.titulo,a.fecha,a.horaInicio,a.horaFin, a.AulaLabel, a.idAula, a.idtematica,b.idPresentacion,b.rutaPresentacion,c.idUsuario,c.responsable,c.rutaDiploma from agenda a inner join presentacion b on a.idAgenda = b.idAgenda inner join autor_x_ficha c on b.idFicha = c.idFicha where a.idCongreso = '$id' and a.esActividad = 0 and c.idUsuario = '$idUsuarioLogueado' "));
			
			$mesesFec  = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
			$diasFec   = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");

			foreach($presentaciones as $objeto)
			{
				$fechaPresentacion  = $diasFec[intval(date('w',strtotime($objeto->fecha)))] . ' ' . date('d',strtotime($objeto->fecha)) . ' de ' ;
                $fechaPresentacion  = $fechaPresentacion . $mesesFec[intval(date('m',strtotime($objeto->fecha)))-1] . ' del ' . date('Y',strtotime($objeto->fecha)) ;
                $horaInicio = date("h:i",strtotime($objeto->horaInicio)) . ' ' . date("A",strtotime($objeto->horaInicio));
                $horaFin    = date("h:i",strtotime($objeto->horaFin)) . ' ' . date("A",strtotime($objeto->horaFin));

				$objeto->fecha = $fechaPresentacion;
				$objeto->horaInicio = $horaInicio;
				$objeto->horaFin  = $horaFin;
			}

			$requierePresentacion = false;
			
			if(count($presentaciones) > 0)
				$requierePresentacion = true;

			$subidaPresentacion = $this->generarAyuda($id);
			
			//Subida de fuentes
			$estadoPaper	= EstadoPaper::where('nombreEstado','=','APROBADA')->first();
			$estaodID		= $estadoPaper->idEstadoPaper;
			$congresoIdDetalle = $detalleCongreso->idDetalleCongreso;
			$papersFuentes = DB::select( DB::raw("SELECT b.tituloPaper,b.idFicha,a.idPaper,a.rutaFuentes,c.idUsuario from paper a inner join ficha b on a.idFicha = b.idFicha inner join autor_x_ficha c on b.idFicha = c.idFicha where b.idDetalleCongreso = '$congresoIdDetalle' and c.idUsuario = '$idUsuarioLogueado' and a.idEstadoPaper = '$estaodID' "));

			$habilitarFuentes = false;
			if(count($papersFuentes) > 0)
			{
				$habilitarFuentes = true;
			}

			$this->layout->content = View::make('sumision/SubidaFicha')->with('info' , $info)->with('idCongreso', $id)->with('requierePresentacion', $requierePresentacion)->with('presentaciones', $presentaciones)->with('subidaPresentacion', $subidaPresentacion)->with('habilitarFuentes',$habilitarFuentes)->with('fuentes',$papersFuentes);
			//</antonio>
		}		
	}


	public function indexEditar($id,$idFicha)
	{
		if(Auth::check())
		{
			if(!$this->verificarFechaRecurso($id,'Llamado a ponencias')) //quiere decir que la fecha esta fuera de rango
				return Redirect::action('UsuariosCongresosController@establecerCongreso',array($id));

			$detalleCongreso = DetalleCongreso::where('idCongreso','=',$id)->firstOrFail();
			$responsable = "";
			$categorias = DB::table('detallecongreso_x_categoria')
			->join('detalle_congreso','detalle_congreso.idDetalleCongreso','=','detallecongreso_x_categoria.idDetalleCongreso')
			->join('categoria','categoria.idCategoria','=','detallecongreso_x_categoria.idCategoria')
			->Where('detallecongreso_x_categoria.idDetalleCongreso','=',  $detalleCongreso->idDetalleCongreso)      
			->get();

			$tematicas = DB::table('congreso_x_tematica')
			->join('congreso','congreso.idCongreso','=','congreso_x_tematica.idCongreso')
			->join('tematica','tematica.idTematica','=','congreso_x_tematica.idTematica')
			->Where('congreso_x_tematica.idCongreso','=',  $id)      
			->get();


			$usuariosAutor = DB::table('autor_x_ficha')
							->where('autor_x_ficha.idFicha','=',$idFicha)
							->lists('autor_x_ficha.idUsuario');
			$usuarios = DB::table('usuariorol_x_congreso')
			->join('usuarios','usuariorol_x_congreso.idUsuario','=','usuarios.idUsuario')
			->join('congreso','usuariorol_x_congreso.idCongreso','=','congreso.idCongreso')
			//->join('detalle_congreso','detalle_congreso.idCongreso','=','congreso.idCongreso')
			->join('rol','usuariorol_x_congreso.idRol','=','rol.idRol')
			//->join('ficha','ficha.idDetalleCongreso','=','detalle_congreso.idDetalleCongreso')
			->Where('usuariorol_x_congreso.idCongreso','=',  $id)
			//->Where('ficha.idFicha','=',$idFicha)      
			->WhereIn('nomRol',['Autor','Revisor','Chair','PC'])
			->whereNotIn('usuarios.idUsuario',$usuariosAutor)
			->get();
			
			$usuariosSel = [];
			$ficha = Ficha::where('idFicha', '=' , $idFicha)->first();
			$autores = DB::table('autor_x_ficha')->where("idFicha", "=", $idFicha)->get();
			foreach ($autores as $autor) {
				$usuario =  DB::table('usuarios')->where("idUsuario", "=",$autor->idUsuario)
															->first();
				array_push($usuariosSel, $usuario);
				if($autor->responsable == 1){
					$responsable = $usuario;
				};
			};

			$subidaPresentacion = $this->generarAyuda($id);
			$info['idcongreso'] = $id;
			$info['detalle'] = $detalleCongreso->idDetalleCongreso;
			$info['longitudMaxima'] = $detalleCongreso->longitudMaxResumenPaper;
			$info['responsable'] = $responsable;
			$info['usuarios'] = $usuarios;
			$info['usuariosSel'] = $usuariosSel;
			$info['categorias'] = $categorias;
			$info['tematicas'] = $tematicas;
			$info['usuarios'] = $usuarios;
			$info['ficha'] = $ficha;
			$info['idficha'] = $ficha->idFicha;

			$this->layout->content = View::make('sumision/EditarFicha')->with('info' , $info)->with('subidaPresentacion', $subidaPresentacion)->with('idCongreso', $id);
			
		}		
	}

public function indexEditarAdmin($id,$idFicha)
	{
		if(Auth::check())
		{
			if(!$this->verificarFechaRecurso($id,'Llamado a ponencias')) //quiere decir que la fecha esta fuera de rango
				return Redirect::action('UsuariosCongresosController@establecerCongreso',array($id));

			$detalleCongreso = DetalleCongreso::where('idCongreso','=',$id)->firstOrFail();
			$responsable = "";
			$categorias = DB::table('detallecongreso_x_categoria')
			->join('detalle_congreso','detalle_congreso.idDetalleCongreso','=','detallecongreso_x_categoria.idDetalleCongreso')
			->join('categoria','categoria.idCategoria','=','detallecongreso_x_categoria.idCategoria')
			->Where('detallecongreso_x_categoria.idDetalleCongreso','=',  $detalleCongreso->idDetalleCongreso)      
			->get();

			$tematicas = DB::table('congreso_x_tematica')
			->join('congreso','congreso.idCongreso','=','congreso_x_tematica.idCongreso')
			->join('tematica','tematica.idTematica','=','congreso_x_tematica.idTematica')
			->Where('congreso_x_tematica.idCongreso','=',  $id)      
			->get();


			$usuariosAutor = DB::table('autor_x_ficha')
							->where('autor_x_ficha.idFicha','=',$idFicha)
							->lists('autor_x_ficha.idUsuario');
			$usuarios = DB::table('usuariorol_x_congreso')
			->join('usuarios','usuariorol_x_congreso.idUsuario','=','usuarios.idUsuario')
			->join('congreso','usuariorol_x_congreso.idCongreso','=','congreso.idCongreso')
			//->join('detalle_congreso','detalle_congreso.idCongreso','=','congreso.idCongreso')
			->join('rol','usuariorol_x_congreso.idRol','=','rol.idRol')
			//->join('ficha','ficha.idDetalleCongreso','=','detalle_congreso.idDetalleCongreso')
			->Where('usuariorol_x_congreso.idCongreso','=',  $id)
			//->Where('ficha.idFicha','=',$idFicha)      
			->WhereIn('nomRol',['Autor','Revisor','Chair','PC'])
			->whereNotIn('usuarios.idUsuario',$usuariosAutor)
			->get();
			
			$usuariosSel = [];
			$ficha = Ficha::where('idFicha', '=' , $idFicha)->first();
			$autores = DB::table('autor_x_ficha')->where("idFicha", "=", $idFicha)->get();
			foreach ($autores as $autor) {
				$usuario =  DB::table('usuarios')->where("idUsuario", "=",$autor->idUsuario)
															->first();
				array_push($usuariosSel, $usuario);
				if($autor->responsable == 1){
					$responsable = $usuario;
				};
			};
			$subidaPresentacion = $this->generarAyuda($id);

			$info['idcongreso'] = $id;
			$info['detalle'] = $detalleCongreso->idDetalleCongreso;
			$info['longitudMaxima'] = $detalleCongreso->longitudMaxResumenPaper;
			$info['responsable'] = $responsable;
			$info['usuarios'] = $usuarios;
			$info['usuariosSel'] = $usuariosSel;
			$info['categorias'] = $categorias;
			$info['tematicas'] = $tematicas;
			$info['usuarios'] = $usuarios;
			$info['ficha'] = $ficha;
			$info['idficha'] = $ficha->idFicha;

			$this->layout->content = View::make('sumision/EditarAdmin')->with('subidaPresentacion', $subidaPresentacion)->with('info' , $info)->with('idCongreso', $id);
			
		}		
	}


public function indexEditarPaper($id,$idFicha)
	{
		if(Auth::check())
		{

			$detalleCongreso = DetalleCongreso::where('idCongreso','=',$id)->firstOrFail();
			$responsable = "";
			$categorias = DB::table('detallecongreso_x_categoria')
			->join('detalle_congreso','detalle_congreso.idDetalleCongreso','=','detallecongreso_x_categoria.idDetalleCongreso')
			->join('categoria','categoria.idCategoria','=','detallecongreso_x_categoria.idCategoria')
			->Where('detallecongreso_x_categoria.idDetalleCongreso','=',  $detalleCongreso->idDetalleCongreso)      
			->get();

			$tematicas = DB::table('congreso_x_tematica')
			->join('congreso','congreso.idCongreso','=','congreso_x_tematica.idCongreso')
			->join('tematica','tematica.idTematica','=','congreso_x_tematica.idTematica')
			->Where('congreso_x_tematica.idCongreso','=',  $id)      
			->get();

			$usuariosAutor = DB::table('autor_x_ficha')
							->where('autor_x_ficha.idFicha','=',$idFicha)
							->lists('autor_x_ficha.idUsuario');
			$usuarios = DB::table('usuariorol_x_congreso')
			->join('usuarios','usuariorol_x_congreso.idUsuario','=','usuarios.idUsuario')
			->join('congreso','usuariorol_x_congreso.idCongreso','=','congreso.idCongreso')
			//->join('detalle_congreso','detalle_congreso.idCongreso','=','congreso.idCongreso')
			->join('rol','usuariorol_x_congreso.idRol','=','rol.idRol')
			//->join('ficha','ficha.idDetalleCongreso','=','detalle_congreso.idDetalleCongreso')
			->Where('usuariorol_x_congreso.idCongreso','=',  $id)
			//->Where('ficha.idFicha','=',$idFicha)      
			->WhereIn('nomRol',['Autor','Revisor','Chair','PC'])
			->whereNotIn('usuarios.idUsuario',$usuariosAutor)
			->get();
			$usuariosSel = [];
			$ficha = Ficha::where('idFicha', '=' , $idFicha)->first();
			$autores = DB::table('autor_x_ficha')->where("idFicha", "=", $idFicha)->get();
			foreach ($autores as $autor) {
				$usuario =  DB::table('usuarios')->where("idUsuario", "=",$autor->idUsuario)
															->first();
				array_push($usuariosSel, $usuario);
				if($autor->responsable == 1){
					$responsable = $usuario;
				};
			};
			$paper = [];
			$paperL = Paper::where('idFicha','=',$idFicha)->first();
			if(count($paperL)){
				$paper = $paperL;
			}else
			{
				$paper = [];
			}
			$subidaPresentacion = $this->generarAyuda($id);
			$info['idcongreso'] = $id;
			$info['detalle'] = $detalleCongreso->idDetalleCongreso;
			$info['longitudMaxima'] = $detalleCongreso->longitudMaxResumenPaper;
			$info['responsable'] = $responsable;
			$info['usuarios'] = $usuarios;
			$info['usuariosSel'] = $usuariosSel;
			$info['categorias'] = $categorias;
			$info['tematicas'] = $tematicas;
			$info['usuarios'] = $usuarios;
			$info['ficha'] = $ficha;
			$info['paper'] = $paper;
			$info['idficha'] = $ficha->idFicha;

			$this->layout->content = View::make('sumision/EditarArticulo')->with('subidaPresentacion', $subidaPresentacion)->with('info' , $info)->with('idCongreso', $id);
		}	
	}


	public function indexVisual($id,$idFicha)
	{
		if(Auth::check())
		{

			$detalleCongreso = DetalleCongreso::where('idCongreso','=',$id)->firstOrFail();
			$responsable = "";
			$categorias = DB::table('detallecongreso_x_categoria')
			->join('detalle_congreso','detalle_congreso.idDetalleCongreso','=','detallecongreso_x_categoria.idDetalleCongreso')
			->join('categoria','categoria.idCategoria','=','detallecongreso_x_categoria.idCategoria')
			->Where('detallecongreso_x_categoria.idDetalleCongreso','=',  $detalleCongreso->idDetalleCongreso)      
			->get();

			$tematicas = DB::table('congreso_x_tematica')
			->join('congreso','congreso.idCongreso','=','congreso_x_tematica.idCongreso')
			->join('tematica','tematica.idTematica','=','congreso_x_tematica.idTematica')
			->Where('congreso_x_tematica.idCongreso','=',  $id)      
			->get();

			$usuariosAutor = DB::table('autor_x_ficha')
							->where('autor_x_ficha.idFicha','=',$idFicha)
							->lists('autor_x_ficha.idUsuario');
			$usuarios = DB::table('usuariorol_x_congreso')
			->join('usuarios','usuariorol_x_congreso.idUsuario','=','usuarios.idUsuario')
			->join('congreso','usuariorol_x_congreso.idCongreso','=','congreso.idCongreso')
			//->join('detalle_congreso','detalle_congreso.idCongreso','=','congreso.idCongreso')
			->join('rol','usuariorol_x_congreso.idRol','=','rol.idRol')
			//->join('ficha','ficha.idDetalleCongreso','=','detalle_congreso.idDetalleCongreso')
			->Where('usuariorol_x_congreso.idCongreso','=',  $id)
			//->Where('ficha.idFicha','=',$idFicha)      
			->WhereIn('nomRol',['Autor','Revisor','Chair','PC'])
			->whereNotIn('usuarios.idUsuario',$usuariosAutor)
			->get();
			$usuariosSel = [];
			$ficha = Ficha::where('idFicha', '=' , $idFicha)->first();
			$autores = DB::table('autor_x_ficha')->where("idFicha", "=", $idFicha)->get();
			foreach ($autores as $autor) {
				$usuario =  DB::table('usuarios')->where("idUsuario", "=",$autor->idUsuario)
															->first();
				array_push($usuariosSel, $usuario);
				if($autor->responsable == 1){
					$responsable = $usuario;
				};
			};
			$paper = [];
			$paperL = Paper::where('idFicha','=',$idFicha)->first();
			if(count($paperL)){
				$paper = $paperL;
			}else
			{
				$paper = [];
			}
			$subidaPresentacion = $this->generarAyuda($id);
			$info['idcongreso'] = $id;
			$info['detalle'] = $detalleCongreso->idDetalleCongreso;
			$info['longitudMaxima'] = $detalleCongreso->longitudMaxResumenPaper;
			$info['responsable'] = $responsable;
			$info['usuarios'] = $usuarios;
			$info['usuariosSel'] = $usuariosSel;
			$info['categorias'] = $categorias;
			$info['tematicas'] = $tematicas;
			$info['usuarios'] = $usuarios;
			$info['ficha'] = $ficha;
			$info['paper'] = $paper;

			$this->layout->content = View::make('sumision/VistaFicha')->with('info' , $info)->with('idCongreso', $id);
		}	
	}


	public function indexPresentaciones($id)
	{
		if(Auth::check())
		{
			if(!$this->verificarFechaRecurso($id,'Recepción de presentación y documentos de apoyo'))
				return Redirect::action('UsuariosCongresosController@establecerCongreso',array($id));

			$idUsuarioLogueado = Auth::user()->idUsuario;
			$presentaciones = DB::select( DB::raw("select distinct a.titulo,a.fecha,a.horaInicio,a.horaFin, a.AulaLabel, a.idAula, a.idtematica,b.idPresentacion,b.rutaPresentacion,c.idUsuario,c.responsable,c.rutaDiploma from agenda a inner join presentacion b on a.idAgenda = b.idAgenda inner join autor_x_ficha c on b.idFicha = c.idFicha where a.idCongreso = '$id' and a.esActividad = 0 and c.idUsuario = '$idUsuarioLogueado' and c.responsable=1"));
			
			$mesesFec  = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
			$diasFec   = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");

			foreach($presentaciones as $objeto)
			{
				$fechaPresentacion  = $diasFec[intval(date('w',strtotime($objeto->fecha)))] . ' ' . date('d',strtotime($objeto->fecha)) . ' de ' ;
	            $fechaPresentacion  = $fechaPresentacion . $mesesFec[intval(date('m',strtotime($objeto->fecha)))-1] . ' del ' . date('Y',strtotime($objeto->fecha)) ;
	            $horaInicio = date("h:i",strtotime($objeto->horaInicio)) . ' ' . date("A",strtotime($objeto->horaInicio));
	            $horaFin    = date("h:i",strtotime($objeto->horaFin)) . ' ' . date("A",strtotime($objeto->horaFin));

				$objeto->fecha = $fechaPresentacion;
				$objeto->horaInicio = $horaInicio;
				$objeto->horaFin  = $horaFin;
			}

			$requierePresentacion = false;

			if(count($presentaciones) > 0)
				$requierePresentacion = true;

			$subidaPresentacion = $this->generarAyuda($id);

			$this->layout->content = View::make('sumision/SubidaPresentaciones')->with('requierePresentacion', $requierePresentacion)->with('presentaciones', $presentaciones)->with('subidaPresentacion', $subidaPresentacion)->with('idCongreso', $id);
		}
	}

	public function indexFuentes($id)
	{
		if(Auth::check())
		{
			if(!$this->verificarFechaRecurso($id,'Recepción de presentación y documentos de apoyo'))
				return Redirect::action('UsuariosCongresosController@establecerCongreso',array($id));
			
			$idUsuarioLogueado = Auth::user()->idUsuario;
			$subidaFuentes = $this->generarAyuda($id);
			
			$estadoPaper	= EstadoPaper::where('nombreEstado','=','APROBADA')->first();
			$estaodID		= $estadoPaper->idEstadoPaper;
			$detalleCongreso = DetalleCongreso::where('idCongreso','=',$id)->firstOrFail();
			$congresoIdDetalle = $detalleCongreso->idDetalleCongreso;
			$papersFuentes = DB::select( DB::raw("SELECT b.tituloPaper,b.idFicha,a.idPaper,a.rutaFuentes,c.idUsuario from paper a inner join ficha b on a.idFicha = b.idFicha inner join autor_x_ficha c on b.idFicha = c.idFicha where b.idDetalleCongreso = '$congresoIdDetalle' and c.idUsuario = '$idUsuarioLogueado' and a.idEstadoPaper = '$estaodID' and c.responsable=1"));

			$habilitarFuentes = false;
			if(count($papersFuentes) > 0)
			{
				$habilitarFuentes = true;
			}

			$this->layout->content = View::make('sumision/SubidaFuentes')->with('idCongreso', $id)->with('subidaFuentes', $subidaFuentes)->with('habilitarFuentes',$habilitarFuentes)->with('fuentes',$papersFuentes);
		}
	}

	/**
	 * Cargar vista principal para subir un articulo (Paper).
	 * @id = id de la ficha relacionada al paper
	 * @return View('login')
	 */
	public function indexPaper($id,$idFicha)
	{
		if(Auth::check())
		{
			$responsable = "";
			$usuarios = [];
			$ficha = [];
			$ficha = Ficha::where('idFicha', '=' , $idFicha)->first();
			$autores = DB::table('autor_x_ficha')->where("idFicha", "=", $idFicha)->get();
			foreach ($autores as $autor) {
				$usuario =  DB::table('usuarios')->where("idUsuario", "=",$autor->idUsuario)
															->first();
				array_push($usuarios, $usuario);
				if($autor->responsable == 1){
					$responsable = $usuario;
				};
			};

			$tematicas = Tematica::where('idTematica','=',$ficha->idTematica)->get();
			$categoria = Categoria::where('idCategoria','=',$ficha->idCategoria)->get();
			$info['idficha'] = $idFicha;
			$info['ficha'] = $ficha;
			$info['categorias'] = $categoria;
			$info['tematicas'] = $tematicas;	 
			$info['responsable'] = $responsable;
			$info['usuarios'] = $usuarios;
			$info['idcongreso'] = $id;
			$info['detalle'] = "";
			
			$detalleCongreso = DetalleCongreso::where('idCongreso','=',$id)->firstOrFail();
			$info['longitudMaxima'] = $detalleCongreso->longitudMaxResumenPaper;
			
			//Aqui agregue la variable que da error, no se si asi estaria bien o te afectaria en algo...
			//Antonio: Del lado de la vista, solo accedo a esas variables si esta variable: requierePresentacion, tiene valor de TRUE,
			//Como ves en las siguiente lineas, en esta llamada la variable SIEMPRE se le pasa valor de FALSE.
			//El cambio final seria en la vista [el blade], validar si esa variable va a true y asi grantizo que mando toda la data.
			//El cambio lo acabo de hacer en la vista. Voy a dejar esto asi, pero estaria de mas.
			$subidaPresentacion = $this->generarAyuda($id);
			$this->layout->content = View::make('sumision/SubidaFicha')->with('info' , $info)->with('idCongreso', $id)->with('requierePresentacion', false)->with('subidaPresentacion',$subidaPresentacion)->with('habilitarFuentes', false);
		}
		
	}

	public function indexFichaLista($id)
	{
		if(Auth::check())
		{
			if(!$this->verificarFechaRecurso($id,'Recepción de Artículos')) //quiere decir que la fecha esta fuera de rango
				return Redirect::action('UsuariosCongresosController@establecerCongreso',array($id));
			$estado = EstadoFicha::where('nombreEstado','=','APROBADA')->first();
			$detalleCongreso = DetalleCongreso::where("idCongreso","=",$id)->first();
			$ficha = DB::table('ficha')->join('autor_x_ficha','autor_x_ficha.idFicha', '=' , 'ficha.idFicha')
										->where('autor_x_ficha.idUsuario','=',Auth::user()->idUsuario)
										->where('autor_x_ficha.responsable', '=','1')
										->where("ficha.idDetalleCongreso", "=",$detalleCongreso->idDetalleCongreso)
										->where('ficha.idEstadoFicha','=',$estado->idEstadoFicha)
										->whereNotExists(function($query)
											{
												$query
												->select(DB::raw(1))
												->from('paper')
												->whereRaw('paper.idFicha = ficha.idFicha');
											})
										->get();			
			$this->layout->content = View::make('sumision/ListaFichas')->with('Fichas' , $ficha)->with('idCongreso',$id);
		}
		
	}


	public function irAHome($id)
	{
		$this->layout->content = View::make('homeAutor')->with('idCongreso',$id);
	}

	public function guardarArticulo()
	{
		return DB::transaction(function()
			{
				DB::beginTransaction();	
				try{
		    		if(Request::ajax())
					{
						$rules = array(
						'resp' =>	'required',
						'titulo' =>	'required|min:1',
						'sintesis' =>	'required|min:1',
						'palabras' =>	'required|min:1',
						'autores' =>	'required'
						);

						$validador = Validator::make(Input::all(), $rules);

						if($validador->fails()) {
							$errores = $validador->messages();
							return Response::json(array('error' => True, 'mensaje' => $errores->first()));
						}
						else
						{
							$responsable = Input::get('resp');
							$estado = EstadoFicha::where('nombreEStado','=', 'PENDIENTE')->first();
							$ficha = new Ficha;			
							$ficha->tituloPaper = Input::get('titulo');
							$ficha->resumenPaper = Input::get('sintesis');
							$ficha->palabrasClaves = input::get('palabras');
							$ficha->idDetalleCongreso = input::get('detalle');
							$ficha->idEstadoFicha = $estado->idEstadoFicha;//Es el estado de Pendiente
							$ficha->agendado = false;
							$ficha->idTematica =  input::get('tematica');
							$ficha->idCategoria = input::get('categoria');
							$ficha->rutaFicha = "";
							$ficha->save();

							$autores = input::get('autores');
							foreach ($autores as $autor) {
								$autorxficha = new AutorXFicha;
								$autorxficha->idUsuario = $autor;
								$autorxficha->idFicha = $ficha->idFicha;
								if($responsable == $autor){
									$autorxficha->responsable = 1;
								}else{
									$autorxficha->responsable = 0;
								}
								$autorxficha->rutaDiploma = "";
								$autorxficha->save();
							}
							$autores=DB::table('autor_x_ficha')
									->join('usuarios',
											'usuarios.idUsuario',
											'=',
											'autor_x_ficha.idUsuario'
										)
									->where('autor_x_ficha.idFicha',
											'=',
											$ficha->idFicha
											)->get();
							DB::commit();
							if(count($autores)==0)throw new Exception("Al notificar a los autores");
							$congreso=DB::table('detalle_congreso')
												->join('congreso',
														'congreso.idCongreso',
														'=',
														'detalle_congreso.idCongreso'
													)
												->where('detalle_congreso.idDetalleCongreso',
														'=',
														Input::get('detalle')
														)->first();
							$tabla="Ficha";
							$length = count($autores);
							for ($i = 0; $i < $length; $i++) {	
								$usr=$autores[$i];
								$banner = URL::asset('banners/'.$congreso->banner);
								Mail::send(
									'emails.notificacionUsuarioFichaPaperRecibido', 
									array(
										'nombreSistema'	=>	$congreso->acronimoCongreso,
										'nombreSistemaCompleto'	=>	$congreso->nomCongreso,
										'correo'		=>	$usr->emailUsuario,
										'nombres'		=>	$usr->nombreUsuario,
										'apellidos'		=>	$usr->apelUsuario,
										'tabla'			=> 	$tabla,
										'tituloFP'		=>	Input::get('titulo'),
										'anio'			=>	date("Y"),
										'rolSistema'	=> 'Autor',
										'URLConfirmar'	=> '',
										'banner_email' 	=>	$banner,
										'URLSitio'		=> URL::action('HomeController@inicio')
									),
									function($mensaje) use ($usr,$tabla)
									{
										$mensaje->to($usr->emailUsuario, $usr->nombreUsuario . ' ' . $usr->apelUsuario)
												->subject('CONUCA: Su envio de '.$tabla.' ha sido recibido.');
									}
								);
							}
							// Ana
							
							$presentacion = new Presentacion();
							$presentacion->idFicha = $ficha->idFicha;
							$presentacion->rutaPresentacion = "";
							$presentacion->tamPresentacion = "";
							$presentacion->idAgenda = 1;
							$presentacion->idSesion = 1;
							$presentacion->idEstadoPresentacion = EstadoPresentacion::all()->random()->idEstadoPresentacion;
							$presentacion->save();



							DB::commit();
							return Response::json(array('error' => False, 'mensaje' => 'Su ficha ha sido almacenada exitosamente','ficha' =>$ficha->idFicha));
						}	
					}
				}				
		    	catch (Exception $e)
		    	{
		    		DB::rollback();
		    		//$e->getMessage()
		    		return Response::json(array('error' => True, 'mensaje' => $e->getMessage()));
		    	}
		    });
	}

	public function EditarArticulo($id)
	{
		return DB::transaction(function()
			{
				DB::beginTransaction();	
				try{
		    		if(Request::ajax())
					{

						$responsable = Input::get('resp');
						$id = Input::get('id');
						$ficha = Ficha::find($id);			
						$ficha->tituloPaper = Input::get('titulo');
						$ficha->resumenPaper = Input::get('sintesis');
						$ficha->palabrasClaves = input::get('palabras');
						$ficha->agendado = false;
						$ficha->idTematica =  input::get('tematica');
						$ficha->idCategoria = input::get('categoria');
						$ficha->rutaFicha = "";
						$ficha->update();


						$autoresBD=DB::table('autor_x_ficha')
								->join('usuarios',
										'usuarios.idUsuario',
										'=',
										'autor_x_ficha.idUsuario'
									)
								->where('autor_x_ficha.idFicha',
										'=',
										$ficha->idFicha
										)->get();

						foreach ($autoresBD as $key) {
							
							$aut = AutorXFicha::find($key->idFichaXAutor);
							$aut->delete();
						}

						$autores = input::get('autores');
						foreach ($autores as $autor) {
							$autorxficha = new AutorXFicha;
							$autorxficha->idUsuario = $autor;
							$autorxficha->idFicha = $ficha->idFicha;
							if($responsable == $autor){
								$autorxficha->responsable = 1;
							}else{
								$autorxficha->responsable = 0;
							}
							$autorxficha->rutaDiploma = "";
							$autorxficha->save();
						}

						DB::commit();
						return Response::json(array('error' => False, 'mensaje' => 'Su ficha ha modificada exitosamente','ficha' =>$ficha->idFicha));
					}
				}				
		    	catch (Exception $e)
		    	{
		    		DB::rollback();
		    		//$e->getMessage()
		    		return Response::json(array('error' => True, 'mensaje' => $e->getMessage()));
		    	}
		    });
	}



	public function EditarArticuloChair($id)
	{
		return DB::transaction(function()
			{
				DB::beginTransaction();	
				try{
		    		if(Request::ajax())
					{

						$idF = Input::get('id');
						$ficha = Ficha::find($idF);			
						$ficha->idTematica =  input::get('tematica');
						$ficha->idCategoria = input::get('categoria');
						$ficha->update();						
						DB::commit();
						return Response::json(array('error' => False, 'mensaje' => 'La ficha ha sido modificada exitosamente','ficha' =>$ficha->idFicha));
					}
				}				
		    	catch (Exception $e)
		    	{
		    		DB::rollback();
		    		//$e->getMessage()
		    		return Response::json(array('error' => True, 'mensaje' => $e->getMessage()));
		    	}
		    });
	}


	public function guardarPaper()
	{

		DB::beginTransaction();
		try {
    		if(Request::ajax())
			{
					$estado = EstadoPaper::where('nombreEstado','=', 'PENDIENTE')->first();
					$ficha = Input::get('ficha');
					$paper = new Paper;			
					$paper->idFicha = $ficha;
					$paper->rutaPaper = "";
					$paper->tamPaper = 0;
					$paper->idEstadoPaper = $estado->idEstadoPaper;
					$paper->rutaFuentes = "";
					$paper->save();
					DB::commit();

					$autores=DB::table('autor_x_ficha')
							->join('usuarios',
									'usuarios.idUsuario',
									'=',
									'autor_x_ficha.idUsuario'
								)
							->where('autor_x_ficha.idFicha',
									'=',
									$ficha
									)->get();
					DB::commit();
					if(count($autores)==0)throw new Exception("Al notificar a los autores");
					$fichaDetalle=DB::table('ficha')
										->where('ficha.idFicha',
												'=',
												$ficha
											)->first();
					$congreso=DB::table('detalle_congreso')
										->join('congreso',
											'congreso.idCongreso',
											'=',
											'detalle_congreso.idCongreso'
										)
										->where('detalle_congreso.idDetalleCongreso',
												'=',
												$fichaDetalle->idDetalleCongreso
												)->first();
					$tabla="Paper";
					$length = count($autores);
					for ($i = 0; $i < $length; $i++) {	
						$usr=$autores[$i];
						$banner = URL::asset('banners/'.$congreso->banner);
						Mail::send(
							'emails.notificacionUsuarioFichaPaperRecibido', 
							array(
								'nombreSistema'	=>	$congreso->acronimoCongreso,
								'nombreSistemaCompleto'	=>	$congreso->nomCongreso,
								'correo'		=>	$usr->emailUsuario,
								'nombres'		=>	$usr->nombreUsuario,
								'apellidos'		=>	$usr->apelUsuario,
								'tabla'			=> 	$tabla,
								'tituloFP'		=>	$fichaDetalle->tituloPaper,
								'anio'			=>	date("Y"),
								'rolSistema'	=> 'Autor',
								'URLConfirmar'	=> '',
								'banner_email' 	=>	$banner,
								'URLSitio'		=> URL::action('HomeController@inicio')
							),
							function($mensaje) use ($usr,$tabla)
							{
								$mensaje->to($usr->emailUsuario, $usr->nombreUsuario . ' ' . $usr->apelUsuario)
										->subject('CONUCA: Su envio de '.$tabla.' ha sido recibido.');
							}
						);
					}
					return Response::json(array('error' => False, 'mensaje' => 'Su documento ha sido almacenado exitosamente', 'paper' =>$paper->idPaper));
			}	
    	} catch (Exception $e) {
    		DB::rollback();
    		return Response::json(array('error' => True, 'mensaje' => $e->getMessage()));
    		
		}		
	}

	public function editarPaper($id)
	{
		return DB::transaction(function()
			{
				DB::beginTransaction();	
				try{
		    		if(Request::ajax())
					{

						$idF = Input::get('id');
						$paper = Paper::find($idF);			
						$paper->rutaPaper = "";
						$paper->tamPaper = 0;
						$paper->save();
						DB::commit();
						return Response::json(array('error' => False, 'mensaje' => 'El artículo ha sido modificada exitosamente'));
					}
				}				
		    	catch (Exception $e)
		    	{
		    		DB::rollback();
		    		//$e->getMessage()
		    		return Response::json(array('error' => True, 'mensaje' => $e->getMessage()));
		    	}
		    });
	}

	


	public function guardarPresentacion()
	{
		return DB::transaction(function()
			{
				DB::beginTransaction();	
				try{
					$mappedPath= '';
	                $filename= '';
	                $destinationPath='';
	                $file=null;
	                $idCongreso = Input::get('idCongreso');
	                $iPre = 1;
	                $totalPresentaciones = Input::get('totalControles');
	                for ($iPre = 1; $iPre <= $totalPresentaciones; $iPre++) {
	                	if (Input::hasFile('filePresentacion_'.$iPre))
	                	{
	                		$file = Input::file('filePresentacion_'.$iPre);

	                		$filename   = $file->getClientOriginalName();
	                		$renombrado = array( "([^a-zA-Z0-9-.])", "(-{2,})" );
	                		$filename 	= preg_replace($renombrado, [], $filename);
							
							$idPresentacion = Input::get('idPresentacion_'.$iPre);

							$pptObj 	= Presentacion::WhereRaw('idPresentacion=?',array($idPresentacion))->first();
							$filename	= $pptObj->idFicha . '_presentacion_' . $idCongreso . '.' . $file->getClientOriginalExtension();

	                		$mappedPath = base_path().'/archivos/presentaciones/' . $idPresentacion;
			                File::makeDirectory($mappedPath, $mode = 0777, true, true);

			                $destinationPath = $mappedPath;
			                $uploadSuccess   = $file->move($destinationPath, $filename);
			                if($uploadSuccess==null)
			                	throw new Exception("ERROR: Al subir el archivo ".$file->getClientOriginalName());   
			                else{
			                	$rutaMySQL = 'presentaciones/' . $idPresentacion .'/'. $filename;
								$presentacion = new Presentacion();
								$presentacion->updateRuta($idPresentacion, $rutaMySQL);
			                }
	                	}
					}
	                DB::commit();
	                return Redirect::action('SumissionController@indexPresentaciones',array($idCongreso));
				}				
		    	catch (Exception $e)
		    	{
		    		DB::rollback();
		    		return Response::json(array('error' => True, 'mensaje' => $e->getMessage()));
		    	}
		    });
	}

	public function guardarFuentes()
	{
		return DB::transaction(function()
			{
				DB::beginTransaction();	
				try{
					$mappedPath= '';
	                $filename= '';
	                $destinationPath='';
	                $file=null;
	                $idCongreso = Input::get('idCongreso');
	                $iPaper = 1;
	                $totalPresentaciones = Input::get('totalControlesFuentes');
	                for ($iPaper = 1; $iPaper <= $totalPresentaciones; $iPaper++) {
	                	if (Input::hasFile('fileFuente_'.$iPaper))
	                	{
	                		$file = Input::file('fileFuente_'.$iPaper);

	                		$filename   = $file->getClientOriginalName();
	                		$renombrado = array( "([^a-zA-Z0-9-.])", "(-{2,})" );
	                		$filename 	= preg_replace($renombrado, [], $filename);
							$idPaper = Input::get('idPaper_'.$iPaper);

							$paperObj 	= Paper::WhereRaw('idPaper=?',array($idPaper))->first();
							$filename	= $paperObj->idFicha . '_fuentes_' . $idCongreso . '.' . $file->getClientOriginalExtension();
	                		
	                		$mappedPath = base_path().'/archivos/archivosfuentes/' . $idPaper;
			                File::makeDirectory($mappedPath, $mode = 0777, true, true);

			                $destinationPath = $mappedPath;
			                $uploadSuccess   = $file->move($destinationPath, $filename);
			                if($uploadSuccess==null)
			                	throw new Exception("ERROR: Al subir el archivo ".$file->getClientOriginalName());   
			                else{
			                	$rutaMySQL = 'archivosfuentes/' . $idPaper .'/'. $filename;
								$UPDObject = new Paper();
								$UPDObject->updateFuentes($idPaper, $rutaMySQL);
			                }
	                	}
					}
	                DB::commit();
	                return Redirect::action('SumissionController@indexFuentes',array($idCongreso));
				}				
		    	catch (Exception $e)
		    	{
		    		DB::rollback();
		    		return Response::json(array('error' => True, 'mensaje' => $e->getMessage()));
		    	}
		    });
	}

	public function generarAyuda($id)
	{
		$extensiones = DB::select( DB::raw("SELECT c.tipoExtension,c.nomExtension,b.tamMaxArchivo FROM detallecongreso_x_extension a inner join detalle_congreso b on a.idDetalleCongreso = b.idDetalleCongreso inner join extension_documento c on a.idExtensionDocumento = c.idExtensionDocumento where b.idCongreso  = '$id' "));
		$extValidas = '';
		$extValidasLabel = '';
		foreach($extensiones as $objeto)
		{
			$extValidas = $extValidas . '\\'. $objeto->tipoExtension . '|';
			$extValidasLabel = $extValidasLabel . $objeto->nomExtension . ',';
			$objetoAyuda['tamanioMax'] = $objeto->tamMaxArchivo;
		}
		if($extValidasLabel == ''){
			$extValidasLabel = '~Aún no se definen tipos de archivo permitidos~~';
			$objetoAyuda['tamanioMax'] = '0';
		}

		$extValidasLabel = '<strong>Tipos de Archivo permitidos para congreso:</strong> ' . substr($extValidasLabel, 0, -1);
		$extValidasLabel = $extValidasLabel . '<br><strong>Tama&ntilde;o M&aacute;ximo de archivos (MB):</strong> ' . $objetoAyuda['tamanioMax'];
		
		$objetoAyuda['ayuda'] = $extValidasLabel;
		$objetoAyuda['extensiones'] = substr($extValidas, 0, -1);

		return $objetoAyuda;
	}

	public function listarFichas($id)
	{
		try{
			if(!$this->verificarFechaRecurso($id,'Llamado a ponencias')) //quiere decir que la fecha esta fuera de rango
				return Redirect::action('UsuariosCongresosController@establecerCongreso',array($id));

			$fichas = DB::table('ficha')
								->join(
									'detalle_congreso',
									'detalle_congreso.idDetalleCongreso',
									'=',
									'ficha.idDetalleCongreso'
								)		
								->join(
									'tematica',
									'tematica.idTematica',
									'=',
									'ficha.idTematica'
								)
								->join(
									'categoria',
									'categoria.idCategoria',
									'=',
									'ficha.idCategoria'
								)
								->join(
									'estado_ficha',
									'estado_ficha.idEstadoFicha',
									'=',
									'ficha.idEstadoFicha'
								)
								->join('autor_x_ficha','autor_x_ficha.idFicha', '=' , 'ficha.idFicha')
										->where('autor_x_ficha.idUsuario','=',Auth::user()->idUsuario)
										->where('autor_x_ficha.responsable', '=','1')
								->Where(
									'estado_ficha.nombreEstado',
									'=',
									'PENDIENTE'
								)
								->Where(
									'detalle_congreso.idCongreso',
									'=',
									$id
								)

								->whereNotExists(function($query)
								{
									$query
									->select(DB::raw(1))
									->from('revision_ficha')
									->whereRaw('revision_ficha.idFicha = ficha.idFicha');
								})
								->get();
										

			$this->layout->content = View::make('sumision/ListaFichasEditar')
											->with('fichas',$fichas)
											->with('idCongreso',$id);
		}
		catch(Exception $e)
		{
			Session::flash('message',  $e->getMessage());
			return Redirect::action('HomeController@inicio');
		}	
	}


	public function listarArticulosEditar($id)
	{
		try{
			if(!$this->verificarFechaRecurso($id,'Recepción de Artículos')) //quiere decir que la fecha esta fuera de rango
				return Redirect::action('UsuariosCongresosController@establecerCongreso',array($id));

			$fichas = DB::table('ficha')
								->join(
									'paper',
									'paper.idFicha',
									'=',
									'ficha.idFicha'
								)
								->join(
									'detalle_congreso',
									'detalle_congreso.idDetalleCongreso',
									'=',
									'ficha.idDetalleCongreso'
								)		
								->join(
									'tematica',
									'tematica.idTematica',
									'=',
									'ficha.idTematica'
								)
								->join(
									'categoria',
									'categoria.idCategoria',
									'=',
									'ficha.idCategoria'
								)
								->join(
									'estado_paper',
									'estado_paper.idEstadoPaper',
									'=',
									'paper.idEstadoPaper'
								)
								->join('autor_x_ficha','autor_x_ficha.idFicha', '=' , 'ficha.idFicha')
										->where('autor_x_ficha.idUsuario','=',Auth::user()->idUsuario)
										->where('autor_x_ficha.responsable', '=','1')
								
								->Where(
									'estado_paper.nombreEstado',
									'=',
									'PENDIENTE'
								)
								->Where(
									'detalle_congreso.idCongreso',
									'=',
									$id
								)

								->whereNotExists(function($query)
								{
									$query
									->select(DB::raw(1))
									->from('revision_paper')
									->whereRaw('revision_paper.idPaper = paper.idPaper');
								})
								->get();
										

			$this->layout->content = View::make('sumision/ListaArticulosEditar')
											->with('fichas',$fichas)
											->with('idCongreso',$id);
		}
		catch(Exception $e)
		{
			Session::flash('message',  $e->getMessage());
			return Redirect::action('HomeController@inicio');
		}	
	}

	public function listarFichasChair($id)
	{
		try{
			if(!$this->verificarFechaRecurso($id,'Llamado a ponencias')) //quiere decir que la fecha esta fuera de rango
				return Redirect::action('UsuariosCongresosController@establecerCongreso',array($id));

			$fichas = DB::table('ficha')
								->join(
									'detalle_congreso',
									'detalle_congreso.idDetalleCongreso',
									'=',
									'ficha.idDetalleCongreso'
								)		
								->join(
									'tematica',
									'tematica.idTematica',
									'=',
									'ficha.idTematica'
								)
								->join(
									'categoria',
									'categoria.idCategoria',
									'=',
									'ficha.idCategoria'
								)
								->join(
									'estado_ficha',
									'estado_ficha.idEstadoFicha',
									'=',
									'ficha.idEstadoFicha'
								)
								
								->Where(
									'estado_ficha.nombreEstado',
									'=',
									'PENDIENTE'
								)
								->Where(
									'detalle_congreso.idCongreso',
									'=',
									$id
								)

								->whereNotExists(function($query)
								{
									$query
									->select(DB::raw(1))
									->from('revision_ficha')
									->whereRaw('revision_ficha.idFicha = ficha.idFicha');
								})
								->get();
										

			$this->layout->content = View::make('sumision/ListaFichasEditarAdmin')
											->with('fichas',$fichas)
											->with('idCongreso',$id);
		}
		catch(Exception $e)
		{
			Session::flash('message',  $e->getMessage());
			return Redirect::action('HomeController@inicio');
		}	
	}

}