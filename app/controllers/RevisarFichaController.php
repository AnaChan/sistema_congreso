<?php

class RevisarFichaController extends \BaseController {

	/**
	 * Variable que indica si se quieren solo fichas sin revisiones.
	 *
	 */
	protected $FICHAS_SIN_REV = 0;

	/**
	 * Variable que indica si se quieren solo fichas con revisiones.
	 *
	 */
	protected $FICHAS_CON_REV = 1;

	/**
	 * Muestra la lista de fichas asociadas al usuario dependiendo de la temática asignada a el.
	 *
	 * @return HTML
	 */
	public function listarFichas($idCongreso)
	{
		
		if(!$this->verificarFechaRecurso($idCongreso,'Confirmación de aceptación de ponencias')) //quiere decir que la fecha esta fuera de rango
			return Redirect::action('UsuariosCongresosController@establecerCongreso',array($idCongreso));

		$fichas = $this->fichasFiltroQuery($idCongreso, $this->FICHAS_SIN_REV);

		$criterios = DB::table('criterio_evaluacion')
		->join(
			'detalle_congreso',
			'detalle_congreso.idDetalleCongreso',
			'=',
			'criterio_evaluacion.idDetalleCongreso'
		)
		->join(
			'congreso',
			'congreso.idCongreso',
			'=',
			'detalle_congreso.idCongreso'
		)
		->where(
			'congreso.idCongreso',
			'=',
			$idCongreso
		)
		->where(
			'indicadorFicha',
			'=',
			'F'
		)
		->whereRaw(
			'criterio_evaluacion.idCriterioEvaluacion = criterio_evaluacion.idCriterioEvaluacionPadre'
		)
		->get();

		foreach ($criterios as $criterio)
		{
			$criterio->hijos = DB::table('criterio_evaluacion')
			->where(
				'idCriterioEvaluacionPadre',
				'=',
				$criterio->idCriterioEvaluacion
			)
			->whereRaw(
				'criterio_evaluacion.idCriterioEvaluacion != criterio_evaluacion.idCriterioEvaluacionPadre'
			)->get();

			foreach($criterio->hijos as $hijo)
			{
				$hijo->puntajes = DB::table('puntaje')
				->where(
					'idCriterioEvaluacion',
					'=',
					$hijo->idCriterioEvaluacion
				)
				->select('nomPuntaje', 'valorPuntaje', 'idPuntaje')
				->orderBy('valorPuntaje','asc')
				->get();
				$hijo->puntajesMax = count($hijo->puntajes);
			}

			$criterio->puntajes = DB::table('puntaje')
			->where(
				'idCriterioEvaluacion',
				'=',
				$criterio->idCriterioEvaluacion
			)
			->select('nomPuntaje', 'valorPuntaje', 'idPuntaje')
			->orderBy('valorPuntaje','asc')
			->get();

			$criterio->puntajesMax = count($criterio->puntajes);
		}

		$this->layout->content = View::Make('congresos.ficha.revisarFicha')->withFichas($fichas)->withCriterios($criterios)->with('idCongreso', $idCongreso);
	}

	/**
	 * retorna la lista de fichas segun el filtro seleccionado.
	 *
	 * @return Json listado de todas las fichas 
	 */
	public function fichasFiltro($idCongreso)
	{

		if(Request::ajax())
		{
			$rules = array(
				'filtro' =>	'required|integer|in:0,1,2'
			);

			$validador = Validator::make(Input::all(), $rules);

			if($validador->fails()) {
				$errores = $validador->messages();
				return Response::json(array('error' => True, 'mensaje' => $errores->first()));
			}
			else
			{
				$fichas = $this->fichasFiltroQuery($idCongreso, Input::get('filtro'));

				foreach ($fichas as $ficha)
				{
					$ficha->esPDF = pathinfo($ficha->rutaFicha, PATHINFO_EXTENSION) == "pdf";
					$ficha->rutaArchivo = URL::action('ArchivoController@servirArchivo', array($idCongreso,$ficha->rutaFicha));
					$ficha->nombreArchivo = pathinfo($ficha->rutaFicha, PATHINFO_BASENAME);
					$ficha->rechazarUrl = URL::action('RevisarFichaController@rechazarFicha', array($idCongreso,$ficha->idFicha));
					$ficha->revisarUrl =  URL::action('RevisarFichaController@evaluarFicha', array($idCongreso,$ficha->idFicha));
					$ficha->revisiones = DB::table('revision_ficha_x_criterio')
					->join(
						'puntaje',
						'puntaje.idPuntaje',
						'=',
						'revision_ficha_x_criterio.idPuntaje'
					)
					->where(
						'revision_ficha_x_criterio.idRevisionFicha',
						'=',
						$ficha->idRevisionFicha
					)
					->select('idCriterioEvaluacion', 'valorPuntaje', 'idRevisionFicha')
					->get();
					$ficha->cantidadRevs = count($ficha->revisiones);
				}

				return Response::json(array('error' => False, 'mensaje' => '','fichas' => $fichas));
			}
		}
	}

	/**
	 * Rechaza la revision de la ficha.
	 *
	 * @param integer $idCongreso id del congreso al que pertenece la ficha.
	 *
	 * @param integer $idFicha id de la ficha a rechazar.
	 *
	 * @return Json 
	 */
	public function rechazarFicha($idCongreso,$idFicha)
	{
		$fichaQuery = DB::Table('ficha')
		->join(
			'detalle_congreso',
			'detalle_congreso.idDetalleCongreso',
			'=',
			'ficha.idDetalleCongreso'
		)
		->join(
			'congreso',
			'congreso.idCongreso',
			'=',
			'detalle_congreso.idCongreso'
		)
		->join(
			'autor_x_ficha',
			'autor_x_ficha.idFicha',
			'=',
			'ficha.idFicha'
		)
		->join(
			'revision_ficha',
			'revision_ficha.idFicha',
			'=',
			'ficha.idFicha'
		)
		->where(
			'congreso.idCongreso',
			'=',
			$idCongreso
		)
		->where(
			'autor_x_ficha.idUsuario',
			'!=',
			Auth::user()->idUsuario
		)
		->where(
			'revision_ficha.idUsuario',
			'=',
			Auth::user()->idUsuario
		)
		->where(
			'ficha.idFicha',
			'=',
			$idFicha
		);

		$ficha = $fichaQuery->first();
		$revisiones = $fichaQuery
		->join(
			'revision_ficha_x_criterio',
			'revision_ficha_x_criterio.idRevisionFicha',
			'=',
			'revision_ficha.idRevisionFicha'
		)->get();

		if(count($ficha) == 0)
		{
			return Response::json(array('error' => True, 'mensaje' => 'No puedes eliminar una ficha del cual eres autor o que no se te haya asignado revisar o que no pertenezca al congreso.'));
		}

		if(count($revisiones) != 0)
		{
			return Response::json(array('error' => True, 'mensaje' => 'No puedes rechazar la revision de una ficha que ya se ha revisado.'));
		}

		try {
			DB::beginTransaction();
			RevisionFicha::find($ficha->idRevisionFicha)->delete();
			$fichaData=Ficha::Where('idFicha','=',$idFicha)->first();
			$cong = Congreso::where('idCongreso', '=',$idCongreso)->first();
			$dc = DetalleCongreso::where('idCongreso', '=',$idCongreso)->first();
			$usr = DB::table('pc_x_tematica')
							->join('tematica',
									'pc_x_tematica.idTematica',	
									'=',
									'tematica.idTematica'
								)
							->join('usuariorol_x_congreso',
									'usuariorol_x_congreso.idUsuariorolXCongreso',
									'=',
									'pc_x_tematica.idUsuariorolXCongreso'
								)
							->join('usuarios',
									'usuarios.idUsuario',
									'=',
									'usuariorol_x_congreso.idUsuario'
								)
							->where('pc_x_tematica.idTematica',
									'=',
									$fichaData->idTematica
								)
							->where('usuariorol_x_congreso.idCongreso',
									'=',
									$idCongreso
								)->first();

			$banner = URL::asset('banners/'.$dc->banner);
			Mail::send(
				'emails.notificacionPCRechazoFicha', 
				array(
					'nombreSistema'	=>	$cong->acronimoCongreso,
					'nombreSistemaCompleto'	=>	$cong->nomCongreso,
					'correo'		=>	$usr->emailUsuario,
					'nombres'		=>	$usr->nombreUsuario,
					'apellidos'		=>	$usr->apelUsuario,
					'anio'			=>	date("Y"),
					'tematica'		=> $usr->nomTematica,
					'tabla'			=> 'Ficha',
					'tituloFP'		=> $fichaData->tituloPaper,
					'rolSistema'	=> 'Administrador de Congreso',
					'banner_email' 	=>	$banner,
					'URLConfirmar'	=> '',
					'URLSitio'		=> URL::action('HomeController@inicio'),
				), 
				function($mensaje) use ($usr)
				{
					$mensaje->to($usr->emailUsuario, $usr->nombreUsuario . ' ' . $usr->apelUsuario)
							->subject('CONUCA: Un revisor ha rechazado su asignación');
				}
			);

			DB::commit();
			return Response::json(array('error' => False, 'mensaje' => 'Se ha rechazado exitosamente.'));
		} catch (\Exception $e) {
			DB::rollback();
			return Response::json(array('error' => True, 'mensaje' => 'No se pudo guardar el registro o no se pudo enviar el correo.'.$e->getMessage()));
		}
	}

	/**
	 * Rechaza la revision de la ficha.
	 *
	 * @param integer $idCongreso id del congreso al que pertenece la ficha.
	 *
	 * @param integer $idFicha id de la ficha a rechazar.
	 *
	 * @return Json 
	 */
	public function evaluarFicha($idCongreso,$idFicha)
	{

		$rules = array(
			'puntajes' =>	'required|array' 
		);

		$validador = Validator::make(Input::all(), $rules);

		if ($validador->fails()) {
			$errores = $validador->messages();
			return Response::json(array('error' => True, 'mensaje' => $errores->first()));
		}
		else
		{
			$fichaQuery = DB::Table('ficha')
			->join(
				'detalle_congreso',
				'detalle_congreso.idDetalleCongreso',
				'=',
				'ficha.idDetalleCongreso'
			)
			->join(
				'congreso',
				'congreso.idCongreso',
				'=',
				'detalle_congreso.idCongreso'
			)
			->join(
				'autor_x_ficha',
				'autor_x_ficha.idFicha',
				'=',
				'ficha.idFicha'
			)
			->join(
				'revision_ficha',
				'revision_ficha.idFicha',
				'=',
				'ficha.idFicha'
			)
			->where(
				'congreso.idCongreso',
				'=',
				$idCongreso
			)
			->where(
				'autor_x_ficha.idUsuario',
				'!=',
				Auth::user()->idUsuario
			)
			->where(
				'revision_ficha.idUsuario',
				'=',
				Auth::user()->idUsuario
			)
			->where(
				'ficha.idFicha',
				'=',
				$idFicha
			);

			$ficha = $fichaQuery->first();
			$revisiones = $fichaQuery
			->join(
				'revision_ficha_x_criterio',
				'revision_ficha_x_criterio.idRevisionFicha',
				'=',
				'revision_ficha.idRevisionFicha'
			)->get();

			if(count($ficha) == 0)
			{
				return Response::json(array('error' => True, 'mensaje' => 'No puedes evaluar una ficha del cual eres autor o que no se te haya asignado revisar o que no pertenezca al congreso.'));
			}

			if(count($revisiones) != 0)
			{
				return Response::json(array('error' => True, 'mensaje' => 'No puedes evaluar una ficha que ya se ha evaluada.'));
			}

			try {
				DB::beginTransaction();
				$puntajes = Input::get('puntajes');

				foreach ($puntajes as $puntaje)
				{
					$rev = array('idRevisionFicha' => $ficha->idRevisionFicha, 'idPuntaje' => $puntaje);
					DB::table('revision_ficha_x_criterio')->insert(
						$rev
					);
				}
				$fichaData=Ficha::Where('idFicha','=',$idFicha)->first();
				$cong = Congreso::where('idCongreso', '=',$idCongreso)->first();
				$dc=DetalleCongreso::where('idCongreso', '=',$idCongreso)->first();
				
				$usr = DB::table('pc_x_tematica')
								->join('tematica',
										'pc_x_tematica.idTematica',	
										'=',
										'tematica.idTematica'
									)
								->join('usuariorol_x_congreso',
										'usuariorol_x_congreso.idUsuariorolXCongreso',
										'=',
										'pc_x_tematica.idUsuariorolXCongreso'
									)
								->join('usuarios',
										'usuarios.idUsuario',
										'=',
										'usuariorol_x_congreso.idUsuario'
									)
								->where('pc_x_tematica.idTematica',
										'=',
										$fichaData->idTematica
									)
								->where('usuariorol_x_congreso.idCongreso',
										'=',
										$idCongreso
									)->first();

				$usrChair = DB::table('usuarios')
								->join('congreso',
										'congreso.idCreador',	
										'=',
										'usuarios.idUsuario'
									)
								->where('congreso.idCongreso',
										'=',
										$idCongreso
									)->first();
				$banner = URL::asset('banners/'.$dc->banner);
				Mail::send(
					'emails.notificacionPCRevisionFicha', 
					array(
						'nombreSistema'	=>	$cong->acronimoCongreso,
						'nombreSistemaCompleto'	=>	$cong->nomCongreso,
						'correo'		=>	$usrChair->emailUsuario,
						'nombres'		=>	$usrChair->nombreUsuario,
						'apellidos'		=>	$usrChair->apelUsuario,
						'anio'			=>	date("Y"),
						'tematica'		=> $usr->nomTematica,
						'tabla'			=> 'Ficha',
						'tituloFP'		=> $fichaData->tituloPaper,
						'rolSistema'	=> 'Administrador de Congreso',
						'banner_email' 	=>	$banner,
						'URLConfirmar'	=> '',
						'URLSitio'		=> URL::action('HomeController@inicio'),
					), 
					function($mensaje) use ($usrChair)
					{
						$mensaje->to($usrChair->emailUsuario, $usrChair->nombreUsuario . ' ' . $usrChair->apelUsuario)
								->subject('CONUCA: Una asignación requiere de su aprobación.');
					}
				);
				DB::commit();
				return Response::json(array('error' => False, 'mensaje' => 'Se ha evaluado exitosamente.'));
			} catch (\Exception $e) {
				DB::rollback();
				return Response::json(array('error' => True, 'mensaje' => 'No se pudo guardar el registro o no se pudo enviar el correo.' . $e->getMessage()));
			}
		}
	}

	/**
	 * Funcion que regresa las fichas que no han sido revisadas.
	 *
	 * @param $idCongreso id del congreso al que hay que sacar la lista de fichas.
	 *
	 * @param $flag Determina el filtro a utilizar. Ocupa los valores de banderas seteados al inicio de la clase
	 *
	 * @return array Lista de objetos Fichas no revisadas.
	 *
	 */
	protected function fichasFiltroQuery($idCongreso, $flag)
	{
		$fichasQuery = DB::Table('ficha')
		->join(
			'detalle_congreso',
			'detalle_congreso.idDetalleCongreso',
			'=',
			'ficha.idDetalleCongreso'
		)
		->join(
			'congreso',
			'congreso.idCongreso',
			'=',
			'detalle_congreso.idCongreso'
		)
		->join(
			'estado_ficha',
			'estado_ficha.idEstadoFicha',
			'=',
			'ficha.idEstadoFicha'
		)
		->join(
			'autor_x_ficha',
			'autor_x_ficha.idFicha',
			'=',
			'ficha.idFicha'
		)
		->join(
			'usuarios',
			'usuarios.idUsuario',
			'=',
			'autor_x_ficha.idUsuario'
		)
		->join(
			'revision_ficha',
			'revision_ficha.idFicha',
			'=',
			'ficha.idFicha'
		)
		->where(
			'congreso.idCongreso',
			'=',
			$idCongreso
		)
		->where(
			'autor_x_ficha.idUsuario',
			'!=',
			Auth::user()->idUsuario
		)
		->where(
			'autor_x_ficha.responsable',
			'=',
			True
		)
		->where(
			'revision_ficha.idUsuario',
			'=',
			Auth::user()->idUsuario
		);

		if($flag == $this->FICHAS_SIN_REV) 
			return $fichasQuery
			->whereNotExists(function($query)
			{
				$query
				->select(DB::raw(1))
				->from('revision_ficha_x_criterio')
				->whereRaw('revision_ficha_x_criterio.idRevisionFicha = revision_ficha.idRevisionFicha');
			})
			->get();
		elseif($flag == $this->FICHAS_CON_REV)
			return $fichasQuery
			->whereExists(function($query)
			{
				$query
				->select(DB::raw(1))
				->from('revision_ficha_x_criterio')
				->whereRaw('revision_ficha_x_criterio.idRevisionFicha = revision_ficha.idRevisionFicha');
			})
			->get();
		else
			return $fichasQuery->get();
	}
}