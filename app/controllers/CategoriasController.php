<?php 

class CategoriasController extends BaseController {
	
	public function index()
	{
		if (Auth::guest()) return Redirect::guest('login');
		$categorias = Categoria::orderBy('nomCategoria')->get();
		$this->layout->content = View::make('adminCategorias')->with('categorias',$categorias);
	}

	public function create()
	{
		if(Request::ajax())
		{
				try {
					$validCategory = Categoria::where("nomCategoria", "=", Input::get('nomCategoria'))
									->count();
					if($validCategory > 0)
					{
						return Response::json(array('error' => True, 'mensaje' => 'Categor&iacute;a ya existente, imposible agregar'));
					}					
					$categoria = new Categoria;
					$categoria->nomCategoria = Input::get('nomCategoria');
					$categoria->save();
					return Response::json(array('error' => False, 'mensaje' => 'Guardando... espere un momento mientras se refresca la pantalla'));
				} catch (Exception $e) {
					return Response::json(array('error' => True, 'mensaje' => 'Problemas para modificar categoria'));
				}
		}
	}

	public function edit($id)
	{
		if(Request::ajax())
		{
			if(Input::get('accion') == 'edit')
			{
				try {
					
					
					
					$validCategory = Categoria::where("nomCategoria", "=", Input::get('nomCategoria'))
									->where("idCategoria", "<>", $id)
									->count();
					if($validCategory > 0)
					{
						return Response::json(array('error' => True, 'mensaje' => 'Categor&iacute;a ya existente, no se puede actualizar'));
					}
					
					
					
					$categoria	= Categoria::find($id);
					$categoria->nomCategoria = Input::get('nomCategoria' );
					if (!$categoria->save()) {
						return Response::json(array('error' => True, 'mensaje' => 'Problemas para modificar categor&iacute;a'));
					}
					return Response::json(array('error' => False, 'mensaje' => 'Actualizando... espere un momento mientras se refresca la pantalla'));
				} catch (Exception $e) {
					return Response::json(array('error' => True, 'mensaje' => 'Problemas para modificar categor&iacute;a'));
				}
			}
			
			if(Input::get('accion') == 'delete')
			{
				try {
					$currentId = $id;
					Categoria::destroy($currentId);
					return Response::json(array('error' => False, 'mensaje' => 'Categor&iacute;a eliminada, espere un momento mientras se refresca la pantalla'));
				} catch (Exception $e) {
					return Response::json(array('error' => True, 'mensaje' => 'ALTO! Categor&iacute;a en uso, no se puede eliminar'));
				}
			}
			
		}
	}

}