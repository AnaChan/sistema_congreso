<?php

//use BaseController;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface;

class CrearAgendaController extends BaseController {

    public function getIndex($id)
    {
        //id del congreso
        $idDC = $id;
        $arrRoles = array();
        $ususario = Auth::User();
        $idUsuario = $ususario->idUsuario;

         //opteniendo el rol del usuario conectado y asegurandome que es pc o chair
        $UsuariorolXCongreso = DB::table('usuariorol_x_congreso')
                                    ->where('idUsuario', $idUsuario)
                                    ->where('idCongreso', $idDC)
                                    ->get();

        foreach ($UsuariorolXCongreso as $UsuariorolXCongreso)
        {
                $idRol = $UsuariorolXCongreso->idRol;
                $rol = DB::table('rol')
                        ->where('idRol','=', $idRol)
                        ->first();
                $nomRol = $rol->nomRol;

                array_push($arrRoles, $nomRol);

        }


       if(in_array("Chair",$arrRoles) == 0 && in_array("PC", $arrRoles) == 0) 
       {
            return Redirect::action('UsuariosCongresosController@establecerCongreso',array($id));

       }
       else
       {
            //$idDC = $id;
            $misUsuarios = Usuario::all();
            $dataTable = '';
            $i = 0;
           
            while ($i < count($misUsuarios)) {

                $dataTable3[] =  array(
                                        'id' =>$misUsuarios[$i]->idUsuario,
                                        'invdate'=>'2010-05-24',
                                        'name'=>$misUsuarios[$i]->NombreUsuario,
                                        'note'=>$misUsuarios[$i]->idUsuario,
                                        'tax'=>$misUsuarios[$i]->idUsuario,
                                        'total'=>$misUsuarios[$i]->idUsuario 
                                        );
                $i++;
            }

            //opteniendo el detalle congreso del congreso 
            $dataTable3 = $this->getEventosAgendados($idDC);
            $detalleCongreso = DB::table('detalle_congreso')
                                    ->where('idCongreso','=', $id)
                                    ->first();
            $idDetalleCongreso = $detalleCongreso->idDetalleCongreso;

            //aulas validas para el congreso
            $aulas_opciones = DB::table('aula')
                            ->where('idDetalleCongreso','=', $idDetalleCongreso)
                            ->orderBy('nomAula', 'asc')
                            ->lists('nomAula','idAula');

            $estilo['1'] = $detalleCongreso->rutaPlantillaDiploma;
            $estilo['2'] = $detalleCongreso->rutaPlantillaDiploma2;
            $estilo['3'] = $detalleCongreso->rutaPlantillaDiploma3;

            $fechaInicio = DB::table('fecha_importante')->join('tipo_fecha','tipo_fecha.idTipoFecha','=','fecha_importante.idTipoFecha')
                ->join('detalle_congreso','detalle_congreso.idDetalleCongreso','=','fecha_importante.idDetalleCongreso')
                ->where('detalle_congreso.idCongreso','=',$id)
                ->where('tipo_fecha.idTipoFecha','6')->first();
            
            $myDate = date('Y-m-d');
            if(count($fechaInicio) > 0) $myDate = strtotime($fechaInicio->fecInicio);

            $iniCongreso['1'] = date('Y', $myDate); $iniCongreso['2'] = date('m', $myDate)-1; $iniCongreso['3'] = date('d', $myDate);
            $this->layout->content = View::make('CrearAgenda',
                array('eventosAgendados' => $dataTable3, 'aulas' => $aulas_opciones, 'idDC' => $idDC,'estilo' => $estilo, 'iniCongreso' => $iniCongreso));
       
       }
        
         
    }

   /*optenienco las presentaciones y las actividades que no han sido agendadas todavia
    Para los pc solo podran ver las presentaciones que son de la tematica que les corresponde
   */
   public function getEventos()
    {
        $idRol = 0;
        $idTematica = 0;
        $actividad = array();        
        $ususario = Auth::User();
        $idUsuario = $ususario->idUsuario;
        $arrRoles = array();
        $jsonRecivido  = Input::get('ACTION');
        $idCongreso    = Input::get('IDCONGRESO');

        $estadoFicha = DB::table('estado_ficha')
                ->where('nombreEstado', '=', "APROBADA")
                ->first();
        $idEstadoFicha = $estadoFicha->idEstadoFicha;


        $detalleCongreso = DB::table('detalle_congreso')
                                ->where('idCongreso','=', $idCongreso)
                                ->first();
        $idDetalleCongreso = $detalleCongreso->idDetalleCongreso;

         $totAulas = DB::table('aula')
                        ->where('idDetalleCongreso','=', $idDetalleCongreso)
                        ->count();

        $totSesiones = DB::table('sesion')
                                ->where('idCongreso','=', $idCongreso)
                                ->count(); 
        if($totAulas > 0 && $totSesiones > 0)
        {

            //opteniendo el rol del usuario conectado y asegurandome que es pc o chair
            $UsuariorolXCongreso = DB::table('usuariorol_x_congreso')
                                    ->where('idUsuario', $idUsuario)
                                    ->where('idCongreso', $idCongreso)
                                    ->get();

            foreach ($UsuariorolXCongreso as $UsuariorolXCongreso)
            {
                $idRol = $UsuariorolXCongreso->idRol;
                $rol = DB::table('rol')
                        ->where('idRol','=', $idRol)
                        ->first();
                $nomRol = $rol->nomRol;

                array_push($arrRoles, $nomRol);

            }

            if(count($UsuariorolXCongreso) > 0)
            {
                $idRol = $UsuariorolXCongreso->idRol;
                $idUsuariorolXCongreso = $UsuariorolXCongreso->idUsuariorolXCongreso;

                if(in_array("Chair",$arrRoles) == 0 && in_array("PC", $arrRoles) == 1)/*$idRol != 1 && $idRol == 4*/
                {
                    $pcXtematica = DB::table('pc_x_tematica')
                                    ->where('idUsuariorolXCongreso','=', $idUsuariorolXCongreso)
                                    ->first();
                    if(isset($pcXtematica->idTematica))
                    {
                        $idTematica = $pcXtematica->idTematica;
                    }

                }
            }

            $cargadoAgenda = "N";

            //opteniendo el id del detalle congreso para saber el congreso del cula debo traer la data
            $detalleCongreso = DB::table('detalle_congreso')->where('idCongreso','=', $idCongreso)->first();
            $idDetCongreso = $detalleCongreso->idDetalleCongreso;

            //PRESENTACIONES
            //$ficha = DB::table('ficha')->where('agendado', '=', false)->get();
            //si el usuario es el pc de la tematica

            if(in_array("PC", $arrRoles) == 1)
            {
                $ficha = DB::table('ficha')
                ->where('agendado', '=', false)
                ->where('idTematica', '=', $idTematica)
                ->where('idDetalleCongreso', '=', $idDetCongreso)
                ->where('idEstadoFicha', '=', $idEstadoFicha)
                ->get();
            }
            else
            {
                //if($idRol > 0)
                //{
                    $ficha = DB::table('ficha')
                    ->where('agendado', '=', false)
                    ->where('idDetalleCongreso', '=', $idDetCongreso)
                    ->where('idEstadoFicha', '=', $idEstadoFicha)
                    ->get();

                //}
            }

            if(count($ficha) > 0)
            {

                $div = "<h4>Presentaciones</h4>";
                $EventosArr[] =  array( 'div' => $div );
                $esActividad = "N";

                foreach ($ficha as $ficha)
                {
                   $titPaper = $ficha->tituloPaper;
                   $idFic = $ficha->idFicha;
                   $tematica = $ficha->idTematica;
                   //nombre de la tematica a la que pertenece
                   $tematicaObj = DB::table('tematica')
                   ->where('idTematica',$tematica)
                   ->first();

                   $nomTematica = $tematicaObj->nomTematica;
                   $div = "<div class='external-event' id='P$idFic' title='$nomTematica'>".$titPaper ."<div hidden>@$idFic</div><div hidden>@$esActividad</div><div hidden>@$cargadoAgenda</div></div>";
                   $EventosArr[] =  array( 'div' => $div );
                 //TODO
                 // var_dump($div);

                }

            }

            //ACTIVIDADES
            //solo el chair puede agendar actividades
            if(in_array("Chair", $arrRoles) == 1)
            {
                $actividad  =   DB::table('actividad')
                    ->where('idCongreso','=',$idCongreso)
                    ->where('actividad.agendado',0)
                    ->orderBy('actividad.titulo')
                    ->get();
                
                if(count($actividad) > 0 )
                {
                    $div = "<h4>Actividades</h4>";
                    $EventosArr[] =  array( 'div' => $div );
                    $esActividad = "S";
                    foreach ($actividad as $actividad)

                    {  $responsableAct = $actividad->responsableAct;
                       $titAct = $actividad->titulo /*. '<br> (' . $actividad->responsableAct .')'*/ ;
                       $idActividad = $actividad->idActividad;
                       $div = "<div class='external-event' id='A$idActividad' title='$responsableAct'>".$titAct ."<div hidden>@$idActividad</div><div hidden>@$esActividad</div><div hidden>@$cargadoAgenda</div></div>";
                       $EventosArr[] =  array( 'div' => $div );

                    }
                }
            }
            if(count($actividad) <= 0 && count($ficha) <= 0 )
            {
               $div = "<h4>No existen datos para ser agendados</h4>";
               $EventosArr[] =  array( 'div' => $div ); 
            }

        }//validacion de aulas y sesiones
        else
        {
            $div = "<h4>Debe existir al menos una sesión y aula<br/> 
            asignada al congreso para poder agendar eventos</h4>";
               $EventosArr[] =  array( 'div' => $div ); 

        }
        $EventosArr = json_encode($EventosArr);
        
        return $EventosArr;
    }

    public function guardarAgendaAdmin()
    {

        $idTematica = 0;
        $idAgenda = 0;
        //valor por defecto de sesion
        //$sesionDef = DB::table('sesion')->first();
        $idSesion = 0;//$sesionDef->idSesion;
        

        $actividad = new Actividad();
        $id = Input::get('IDFICHA');
        $titulo = Input::get('TITULO');

        $fecHoraIni = Input::get('FECINI');
        $fecHoraFin = Input::get('FECFIN');
        $idAula = Input::get('IDAULA');
        $idElemento = (String)Input::get('IDELEMENTO');
        $esActividad = Input::get('ESACTIVIDAD');
        $responsable = "Responsable";
        $enBase = Input::get('ENBASE');
        $idCongreso = Input::get('IDCONGRESO');

        $fecHorIniArr = explode(" ", $fecHoraIni);
        $fecIni = $fecHorIniArr[0];
        $horaIni = $fecHorIniArr[1];

        $aula = DB::table('aula')->where('idAula', $idAula)->first();
        $aulaLabel = $aula->nomAula;

        if($fecHoraFin != "")
        {
            $fecHorFinArr = explode(" ", $fecHoraFin);
            $fecFin = $fecHorFinArr[0];
            $horaFin = $fecHorFinArr[1];
        }
        else
        {
            $fecFin = $fecIni;
            $horaFin = $horaIni;

        }

        $horaFinLabel = strtotime('%I:%M',strtotime($horaFin));
        //opteniendo el id detalle congreso del congreso
        $detalleCongreso = DB::table('detalle_congreso')->where('idCongreso','=', $idCongreso)->first();
        $idDetCongreso = $detalleCongreso->idDetalleCongreso;

        if($esActividad == "N")
        {
             $ficha = DB::table('ficha')
            ->where('idFicha',$id)
            ->where('idDetalleCongreso', $idDetCongreso)
            ->first();

            $idTematica = $ficha->idTematica;
                
            //sacando el id la sesion para la tematica
            $sesion = DB::table('sesion')
            ->where('fecSesion','like', "$fecIni%")
            ->where('horaInicio','<=', $horaIni.":00")
            ->where('horaFin','>=',$horaFin.":00")
            //->where('idTematica',$idTematica)
            ->where('idCongreso',$idCongreso)
            ->first();

            $contSesion = count($sesion);
            if($contSesion > 0)
            {
                $sesionAux = DB::table('sesion')
                ->where('fecSesion','like', "$fecIni%")
                ->where('horaInicio','<=', $horaIni.":00")
                ->where('horaFin','>=',$horaFin.":00")
                ->where('idTematica',$idTematica)
                ->where('idCongreso',$idCongreso)
                ->first();
                $contSesionAux = count($sesionAux);
                if($contSesionAux > 0) $idSesion = $sesionAux->idSesion;
                else $idSesion = $sesion->idSesion;

                //$idSesion = $sesion->idSesion;
                $esActBool = false;
                
                if(count($ficha) > 0)
                {
                    $agendado = $ficha->agendado;
                    $idTematica = $ficha->idTematica;
                    
                    $titulo = $ficha->tituloPaper;
                    if($agendado == true)
                    {
                        $update = true;
                        $presentacion = DB::table('presentacion')->where('idFicha', $id)->first();
                        $idAgenda = $presentacion->idAgenda;
                    }
                    else
                    {
                        $update = false;
                    }
                               
                }
            }//validacion si existen sesiones

        }
        else
        {
            $esActBool = true;
            $actividad = DB::table('actividad')->where('idActividad', $id)
                                               ->where('idCongreso', $idCongreso)
                                               ->first();
            if(count($actividad) > 0)
            {
                $titulo = $actividad->titulo;
                $agendado = $actividad->agendado;
                if($agendado == true)
                {
                    $update = true;
                    $idAgenda = $actividad->idAgenda;

                }
                else
                {
                    $update = false;
                }

            }                                   
            
        }
    if(($esActividad == "N" && $contSesion > 0) || $esActividad == "S")
    {

            if($update == true)
            {
                $agendaAntes = DB::table('agenda')->where('idAgenda', $idAgenda)->first();
                $fecAntes = $agendaAntes->fecha ;
                $horaIniAntes = $agendaAntes->horaInicio;
                $horaFinAntes = $agendaAntes->horaFin;

                if( $fecAntes != $fecIni." 00:00:00" || 
                    $horaIniAntes != $horaIni.":00" || 
                    $horaFinAntes != $horaFin.":00" )
                {
                     DB::table('agenda')
                    ->where('idAgenda', $idAgenda)
                    ->update(array(
                            'fecha' => $fecIni, 
                            'horaInicio' => $horaIni,
                            'horaFin' => $horaFin,
                            'titulo' => $titulo,
                            'idAula' => $idAula,
                            'aulaLabel' => $aulaLabel,
                            'esActividad' => $esActBool,
                            'idElementoHtml' => $idElemento,
                            'idCongreso' => $idCongreso,
                            'idTematica' => $idTematica,
                            'idSesion' => $idSesion
                            ));

                }  

            }
            else
            {
                if($enBase == "N")
                {
                    $idAgenda = DB::table('agenda')
                    ->insertGetId(array(
                            'fecha' => $fecIni, 
                            'horaInicio' => $horaIni,
                            'horaFin' => $horaFin,
                            'titulo' => $titulo,
                            'idAula' => $idAula,
                            'aulaLabel' => $aulaLabel,
                            'esActividad' => $esActBool,
                            'idElementoHtml' => $idElemento,
                            'idCongreso' => $idCongreso,
                            'idTematica' => $idTematica,
                            'idSesion' => $idSesion 
                            ));
                }

            }

            //modificcando ficha a agandado
            if($esActividad == "N")
            {
                DB::table('ficha')
                ->where('idFicha', $id)
                ->update(array('agendado' => true));
                
                DB::table('presentacion')
                ->where('idFicha', $id)
                ->update(array('idAgenda' => $idAgenda,'idSesion' => $idSesion));
            }
            else
            {
                DB::table('actividad')
                ->where('idActividad', $id)
                ->update(array('idAgenda' => $idAgenda,'agendado' => true));
            }
        $dataTable3[] =  array('result' => $idAgenda);
        $dataTable3 = json_encode($dataTable3);
    }
    else
    {
        $fechaErr = date("d/m/Y",strtotime($fecIni));
        $horaIniErr = $horaIni;
        $horafinErr = $horaFin;
        $dataTable3[] =  array('result' => "ErS001",'mensaje' => "No existen sesiones disponibles para $fechaErr de $horaIniErr - $horafinErr");
        $dataTable3 = json_encode($dataTable3);

    }
        
    return $dataTable3;

    }

    public function eliminarAgenda()
    {
        $id = Input::get('ID');
        $esActividad = Input::get('ESACTIVIDAD');
        
        //valor por defecto de agenda
        $agenda = DB::table('agenda')->first();
        $idAgendaDef = $agenda->idAgenda;


        if($esActividad == "S")
        {

            $actividad = DB::table('actividad')->where('idActividad', $id)->first();
            if(count($actividad) > 0)
            {
                $idAgenda = $actividad->idAgenda;

                DB::table('actividad')
                ->where('idActividad', $id)
                ->update(array('idAgenda' => $idAgendaDef, 'agendado' => false));

                if(isset($idAgenda))
                {
                    DB::table('agenda')->where('idAgenda', '=', $idAgenda)->delete();
                } 

            }
            

        }
        else
        {
            $presentacion = DB::table('presentacion')->where('idFicha', $id)->first();
            
            if(count($presentacion) > 0)
            {

                $idAgenda = $presentacion->idAgenda;


                DB::table('presentacion')
                ->where('idFicha', $id)
                ->update(array('idAgenda' => $idAgendaDef)); 

                DB::table('ficha')
                ->where('idFicha', $id)
                ->update(array('agendado' => false));

                if(isset($idAgenda))
                {
                    DB::table('agenda')->where('idAgenda', '=', $idAgenda)->delete();
                }

            }
            


        }

        $dataTable3[] =  array('result' => $idAgenda);
        $dataTable3 = json_encode($dataTable3);
        return $dataTable3;
    }


    public function getEventosAgendados($idCongreso)
    {
        //validaciones segun el usuario
        $idRol = 0;
        $idTematica = 0;
        $ususario = Auth::User();
        $idUsuario = $ususario->idUsuario;

        $arrRoles = array();

        //opteniendo el rol del usuario conectado y asegurandome que es pc o chair
        $UsuariorolXCongreso = DB::table('usuariorol_x_congreso')
                                    ->where('idUsuario', $idUsuario)
                                    ->where('idCongreso', $idCongreso)
                                    ->get();

        foreach ($UsuariorolXCongreso as $UsuariorolXCongreso)
        {
            $idRol = $UsuariorolXCongreso->idRol;
            $rol = DB::table('rol')
                    ->where('idRol','=', $idRol)
                    ->first();
            $nomRol = $rol->nomRol;

            array_push($arrRoles, $nomRol);

        }

        if(count($UsuariorolXCongreso) > 0)
        {
            $idRol = $UsuariorolXCongreso->idRol;
            $idUsuariorolXCongreso = $UsuariorolXCongreso->idUsuariorolXCongreso;

            if(in_array("Chair", $arrRoles) == false && in_array("PC", $arrRoles) == true)
            {
                $pcXtematica = DB::table('pc_x_tematica')
                            ->where('idUsuariorolXCongreso','=', $idUsuariorolXCongreso)
                            ->first();
                if(isset($pcXtematica->idTematica))
                {
                        $idTematica = $pcXtematica->idTematica;
                }
            }

        }

        $dataTable3 = array();
        $fecha = date("Y-m-d");

        if(in_array("PC", $arrRoles) == true)
        {
            //los pc solo pueden tener acceso a las presentaciones de su tematica
                $agenda = DB::table('agenda')
                            ->where('fecha', '>=', $fecha - 7)
                            ->where('fecha', '<=', $fecha + 7)
                            ->where('idCongreso', '=', $idCongreso)
                            ->where('idTematica', '=', $idTematica)
                            ->get();
        }
        else
        {
            //if($idRol > 0)
            //{
                $agenda = DB::table('agenda')
                            ->where('fecha', '>=', $fecha - 7)
                            ->where('fecha', '<=', $fecha + 7)
                            ->where('idCongreso', '=', $idCongreso)
                            ->get();
           // }
        }

        //si existe algo agendado 
        if(count($agenda) > 0)
        {
            foreach ($agenda as $agenda)
            {
                $idAgenda = $agenda->idAgenda;
                $esActividad = $agenda->esActividad;
                $startDate = date("Y-m-d", strtotime($agenda->fecha));
                $horaIni = $agenda->horaInicio;
                $horaFin = $agenda->horaFin;
                $fecHoraIni = $startDate ."T".$horaIni;
                $fecHoraFin = $startDate ."T".$horaFin;
                $hayEventos = false;
                $idAula = $agenda->idAula;

                $aula = DB::table('aula')
                                    ->where('idAula', $idAula)
                                    ->first();
                $aulaLabel = $aula->nomAula;

                if($esActividad == 1)
                {
                    $actividad = DB::table('actividad')
                                ->where('idAgenda', $idAgenda)
                                ->first();

                    if(count($actividad) > 0)
                    {
                        $idFic = $actividad->idActividad;
                        $esActividadLabel= "S";
                        $hayEventos = true;
                        $titulo = $actividad->titulo;
                    }
                }
                else
                {
                    $presentacion = DB::table('presentacion')
                                    ->where('idAgenda', $idAgenda)
                                    ->first();
                    if(count($presentacion) > 0)
                    {
                        $idFic = $presentacion->idFicha;
                        $esActividadLabel= "N";
                        $hayEventos = true;

                        $ficha = DB::table('ficha')
                                    ->where('idFicha', $idFic)
                                    ->first();
                        $titulo = $ficha->tituloPaper;

                    }
                }

                if($hayEventos == true)
                {
                    $cargadoAgenda = "S";
                    //$titulo = $agenda->titulo;
                    $dataTable3[] = array(
                                        'title' => "($aulaLabel) \n".$titulo ,
                                        'start'=> $fecHoraIni,
                                        'end'=> $fecHoraFin,
                                        'allDay'=> false,
                                        'newEventObject'=> "$idFic@$esActividadLabel@$cargadoAgenda"
                                    );

                }

            }

        }//validacion vacio 

        $dataTable3 = json_encode($dataTable3);

        return $dataTable3;
    }

    public function verificarDiplomas($idC)
    {
        try{
            $misFichas = DB::select( DB::raw("select distinct a.titulo,a.fecha,a.horaInicio,a.horaFin, a.AulaLabel, a.idAula,
                                    a.idtematica,b.rutaPresentacion,c.idUsuario,c.responsable,c.rutaDiploma from agenda a 
                                    inner join presentacion b on a.idAgenda = b.idAgenda inner join autor_x_ficha c 
                                    on b.idFicha = c.idFicha where a.idCongreso = '$idC' and a.esActividad = 0"));
            if(count($misFichas)!=0)
                return Response::json(array('error' => False, 'mensaje' => ''));
            else
                return Response::json(array('error' => True, 'mensaje' => '<strong>No se encontraron presentaciones agendadas para este congreso.</strong>'));
        }catch (Exception $e){
            return Response::json(array('error' => True, 'mensaje' => 'Error: ' . $e->getMessage()));    
        }
    }

    public function imprimirDiplomas($idC,$opcion)
    {
        $redirectTo = '';
        $detalleCongreso = DetalleCongreso::where('idCongreso','=',$idC)->firstOrFail();
        switch ($opcion) {
            case 1:
                $rutaImagen = $detalleCongreso->rutaPlantillaDiploma;
                $redirectTo = $this->estilo1Diploma1($idC,$rutaImagen);
                break;
            case 2:
                $rutaImagen = $detalleCongreso->rutaPlantillaDiploma2;
                $redirectTo = $this->estilo1Diploma2($idC,$rutaImagen);
                break;
            case 3:
                $rutaImagen = $detalleCongreso->rutaPlantillaDiploma3;
                $redirectTo = $this->estilo1Diploma1($idC,$rutaImagen);
                break;
        }
        if($redirectTo == ''){
            $this->layout->content = View::make('ImprimirDiplomas',array('mensaje' => 'No se encontraron eventos agendados para este congreso. Puede cerrar esta p&aacute;gina.'));
        }else return Redirect::to('diplomas/'.$redirectTo);        
    }

    public function estilo1Diploma1($idC,$rutaImagen)
    {
        $anchoPagina = 279;
        $altoPagina  = 215; //210 deberia ser el standard

        $area1['salto'] = 40+1;
        $area1['altoCuadro'] = 48.5+1.5;
        $area1['letraTam'] = 60;

        $area2['salto'] = 59+1.5;
        $area2['altoCuadro'] = 31.5+1.5;
        $area2['letraTam'] = 30;

        $area3['salto'] = 38.5+1;
        $area3['altoCuadro'] = 8+0.5;
        $area3['letraTam'] = 14;


        Fpdf::SetRightMargin(13);
        Fpdf::SetLeftMargin(13);

        $diasLetras = array("un","dos","tres","cuatro","cinco","seis","siete","ocho","nueve","diez","once","doce","trece","catorce","quince","dieciséis","diecisiete","dieciocho","diecinueve","veinte","veintiún","veintidós","veintitrés","veinticuatro","veinticinco","veintiséis","veintisiete","veintiocho","veintinueve","treinta","treinta y un");

        $mesesFec   = array("enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre");

        $aniosLetras = array("dos mil catorce","dos mil quince","dos mil dieciséis","dos mil diecisiete","dos mil dieciocho","dos mil diecinueve","dos mil veinte","dos mil veintiún","dos mil veintidós","dos mil veintitrés","dos mil veinticuatro","dos mil veinticinco");
            

        $misFichas = DB::select( DB::raw("select distinct a.titulo,a.fecha,a.horaInicio,a.horaFin, a.AulaLabel, a.idAula,
                                    a.idtematica,b.rutaPresentacion,c.idUsuario,c.responsable,c.rutaDiploma from agenda a 
                                    inner join presentacion b on a.idAgenda = b.idAgenda inner join autor_x_ficha c 
                                    on b.idFicha = c.idFicha where a.idCongreso = '$idC' and a.esActividad = 0 and c.responsable=1"));
        if(count($misFichas)!=0)
        {

            foreach ($misFichas as $objeto)
            {
                $user   = Usuario::find($objeto->idUsuario);

                if(!strrpos($user['nombreUsuario'], " "))
                    $usuarioNombre  = $user['nombreUsuario'];
                else
                    $usuarioNombre  = substr($user['nombreUsuario'],0,strrpos($user['nombreUsuario'], " "));

                if(!strrpos($user['apelUsuario'], " "))
                    $usuarioApellido  = $user['apelUsuario'];
                else
                    $usuarioApellido  = substr($user['apelUsuario'],0,strrpos($user['apelUsuario'], " "));

                Fpdf::AddPage(' L');
                Fpdf::Image($rutaImagen,0.2,0.2,$anchoPagina,$altoPagina);
                
                Fpdf::SetFont('Times','I',$area1['letraTam']);
                Fpdf::Ln($area1['salto']);
                Fpdf::Cell(0,$area1['altoCuadro'],utf8_decode($usuarioNombre) . ' ' . utf8_decode($usuarioApellido),0,0,'C');

                
                Fpdf::SetFont('Times','I',$area2['letraTam']);
                Fpdf::Ln($area2['salto']);
                Fpdf::Cell(0,$area2['altoCuadro'], '"' . utf8_decode($objeto->titulo) . '"',0,0,'C');

                Fpdf::SetFont('Times','',$area3['letraTam']);
                Fpdf::Ln($area3['salto']);
                
                $letraAnio = $aniosLetras[intval(date('Y',strtotime($objeto->fecha)))-2014];

                $fechaEnLetras = 'A los '.$diasLetras[intval(date('d',strtotime($objeto->fecha)))-1].' días del mes de ' . $mesesFec[intval(date('m',strtotime($objeto->fecha)))-1] .' de ' . $letraAnio;

                Fpdf::Cell(0,$area3['altoCuadro'], utf8_decode($fechaEnLetras),0,0,'C');
            }

            $fecha = date('d-m-Y');
            $fileName = 'Congreso_'.$idC.'_'.$fecha.'.pdf';
            $fileRoot = public_path().'/diplomas/'.$fileName;

            $mappedPath = public_path().'/diplomas';
            File::makeDirectory($mappedPath, $mode = 0777, true, true);

            /*if (!file_exists(public_path().'/diplomas')) {
                mkdir(public_path().'/diplomas', 0777, true);
            }*/
            
            Fpdf::Output($fileRoot,'F');
            return $fileName;
        }else return '';
    }

    public function estilo1Diploma2($idC,$rutaImagen)
    {
        /*$anchoPagina = 279;
        $altoPagina  = 215; //210 deberia ser el standard*/

        $anchoPagina = 216;
        $altoPagina  = 279;

        $area1['salto'] = 72.5;
        $area1['altoCuadro'] = 38;
        $area1['letraTam'] = 40;

        $area2['salto'] = 48.5;
        $area2['altoCuadro'] = 25;
        $area2['letraTam'] = 30;

        $area3['salto'] = 40.5;
        $area3['altoCuadro'] = 12.5;
        $area3['letraTam'] = 14;


        Fpdf::SetRightMargin(6);
        Fpdf::SetLeftMargin(6.5);

        $diasLetras = array("un","dos","tres","cuatro","cinco","seis","siete","ocho","nueve","diez","once","doce","trece","catorce","quince","dieciséis","diecisiete","dieciocho","diecinueve","veinte","veintiún","veintidós","veintitrés","veinticuatro","veinticinco","veintiséis","veintisiete","veintiocho","veintinueve","treinta","treinta y un");

        $mesesFec   = array("enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre");

        $aniosLetras = array("dos mil catorce","dos mil quince","dos mil dieciséis","dos mil diecisiete","dos mil dieciocho","dos mil diecinueve","dos mil veinte","dos mil veintiún","dos mil veintidós","dos mil veintitrés","dos mil veinticuatro","dos mil veinticinco");
            

        $misFichas = DB::select( DB::raw("select distinct a.titulo,a.fecha,a.horaInicio,a.horaFin, a.AulaLabel, a.idAula,
                                    a.idtematica,b.rutaPresentacion,c.idUsuario,c.responsable,c.rutaDiploma from agenda a 
                                    inner join presentacion b on a.idAgenda = b.idAgenda inner join autor_x_ficha c 
                                    on b.idFicha = c.idFicha where a.idCongreso = '$idC' and a.esActividad = 0 and c.responsable=1"));
        if(count($misFichas)!=0)
        {

            foreach ($misFichas as $objeto)
            {
                $user   = Usuario::find($objeto->idUsuario);
                
                if(!strrpos($user['nombreUsuario'], " "))
                    $usuarioNombre  = $user['nombreUsuario'];
                else
                    $usuarioNombre  = substr($user['nombreUsuario'],0,strrpos($user['nombreUsuario'], " "));

                if(!strrpos($user['apelUsuario'], " "))
                    $usuarioApellido  = $user['apelUsuario'];
                else
                    $usuarioApellido  = substr($user['apelUsuario'],0,strrpos($user['apelUsuario'], " "));

                Fpdf::AddPage('P');
                Fpdf::Image($rutaImagen,0.2,0.2,$anchoPagina,$altoPagina);
                
                Fpdf::SetFont('Times','I',$area1['letraTam']);
                Fpdf::Ln($area1['salto']);
                Fpdf::Cell(0,$area1['altoCuadro'],utf8_decode($usuarioNombre) . ' ' . utf8_decode($usuarioApellido),0,0,'C');

                
                Fpdf::SetFont('Times','I',$area2['letraTam']);
                Fpdf::Ln($area2['salto']);
                Fpdf::Cell(0,$area2['altoCuadro'], '"' . utf8_decode($objeto->titulo) . '"',0,0,'C');

                Fpdf::SetFont('Times','',$area3['letraTam']);
                Fpdf::Ln($area3['salto']);
                
                $letraAnio = $aniosLetras[intval(date('Y',strtotime($objeto->fecha)))-2014];

                $fechaEnLetras = 'A los '.$diasLetras[intval(date('d',strtotime($objeto->fecha)))-1].' días del mes de ' . $mesesFec[intval(date('m',strtotime($objeto->fecha)))-1] .' de ' . $letraAnio;

                Fpdf::Cell(0,$area3['altoCuadro'], utf8_decode($fechaEnLetras),0,0,'C');
            }

            $fecha = date('d-m-Y');
            $fileName = 'Congreso_'.$idC.'_'.$fecha.'.pdf';
            $fileRoot = public_path().'/diplomas/'.$fileName;

            $mappedPath = public_path().'/diplomas';
            File::makeDirectory($mappedPath, $mode = 0777, true, true);
            Fpdf::Output($fileRoot,'F');
            return $fileName;
        }else return '';
    }

    public function verificarAgenda($idC)
    {
        try{
            $actividadesCheck = DB::select( DB::raw("select idAgenda from agenda where idCongreso = '$idC'"));
            $numeroActividades = count($actividadesCheck);
            if($numeroActividades!=0)
                return Response::json(array('error' => False, 'mensaje' => ''));
            else
                return Response::json(array('error' => True, 'mensaje' => '<strong>No se encontraron eventos agendados para este congreso.</strong>'));
        }catch (Exception $e){
            return Response::json(array('error' => True, 'mensaje' => 'Error: ' . $e->getMessage()));    
        }
    }

    public function imprimirAgenda($idC)
    {
        try{
            $actividadesCheck = DB::select( DB::raw("select idAgenda from agenda where idCongreso = '$idC'"));
            $numeroActividades = count($actividadesCheck);
            if($numeroActividades!=0)
            {
                $mesesFec  = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
                $actividadesDias = DB::select( DB::raw("select distinct fecha from agenda where idCongreso = '$idC' order by fecha"));
                $diaNumero = 1;
                foreach ($actividadesDias as $dailyObj)
                {
                    $x = 10;
                    $y = 15;

                    Fpdf::AddPage('L');
                    Fpdf::SetFont('Arial','B',15);
                    $mesCongreso = $mesesFec[intval(date('m',strtotime($dailyObj->fecha)))-1];
                    Fpdf::Cell(262,0,'DIA ' . $diaNumero . ': ' . date('d',strtotime($dailyObj->fecha)) . ' ' . $mesCongreso ,0,0,'C');
                    $diaNumero = $diaNumero + 1;

                    Fpdf::setXY($x,$y);
                    Fpdf::SetFillColor(255,255,255);
                    Fpdf::SetTextColor(0);

                    $fechaX = $dailyObj->fecha;
                    $actividades = DB::select( DB::raw("select distinct a.titulo,a.fecha,a.horaInicio,a.horaFin, a.AulaLabel, a.idAula,a.idAgenda, a.idtematica from agenda a where a.idCongreso = '$idC' and a.esActividad = 1  and a.fecha like '$fechaX%' order by a.horaInicio"));
            
                    $altoFrame = 5;
                    $dibujarRecuadro = 1;
                    $fontSize = 9;

                    Fpdf::SetFont('Arial','B',$fontSize);
                    Fpdf::Cell(30,$altoFrame,'Hora',$dibujarRecuadro,0,'C');
                    Fpdf::SetFont('Arial','',$fontSize);
                    Fpdf::Cell(132,$altoFrame,'Actividades',$dibujarRecuadro,0,'C');
                    Fpdf::Cell(60,$altoFrame,'Responsable',$dibujarRecuadro,0,'C');
                    Fpdf::Cell(40,$altoFrame,'Lugar',$dibujarRecuadro,1,'C');

                    Fpdf::Ln(1);
                
                    Fpdf::SetWidths(array(30,132,60,40,25,25,25,25,25,25));
                    Fpdf::SetAligns(array('C','C','C','C','C','C','C','C','C','C'));
                    
                    $detalleAct = Array($numeroActividades);
                    $iteratorDet = 0;
                    foreach ($actividades as $objeto)
                    {
                        $actualIDAgenda = $objeto->idAgenda;
                        $infoActividad = Actividad::where('idCongreso',$idC)->where('idAgenda',$actualIDAgenda)->firstOrFail();
                        
                        $nomAct = '';
                        $nomResp = '';
                        if(count($infoActividad) != 0)
                        {
                            $nomAct = $infoActividad->titulo;
                            $nomResp = $infoActividad->responsableAct;
                        }

                        $aulaIDAgenda = $objeto->idAula;
                        $infoActividadAula = Aula::where('idAula',$aulaIDAgenda)->first();
                        $aulaActividad = (count($infoActividadAula) == 0)? 'NA' : $infoActividadAula->nomAula;

                        $detalleAct[$iteratorDet] = array(date("h:i",strtotime($objeto->horaInicio)) . ' | ' . date("h:i",strtotime($objeto->horaFin)) ,$nomAct,$nomResp,$aulaActividad);
                        Fpdf::Row($detalleAct[$iteratorDet]);
                    }
                    Fpdf::SetFont('Arial','B',$fontSize);
                    Fpdf::Ln(1);

                    $sesionesCounter = DB::select( DB::raw("select distinct horaInicio from sesion a where a.idCongreso = '$idC' and fecSesion like '$fechaX%' order by a.horaInicio"));
                    if(count($sesionesCounter)!=0)
                    {
                        $saltoDeLinea = chr(13) . chr(10);
                        foreach ($sesionesCounter as $sesionXHora)
                        {
                            $horaPivote = $sesionXHora->horaInicio;

                            $sesiones = DB::select( DB::raw("select fecSesion, horaInicio, horaFin, moderador, idTematica, nombreSesion, a.idSesion from sesion a where a.horaInicio = '$horaPivote' and a.fecSesion like '$fechaX%'"));

                            
                            $anchoColumna = 232/count($sesiones);

                            $misSesiones[1] = '0';
                            $misSesiones[2] = '0';
                            $misSesiones[3] = '0';
                            $misSesiones[4] = '0';
                            $misSesiones[5] = '0';
                            $misSesiones[6] = '0';
                            $misSesiones[7] = '0';
                            $misSesiones[8] = '0';
                            $misSesiones[9] = '0';

                            $contadorSesion = 1;
                            $dataTableHeaders[0] =  array('Sesiones','','','','','','','','','');

                            foreach ($sesiones as $sesion)
                            {
                                $misSesiones[$contadorSesion] = (string)$sesion->idSesion;
                                $contadorSesion++;
                                
                                $tematicaObj = DB::table('tematica')
                                ->where('idTematica',$sesion->idTematica)
                                ->first();
                                $tematicaLabel = $tematicaObj->nomTematica;

                                $dataTableHeaders[0][$contadorSesion-1] = utf8_decode('Sesión: ' . $sesion->nombreSesion . $saltoDeLinea . ' Moderador: ' . $sesion->moderador . $saltoDeLinea . ' Temática: ' . $tematicaLabel );
                            }

                            Fpdf::SetFillColor(255,255,255);
                            Fpdf::SetFont('Times','B',$fontSize-2);
                            Fpdf::SetWidths(array(30,$anchoColumna,$anchoColumna,$anchoColumna,$anchoColumna,$anchoColumna,$anchoColumna,$anchoColumna,$anchoColumna,$anchoColumna));
                            
                            for ($quitarExtras = count($sesiones) + 1; $quitarExtras <= 9; $quitarExtras++)
                            {
                                array_splice($dataTableHeaders[0], $quitarExtras);
                            }
                            Fpdf::Row($dataTableHeaders[0]);
                            //Fpdf::Ln($altoFrame + 1);

                            $detalleSesion = Sesion::where('idCongreso','=',$idC)->where('horaInicio','like',"$horaPivote%" )->firstOrFail();
                            $horaFin       = $detalleSesion->horaFin;
                            $rangoHora = date("h:i A",strtotime($horaPivote)) . chr(13).chr(10) . date("h:i A",strtotime($horaFin));

                            Fpdf::SetFillColor(255,255,255);
                            Fpdf::SetWidths(array(30,$anchoColumna,$anchoColumna,$anchoColumna,$anchoColumna,$anchoColumna,$anchoColumna,$anchoColumna,$anchoColumna,$anchoColumna));
                            Fpdf::SetFont('Arial','',$fontSize);

                            $columna1 = '';
                            $columna2 = '';
                            $columna3 = '';
                            $columna4 = '';
                            $columna5 = '';
                            $columna6 = '';
                            $columna7 = '';
                            $columna8 = '';
                            $columna9 = '';

                            $saltoDeLinea = chr(13) . chr(10);
                            $inicioSesion = 'Introducción, presentación del tema' . $saltoDeLinea . $saltoDeLinea;
                            $finalSesion  = $saltoDeLinea . 'Comentarios generales';

                            $dataTable[0] =  array($rangoHora,'','','','','','','','','');


                            //******** 1 **********
                            $tempSesion = $misSesiones[1];
                            $dataAgenda = DB::select( DB::raw("select horaInicio, horaFin, aulaLabel,titulo,idTematica,idSesion from agenda where idCongreso = '$idC' and idSesion = '$tempSesion' and esActividad = 0"));
                            $iDataAgenda = 0;
                            while ($iDataAgenda < count($dataAgenda)) {
                                $columna1 = $columna1 . $dataAgenda[$iDataAgenda]->titulo . ' - Aula: ' . $dataAgenda[$iDataAgenda]->aulaLabel.$saltoDeLinea  ;
                                $iDataAgenda++;
                            }
                            $columna1 = ($columna1 == '') ? '' : utf8_decode($inicioSesion . $columna1 . $finalSesion);

                            //******** 2 **********
                            $tempSesion = $misSesiones[2];
                            $dataAgenda = DB::select( DB::raw("select horaInicio, horaFin, aulaLabel,titulo,idTematica,idSesion from agenda where idCongreso = '$idC' and idSesion = '$tempSesion' and esActividad = 0"));
                            $iDataAgenda = 0;
                            while ($iDataAgenda < count($dataAgenda)) {
                                $columna2 = $columna2 . $dataAgenda[$iDataAgenda]->titulo  . ' - Aula: ' . $dataAgenda[$iDataAgenda]->aulaLabel.$saltoDeLinea ;
                                $iDataAgenda++;
                            }
                            $columna2 = ($columna2 == '') ? '' : utf8_decode($inicioSesion . $columna2 . $finalSesion);

                            //******** 3 **********
                            $tempSesion = $misSesiones[3];
                            $dataAgenda = DB::select( DB::raw("select horaInicio, horaFin, aulaLabel,titulo,idTematica,idSesion from agenda where idCongreso = '$idC' and idSesion = '$tempSesion' and esActividad = 0"));
                            $iDataAgenda = 0;
                            while ($iDataAgenda < count($dataAgenda)) {
                                $columna3 = $columna3 . $dataAgenda[$iDataAgenda]->titulo  . ' - Aula: ' . $dataAgenda[$iDataAgenda]->aulaLabel.$saltoDeLinea ;
                                $iDataAgenda++;
                            }
                            $columna3 = ($columna3 == '') ? '' : utf8_decode($inicioSesion . $columna3 . $finalSesion);

                            //******** 4 **********
                            $tempSesion = $misSesiones[4];
                            $dataAgenda = DB::select( DB::raw("select horaInicio, horaFin, aulaLabel,titulo,idTematica,idSesion from agenda where idCongreso = '$idC' and idSesion = '$tempSesion' and esActividad = 0"));
                            $iDataAgenda = 0;
                            while ($iDataAgenda < count($dataAgenda)) {
                                $columna4 = $columna4 . $dataAgenda[$iDataAgenda]->titulo  . ' - Aula: ' . $dataAgenda[$iDataAgenda]->aulaLabel.$saltoDeLinea ;
                                $iDataAgenda++;
                            }
                            $columna4 = ($columna4 == '') ? '' : utf8_decode($inicioSesion . $columna4 . $finalSesion);

                            //******** 5 **********
                            $tempSesion = $misSesiones[5];
                            $dataAgenda = DB::select( DB::raw("select horaInicio, horaFin, aulaLabel,titulo,idTematica,idSesion from agenda where idCongreso = '$idC' and idSesion = '$tempSesion' and esActividad = 0"));
                            $iDataAgenda = 0;
                            while ($iDataAgenda < count($dataAgenda)) {
                                $columna5 = $columna5 . $dataAgenda[$iDataAgenda]->titulo  . ' - Aula: ' . $dataAgenda[$iDataAgenda]->aulaLabel.$saltoDeLinea ;
                                $iDataAgenda++;
                            }
                            $columna5 = ($columna5 == '') ? '' : utf8_decode($inicioSesion . $columna5 . $finalSesion);

                            //******** 6 **********
                            $tempSesion = $misSesiones[6];
                            $dataAgenda = DB::select( DB::raw("select horaInicio, horaFin, aulaLabel,titulo,idTematica,idSesion from agenda where idCongreso = '$idC' and idSesion = '$tempSesion' and esActividad = 0"));
                            $iDataAgenda = 0;
                            while ($iDataAgenda < count($dataAgenda)) {
                                $columna6 = $columna6 . $dataAgenda[$iDataAgenda]->titulo  . ' - Aula: ' . $dataAgenda[$iDataAgenda]->aulaLabel.$saltoDeLinea ;
                                $iDataAgenda++;
                            }
                            $columna6 = ($columna6 == '') ? '' : utf8_decode($inicioSesion . $columna6 . $finalSesion);

                            //******** 7 **********
                            $tempSesion = $misSesiones[7];
                            $dataAgenda = DB::select( DB::raw("select horaInicio, horaFin, aulaLabel,titulo,idTematica,idSesion from agenda where idCongreso = '$idC' and idSesion = '$tempSesion' and esActividad = 0"));
                            $iDataAgenda = 0;
                            while ($iDataAgenda < count($dataAgenda)) {
                                $columna7 = $columna7 . $dataAgenda[$iDataAgenda]->titulo  . ' - Aula: ' . $dataAgenda[$iDataAgenda]->aulaLabel.$saltoDeLinea ;
                                $iDataAgenda++;
                            }
                            $columna7 = ($columna7 == '') ? '' : utf8_decode($inicioSesion . $columna7 . $finalSesion);

                            //******** 8 **********
                            $tempSesion = $misSesiones[8];
                            $dataAgenda = DB::select( DB::raw("select horaInicio, horaFin, aulaLabel,titulo,idTematica,idSesion from agenda where idCongreso = '$idC' and idSesion = '$tempSesion' and esActividad = 0"));
                            $iDataAgenda = 0;
                            while ($iDataAgenda < count($dataAgenda)) {
                                $columna8 = $columna8 . $dataAgenda[$iDataAgenda]->titulo  . ' - Aula: ' . $dataAgenda[$iDataAgenda]->aulaLabel.$saltoDeLinea ;
                                $iDataAgenda++;
                            }
                            $columna8 = ($columna8 == '') ? '' : utf8_decode($inicioSesion . $columna8 . $finalSesion);

                            //******** 9 **********
                            $tempSesion = $misSesiones[9];
                            $dataAgenda = DB::select( DB::raw("select horaInicio, horaFin, aulaLabel,titulo,idTematica,idSesion from agenda where idCongreso = '$idC' and idSesion = '$tempSesion' and esActividad = 0"));
                            $iDataAgenda = 0;
                            while ($iDataAgenda < count($dataAgenda)) {
                                $columna9 = $columna9 . $dataAgenda[$iDataAgenda]->titulo  . ' - Aula: ' . $dataAgenda[$iDataAgenda]->aulaLabel.$saltoDeLinea ;
                                $iDataAgenda++;
                            }
                            $columna9 = ($columna9 == '') ? '' : utf8_decode($inicioSesion . $columna9 . $finalSesion);

                            $dataTable[0] =  array($rangoHora,$columna1,$columna2,$columna3,$columna4,$columna5,$columna6,$columna7,$columna8,$columna9);
                            for ($quitarExtras = count($sesiones) + 1; $quitarExtras <= 9; $quitarExtras++)
                            {
                                array_splice($dataTable[0], $quitarExtras);
                            }
                            Fpdf::Row($dataTable[0]);
                        }
                    }
                }

                $fecha = date('d-m-Y');
                $fileName = 'Agenda_'.$idC.'_'.$fecha.'.pdf';
                $fileRoot = public_path().'/agenda/'.$fileName;

                
                $mappedPath = public_path().'/agenda';
                File::makeDirectory($mappedPath, $mode = 0777, true, true);
                Fpdf::Output($fileRoot,'F');
                return Redirect::to('agenda/'.$fileName);
            }else  $this->layout->content = View::make('ImprimirDiplomas',array('mensaje' => 'No se encontraron eventos agendados para este congreso. Puede cerrar esta p&aacute;gina.'));
        }catch (Exception $e){
            $this->layout->content = View::make('ImprimirDiplomas',array('mensaje' => 'Problemas para generar el reporte. Contacte al administrador. >' . $e->getMessage()));
        }
    }

public function exportarAgenda($idC)
    {
        try{
            $agendaXLS = "";
            $actividadesCheck = DB::select( DB::raw("select idAgenda from agenda where idCongreso = '$idC'"));
            $numeroActividades = count($actividadesCheck);
            if($numeroActividades!=0)
            {
                $mesesFec  = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
                $actividadesDias = DB::select( DB::raw("select distinct fecha from agenda where idCongreso = '$idC' order by fecha"));
                $diaNumero = 1;

                $agendaXLS .= "<table width=700>";
                foreach ($actividadesDias as $dailyObj)
                {
                    $x = 10;
                    $y = 15;

                    $mesCongreso = $mesesFec[intval(date('m',strtotime($dailyObj->fecha)))-1];

                    $agendaXLS .= "<tr><th>DIA ".$diaNumero.": ".date('d',strtotime($dailyObj->fecha))." ".$mesCongreso."</th></tr>";
                    $diaNumero = $diaNumero + 1;

                    $fechaX = $dailyObj->fecha;
                    $actividades = DB::select( DB::raw("select distinct a.titulo,a.fecha,a.horaInicio,a.horaFin, a.AulaLabel, a.idAula,a.idAgenda, a.idtematica from agenda a where a.idCongreso = '$idC' and a.esActividad = 1  and a.fecha like '$fechaX%' order by a.horaInicio"));
            
                    $altoFrame = 5;
                    $dibujarRecuadro = 1;
                    $fontSize = 9;

                    $agendaXLS .= "<tr><td>";
                    $agendaXLS .= "<table border=1><tr>";
                    $agendaXLS .= "<th>Hora</th><th>Actividades</th><th>Responsable</th><th>Lugar</th>";
                    $agendaXLS .= "</tr>";

                    Fpdf::Ln(1);
                
                    Fpdf::SetWidths(array(30,132,60,40,25,25,25,25,25,25));
                    Fpdf::SetAligns(array('C','C','C','C','C','C','C','C','C','C'));
                    
                    $detalleAct = Array($numeroActividades);
                    $iteratorDet = 0;
                    foreach ($actividades as $objeto)
                    {
                        $actualIDAgenda = $objeto->idAgenda;
                        $infoActividad = Actividad::where('idCongreso',$idC)->where('idAgenda',$actualIDAgenda)->firstOrFail();
                        
                        $nomAct = '';
                        $nomResp = '';
                        if(count($infoActividad) != 0)
                        {
                            $nomAct = $infoActividad->titulo;
                            $nomResp = $infoActividad->responsableAct;
                        }

                        $detalleAct[$iteratorDet] = array(date("h:i",strtotime($objeto->horaInicio)) . ' | ' . date("h:i",strtotime($objeto->horaFin)) ,$nomAct,$nomResp,$objeto->AulaLabel);
                        $agendaXLS .= "<tr>";
                        $agendaXLS .= "<td align=center>".date("h:i",strtotime($objeto->horaInicio))." | ".date("h:i",strtotime($objeto->horaFin))."</td>";
                        $agendaXLS .= "<td align=center>$nomAct";
                        $agendaXLS .= "<td align=center>$nomResp</td>";
                        $agendaXLS .= "<td align=center>".$objeto->AulaLabel."</td>";
                        $agendaXLS .= "</tr>";
                    }
                    $agendaXLS .= "</table></td></tr>";

                    $sesionesCounter = DB::select( DB::raw("select distinct horaInicio from sesion a where a.idCongreso = '$idC' and fecSesion like '$fechaX%' order by a.horaInicio"));
                    if(count($sesionesCounter)!=0)
                    {
                        $agendaXLS .= "<tr><td>";
                        $agendaXLS .= "<table border=1>";

                        $saltoDeLinea = '<br>';
                        foreach ($sesionesCounter as $sesionXHora)
                        {
                            $horaPivote = $sesionXHora->horaInicio;

                            $sesiones = DB::select( DB::raw("select fecSesion, horaInicio, horaFin, moderador, idTematica, nombreSesion, a.idSesion from sesion a where a.horaInicio = '$horaPivote' and a.fecSesion like '$fechaX%'"));

                            
                            $anchoColumna = 232/count($sesiones);

                            $misSesiones[1] = '0';
                            $misSesiones[2] = '0';
                            $misSesiones[3] = '0';
                            $misSesiones[4] = '0';
                            $misSesiones[5] = '0';
                            $misSesiones[6] = '0';
                            $misSesiones[7] = '0';
                            $misSesiones[8] = '0';
                            $misSesiones[9] = '0';

                            $contadorSesion = 1;
                            $dataTableHeaders[0] =  array('Sesiones','','','','','','','','','');

                            foreach ($sesiones as $sesion)
                            {

                                $misSesiones[$contadorSesion] = (string)$sesion->idSesion;
                                $contadorSesion++;
                                
                                $tematicaObj = DB::table('tematica')
                                ->where('idTematica',$sesion->idTematica)
                                ->first();
                                $tematicaLabel = $tematicaObj->nomTematica;

                                $dataTableHeaders[0][$contadorSesion-1] = 'Sesión: ' . $sesion->nombreSesion . $saltoDeLinea . ' Moderador: ' . $sesion->moderador . $saltoDeLinea . ' Temática: ' . $tematicaLabel;
                            }

                            Fpdf::SetFillColor(255,255,255);
                            Fpdf::SetFont('Times','B',$fontSize-2);
                            Fpdf::SetWidths(array(30,$anchoColumna,$anchoColumna,$anchoColumna,$anchoColumna,$anchoColumna,$anchoColumna,$anchoColumna,$anchoColumna,$anchoColumna));
                            
                            for ($quitarExtras = count($sesiones) + 1; $quitarExtras <= 9; $quitarExtras++)
                            {
                                array_splice($dataTableHeaders[0], $quitarExtras);
                            }
                            Fpdf::Row($dataTableHeaders[0]);

                            $agendaXLS .= "<tr>";
                            $alternante = 0;
                            foreach ($dataTableHeaders[0] as $valor) {
                                if ($alternante == 0) {
                                    $agendaXLS .= "<td align=center>$valor</td>";
                                    $alternante = 1;
                                }else{
                                    $agendaXLS .= "<td colspan=3 align=center >$valor</td>";
                                    $alternante = 0;
                                }
                            }
                            $agendaXLS .= "</tr>";

                            $detalleSesion = Sesion::where('idCongreso','=',$idC)->where('horaInicio','like',"$horaPivote%" )->firstOrFail();
                            $horaFin       = $detalleSesion->horaFin;
                            $rangoHora = date("h:i A",strtotime($horaPivote)) . '<br>' . date("h:i A",strtotime($horaFin));

                            Fpdf::SetFillColor(255,255,255);
                            Fpdf::SetWidths(array(30,$anchoColumna,$anchoColumna,$anchoColumna,$anchoColumna,$anchoColumna,$anchoColumna,$anchoColumna,$anchoColumna,$anchoColumna));
                            Fpdf::SetFont('Arial','',$fontSize);

                            $columna1 = '';
                            $columna2 = '';
                            $columna3 = '';
                            $columna4 = '';
                            $columna5 = '';
                            $columna6 = '';
                            $columna7 = '';
                            $columna8 = '';
                            $columna9 = '';

                            $saltoDeLinea = '<br>';
                            $inicioSesion = 'Introducción, presentación del tema' . $saltoDeLinea . $saltoDeLinea;
                            $finalSesion  = $saltoDeLinea . 'Comentarios generales';

                            $dataTable[0] =  array($rangoHora,'','','','','','','','','');


                            //******** 1 **********
                            $tempSesion = $misSesiones[1];
                            $dataAgenda = DB::select( DB::raw("select horaInicio, horaFin, aulaLabel,titulo,idTematica,idSesion from agenda where idCongreso = '$idC' and idSesion = '$tempSesion' and esActividad = 0"));
                            $iDataAgenda = 0;
                            while ($iDataAgenda < count($dataAgenda)) {
                                $columna1 = $columna1 . $dataAgenda[$iDataAgenda]->titulo . ' - Aula: ' . $dataAgenda[$iDataAgenda]->aulaLabel.$saltoDeLinea  ;
                                $iDataAgenda++;
                            }
                            $columna1 = ($columna1 == '') ? '' : $inicioSesion . $columna1 . $finalSesion;

                            //******** 2 **********
                            $tempSesion = $misSesiones[2];
                            $dataAgenda = DB::select( DB::raw("select horaInicio, horaFin, aulaLabel,titulo,idTematica,idSesion from agenda where idCongreso = '$idC' and idSesion = '$tempSesion' and esActividad = 0"));
                            $iDataAgenda = 0;
                            while ($iDataAgenda < count($dataAgenda)) {
                                $columna2 = $columna2 . $dataAgenda[$iDataAgenda]->titulo  . ' - Aula: ' . $dataAgenda[$iDataAgenda]->aulaLabel.$saltoDeLinea ;
                                $iDataAgenda++;
                            }
                            $columna2 = ($columna2 == '') ? '' : $inicioSesion . $columna2 . $finalSesion;

                            //******** 3 **********
                            $tempSesion = $misSesiones[3];
                            $dataAgenda = DB::select( DB::raw("select horaInicio, horaFin, aulaLabel,titulo,idTematica,idSesion from agenda where idCongreso = '$idC' and idSesion = '$tempSesion' and esActividad = 0"));
                            $iDataAgenda = 0;
                            while ($iDataAgenda < count($dataAgenda)) {
                                $columna3 = $columna3 . $dataAgenda[$iDataAgenda]->titulo  . ' - Aula: ' . $dataAgenda[$iDataAgenda]->aulaLabel.$saltoDeLinea ;
                                $iDataAgenda++;
                            }
                            $columna3 = ($columna3 == '') ? '' : $inicioSesion . $columna3 . $finalSesion;

                            //******** 4 **********
                            $tempSesion = $misSesiones[4];
                            $dataAgenda = DB::select( DB::raw("select horaInicio, horaFin, aulaLabel,titulo,idTematica,idSesion from agenda where idCongreso = '$idC' and idSesion = '$tempSesion' and esActividad = 0"));
                            $iDataAgenda = 0;
                            while ($iDataAgenda < count($dataAgenda)) {
                                $columna4 = $columna4 . $dataAgenda[$iDataAgenda]->titulo  . ' - Aula: ' . $dataAgenda[$iDataAgenda]->aulaLabel.$saltoDeLinea ;
                                $iDataAgenda++;
                            }
                            $columna4 = ($columna4 == '') ? '' : $inicioSesion . $columna4 . $finalSesion;

                            //******** 5 **********
                            $tempSesion = $misSesiones[5];
                            $dataAgenda = DB::select( DB::raw("select horaInicio, horaFin, aulaLabel,titulo,idTematica,idSesion from agenda where idCongreso = '$idC' and idSesion = '$tempSesion' and esActividad = 0"));
                            $iDataAgenda = 0;
                            while ($iDataAgenda < count($dataAgenda)) {
                                $columna5 = $columna5 . $dataAgenda[$iDataAgenda]->titulo  . ' - Aula: ' . $dataAgenda[$iDataAgenda]->aulaLabel.$saltoDeLinea ;
                                $iDataAgenda++;
                            }
                            $columna5 = ($columna5 == '') ? '' : $inicioSesion . $columna5 . $finalSesion;

                            //******** 6 **********
                            $tempSesion = $misSesiones[6];
                            $dataAgenda = DB::select( DB::raw("select horaInicio, horaFin, aulaLabel,titulo,idTematica,idSesion from agenda where idCongreso = '$idC' and idSesion = '$tempSesion' and esActividad = 0"));
                            $iDataAgenda = 0;
                            while ($iDataAgenda < count($dataAgenda)) {
                                $columna6 = $columna6 . $dataAgenda[$iDataAgenda]->titulo  . ' - Aula: ' . $dataAgenda[$iDataAgenda]->aulaLabel.$saltoDeLinea ;
                                $iDataAgenda++;
                            }
                            $columna6 = ($columna6 == '') ? '' : $inicioSesion . $columna6 . $finalSesion;

                            //******** 7 **********
                            $tempSesion = $misSesiones[7];
                            $dataAgenda = DB::select( DB::raw("select horaInicio, horaFin, aulaLabel,titulo,idTematica,idSesion from agenda where idCongreso = '$idC' and idSesion = '$tempSesion' and esActividad = 0"));
                            $iDataAgenda = 0;
                            while ($iDataAgenda < count($dataAgenda)) {
                                $columna7 = $columna7 . $dataAgenda[$iDataAgenda]->titulo  . ' - Aula: ' . $dataAgenda[$iDataAgenda]->aulaLabel.$saltoDeLinea ;
                                $iDataAgenda++;
                            }
                            $columna7 = ($columna7 == '') ? '' : $inicioSesion . $columna7 . $finalSesion;

                            //******** 8 **********
                            $tempSesion = $misSesiones[8];
                            $dataAgenda = DB::select( DB::raw("select horaInicio, horaFin, aulaLabel,titulo,idTematica,idSesion from agenda where idCongreso = '$idC' and idSesion = '$tempSesion' and esActividad = 0"));
                            $iDataAgenda = 0;
                            while ($iDataAgenda < count($dataAgenda)) {
                                $columna8 = $columna8 . $dataAgenda[$iDataAgenda]->titulo  . ' - Aula: ' . $dataAgenda[$iDataAgenda]->aulaLabel.$saltoDeLinea ;
                                $iDataAgenda++;
                            }
                            $columna8 = ($columna8 == '') ? '' : $inicioSesion . $columna8 . $finalSesion;

                            //******** 9 **********
                            $tempSesion = $misSesiones[9];
                            $dataAgenda = DB::select( DB::raw("select horaInicio, horaFin, aulaLabel,titulo,idTematica,idSesion from agenda where idCongreso = '$idC' and idSesion = '$tempSesion' and esActividad = 0"));
                            $iDataAgenda = 0;
                            while ($iDataAgenda < count($dataAgenda)) {
                                $columna9 = $columna9 . $dataAgenda[$iDataAgenda]->titulo  . ' - Aula: ' . $dataAgenda[$iDataAgenda]->aulaLabel.$saltoDeLinea ;
                                $iDataAgenda++;
                            }
                            $columna9 = ($columna9 == '') ? '' : $inicioSesion . $columna9 . $finalSesion;

                            $dataTable[0] =  array($rangoHora,$columna1,$columna2,$columna3,$columna4,$columna5,$columna6,$columna7,$columna8,$columna9);
                            for ($quitarExtras = count($sesiones) + 1; $quitarExtras <= 9; $quitarExtras++)
                            {
                                array_splice($dataTable[0], $quitarExtras);
                            }
                            $agendaXLS .= "<tr>";
                            $alternante = 0;
                            foreach ($dataTable[0] as $valor) {
                                if ($alternante == 0) {
                                    $agendaXLS .= "<td align=center>$valor</td>";
                                    $alternante = 1;
                                }else{
                                    $agendaXLS .= "<td colspan=3 align=center >$valor</td>";
                                    $alternante = 0;
                                }
                                
                            }
                            $agendaXLS .= "</tr>";
                        }
                        $agendaXLS .= "</table></td></tr>";
                    }
                    $agendaXLS .= "<tr><td></td></tr>";
                }
                
                $agendaXLS .= "</table>";
                //$agendaXLS  = str_replace('&','<amp>',$agendaXLS);
                //$agendaXLS  = str_replace('"','<doublequot>',$agendaXLS);

                $fecha = date('d-m-Y');
                $fileName = 'Agenda_'.$idC.'_'.$fecha.'.xls';

                $xlsContenidoArchivo = '<!DOCTYPE HTML><html lang="en-US"><head><meta charset="UTF-8"><title></title></head><body>';
                $xlsContenidoArchivo .= $agendaXLS . '</body></html>';

                $myfile = fopen(public_path().'/excel/'.$fileName, "w");
                fclose($myfile);

                $myfile = fopen(public_path().'/excel/'.$fileName, "w");
                //fwrite($myfile, '' );
                fwrite($myfile, $xlsContenidoArchivo);
                fclose($myfile);
                return Redirect::to('excel/'.$fileName);

                /*
                $htmlvar = "
                <script  type='text/javascript'>
                var x=$(document);
                x.ready(inicio);
                setTimeout(function(){close()},500);
                function inicio()
                {
                    var x=$('.clearfix');
                    x.append('<center><h2>La exportaci&oacute;n ha sido exitosa. Ya puede cerrar esta ventana</h2></center>');
                    window.location.assign(\"http://localhost/conuca/public/excel/index.php?var=$agendaXLS&file=$fileName\");
                }
                </script>
                <style>
                    td{
                        text-align: center;
                        vertical-align: middle;
                    }
                </style>
                ";

                $this->layout->content = View::make('ImprimirDiplomas',array('mensaje' => $htmlvar));
                */

            }else  $this->layout->content = View::make('ImprimirDiplomas',array('mensaje' => 'No se encontrar&oacute;n eventos agendados para este congreso. Puede cerrar esta p&aacute;gina.'));
        }catch (Exception $e){
            $this->layout->content = View::make('ImprimirDiplomas',array('mensaje' => 'Problemas para generar el reporte. Contacte al administrador. >' . $e->getMessage()));
        }
    }

    public function notificarUsuario($idDC)
    {
        try {

            $misFichas = DB::select( DB::raw("select distinct a.titulo,a.fecha,a.horaInicio,a.horaFin, a.AulaLabel, a.idAula, a.idtematica,
                                            c.idUsuario,c.responsable,c.rutaDiploma from agenda a inner join presentacion b 
                                            on a.idAgenda = b.idAgenda inner join autor_x_ficha c on b.idFicha = c.idFicha 
                                            where a.idCongreso = '$idDC' and a.esActividad = 0 and c.responsable=1"));
            
            $congreso   = Congreso::WhereRaw('idCongreso=?',array($idDC))->first();
            $dc         = DetalleCongreso::WhereRaw('idCongreso=?',array($idDC))->first();

            $returnInfo = '';

            foreach ($misFichas as $objeto) {
                $user   = Usuario::find($objeto->idUsuario);
                $correoEnviar   = $user['emailUsuario'];
                $usuarioNombre  = $user['nombreUsuario'];
                $usuarioApellido= $user['apelUsuario'];
                
                $mesesFec  = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
                $diasFec   = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
                
                $fechaPresentacion  = $diasFec[intval(date('w',strtotime($objeto->fecha)))] . ' ' . date('d',strtotime($objeto->fecha)) . ' de ' ;
                $fechaPresentacion  = $fechaPresentacion . $mesesFec[intval(date('m',strtotime($objeto->fecha)))-1] . ' del ' . date('Y',strtotime($objeto->fecha)) ;
                $horaInicio = date("h:i",strtotime($objeto->horaInicio)) . ' ' . date("A",strtotime($objeto->horaInicio));
                $horaFin    = date("h:i",strtotime($objeto->horaFin)) . ' ' . date("A",strtotime($objeto->horaFin));

                Mail::send(
                    'emails.notificacionPonencia', 
                    array(
                        'nombreSistema' =>  $congreso->acronimoCongreso,
                        'nombreSistemaCompleto' =>  $congreso->nomCongreso,
                        'correo'        =>  $correoEnviar,
                        'nombres'       =>  $usuarioNombre,
                        'apellidos'     =>  $usuarioApellido,
                        'titulo' =>  $objeto->titulo,
                        'fechaPre' =>  $fechaPresentacion,
                        'horaInicio' =>  $horaInicio,
                        'horaFin' =>  $horaFin,
                        'aula' =>  $objeto->AulaLabel,
                        'banner_email'  => URL::asset('banners/'.$dc->banner),
                        'URLSitio'      => URL::action('SumissionController@indexPresentaciones', array($idDC))
                    ),
                    function($mensaje) use ($user)
                    {
                        $mensaje->to( $user['emailUsuario'] , $user['nombreUsuario'] . ' ' . $user['apelUsuario'])
                        ->subject('CONUCA: Participación en Congreso');
                    }
                );
                $returnInfo = $returnInfo . '<tr>';
                $returnInfo = $returnInfo . '<td>'.$usuarioNombre.' '.$usuarioApellido.'</td>';
                $returnInfo = $returnInfo . '<td>'.$correoEnviar.'</td>';
                $returnInfo = $returnInfo . '<td>'.$objeto->titulo.'</td>';
                $returnInfo = $returnInfo . '<td>'.$fechaPresentacion.'</td>';
                $returnInfo = $returnInfo . '</tr>';
            }
            return Response::json(array('error' => False, 'mensaje' => $returnInfo));
        }catch (Exception $e)
        { return Response::json(array('error' => True, 'mensaje' => 'Se encontraron errores! ' . $e->getMessage())); }
    }

    // presentaicones (archivos)
    public function verPresentaciones($idDC)
    {
        try {

            $misFichas = DB::select( DB::raw("select distinct a.titulo,a.fecha,a.horaInicio,a.horaFin, a.AulaLabel, a.idAula,
                                    a.idtematica,b.rutaPresentacion,c.idUsuario,c.responsable,c.rutaDiploma from agenda a 
                                    inner join presentacion b on a.idAgenda = b.idAgenda inner join autor_x_ficha c 
                                    on b.idFicha = c.idFicha where a.idCongreso = '$idDC' and a.esActividad = 0 and c.responsable=1"));

            $returnInfo = '';

            foreach ($misFichas as $objeto) {
                $user   = Usuario::find($objeto->idUsuario);
                $usuarioNombre  = $user['nombreUsuario'];
                $usuarioApellido= $user['apelUsuario'];
                
                $mesesFec  = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
                $diasFec   = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
                
                $fechaPresentacion  = $diasFec[intval(date('w',strtotime($objeto->fecha)))] . ' ' . date('d',strtotime($objeto->fecha)) . ' de ' ;
                $fechaPresentacion  = $fechaPresentacion . $mesesFec[intval(date('m',strtotime($objeto->fecha)))-1] . ' del ' . date('Y',strtotime($objeto->fecha)) ;

                $returnInfo = $returnInfo . '<tr>';
                $returnInfo = $returnInfo . '<td>'.$usuarioNombre.' '.$usuarioApellido.'</td>';
                $returnInfo = $returnInfo . '<td>'.$fechaPresentacion.'</td>';

                if($objeto->rutaPresentacion == "")
                    $returnInfo = $returnInfo . '<td>'.$objeto->titulo.' - No hay documento asociado</td>';
                else
                    $returnInfo = $returnInfo . '<td><strong>'.$objeto->titulo.'</strong> <a class="btn btn-default btn-sm" href="' . URL::action('ArchivoController@servirArchivo',array('idCongreso'=>$idDC,'nombreArchivo'=>$objeto->rutaPresentacion)) . '" >
                    <span class="glyphicon glyphicon-file"></span> Descargar
                    </td>';
                $returnInfo = $returnInfo . '</tr>';
            }
            return Response::json(array('error' => False, 'mensaje' => $returnInfo));
        }catch (Exception $e)
        { return Response::json(array('error' => True, 'mensaje' => 'Se encontraron errores! ' . $e->getMessage())); }
    }

    public function verFuentes($idDC)
    {
        try {
            
            $detalleCongreso = DetalleCongreso::where('idCongreso','=',$idDC)->firstOrFail();
            $congresoIdDetalle = $detalleCongreso->idDetalleCongreso;
            $estadoPaper    = EstadoPaper::where('nombreEstado','=','APROBADA')->first();
            $estaodID       = $estadoPaper->idEstadoPaper;

            $papersFuentes = DB::select( DB::raw("SELECT b.tituloPaper,b.idFicha,a.idPaper,a.rutaFuentes,c.idUsuario from paper a inner join ficha b on a.idFicha = b.idFicha inner join autor_x_ficha c on b.idFicha = c.idFicha where b.idDetalleCongreso = '$congresoIdDetalle' and c.responsable=1 and a.idEstadoPaper = '$estaodID'"));


            $returnInfo = '';

            foreach ($papersFuentes as $objeto) {
                $user   = Usuario::find($objeto->idUsuario);
                $usuarioNombre  = $user['nombreUsuario'];
                $usuarioApellido= $user['apelUsuario'];

                $returnInfo = $returnInfo . '<tr>';
                $returnInfo = $returnInfo . '<td>'.$usuarioNombre.' '.$usuarioApellido.'</td>';
                $returnInfo = $returnInfo . '<td><strong>'.$objeto->tituloPaper.'</strong></td>';

                if($objeto->rutaFuentes == "")
                    $returnInfo = $returnInfo . '<td>No hay archivo asociado</td>';
                else
                    $returnInfo = $returnInfo . '<td><a class="btn btn-default btn-sm" href="' . URL::action('ArchivoController@servirArchivo',array('idCongreso'=>$idDC,'nombreArchivo'=>$objeto->rutaFuentes)) . '">
                    <span class="glyphicon glyphicon-file"></span> Descargar
                    </td>';
                $returnInfo = $returnInfo . '</tr>';
            }
            return Response::json(array('error' => False, 'mensaje' => $returnInfo));
        }catch (Exception $e)
        { return Response::json(array('error' => True, 'mensaje' => 'Se encontraron errores! ' . $e->getMessage())); }
    }

    public function adminDiplomas($id)
    {
        $detalleCongreso = DetalleCongreso::where('idCongreso','=',$id)->firstOrFail();
        $estilo['1'] = $detalleCongreso->rutaPlantillaDiploma;
        $estilo['2'] = $detalleCongreso->rutaPlantillaDiploma2;
        $estilo['3'] = $detalleCongreso->rutaPlantillaDiploma3;

        $this->layout->content = View::make('congresos.ConfigurarDiploma',array('idCongreso' => $id,'estilo' => $estilo));
    }

    public function guardarEstilosDiplomas()
    {
        return DB::transaction(function()
        {
            DB::beginTransaction(); 
            try{
                $idCongreso = Input::get('idCongreso');

                $detalleCongreso = DetalleCongreso::where('idCongreso','=',$idCongreso)->firstOrFail();
                $congresoIdDetalle = $detalleCongreso->idDetalleCongreso;

                $mappedPath= '';
                $filename= '';
                $destinationPath='';
                $file=null;
                $iEstilo = 1;
                $totalEstilos = Input::get('totalControles');
                for ($iEstilo = 1; $iEstilo <= $totalEstilos; $iEstilo++) {
                    if (Input::hasFile('estilo'.$iEstilo))
                    {
                        $file = Input::file('estilo'.$iEstilo);

                        $filename   = 'E' . $iEstilo . '.' . $file->getClientOriginalExtension();
                        $mappedPath = public_path().'/diplomasEstilo/' . $idCongreso;
                        File::makeDirectory($mappedPath, $mode = 0777, true, true);

                        $destinationPath = $mappedPath;
                        $uploadSuccess   = $file->move($destinationPath, $filename);
                        if($uploadSuccess==null)
                            throw new Exception("ERROR: Al subir el archivo ".$file->getClientOriginalName());   
                        else{
                            $rutaMySQL = 'diplomasEstilo/' . $idCongreso .'/'. $filename;
                            $UPDObject = new DetalleCongreso();
                            $UPDObject->updateRuta($congresoIdDetalle, $rutaMySQL,$iEstilo);
                        }
                    }
                }
                DB::commit();
                return Redirect::action('CrearAgendaController@adminDiplomas',array($idCongreso));
            }               
            catch (Exception $e)
            {
                DB::rollback();
                return Response::json(array('error' => True, 'mensaje' => $e->getMessage()));
            }
        });
    }

    public function solicitarArticulo($idDC)
    {
        try {
            $congreso   = Congreso::WhereRaw('idCongreso=?',array($idDC))->first();
            $dc         = DetalleCongreso::WhereRaw('idCongreso=?',array($idDC))->first();

            $estado = EstadoFicha::where('nombreEstado','=','APROBADA')->first();
            $misFichas  = DB::table('ficha')->join('autor_x_ficha','autor_x_ficha.idFicha', '=' , 'ficha.idFicha')
                                        ->where('autor_x_ficha.responsable', '=','1')
                                        ->where('ficha.idEstadoFicha','=',$estado->idEstadoFicha)
                                        ->where('ficha.idDetalleCongreso','=',$dc->idDetalleCongreso)
                                        ->whereNotExists(function($query)
                                            {
                                                $query
                                                ->select(DB::raw(1))
                                                ->from('paper')
                                                ->whereRaw('paper.idFicha = ficha.idFicha');
                                            })
                                        ->get(array('ficha.tituloPaper','autor_x_ficha.idUsuario'));

            $returnInfo = '';

            foreach ($misFichas as $objeto) {
                $user   = Usuario::find($objeto->idUsuario);
                $correoEnviar   = $user['emailUsuario'];
                $usuarioNombre  = $user['nombreUsuario'];
                $usuarioApellido= $user['apelUsuario'];

                Mail::send(
                    'emails.notificacionCargaArt', 
                    array(
                        'nombreSistema' =>  $congreso->acronimoCongreso,
                        'nombreSistemaCompleto' =>  $congreso->nomCongreso,
                        'correo'        =>  $correoEnviar,
                        'cuerpomsg'     =>  'art&iacute;culo de la ficha',
                        'nombres'       =>  $usuarioNombre,
                        'apellidos'     =>  $usuarioApellido,
                        'titulo' =>  $objeto->tituloPaper,
                        'banner_email'  => URL::asset('banners/'.$dc->banner),
                        'URLSitio'      => URL::action('SumissionController@indexFichaLista', array($idDC))
                    ),
                    function($mensaje) use ($user)
                    {
                        $mensaje->to( $user['emailUsuario'] , $user['nombreUsuario'] . ' ' . $user['apelUsuario'])
                        ->subject('CONUCA: Carga de artículo');
                    }
                );
                $returnInfo = $returnInfo . '<tr>';
                $returnInfo = $returnInfo . '<td>'.$usuarioNombre.' '.$usuarioApellido.'</td>';
                $returnInfo = $returnInfo . '<td>'.$correoEnviar.'</td>';
                $returnInfo = $returnInfo . '<td>'.$objeto->tituloPaper.'</td>';
                $returnInfo = $returnInfo . '</tr>';
            }
            return Response::json(array('error' => False, 'mensaje' => $returnInfo));
        }catch (Exception $e)
        { return Response::json(array('error' => True, 'mensaje' => 'Se encontraron errores! ' . $e->getMessage())); }
    }

    public function solicitarFuentes($idDC)
    {
        try {
            
            $congreso   = Congreso::WhereRaw('idCongreso=?',array($idDC))->first();
            $dc         = DetalleCongreso::WhereRaw('idCongreso=?',array($idDC))->first();
            $congresoIdDetalle = $dc->idDetalleCongreso;
            $estadoPaper    = EstadoPaper::where('nombreEstado','=','APROBADA')->first();
            $estaodID       = $estadoPaper->idEstadoPaper;

            $papersFuentes = DB::select( DB::raw("SELECT b.tituloPaper,b.idFicha,a.idPaper,a.rutaFuentes,c.idUsuario from paper a inner join ficha b on a.idFicha = b.idFicha inner join autor_x_ficha c on b.idFicha = c.idFicha where b.idDetalleCongreso = '$congresoIdDetalle' and c.responsable=1 and a.idEstadoPaper = '$estaodID'"));


            $returnInfo = '';
            foreach ($papersFuentes as $objeto) {
                $user   = Usuario::find($objeto->idUsuario);
                $correoEnviar   = $user['emailUsuario'];
                $usuarioNombre  = $user['nombreUsuario'];
                $usuarioApellido= $user['apelUsuario'];

                Mail::send(
                    'emails.notificacionCargaArt', 
                    array(
                        'nombreSistema' =>  $congreso->acronimoCongreso,
                        'nombreSistemaCompleto' =>  $congreso->nomCongreso,
                        'correo'        =>  $correoEnviar,
                        'cuerpomsg'     =>  'archivo fuente de la ficha',
                        'nombres'       =>  $usuarioNombre,
                        'apellidos'     =>  $usuarioApellido,
                        'titulo' =>  $objeto->tituloPaper,
                        'banner_email'  => URL::asset('banners/'.$dc->banner),
                        'URLSitio'      => URL::action('SumissionController@indexFuentes', array($idDC))
                    ),
                    function($mensaje) use ($user)
                    {
                        $mensaje->to( $user['emailUsuario'] , $user['nombreUsuario'] . ' ' . $user['apelUsuario'])
                        ->subject('CONUCA: Carga de archivo fuente para artículo');
                    }
                );
                $returnInfo = $returnInfo . '<tr>';
                $returnInfo = $returnInfo . '<td>'.$usuarioNombre.' '.$usuarioApellido.'</td>';
                $returnInfo = $returnInfo . '<td>'.$correoEnviar.'</td>';
                $returnInfo = $returnInfo . '<td>'.$objeto->tituloPaper.'</td>';
                $returnInfo = $returnInfo . '</tr>';
            }
            return Response::json(array('error' => False, 'mensaje' => $returnInfo));
        }catch (Exception $e)
        { return Response::json(array('error' => True, 'mensaje' => 'Se encontraron errores! ' . $e->getMessage())); }
    }
}