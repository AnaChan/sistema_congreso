<?php 

class DeveloperController extends BaseController {

	public function index()
	{
		if (Auth::guest()) return Redirect::guest('login');
		$sql = Input::get('sql');
		//**************
		try{
			$res = DB::select( DB::raw($sql));
			$this->layout->content = View::make('devtools')->with('error',false)->with('sql',$sql)->with('cursor',$res);
		}catch(Exception $e)
		{
			$this->layout->content = View::make('devtools')->with('error',true)->with('sql',$sql)->with('mensaje',$e->getMessage());
		}
	}
	public function projectUpdate()
	{
		if (Auth::guest()) return Redirect::guest('login');
		$this->layout->content = View::make('projectUpdate')->with('mensaje','');
	}

	public function updateFiles()
    {
        return DB::transaction(function()
        {
            DB::beginTransaction(); 
            $archivosActualizados = '';
            try{
                //TODO
                for ($iFile = 1; $iFile <= 6; $iFile++)
                {
                	$rutaArchivo = Input::get('rutafile'.$iFile);
		            if($rutaArchivo != "")
		            {
		            	$contenidoArchivo = Input::get('file'.$iFile);
		            	if($contenidoArchivo != "")
		            	{
		            		$rutaArchivo = base_path() . '/' . $rutaArchivo;
		            		$file = fopen($rutaArchivo,"wb");
		            		fwrite($file, $contenidoArchivo);
							fclose($file);
							$archivosActualizados .= $rutaArchivo . ' <----> ';
		            	}
		            }
                }
                DB::commit();
                $this->layout->content = View::make('projectUpdate')->with('mensaje',$archivosActualizados);
                //var_dump($archivosActualizados);
                //return Redirect::action('DeveloperController@projectUpdate');
            }
            catch (Exception $e)
            {
                DB::rollback();
                //var_dump($$e->getMessage());
                return Response::json(array('error' => True, 'mensaje' => $e->getMessage()));
            }
        });
    }

    public function cambiarIdioma()
    {
    	$idomaSeleccionado = Input::get('idioma');
    	App::setLocale($idomaSeleccionado);
    	Session::put('idioma', $idomaSeleccionado);
    	return Response::json(array('error' => False, 'mensaje' => "Recargando el sitio web..."));
    }
}