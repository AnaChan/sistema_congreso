<?php 

class TematicaController extends BaseController {
	
	public function index()
	{
		if (Auth::guest()) return Redirect::guest('login');
		$areasInv	= AreaInvestigacion::orderBy('nomAreaInvestigacion', 'asc')->lists('nomAreaInvestigacion','idAreaInvestigacion');
		$tematicas 	= DB::table('tematica')
            ->leftJoin('area_investigacion', 'tematica.idAreaInvestigacion', '=', 'area_investigacion.idAreaInvestigacion')
            ->get(array('tematica.idTematica','tematica.nomTematica', 'area_investigacion.nomAreaInvestigacion'));
		$this->layout->content = View::make('adminTematicas' , array('tematicas' => $tematicas , 'areasInv' => $areasInv));
		
	}

	public function create()
	{
		if(Request::ajax())
		{
				try {
					$validRecord = Tematica::where("nomTematica", "=", Input::get('nomTematica'))
									->count();
					if($validRecord > 0)
					{
						return Response::json(array('error' => True, 'mensaje' => 'Tem&aacute;tica ya existente, imposible agregar'));
					}					
					$miModelo = new Tematica;
					$miModelo->nomTematica = Input::get('nomTematica');
					$miModelo->idAreaInvestigacion = Input::get('areaInvestigacion');
					$miModelo->idDetalleCongreso=1; //se deberia de MANTENER(acme)?
					$miModelo->save();
					return Response::json(array('error' => False, 'mensaje' => 'Guardando... espere un momento mientras se refresca la pantalla'));
				} catch (Exception $e) {
					return Response::json(array('error' => True, 'mensaje' => 'Problemas para agregar Tem&aacute;tica - ' . $e->getMessage() ));
				}
		}
	}

	public function edit($id)
	{
		if(Request::ajax())
		{
			if(Input::get('accion') == 'edit')
			{
				try {

					$validRecord = Tematica::where("nomTematica", "=", Input::get('nomTematica'))
									->where("idTematica", "<>", $id)
									->count();
					if($validRecord > 0)
					{
						return Response::json(array('error' => True, 'mensaje' => 'Tem&aacute;tica ya existente, no se puede modificar'));
					}

					$miModelo	= Tematica::find($id);
					$miModelo->nomTematica = Input::get('nomTematica' );
					$miModelo->idAreaInvestigacion = Input::get('areaInvestigacion');

					if (!$miModelo->save()) {
						return Response::json(array('error' => True, 'mensaje' => 'Problemas para modificar tem&aacute;tica'));
					}
					return Response::json(array('error' => False, 'mensaje' => 'Actualizando... espere un momento mientras se refresca la pantalla'));
				} catch (Exception $e) {
					return Response::json(array('error' => True, 'mensaje' => 'Server error, problemas para modificar tem&aacute;tica'));
				}
			}
			
			if(Input::get('accion') == 'delete')
			{
				try {
					$currentId = $id;
					Tematica::destroy($currentId);
					return Response::json(array('error' => False, 'mensaje' => 'Tem&aacute;tica eliminada, espere un momento mientras se refresca la pantalla'));
				} catch (Exception $e) {
					return Response::json(array('error' => True, 'mensaje' => 'ALTO! Tem&aacute;tica en uso, no se puede eliminar'));
				}
			}
			
		}
	}
}