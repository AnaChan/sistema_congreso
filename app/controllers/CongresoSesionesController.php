<?php

class CongresoSesionesController  extends BaseController {

	public function getIndex($id)
	{
		$nombreDeCongreso = DB::table('congreso')
					->where('idCongreso',$id)
                    ->first();

        $nombreCongreso = '<< error >>';
        $congresoOwner	= -1;
        $idDetCongreso 	= -1;
        if($nombreDeCongreso != null)
		{
			$nombreCongreso = $nombreDeCongreso->nomCongreso;
			if(Auth::user()->idUsuario == $nombreDeCongreso->idCreador)
				$congresoOwner	= 1;

			$detalleCongreso = DB::table('detalle_congreso')->where('idCongreso','=', $id)->first();
        	$idDetCongreso 	 = $detalleCongreso->idDetalleCongreso;
		}
		//*************************************************************************
		$sesionesXCongreso 	= DB::table('sesion')
            ->leftJoin('tematica', 'sesion.idTematica', '=', 'tematica.idTematica')
            ->where('idCongreso',$id)
            ->get(array('sesion.nombreSesion','sesion.moderador', 'tematica.nomTematica','sesion.horaInicio','sesion.horaFin','sesion.duracionSesion','sesion.idSesion','sesion.fecSesion'));

		foreach($sesionesXCongreso as $objeto)
		{
	        $horaInicio = date("h:i",strtotime($objeto->horaInicio)) . ' ' . date("A",strtotime($objeto->horaInicio));
			$horaFin    = date("h:i",strtotime($objeto->horaFin)) . ' ' . date("A",strtotime($objeto->horaFin));

			$lastTime = strtotime($objeto->horaFin);
			$iniTime  = strtotime($objeto->horaInicio);
			$timeDiff = ($lastTime-$iniTime)/60;
			
			$horas = floor($timeDiff / 60);
			$minutos = $timeDiff % 60;

			$objeto->horaInicio = $horaInicio;
			$objeto->horaFin  = $horaFin;
			$objeto->duracionSesion  = $horas . ' h ' .$minutos . ' m' ;
		}

		$tematicas 	= DB::table('congreso_x_tematica')->leftJoin('tematica','congreso_x_tematica.idTematica','=','tematica.idTematica')
					->where('congreso_x_tematica.idCongreso',$id)->select(array('congreso_x_tematica.idTematica','tematica.nomTematica'))->lists('nomTematica','idTematica');
					
		$tematicasNoDefinidas = false;
		if(count($tematicas)==0)
		{$tematicasNoDefinidas=true;}

		$this->layout->content = View::make('congresos.ConfigurarSesiones',array('idCongreso' => $id,'sesionesCongreso' => $sesionesXCongreso,'nombreCongreso' => $nombreCongreso , 'congresoOwner' => $congresoOwner,'tematicas' => $tematicas, 'sinTematicas' => $tematicasNoDefinidas));
	}
	
	public function actualizarData()
	{
		try {
    		if(Request::ajax())
			{
				$action = Input::get('action');
				$idDC 	= Input::get('idDC');
				$error	= false;
				$returnMSG = 'Actualizado';

				switch ($action) {
				    case "ADD":
						$checkExists = DB::table('sesion')
            							->where('idCongreso','=',$idDC)->where('nombreSesion','=',Input::get('sesion'))->count();

									
						if($checkExists == 0)
						{
							$objeto = new Sesion;
							$objeto->nombreSesion	= Input::get('sesion');
							$objeto->moderador	= Input::get('moderador');
							$objeto->idTematica	= Input::get('tematica');
							$objeto->fecSesion	= Input::get('fecha');
							$objeto->horaInicio	= Input::get('inicio');
							$objeto->horaFin	= Input::get('fin');
							$objeto->idCongreso	= $idDC;
							$objeto->save();
							$returnMSG = 'Agregado';
						}else{
							$error	= true;
							$returnMSG = 'Sesion ya esta creada para este congreso';
						}
				        break;
				    case "UPDATE":
				    	$validRecord = Sesion::where("nombreSesion", "=", Input::get('sesion'))
										->where("idSesion", "<>", Input::get('currentId'))
										->count();
						if($validRecord > 0){
							$error	= true;
							$returnMSG = 'Sesion ya esta creada, no se puede modificar';
						}else{
					    	$objeto = Sesion::find(Input::get('currentId'));
							$objeto->nombreSesion	= Input::get('sesion');
							$objeto->moderador	= Input::get('moderador');
							$objeto->idTematica	= Input::get('tematica');
							$objeto->fecSesion	= Input::get('fecha');
							$objeto->horaInicio	= Input::get('inicio');
							$objeto->horaFin	= Input::get('fin');
							$objeto->save();
						}
				        break;
				    case "DELETE":
				        Sesion::destroy(Input::get('currentId'));
				        $returnMSG = 'Eliminado';
				        break;
				}
				if($error)
					return Response::json(array('error' => True, 'mensaje' => $returnMSG ));
				else
					return Response::json(array('error' => False, 'mensaje' => $returnMSG . ' con &eacute;xito, refrescando...'));
			}
		}catch(Exception $e)
		{
			return Response::json(array('error' => True, 'mensaje' => 'Problemas para actualizar: '  . $e->getMessage()));
		}

	}
}