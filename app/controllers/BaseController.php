<?php

class BaseController extends Controller {

	/**
	 * Variable para mostrar o ocultar las barras. Ocupada para ocultar labarra en el login
	 *
	 */
	protected $mostrar_barra = True;

	/**
	 * Nombre del layout de todos la aplicacion.
	 *
	 */
	protected $layout = "layouts.master";

	/**
	 * Variable que tiene el nombre del rol segun el usuario x congreso. Ocupada principalmente en las rutas /congreso/{id}/.*
	 *
	 */
	protected $nombre_rol = "No Determinado";

	/**
	 * Variable que me indica quien soy como rol. Ocupada principalmente en las rutas /congreso/{id}/.*
	 *
	 */
	protected $nombre_rol_original = "No Determinado";

	/**
	 * Variable que permite visualizar la seleccion del rol. Ocupada principalmente en las rutas /congreso/{id}/.*
	 *
	 */
	protected $mostrar_rol_seteado = 0;


	/**
	 * Variable que contiene la informacion de UsuarioXCongresoXRol. Solo se establece cuando la ruta es /congreso/{id}/.*
	 *
	 */
	protected $permisos;

	/**
	 * Variable para ocultar el input de busqueda cuando no sea necesario
	 *
	 */
	protected $mostrar_buscar = False;

	/**
	 * Variable que se settea con el id del congreso si la ruta es /congreso/{idCongreso}
	 *
	 */
	protected $idCongreso = 0;

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		App::setLocale(Session::get('idioma'));
		$this->layout = View::make($this->layout);
		$this->layout->mostrar_barra = $this->mostrar_barra;
		$this->layout->nombre_rol = $this->nombre_rol;
		$this->layout->mostrar_buscar = $this->mostrar_buscar;
		$this->layout->idCongreso = $this->idCongreso;
		$this->layout->nombre_rol_original = $this->nombre_rol_original;
		$this->layout->mostrar_rol_seteado = $this->mostrar_rol_seteado;
	}

    /**
     * Verifica si algun recurso del sistema puede ser accedido por el usuario
     * es decir si se encuentra en una fecha valida.
     * @return boolean
     */
    public function verificarFechaRecurso($idCongreso,$nomTipoFecha)
    {

		$debug = Config::get('app.debug');
		if($debug) return true;
        try{
        	$estadoCongreso=DB::table('congreso')
        							->join('estado_congreso',
        									'estado_congreso.idEstadoCongreso',
        									'=',
        									'congreso.idEstadoCongreso'
        								)
        							->where('idCongreso',
        									'=',
        									$idCongreso
        									)->first();
        	if(strcmp($estadoCongreso->valorEstado,'CER')==0)
        		throw new Exception("<strong>ERROR:</strong> El congreso está cerrado.");

        	$permiso=DB::table('usuariorol_x_congreso')
        					->join('rol',
        							'rol.idRol',
        							'=',
        							'usuariorol_x_congreso.idRol'
        						)
        					->Where('usuariorol_x_congreso.idCongreso',
        							'=',
        							$idCongreso
        							)
        					->Where('usuariorol_x_congreso.idUsuario',
        							'=',
        							Auth::user()->idUsuario
        						)
        					->Where('rol.nomRol',
        							'=',
        							'Chair'
        						)
        					->first();
			if(count($permiso)!=0)return true; //quiere decir que es el chair , el tiene acceso a todo

            $idDetalleCongreso=DetalleCongreso::Where('idCongreso','=',$idCongreso)->first()->idDetalleCongreso;
            $consultaFecha=DB::table('fecha_importante')
                            ->join('tipo_fecha',
                                    'tipo_fecha.idTipoFecha',
                                    '=',
                                    'fecha_importante.idTipoFecha'
                                )
                            ->Where('fecha_importante.idDetalleCongreso',
                                    '=',
                                    $idDetalleCongreso
                                )
                            ->Where('tipo_fecha.valorTipoFecha',
                                    '=',
                                    $nomTipoFecha
                                )
                            ->first();
            if(count($consultaFecha)==0)throw new Exception("<strong>MENSAJE:</strong> La fecha para acceder al recurso no ha sido configurada. (".$nomTipoFecha.")");
            $inicio = strtotime($consultaFecha->fecInicio);
            $fin = strtotime($consultaFecha->fecFin);
            $hoy = strtotime(date('Y-m-d').'');
            if($hoy>$fin || $hoy<$inicio)
            {
            	$objetoFechaNombre	= TipoFecha::WhereRaw('valorTipoFecha=?',array($nomTipoFecha))->first();
            	$nomTipoFecha		= $objetoFechaNombre->nomTipoFecha;

            	throw new Exception("<strong>MENSAJE:</strong> El periodo de '".$nomTipoFecha ."' es del <strong>" .date("d-m-Y",$inicio)."</strong> hasta <strong>".date("d-m-Y", $fin)."</strong> <br> <strong>Fecha Actual: </strong>".date("d-m-Y", $hoy));
            }
            return true;
        }catch(Exception $e){
            Session::flash('message',  $e->getMessage());
            return false;
        }


    }

	public function __construct()
	{
		$ruta = Request::path();

		if(preg_match("/^.*?congreso\/\d{1,}\/?.*?$/", $ruta))
		{
			$this->idCongreso = Request::segment(2);

			$this->permisos = DB::table('detalle_congreso')
			->join(
				'congreso',
				'congreso.idCongreso',
				'=',
				'detalle_congreso.idCongreso'
				)
			->join(
				'usuariorol_x_congreso',
				'usuariorol_x_congreso.idCongreso',
				'=',
				'detalle_congreso.idCongreso'
				)
			->join(
				'estado_congreso',
				'estado_congreso.idEstadoCongreso',
				'=',
				'congreso.idEstadoCongreso'
				)
			->join(
				'rol',
				'rol.idRol',
				'=',
				'usuariorol_x_congreso.idRol'
				)
			->Where(
				'usuariorol_x_congreso.idUsuario',
				'=',
				Auth::user()->idUsuario
				)
			->Where(
				'estado_congreso.valorEstado',
				'!=',
				'REC'
				)
			->Where(
				'usuariorol_x_congreso.idCongreso',
				'=',
				$this->idCongreso
			);
			
			if(preg_match("/^.*?chair\/?.*?$/", $ruta))
			{
				$this->permisos->where(
					'rol.nomRol',
					'Chair'
				);
			}
			elseif (preg_match("/^.*?comite\/?.*?$/", $ruta))
			{
				$this->permisos->where(function($query)
				{
					$query
					->Where('rol.nomRol','PC')
					->orWhere('rol.nomRol','Chair');
				});
			}
			elseif (preg_match("/^.*?revisor\/?.*?$/", $ruta))
			{
				$this->permisos->where(function($query)
				{
					$query
					->where('rol.nomRol','Revisor')
					->orWhere('rol.nomRol','PC')
					->orWhere('rol.nomRol','Chair');
				});
			}
			elseif (preg_match("/^.*?autor\/?.*?$/", $ruta))
			{
				$this->permisos->where(function($query)
				{
					$query
					->where('rol.nomRol','Autor')
					->orWhere('rol.nomRol','Revisor')
					->orWhere('rol.nomRol','PC')
					->orWhere('rol.nomRol','Chair');
				});
			}

			$this->permisos = $this->permisos->get();

			if (count($this->permisos) > 0)
			{
				if (Session::has('rol_barra')) {
					$this->nombre_rol = Session::get('rol_barra');
				}
				else
				{
					$this->nombre_rol = $this->permisos[0]->nomRol;
				}
				$this->mostrar_rol_seteado = 1;
				$this->nombre_rol_original = $this->permisos[0]->nomRol;
			}
			else
			{
				App::abort(404);
			}
		}
		elseif (preg_match("/^.*?login\/?.*?$/",$ruta)) 
		{
			$this->mostrar_barra = False;
		}
		elseif (preg_match("/^.*?inicio\/?.*?$/", $ruta))
		{
			$idUsuario = Auth::check()?Auth::user()->idUsuario:0;

			$usuario = DB::table('usuarios')
			->join(
				'estado_usuario',
				'estado_usuario.idEstadoUsuario',
				'=',
				'usuarios.idEstadoUsuario'
				)
			->Where(
				'estado_usuario.valorEstado',
				'ADM'
				)
			->where(
				'usuarios.idUsuario',
				'=',
				$idUsuario
			)
			->get();
			if(count($usuario) > 0) $this->nombre_rol = 'ADM';
		}
	}
}
