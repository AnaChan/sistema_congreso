<?php

class ManejoArchivosController extends \BaseController {
    /**
     * Envia los archivos al servidor 
     * @return JSON
     */ 
    public function subirArchivo()
    {
        return DB::transaction(function()
        {       
            DB::beginTransaction();
            try {
                $mappedPath= '';
                $filename= '';
                $destinationPath='';
                $tipoArchivo=Input::get('tipoArchivo'); //Este es el tipo de archivo a subir , se utilizara para filtrar
                $file=null;                                         // las    tablas de actualizacion y los folder destino
                $idCongreso=Input::get('idCongreso');

                //Configuracion de la ruta antes de subir el archivo
                       
                if (Input::hasFile('nombreArchivo'))
                     $file            = Input::file('nombreArchivo');
                else
                    throw new Exception("ERROR: Al subir ".$tipoArchivo);

                if(strcmp($tipoArchivo, 'banner')==0){
                    $mappedPath=public_path().'/banners';
                    $filename        = 'congreso_'.$idCongreso.'.'.$file->getClientOriginalExtension();
                }elseif(strcmp($tipoArchivo, 'ficha')==0){
                    $idFicha = Input::get("idFicha");
                    $mappedPath=base_path().'/archivos/fichas';
                    $filename        = $idFicha.'_Resumen_'.$idCongreso.'.'.$file->getClientOriginalExtension(); 
                }elseif(strcmp($tipoArchivo, 'paper')==0){
                    $idPaper = Input::get("idPaper");
                    $idFicha = Input::get("idFicha");
                    $mappedPath= base_path().'/archivos/papers';
                    $filename = $idFicha.'_Articulo_'.$idCongreso.'.'.$file->getClientOriginalExtension(); 
                }else{
                    throw new Exception("ERROR: Tipo archivo".$tipoArchivo." Inválido");
                }

                File::makeDirectory($mappedPath, $mode = 0777, true, true);
                $destinationPath = $mappedPath;
                $uploadSuccess   = $file->move($destinationPath, $filename);
                if($uploadSuccess==null)throw new Exception("ERROR: La información se guardó correctamente, sin embargo ocurrió un error al subir el archivo ".$file->getClientOriginalName());   
    
                $update=null;
                if(strcmp($tipoArchivo, 'banner')==0){  //Si es banner actualizamos la ruta en la base OJO: solo el nombre
                    $update=DetalleCongreso::where('idCongreso', '=', $idCongreso)
                                ->update(array('banner' =>'congreso_'.$idCongreso.'.'.$file->getClientOriginalExtension()));
                }
                elseif(strcmp($tipoArchivo, 'ficha')==0){
                    $idFicha = Input::get("idFicha");
                    $update=Ficha::where('idFicha', '=', $idFicha)
                                ->update(array('rutaFicha' => "fichas/".$filename));
                }
                elseif(strcmp($tipoArchivo, 'paper')==0){
                    $idPaper = Input::get("idPaper");
                    $update=Paper::where('idPaper', '=', $idPaper)
                                ->update(array('rutaPaper' => "papers/".$filename));
                }
                else{
                    throw new Exception("ERROR: Al actualizar la base parra el archivo de tipo ".$tipoArchivo);
                }
                if(count($update)==0)throw new Exception("ERROR: Al actuaizar la base");

                DB::commit();
                return Redirect::action('UsuariosCongresosController@establecerCongreso',array($idCongreso));
            } catch(Exception $e)
            {
                DB::rollback();
                Session::flash('message',  $e->getMessage());
                return Redirect::action('UsuariosCongresosController@establecerCongreso',array($idCongreso));
            }
        });  
    }
}