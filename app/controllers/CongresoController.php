<?php 

class CongresoController extends BaseController {

	/**
	 * Cargar vista de solicitar un congreso.
	 *
	 * @return View('SolicitarCongreso')
	 */
	public function index()
	{

		$this->layout->content = View::make('inicio.SolicitarCongreso');
	}

	/**
	 * Cargar vista de solicitar un congreso.
	 *
	 * @return View('SolicitarCongreso')
	 */
	public function acercade()
	{

		$this->layout->content = View::make('inicio.AcercaDe');
	}

	/**
	* Carga la lista de los congresos pendientes a la aprobacion del administrador
	*
	*@return View('AprobacionCongresoLista') 
	*/
	public function MostrarSolicitudesCongreso(){
		if(Auth::check())
		{
			$estadoCon=DB::table('estado_congreso')->Where('valorEstado','=','INS')->first();
			$congreso = Congreso::where('idEstadoCongreso','=',$estadoCon->idEstadoCongreso)->get();
			$this->layout->content = View::make('AprobacionCongresoLista')->with('congresos',$congreso);
		}
	}

	public function MostrarSolicitudDetalles($id){
		if(Auth::check())
		{
			$congreso = Congreso::where('idCongreso','=',$id)->firstOrFail();
			$this->layout->content = View::make('AprobacionCongresoDetalles')->with('congreso',$congreso);	
		}
	}

	public function guardarSolicitud()
	{
		return DB::transaction(function()
		{
			DB::beginTransaction();
			try {
	    		if(Request::ajax())
				{
					$congreso = new Congreso;			
					$congreso->nomCongreso = Input::get('nombreCongreso');
					$congreso->acronimoCongreso = Input::get('acronimo');
					$congreso->webCongreso = Input::get('sitioWeb');
					$congreso->ciudad = Input::get('ciudad');
					$congreso->pais = Input::get('pais');
					$congreso->fecIniCongreso =  Input::get('fechaInicio');
					$congreso->fecModCongreso = date('Y-m-d H:i:s');
					$congreso->fecFinCongreso = Input::get('fechaFin');				
					$leEstadoC=DB::table('estado_congreso')->Where('valorEstado','=','INS')->first();
					if(count($leEstadoC)==0)throw new Exception("ERROR: Al establecer el estado del congreso");
					$congreso->idEstadoCongreso = $leEstadoC->idEstadoCongreso;
					$congreso->emailOrgCongreso = Input::get('email');
					$congreso->idCreador = auth::user()->idUsuario;
					$congreso->save();

					$usr =Usuario::where('idUsuario', '=',$congreso->idCreador)->first();
					$banner = URL::asset('imagenes/banner_mail_conuca.png');
					
					$sent=Mail::send(
						'emails.notificacionCongreso', 
						array(
							'nombreSistema'	=>	Input::get('acronimo'),
							'nombreSistemaCompleto'	=>	Input::get('nombreCongreso'),
							'correo'		=>	Input::get('email'),
							'nombres'		=>	$usr->nombreUsuario,
							'apellidos'		=>	$usr->apelUsuario,
							'anio'			=>	date("Y"),
							'rolSistema'	=> 'Administrador de Congreso',
							'URLConfirmar'	=> '',
							'banner_email' => $banner,
							'URLSitio'		=> URL::action('HomeController@inicio'),
						), 
						function($mensaje) use ($usr)
						{
							$mensaje->to(Input::get('email'), $usr->nombreUsuario . ' ' . $usr->apelUsuario)
									->subject('CONUCA: Respuesta a su solicitud registro de congreso');
						}
					);
					if($sent==0) throw new Exception('No se pudo enviar el correo de notificación , favor intentelo mas tarde.');

					$admin=DB::table('usuarios')
							->join('estado_usuario',
									'estado_usuario.idEstadoUsuario',
									'=',
									'usuarios.idEstadoUsuario'
								)
							->Where('estado_usuario.valorEstado',
									'=',
									'ADM'
									)->first();

					$sent=Mail::send(
						'emails.notificacionAdmin', 
						array(
							'nombreSistema'	=>	Input::get('acronimo'),
							'nombreSistemaCompleto'	=>	Input::get('nombreCongreso'),
							'correo'		=>	$usr->emailUsuario,
							'nombres'		=>	$usr->nombreUsuario,
							'apellidos'		=>	$usr->apelUsuario,
							'anio'			=>	date("Y"),
							'rolSistema'	=> 'Administrador de Congreso',
							'URLConfirmar'	=> '',
							'banner_email' => $banner,
							'URLSitio'		=> URL::action('HomeController@inicio'),
						), 
						function($mensaje) use ($usr,$admin)
						{
							$mensaje->to($admin->emailUsuario, $usr->nombreUsuario . ' ' . $usr->apelUsuario)
									->subject('CONUCA: Auto-Mensaje , Congreso requiere respuesta.');
						}
					);
					if($sent==0) throw new Exception('No se pudo enviar el correo de notificación , favor intentelo mas tarde.');
					DB::commit();
					return Response::json(array('error' => False, 'mensaje' => 'La solicitud ha sido enviada, recibirá un correo de notificación.'));
				}	
	    	} catch (Exception $e) {
	    		DB::rollback();
	    		return Response::json(array('error' => True, 'mensaje' => $e->getMessage()));
			}		
		});
	}


	public function modificarSolicitud()
	{
		return DB::transaction(function()
		{
			DB::beginTransaction();
			try {
	    		if(Request::ajax())
				{
					$id = Input::get("id");
					$estado = Input::get("estado");
					$comentarios = Input::get("comentarios");
					$correo = Input::get("correo");
					if($estado == 'aprobada')
					{
						$leEstadoC=DB::table('estado_congreso')->Where('valorEstado','=','SNC')->first();
						$res=Congreso::where('idCongreso', '=',$id)->update(array('idEstadoCongreso' => $leEstadoC->idEstadoCongreso, 'fecModCongreso' => date('Y-m-d H:i:s')));
						//Creacion del detalle Hotel por defecto
						DB::table('detalle_congreso')->insert(
								array('banner' => 'convacio.PNG' , 'idCongreso' => $id)
							);
						$cong = Congreso::where('idCongreso', '=',$id)->first();
						$usr =Usuario::where('idUsuario', '=',$cong->idCreador)->first();

						$usuarioxc = new UsuariorolXCongreso;
						$usuarioxc->idUsuario = $cong->idCreador;
						$leRol=DB::table('rol')->Where('nomRol','=','Chair')->first();
						$usuarioxc->idRol= $leRol->idRol;
						$usuarioxc->idCongreso= $id;
						$leEstadoURC=DB::table('estado_urc')->Where('nombreEstado','=','ACTIVO')->first();
						$usuarioxc->idEstadoURC = $leEstadoURC->idEstadoURC;
						$usuarioxc->save();

						$banner = URL::asset('imagenes/banner_mail_conuca.png');
						$sent=Mail::send(
							'emails.notificacionAprobacionCongreso', 
							array(
								'nombreSistema'	=>	$cong->acronimoCongreso,
								'nombreSistemaCompleto'	=>	$cong->nomCongreso,
								'correo'		=>	$correo,
								'nombres'		=>	$usr->nombreUsuario,
								'apellidos'		=>	$usr->apelUsuario,
								'anio'			=>	date("Y"),
								'comentario'	=> $comentarios,
								'rolSistema'	=> 'Administrador de Congreso',
								'banner_email' 	=>	$banner,
								'URLConfirmar'	=> '',
								'URLSitio'		=> URL::action('HomeController@inicio'),
							), 
							function($mensaje) use ($usr)
							{
								$mensaje->to($usr->emailUsuario, $usr->nombreUsuario . ' ' . $usr->apelUsuario)
										->subject('CONUCA: Respuesta a su solicitud de registro de congreso');
							}
						);
						if($sent==0) throw new Exception('No se pudo enviar el correo de notificación , favor intentelo mas tarde.');
					}
					else 
					{
						$leEstadoC=DB::table('estado_congreso')->Where('valorEstado','=','REC')->first();
						$res=Congreso::where('idCongreso', '=',$id)->update(array('idEstadoCongreso' => $leEstadoC->idEstadoCongreso, 'fecModCongreso' => date('Y-m-d H:i:s')));
						$cong = Congreso::where('idCongreso', '=',$id)->first();
						$usr =Usuario::where('idUsuario', '=',$cong->idCreador)->first();
						$banner = URL::asset('imagenes/banner_mail_conuca.png');
						
						$sent=Mail::send(
							'emails.notificacionUsuarioRechazo', 
							array(
								'nombreSistema'	=>	$cong->acronimoCongreso,
								'nombreSistemaCompleto'	=>	$cong->nomCongreso,
								'correo'		=>	$correo,
								'nombres'		=>	$usr->nombreUsuario,
								'apellidos'		=>	$usr->apelUsuario,
								'anio'			=>	date("Y"),
								'comentario'	=> $comentarios,
								'banner_email' 	=>	$banner,
								'rolSistema'	=> 'Administrador de Congreso',
								'URLConfirmar'	=> '',
								'URLSitio'		=> URL::action('HomeController@inicio'),
							), 
							function($mensaje) use ($usr)
							{
								$mensaje->to($usr->emailUsuario, $usr->nombreUsuario . ' ' . $usr->apelUsuario)
										->subject('CONUCA: Respuesta a su solicitud de registro de congreso');
							}
						);
						if($sent==0) throw new Exception('No se pudo enviar el correo de notificación , favor intentelo mas tarde.');
					}
					
					
					DB::commit();
					return Response::json(array('error' => False, 'mensaje' => 'La solicitud ha sido '.$estado));
					
				}	
	    	} catch (Exception $e) {
	    		DB::rollback();
	    		return Response::json(array('error' => True, 'mensaje' => $e->getMessage()));
			}		
		});
	}

	public function eliminarCongreso()
	{
		return DB::transaction(function()
		{
			DB::beginTransaction();
			try {
	    		if(Request::ajax())
				{
					$idUsuario = Auth::check()?Auth::user()->idUsuario:0;
					$isAdmin=DB::table('usuarios')
								->join('estado_usuario',
										'estado_usuario.idEstadoUsuario',
										'=',
										'usuarios.idEstadoUsuario'
									)
								->Where('estado_usuario.valorEstado','=','ADM')
								->Where('usuarios.idUsuario','=',$idUsuario)
								->first();
					if(count($isAdmin)!=1)
						throw new Exception("ERROR: Solamente el Administrador del sistema puede eliminar congresos.");

					$id = Input::get("idCongreso");
					$leEstadoC=DB::table('estado_congreso')->Where('valorEstado','=','REC')->first();
					$res=Congreso::where('idCongreso', '=',$id)->update(array('idEstadoCongreso' => $leEstadoC->idEstadoCongreso,
																	 'fecModCongreso' => date('Y-m-d H:i:s'),
																	 'comentarioCongreso' => ''
																	 ));
		
					DB::commit();
					return Response::json(array('error' => False, 'mensaje' => 'El congreso ha sido eliminado.'));
					
				}	
	    	} catch (Exception $e) {
	    		DB::rollback();
	    		return Response::json(array('error' => True, 'mensaje' => "ERROR: Al eliminar el congreso."));
			}		
		});
	}

	public function SolicitudEliminarCongreso()
	{
		return DB::transaction(function()
		{
			DB::beginTransaction();
			try {
	    		if(Request::ajax())
				{
					$id = Input::get("idCongreso");
					$estadoCon=DB::table('estado_congreso')->Where('valorEstado','=','SNC')->first();
					$nuevoComentario="CREA_SL-ELI_RQST-ADM-AS4P";
					$res=Congreso::where('idCongreso', '=',$id)->update(array('comentarioCongreso' => $nuevoComentario,
																			'fecModCongreso' => date('Y-m-d H:i:s'),
																			'idEstadoCongreso' => $estadoCon->idEstadoCongreso));
					if(count($res)==0)
						throw new Exception("ERROR: Al enviar petición");

					$admin=DB::table('usuarios')
							->join('estado_usuario',
									'estado_usuario.idEstadoUsuario',
									'=',
									'usuarios.idEstadoUsuario'
								)
							->Where('estado_usuario.valorEstado',
									'=',
									'ADM'
									)->first();
					$res=Congreso::where('idCongreso', '=',$id)->first();
					$usr =Usuario::where('idUsuario', '=',$res->idCreador)->first();
					$banner = URL::asset('imagenes/banner_mail_conuca.png');
					$sent=Mail::send(
						'emails.notificacionEliminarCongresoAdmin', 
						array(
							'nombreSistema'	=>	$res->acronimoCongreso,
							'nombreSistemaCompleto'	=>	$res->nomCongreso,
							'correo'		=>	$usr->emailUsuario,
							'nombres'		=>	$usr->nombreUsuario,
							'apellidos'		=>	$usr->apelUsuario,
							'anio'			=>	date("Y"),
							'rolSistema'	=> 'Administrador de Congreso',
							'URLConfirmar'	=> '',
							'banner_email' => $banner,
							'URLSitio'		=> URL::action('HomeController@inicio'),
						), 
						function($mensaje) use ($usr,$admin)
						{
							$mensaje->to($admin->emailUsuario, $usr->nombreUsuario . ' ' . $usr->apelUsuario)
									->subject('CONUCA: Auto-Mensaje , solicitud de eliminación requiere gestión.');
						}
					);
					if($sent==0) throw new Exception('No se pudo enviar el correo de notificación , favor intentelo mas tarde.');

					DB::commit();
					return Response::json(array('error' => False, 'mensaje' => 'Su solicitud ha sido enviada, su congreso volverá al estado inicial "SIN CONFIGURAR" hasta que esta sea gestionada por el Administrador del sistema.'));
					
				}	
	    	} catch (Exception $e) {
	    		DB::rollback();
	    		return Response::json(array('error' => True, 'mensaje' => "ERROR: Al solicitar la eliminación del congreso. ".$e->getMessage()));
			}		
		});
	}

	public function cambiarEstadoCongreso()
	{
		return DB::transaction(function()
		{
			DB::beginTransaction();
			try {
	    		if(Request::ajax())
				{
					$idUsuario = Auth::check()?Auth::user()->idUsuario:0;
					$isAdmin=DB::table('usuarios')
								->join('estado_usuario',
										'estado_usuario.idEstadoUsuario',
										'=',
										'usuarios.idEstadoUsuario'
									)
								->Where('estado_usuario.valorEstado','=','ADM')
								->Where('usuarios.idUsuario','=',$idUsuario)
								->first();
					if(count($isAdmin)!=1)
						throw new Exception("ERROR: Solamente el Administrador del sistema puede realizar esta acción.");

					$id = Input::get("idCongreso");
					$estado = Input::get("estado");
					$leEstadoC="";
					if(strcasecmp($estado,"true")==0)
						$leEstadoC=DB::table('estado_congreso')->Where('valorEstado','=','ACT')->first();
					else
						$leEstadoC=DB::table('estado_congreso')->Where('valorEstado','=','SNC')->first();

					$res=Congreso::where('idCongreso', '=',$id)->update(
															array('idEstadoCongreso' => $leEstadoC->idEstadoCongreso,
																 'fecModCongreso' => date('Y-m-d H:i:s'),
																 'comentarioCongreso' => ''
																 ));
					if(count($res)==0)
						throw new Exception("ERROR: Al actualizar el congreso.");
		
					DB::commit();
					return Response::json(array('error' => False, 'mensaje' => 'El estado del congreso ha sido cambiado.'));
					
				}	
	    	} catch (Exception $e) {
	    		DB::rollback();
	    		return Response::json(array('error' => True, 'mensaje' => "ERROR: Al cambiar estado del congreso."));
			}		
		});
	}

	public function listarTematicasDisponibles()
	{
		try {
    		if(Request::ajax())
			{
				$idCongreso = Input::get("idCongreso");
				$idUsuario = Input::get("idUsuario");

				$inscritoPC=DB::table('pc_x_tematica')
								->join('usuariorol_x_congreso',
										'pc_x_tematica.idUsuariorolXCongreso',
										'=',
										'usuariorol_x_congreso.idUsuariorolXCongreso'
								)
								->where('usuariorol_x_congreso.idCongreso',
										'=',
										$idCongreso
									)
								->where('usuariorol_x_congreso.idUsuario',
										'=',
										$idUsuario
									)
								->get();

				if(count($inscritoPC)!=0)
					return Response::json(array('error' => True, 'mensaje' => 'Usted ya esta inscrito como encargado de una temática y no puede ser revisor de otra.'));

				$inscrito=DB::table('pc_x_revisor_area')
							->join('pc_x_tematica',
									'pc_x_tematica.idPcTematica',
									'=',
									'pc_x_revisor_area.idPcRevisor'
								)
							->join('usuariorol_x_congreso',
									'usuariorol_x_congreso.idUsuariorolXCongreso',
									'=',
									'pc_x_tematica.idUsuariorolXCongreso'
								)
							->where('usuariorol_x_congreso.idCongreso',
									'=',
									$idCongreso
								)
							->where('pc_x_revisor_area.idUsuario',
									'=',
									$idUsuario
								)
							->select('pc_x_revisor_area.idTematica')
							->lists('pc_x_revisor_area.idTematica');

				$tematicas=DB::table('pc_x_tematica')
						->join('tematica',
								'tematica.idTematica',
								'=',
								'pc_x_tematica.idTematica'
							)
						->join('usuariorol_x_congreso',
								'usuariorol_x_congreso.idUsuariorolXCongreso',
								'=',
								'pc_x_tematica.idUsuariorolXCongreso'
							)
						->join('estado_urc',
								'estado_urc.idEstadoURC',
								'=',
								'usuariorol_x_congreso.idEstadoURC'
							)
						->join('rol',
								'rol.idRol',
								'=',
								'usuariorol_x_congreso.idRol'
							)
						->where('usuariorol_x_congreso.idCongreso',
								'=',
								$idCongreso
							)
						->where('estado_urc.valorEstado',
								'=',
								'ACT'
							)
						->whereIn('rol.nomRol',
								['PC','Chair']
							)->get();

					if(count($inscrito)!=0)
						$tematicas=DB::table('pc_x_tematica')
							->join('tematica',
									'tematica.idTematica',
									'=',
									'pc_x_tematica.idTematica'
								)
							->join('usuariorol_x_congreso',
									'usuariorol_x_congreso.idUsuariorolXCongreso',
									'=',
									'pc_x_tematica.idUsuariorolXCongreso'
								)
							->join('estado_urc',
									'estado_urc.idEstadoURC',
									'=',
									'usuariorol_x_congreso.idEstadoURC'
								)
							->join('rol',
									'rol.idRol',
									'=',
									'usuariorol_x_congreso.idRol'
								)
							->where('usuariorol_x_congreso.idCongreso',
									'=',
									$idCongreso
								)
							->where('estado_urc.valorEstado',
									'=',
									'ACT'
								)
							->whereNotIn('pc_x_tematica.idTematica',$inscrito)
							->whereIn('rol.nomRol',
									['PC','Chair']
								)->get();
				
				if(count($tematicas)==0)
					return Response::json(array('error' => True, 'mensaje' => 'No hay temáticas disponibles para aplicar.'));
				$htmlVista= $this->generarHTMLTematicas($tematicas);
				return Response::json(array('error' => False, 'mensaje' => 'Listado de temáticas.','htmlvista' => $htmlVista));
				
			}	
    	} catch (Exception $e) {
    		return Response::json(array('error' => True, 'mensaje' => "ERROR: Extraer temáticas."));
		}		
	}

	/**
	 * Funcion que genera una salida en HTML para la vista que contiene el listado de temáticas disponibles para el congreso
	 * Utilizada para solicitar ser revisor
	 * @return HTML
	 */	
	public function generarHTMLTematicas($tematicas)
	{
		if(count($tematicas)==0)return "";
		$cade="";
		$cade.='<select data-placeholder="Seleccione una temática" class="chosen" id="listaTematicas">';
		foreach($tematicas as $tematica)
		{
			$cade.='<option value='.$tematica->idPcTematica.'>'.$tematica->nomTematica.'</option>';
		}
		$cade.='</select>';
		return $cade;
	}

}