<?php 

class TipoFechaController extends BaseController {
	
	public function index()
	{
		if (Auth::guest()) return Redirect::guest('login');
		$tiposF = TipoFecha::orderBy('nomTipoFecha')->get();
		$this->layout->content = View::make('adminTipoFechas')->with('tiposF',$tiposF);
	}

	public function create()
	{
		if(Request::ajax())
		{
				try {
					$validRecord = TipoFecha::where("nomTipoFecha", "=", Input::get('nomTipoFecha'))
									->count();
					if($validRecord > 0)
					{
						return Response::json(array('error' => True, 'mensaje' => 'Tipo de Fecha ya existente, imposible agregar'));
					}					
					$miModelo = new TipoFecha;
					$miModelo->nomTipoFecha = Input::get('nomTipoFecha');
					$miModelo->save();
					return Response::json(array('error' => False, 'mensaje' => 'Guardando... espere un momento mientras se refresca la pantalla'));
				} catch (Exception $e) {
					return Response::json(array('error' => True, 'mensaje' => 'Problemas para agregar Tipo Fecha'));
				}
		}
	}

	public function edit($id)
	{
		if(Request::ajax())
		{
			if(Input::get('accion') == 'edit')
			{
				try {

					$validRecord = TipoFecha::where("nomTipoFecha", "=", Input::get('nomTipoFecha'))
									->where("idTipoFecha", "<>", $id)
									->count();
					if($validRecord > 0)
					{
						return Response::json(array('error' => True, 'mensaje' => 'Tipo de Fecha ya existente, no se puede modificar'));
					}

					$miModelo	= TipoFecha::find($id);
					$miModelo->nomTipoFecha = Input::get('nomTipoFecha' );

					if (!$miModelo->save()) {
						return Response::json(array('error' => True, 'mensaje' => 'Problemas para modificar Tipo de Fecha'));
					}
					return Response::json(array('error' => False, 'mensaje' => 'Actualizando... espere un momento mientras se refresca la pantalla'));
				} catch (Exception $e) {
					return Response::json(array('error' => True, 'mensaje' => 'Problemas para modificar Tipo Fecha'));
				}
			}
			
			if(Input::get('accion') == 'delete')
			{
				try {
					$currentId = $id;
					TipoFecha::destroy($currentId);
					return Response::json(array('error' => False, 'mensaje' => 'Tipo de Fecha eliminada, espere un momento mientras se refresca la pantalla'));
				} catch (Exception $e) {
					return Response::json(array('error' => True, 'mensaje' => 'ALTO!  Fecha en uso, no se puede eliminar'));
				}
			}
			
		}
	}
}