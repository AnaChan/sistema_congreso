<?php

return array(
	"lenguaje"		=>	"Language",
	"idiomaingles"	=>	"English",
	"idiomaespanol"	=>	"Spanish",
	"inisesion"		=>	"Log in",
	"credenciales"	=>	"Credentials",
	"correo"		=>	"Email",
	"password"		=>	"Password",
	"recordardatos"	=>	"Remember user",
	"ingresar"		=>	"Log in",
	"accessError"	=>	"Session has expired. You logged in another machine.",
	"copyright"		=>	"All rights reserved",
);
