<?php

return array(
	"lenguaje"		=>	"Lenguaje",
	"idiomaingles"	=>	"Inglés",
	"idiomaespanol"	=>	"Español",
	"inisesion"		=>	"Iniciar sesión",
	"credenciales"	=>	"Credenciales",
	"correo"		=>	"Correo electrónico ",
	"password"		=>	"Contraseña",
	"recordardatos"	=>	"Recordar usuario",
	"ingresar"		=>	"Ingresar",
	"accessError"	=>	"Sesión ha caducado. Usted ha entrado en otra máquina.",
	"copyright"		=>	"Todos los derechos reservados",
);
