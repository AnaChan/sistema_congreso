<?php

class Congreso extends Eloquent {

	/**
	 * Nombre de la tabla.
	 *
	 * @var string
	*/
	protected $table = 'congreso';

	/**
	* Campo primario de la tabla.
	*
	* @var string
	*/
	protected $primaryKey = 'idCongreso';
	protected $key = 'idCongreso';
	
	/**
	* Si posee los campos generados por la migration.
	*
	* @var array
	*/
	public $timestamps = false;
	
	protected $softDelete = false;

}