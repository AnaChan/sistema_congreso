<?php

class DetallecongresoXExtension extends Eloquent {
	protected $table = 'detallecongreso_x_extension';
	protected $fillable = ['idExtensionDocumento','idDetalleCongreso'];
	public $timestamps = true;
	protected $softDelete = false;
}