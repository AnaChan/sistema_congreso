<?php

class AutorXFicha extends Eloquent {

	protected $table = 'autor_x_ficha';
	public $timestamps = true;
	protected $primaryKey  = 'idFichaXAutor';
	protected $softDelete = false;

	public function updateRuta($id,$ruta)
	{
		$paper = paper::find($id);
		$modificado = paper::where('idFichaXAutor', '=', $id)->update(array('rutaDiploma' => $ruta));

	}

}