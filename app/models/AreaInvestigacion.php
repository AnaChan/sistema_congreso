<?php

class AreaInvestigacion extends Eloquent {

	protected $table = 'area_investigacion';
	public $timestamps = false;
	protected $softDelete = false;

	protected $primaryKey = 'idAreaInvestigacion';
	protected $key = 'idAreaInvestigacion';

}