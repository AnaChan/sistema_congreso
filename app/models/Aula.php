<?php

class Aula extends Eloquent {

	protected $table = 'aula';
	public $timestamps = false;
	protected $softDelete = false;
	protected $primaryKey  = 'idAula';
}