<?php

class EstadoFicha extends Eloquent {

	protected $table = 'estado_ficha';
	protected $primaryKey  = 'idEstadoFicha'; //arodriguez
	protected $fillable = ['nombreEstado', 'valorEstado'];
	public $timestamps = true;
	protected $softDelete = false;
}