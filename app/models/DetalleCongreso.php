<?php

class DetalleCongreso extends Eloquent {

	protected $table = 'detalle_congreso';
	public $timestamps = true;
	protected $primaryKey  = 'idDetalleCongreso';
	protected $softDelete = false;
	protected $fillable = ['banner','longitudMaxResumenPaper','maxChairVerSumision',
						'numFaxAutor','dirAutor','nomRevisorVisible','tamMaxArchivo','rutaPlantillaDiploma','idCongreso'];

	public function updateRuta($id,$ruta,$styleNum)
	{
		$paper = DetalleCongreso::find($id);
		switch ($styleNum) {
		    case 1:
		    	$modificado = DetalleCongreso::where('idDetalleCongreso', '=', $id)->update(array('rutaPlantillaDiploma' => $ruta));
		        break;
		    case 2:
		    	$modificado = DetalleCongreso::where('idDetalleCongreso', '=', $id)->update(array('rutaPlantillaDiploma2' => $ruta));
		        break;
		    case 3:
		    	$modificado = DetalleCongreso::where('idDetalleCongreso', '=', $id)->update(array('rutaPlantillaDiploma3' => $ruta));
		        break;
		}
	}

}