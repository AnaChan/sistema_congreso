<?php

class PlantillaCorreo extends Eloquent {

	protected $table = 'plantilla_correo';
	public $timestamps = false;
	protected $softDelete = false;

}