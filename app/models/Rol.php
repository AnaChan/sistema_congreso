<?php

class Rol extends Eloquent {

	protected $table = 'rol';
	protected $primaryKey  = 'idRol'; 
	protected $key  = 'idRol'; 
	protected $fillable = ['nomRol'];
	public $timestamps = false;
	protected $softDelete = false;

}