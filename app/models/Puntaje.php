<?php

class Puntaje extends Eloquent {

	protected $table = 'puntaje';
	public $timestamps = false;
	protected $softDelete = false;
	protected $fillable = ['valorPuntaje','idDetalleCongreso','nomPuntaje'];
	
}