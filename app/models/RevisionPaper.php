<?php

class RevisionPaper extends Eloquent {

	protected $table = 'revision_paper';
	protected $primaryKey  = 'idRevisionPaper';
	public $timestamps = true;
	protected $softDelete = false;
	protected $fillable = ['idUsuario','idPaper','comentario'];

}