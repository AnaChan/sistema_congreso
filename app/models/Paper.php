<?php

class Paper extends Eloquent {

	protected $table = 'paper';
	protected $primaryKey  = 'idPaper'; //arodriguez
	public $timestamps = true;
	protected $softDelete = false;

	public function obtenerRuta($id)
	{
		$paper = paper::find($id);
		$ruta = $paper[0]->rutaFuentes;
		return $ruta;
	}

	public function updateRuta($id,$ruta)
	{
		$paper = paper::find($id);
		$modificado = paper::where('idPaper', '=', $id)->update(array('rutaPaper' => $ruta));

	}

	public function updateFuentes($id,$ruta)
	{
		$paper = paper::find($id);
		$modificado = paper::where('idPaper', '=', $id)->update(array('rutaFuentes' => $ruta));

	}

}