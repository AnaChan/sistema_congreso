<?php

class TipoFecha extends Eloquent {

	protected $table = 'tipo_fecha';
	public $timestamps = false;
	protected $softDelete = false;

	protected $primaryKey = 'idTipoFecha';
	protected $key = 'idTipoFecha';

}