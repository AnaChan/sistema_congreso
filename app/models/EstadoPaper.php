<?php

class EstadoPaper extends Eloquent {

	protected $table = 'estado_paper';
	protected $primaryKey  = 'idEstadoPaper';
	protected $fillable = ['nombreEstado'];
	public $timestamps = true;
	protected $softDelete = false;
}

