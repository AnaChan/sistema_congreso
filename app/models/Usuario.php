<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class Usuario extends \Eloquent implements UserInterface, RemindableInterface{
	
	/**
	 * Nombre de la tabla.
	 *
	 * @var string
	*/
	protected $table = 'usuarios';

	/**
	* Campo primario de la tabla.
	*
	* @var string
	*/
	protected $primaryKey = 'idUsuario';
	protected $key = 'idUsuario';

	/**
	* Campos que se pueden llenar con un seeder.
	*
	* @var array
	*/
	protected $fillable = ['nombreUsuario', 'apelUsuario', 'passwordUsuario', 'emailUsuario', 'idEstadoUsuario', 'fecModUsuario', 'codConfirmarUsuario','resumenUsuario'];
	
	/**
	* Si posee los campos generados por la migration.
	*
	* @var array
	*/
	public $timestamps = false;

	/**
	 * Estos atributos no son pasado cuando se realiza el JSON de este objeto.
	 *
	 * @var array
	 */
	protected $hidden = array('passwordUsuario');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->passwordUsuario;
	}

	/**
	 * Get the token value for the "remember me" session.
	 *
	 * @return string
	 */
	public function getRememberToken()
	{
		return $this->recordarUsuario;
	}

	/**
	 * Set the token value for the "remember me" session.
	 *
	 * @param  string  $value
	 * @return void
	 */
	public function setRememberToken($value)
	{
		$this->recordarUsuario = $value;
	}

	/**
	 * Get the column name for the "remember me" token.
	 *
	 * @return string
	 */
	public function getRememberTokenName()
	{
		return 'recordarUsuario';
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->emailUsuario;
	}

	public static function boot()
    {
        parent::boot();

        static::creating(function($post)
        {
            $post->nombreUsuario = mb_strtoupper($post->nombreUsuario);
            $post->apelUsuario = mb_strtoupper($post->apelUsuario);
        });

        static::updating(function($post)
        {
			$post->nombreUsuario = mb_strtoupper($post->nombreUsuario);
			$post->apelUsuario = mb_strtoupper($post->apelUsuario);
        });
    }

}