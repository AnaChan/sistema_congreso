<?php

class PcXTematica extends Eloquent {

	protected $table = 'pc_x_tematica';
	protected $fillable = ['idUsuariorolXCongreso','idTematica'];
	public $timestamps = true;
	protected $softDelete = false;

}