<?php

class PcXRevisorArea extends Eloquent {

	protected $table = 'pc_x_revisor_area';
	protected $primaryKey  = 'idPcRevisorArea'; 
	protected $key  = 'idPcRevisorArea'; 
	protected $fillable = ['idPcRevisor','idTematica','idUsuario'];
	public $timestamps = true;
	protected $softDelete = false;
}