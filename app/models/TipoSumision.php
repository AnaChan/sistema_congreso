<?php

class TipoSumision extends Eloquent {

	protected $table = 'tipo_sumision';
	public $timestamps = false;
	protected $softDelete = false;

}