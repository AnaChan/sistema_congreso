<?php

class FechaImportante extends Eloquent {

	protected $table = 'fecha_importante';

	protected $fillable = ['idTipoFecha', 'idDetalleCongreso','fecInicio','fecFin'];

	public $timestamps = true;
	protected $softDelete = false;

}