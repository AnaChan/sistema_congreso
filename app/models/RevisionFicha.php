<?php

class RevisionFicha extends Eloquent {

	/**
	 *
	 * Nombre de la tabla en DB.
	 */
	protected $table = 'revision_ficha';

	/**
	 *
	 * llave primaria de la tabla en DB.
	*/
	protected $primaryKey  = 'idRevisionFicha';
	
	public $timestamps = true;
	protected $softDelete = false;
	protected $fillable = ['idUsuario','idFicha','comentarioFicha'];

}