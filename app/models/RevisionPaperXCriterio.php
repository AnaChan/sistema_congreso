<?php

class RevisionPaperXCriterio extends Eloquent {

	/**
	 *
	 * Nombre de la tabla en DB.
	 */
	protected $table = 'revision_paper_x_criterio';

	/**
	 *
	 * llave primaria de la tabla en DB.
	*/
	protected $primaryKey = 'idRevisionPaperXCriteriO';

	public $timestamps = true;
	protected $softDelete = false;
	protected $fillable = ['idRevisionPaper','idPuntaje'];

}