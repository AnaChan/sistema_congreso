<?php

class Tematica extends Eloquent {

	protected $table = 'tematica';
	protected $fillable = ['nomTematica','idDetalleCongreso','idAreaInvestigacion'];
	public $timestamps = false;
	protected $softDelete = false;
	protected $primaryKey  = 'idTematica';//arodriguez.05.19


}