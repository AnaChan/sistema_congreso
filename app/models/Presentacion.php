<?php

class Presentacion extends Eloquent {

	protected $table = 'presentacion';
	protected $primaryKey  = 'idPresentacion';
	public $timestamps = false;
	protected $softDelete = false;

	public function updateRuta($id,$ruta)
	{
		$presentacion = presentacion::find($id);
		$modificado = presentacion::where('idPresentacion', '=', $id)->update(array('rutaPresentacion' => $ruta));

	}

}