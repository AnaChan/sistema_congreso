<?php

class CongresoXTematica extends Eloquent {

	protected $table = 'congreso_x_tematica';
	protected $fillable = ['idCongreso','idTematica'];
	public $timestamps = true;
	protected $softDelete = false;

}