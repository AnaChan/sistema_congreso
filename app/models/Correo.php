<?php 
/*
	Clase encargada del envio de correos, pues solo se utiliza una libreria que ya trae laravel para el envio de correos. 
	solo se ha parametrizado de acuerdo a los datos enviados en la llamada al metodo
*/
class Correo extends Mail{
	public static function enviarCorreos($data)
	{
		Mail::send( $data['plantilla'], $data, function( $message ) use ($data) //Envio del correo
		{
			$message->to( $data['email'] )->from( $data['from'], $data['first_name'] )->subject( 'CONUCA Información' );
		});
	}
}

