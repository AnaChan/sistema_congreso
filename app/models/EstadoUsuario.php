<?php

class EstadoUsuario extends Eloquent {

	protected $table = 'estado_usuario';
	public $timestamps = false;
	protected $softDelete = false;

	protected $primaryKey = 'idEstadoUsuario';
	protected $key = 'idEstadoUsuario';
}