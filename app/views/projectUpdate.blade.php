@section('content')
<div class="container">
		<div class="panel-body">
			<div class="row">
				<div class="col-sm-12">
					<div class="alert alert-info fade in">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						{{$mensaje}}
						<br><br>
						Básicamente esta pantalla espera como entrada la ruta de un archivo a actualizar, y el contenido del archivo a actualizar.<br><br>
						Se ponen las rutas a partir del BASE PATH del proyecto.<br><br>
						Ejemplo:<br>
						Si quieres actualizar el archivo routes.php, se debería de hacer de la siguiente manera:<br>

						Caja de arriba [input text]:    app\routes.php <br>
						Caja grande [textarea control]: XXXXXX….todo  el contenido del archivo…. XXXXX<br><br>

						Se actualiza todo el contenido de los archivos.<br>
						En la ruta proporcionada se debe de incluir la extensión del archivo.<br>
						Si se especifica una ruta de archivo, pero el contenido va en blanco… este no se actualizara<br>
						Si se proporciona una ruta apuntando a un archivo que NO existe, este se creara.<br>
						Al terminar la tarea, se mostrara un listado de todos los archivos afectados.

					</div>
					<form role="form"  id="projectUpdate" name="projectUpdate" enctype="multipart/form-data" action="{{URL::action('DeveloperController@updateFiles')}}" method="POST"  class="form-horizontal">
					<fieldset>
						<legend>Actulización del repositorio</legend>
						<div class="form-group">
							<div class="col-sm-4">
								<label>Archivo 1</label><br>
								{{ Form::text('rutafile1') }}<br><br>
								{{ Form::textarea('file1') }}
							</div>
							<div class="col-sm-4">
								<label>Archivo 2</label><br>
								{{ Form::text('rutafile2') }}<br><br>
								{{ Form::textarea('file2') }}
							</div>
							<div class="col-sm-4">
								<label>Archivo 3</label><br>
								{{ Form::text('rutafile3') }}<br><br>
								{{ Form::textarea('file3') }}
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4">
								<label>Archivo 4</label><br>
								{{ Form::text('rutafile4') }}<br><br>
								{{ Form::textarea('file4') }}
							</div>
							<div class="col-sm-4">
								<label>Archivo 5</label><br>
								{{ Form::text('rutafile5') }}<br><br>
								{{ Form::textarea('file5') }}
							</div>
							<div class="col-sm-4">
								<label>Archivo 6</label><br>
								{{ Form::text('rutafile6') }}<br><br>
								{{ Form::textarea('file6') }}
							</div>
						</div>
					</fieldset>
				</form>
				</div>
			</div>
		</div>
		<center>
			<button type="button" id="performAction"  class="btn btn-primary btn-default">Actualizar</button>
			<br><br>
		</center>
</div>

<script type="text/javascript">
	$(document).ready(function()
	{
		$("#performAction").click(function()
		{
			//Fake ajax .. para dar una sensacion de progreso al usuario.
        	$('#cargando-img').removeClass('hidden');
			$("#fade").fadeToggle("slow");
			document.forms["projectUpdate"].submit();
		});
	});
</script>
@stop