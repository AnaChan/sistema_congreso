@section('content')
	<style>
       .table td{
        cursor:pointer;
       }
     </style>
<div class="container">
	<div class="col-sm-10 col-sm-offset-1" style="margin-top: 50px;">
		<div class="panel panel-primary" style="border-color: #205081; border-radius: 2px; box-shadow: 0 5px 10px gray;">
			<div class="panel-heading" style="border-radius: 0px; background: #205081; border-color: #205081;">
			@if ($info['tematica'] == [])
				<div class="panel-title">Asignaci&oacute;n de Fichas</div>
						
			@else
				<div class="panel-title">Asignaci&oacute;n de Fichas para la tem&aacute;tica: <strong> {{{$info['tematica']->nomTematica}}}</strong></div>
			
			@endif
			</div>
			<div class="panel-body">
				<div class="col-md-10 column col-sm-offset-1">
					<fieldset>
						<legend>
							<h3>
								Listado de fichas
								@if ($info['tematica'] == [])
								<div class="alert alert-info fade in">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									No ha sido asignado como un miembro del comite para una tematica.
								</div>
								@endif
							</h3>
						</legend>
							@if ($info['fichas'] == [])

								<div class="alert alert-info fade in">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									No hay fichas disponibles
								</div>
							@else
								<div class="alert alert-info fade in">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									Seleccione una ficha para ver los revisores asignados o los que se encuentran disponibles.
								</div>
							@endif
								<table id="tblCongresos" class="display table">
									<thead>
										<tr>
											<th>
												Id
											</th>
											<th>
												Ficha
											</th>
											<th>
												Tem&aacute;tica
											</th>
											<th>
												Revisores
											</th>
											<th>
												Previa
											</th>
										</tr>
									</thead>
									<tbody>
										@foreach ($info['fichas'] as $ficha)
											<tr class="clickRow" style="cursor:pointer;">
												<td visible="false">
													{{$ficha->idFicha}}
												</td>

												<td>
													{{$ficha->tituloPaper}}
												</td>
												<td >
													{{$info['tematica']->nomTematica}}
												</td>
												<td style="text-align:center;">
													{{$ficha->cantRev}}
												</td>
												<td style="text-align:center;">
													<a target="_blank" href="{{ URL::action('ArchivoController@previsualizarArchivo', array($idCongreso,$ficha->rutaFicha)) }}"><span class="glyphicon glyphicon-search"></span></a>
												</td>
											</tr>
												
										@endforeach
									</tbody>
								</table>
								<br/>
							</fieldset>
						</div>
						<br/>

				<div class="col-md-10 column col-sm-offset-1" id="revisores" visible="false">
					<fieldset>
						<legend><h3>Revisores Asignados</h3></legend>
						<div class="form-group 	">
							<select multiple data-placeholder="Seleccione los revisores" class="chosen" id="revLista">
							@foreach($info['revisores'] as $revisor)
								<option  value="{{$revisor->idUsuario}}">{{$revisor->nombreUsuario .' '. $revisor->apelUsuario}}</option>
							@endforeach
							</select>
						</div>
					</fieldset>
				</div>
				<br/>
				<div class="col-md-10 column col-sm-offset-1" align="right">
					<button type="button" class="btn btn-primary" id="regresar">Regresar</button>
				</div>
			</div>
		</div>
	</div>
<script type="text/javascript">

	$(document).ready(function()
	{
		$("#revisores").hide();
		$("#revLista").chosen(width= '250px');
		$('#tblCongresos').DataTable({
			  "columns": [
						    { "width": "5%" },
						    { "width": "50%" },
						    { "width": "20%" },
						    { "width": "10%" },
						    { "width": "15%" },
						  ],
            "language": {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
							"sFirst":    "Primero",
							"sLast":     "Último",
							"sNext":     "Siguiente",
							"sPrevious": "Anterior"
							},
				"oAria": {
						"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
						"sSortDescending": ": Activar para ordenar la columna de manera descendente"
						}
            }
		});

		var actual;
		var id;
		var elementosActuales=$('#revLista').val();

		$("#tblCongresos tbody" ).on('click', 'tr.clickRow',  function()
		{
			$(actual).removeClass("info");
			actual = this;
			id=$(this).children().eq(0).html().trim();
			actual = this;
            $("#fichaActiva").val(id);    
			$("#revLista option").prop('selected', false);
			$('#revLista').trigger("chosen:updated");
			$.post("{{URL::action('AsignarFichaController@revisoresFicha')}}", {fichaEditar: id})
			.done(function(data, status,jqXHR)
			{
				if(data.error)
				{
					alertify.error(data.mensaje);
				}
				else
				{
					$(actual).removeClass("info").addClass("info");
					$("#revisores").show();
					var seleccionados =data.agregados;
					
					var arrayLength = seleccionados.length;
					
					var arreglo = [];
					for (var i = 0; i < arrayLength; i++) {
						arreglo.push(seleccionados[i]);
						
					}
					$('#revLista').val(arreglo);
					$('#revLista').trigger("chosen:updated");
					elementosActuales=$('#revLista').val();
					
				}
			});
      	});

      	function buscarID(id,array)
		{
			for (var i=0; i<array.length; i++){
				if(array[i]==id) return i;
			}
			return -1;
		}

		$('#revLista').unbind('change');
		$('#revLista').on('change', function(event, params) {
			event.preventDefault();
			if(params.selected){
				var revisor = new Array();
				revisor.push(params.selected);
				$.post("{{URL::action('AsignarFichaController@guardarRevisores')}}", {fichaEditar: id,arregloIDRevisores:JSON.stringify(revisor)})
				.done(function(data, status,jqXHR)
				{
					if(data.error)
					{
						alertify.error(data.mensaje);
						$('#revLista').val(elementosActuales);
						$('#revLista').trigger("chosen:updated");
					}
					else
					{
						alertify.success(data.mensaje);
						elementosActuales=$('#revLista').val();
						actual.cells[3].innerHTML=parseInt(actual.cells[3].innerHTML)+1;
					}
				});	
			}
			else{
				var idS=params.deselected
			
				$.post("{{URL::action('AsignarFichaController@eliminarRevisorFicha')}}", {fichaEditar: id,idUsuario:idS})
				.done(function(data, status,jqXHR)
				{
					if(data.error)
					{
						alertify.error(data.mensaje);
						$('#revLista').val(elementosActuales);
						$('#revLista').trigger("chosen:updated");
					}
					else
					{
						alertify.success(data.mensaje);
						elementosActuales=$('#revLista').val();
						actual.cells[3].innerHTML=parseInt(actual.cells[3].innerHTML)-1;
					}
				});

			}
		});


		function quitarElemento(id)
		{
			var arreglo;
			var valor;
			for (var i=0; i<revisores.length; i++){
				arreglo = revisores[i];
				valor = arreglo[0];
				if(revisores[i][0]==id) 
				{
					revisores.splice(i,1);

					return true
				};
			}
			return false;
		}

		$("#regresar").click(function()
		{
			window.location.href="{{URL::action('UsuariosCongresosController@establecerCongreso',array($idCongreso))}}";
      	});
	});
</script>
@stop