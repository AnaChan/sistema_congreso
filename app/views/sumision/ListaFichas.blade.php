@section('content')
	<style>
       .table td{
        cursor:pointer;
       }
     </style>
	<div class="container">
		<div class="col-sm-10 col-sm-offset-1" style="margin-top: 50px;">
			<div class="panel panel-primary" style="border-color: #205081; border-radius: 2px; box-shadow: 0 5px 10px gray;">
				<div class="panel-heading" style="border-radius: 0px; background: #205081; border-color: #205081;">
					<div class="panel-title">Asignaci&oacute;n de Fichas para la tem&aacute;tica: <strong> </strong></div>
				</div>
				<div class="panel-body">
					
					<div class="row">
						<div class="col-md-10 column col-sm-offset-1">
							<fieldset>
								<legend>
									<h3>
										Listado de fichas:
									</h3>
								</legend>
								<div aling="justify">
									<p>Seleccione una ficha para asignarle un articulo</p>
								</div>

								<table id="tblCongresos" class="display">
									<thead>
										<tr>
											<th>
												Id de Ficha
											</th>
											<th>
												Titulo
											</th>
											<th>
												Tematica
											</th>
											<th>
												Categoria
											</th>
										</tr>
									</thead>
									<tbody>
										@foreach ($Fichas as $ficha)
											<tr class="clickRow" style="cursor:pointer;">
												<td visible="false">
													{{$ficha->idFicha}}
												</td>
												<td>
													{{$ficha->tituloPaper}}
												</td>
												<td>
													{{Tematica::Find($ficha->idTematica)->nomTematica}}
												</td>
												<td>
													{{Categoria::Find($ficha->idCategoria)->nomCategoria}}
												</td>
											</tr>
												
										@endforeach
									</tbody>
								</table>
							</fieldset>
						</div>
					</div>
				</div>
			<div class="panel-footer">
				<div class="row">
					<div class="col-sm-4">
						<button type="button" id="cancelarTodo" class="btn btn-default btn-default">Regresar</button>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>
<!--Ventana Modal para enviar solicitud para ser revisor -->

<div class="modal fade" id="nueva-solicitud-revisor-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="padding-top: 12%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" >Enviar solicitud para ser revisor del congreso.</h4>
      </div>
      <div class="modal-body">
		<div class="container" style="width:450px">
			<div class="row clearfix">
				<div class="col-md-4 column">
					<span style="font-weight:bold">Elija la Tem&aacute;tica</span>
				</div>
				<div class="col-md-8 column" id="divTematicas" > 
					<select class="chosen" id="listaTematicas" placeholder="Lista de temáticas" >
					</select>
				</div>
			</div>
			<br/>
     	</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" id="agregarSolicitud">Enviar Aplicaci&oacute;n</button>
			<button type="button" class="btn btn-primary" data-dismiss="modal" >Cancelar</button>
		</div>
    </div>
  </div>
</div>
</div>

<script type="text/javascript">
	$("#listaTematicas").chosen();
	$("#nueva-solicitud-revisor").click(function(){

		$.post("{{URL::action('CongresoController@listarTematicasDisponibles')}}", {idCongreso: "{{$idCongreso}}",idUsuario:"{{Auth::user()->idUsuario}}"})
		.done(function(data)
		{
			if(data.error){
				alertify.error(data.mensaje);
			}
			else{
				console.log(data.htmlVista);
				$("#divTematicas").html(data.htmlvista);
				$("#listaTematicas").chosen();
				$('#listaTematicas').trigger("chosen:updated");
				$('#nueva-solicitud-revisor-form').modal('show');
			}
		})
		.fail(function(data, status, jqXHR)
		{
			alertify.error("Error: No se puede obtener el listado de temáticas.");
		});
	});

	$("#agregarSolicitud").click(function(){
		alertify.confirm("Si envía la solicitud deberá esperar hasta que esta sea gestionada por el Administrador del congreso. <br>¿Está seguro que desea enviarla en este momento?", function (e) 
		{
			if (e) {
				enviarSolicitudRevisor();

			}
		});
	});

	function enviarSolicitudRevisor()
	{
		var idCongreso="{{$idCongreso}}";
		var idTematica="";
		var idPcTematica=$("#listaTematicas").val();
		var usuariosEnTabla = new Array();
		usuariosEnTabla.push("{{Auth::user()->idUsuario}}");
		$.post("{{URL::action('UsuariosCongresosController@actualizarRevisores')}}",
		 {idCongreso: "{{$idCongreso}}",
		idTematica: idTematica,
		idPcTematica:idPcTematica,
		listaUsuarios:JSON.stringify(usuariosEnTabla)
		})
		.done(function(data)
		{
			if(data.mensaje.indexOf("ERROR") != -1){
				alertify.error(data.mensaje);
			}
			else{
				alertify.success("Petición enviada existosamente, redireccionando...");
				window.location.href="{{URL::action('UsuariosCongresosController@establecerCongreso',array($idCongreso))}}";
			}
		})
		.fail(function(data, status, jqXHR)
		{
			alertify.error("Error en el servidor");
		});
	}

</script>
<script type="text/javascript">

	$(document).ready(function()
	{
		$('#tblCongresos').DataTable({
            "language": {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
							"sFirst":    "Primero",
							"sLast":     "Último",
							"sNext":     "Siguiente",
							"sPrevious": "Anterior"
							},
				"oAria": {
						"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
						"sSortDescending": ": Activar para ordenar la columna de manera descendente"
						}
            }
		});
		
		$("#cancelarTodo").click(function(){
			window.location.href="{{URL::action('UsuariosCongresosController@establecerCongreso',array($idCongreso))}}";
		});
      	$("#tblCongresos tbody" ).on('click', 'tr.clickRow',  function()
		{
			var id= $(this).children().eq(0).html();
            window.location.replace("SubidaFicha/" + id);
		});
	});
</script>

@stop