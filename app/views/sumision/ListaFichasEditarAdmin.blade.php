@section('content')
<style>
	.table td{
		cursor:pointer;
	}
</style>
<div class="container">
	<div class="col-sm-10 col-sm-offset-1" style="margin-top: 50px;">
	<div class="panel panel-primary" style="border-color: #205081; border-radius: 2px; box-shadow: 0 5px 10px gray;">
		<div class="panel-heading" style="border-radius: 0px; background: #205081; border-color: #205081;">
			<div class="panel-title">Selecci&oacute;n de ficha del Congreso: <strong> {{Congreso::Where('idCongreso','=',$idCongreso)->first()->nomCongreso}} </strong></div>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1">
					<form role="form" class="form-horizontal">
						<fieldset>
							<legend>Listado de fichas enviadas por los autores</legend>
							<div class="alert alert-info fade in">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								- Solo el autor responsable es el que puede realizar el cambio de la informaci&oacute;n.<br>
								- Los cambios pueden ser realizados hasta que el documento sea asignado a un revisor.
							</div> 
							<div class="form-group">
								<div class="col-sm-12">
									<table class="table table-hover table-condensed" id="tablaFichas">
										<thead>
										<tr>
											<th>
												#
											</th>
											<th>
												T&iacute;tulo
											</th>
											<th>
												Categor&iacute;a
											</th>
											<th>
												Tem&aacute;tica
											</th>
											<th>
												Fecha
											</th>
										</tr>
										</thead>
										<tbody>
										@foreach($fichas as $ficha)
										<tr id="{{$ficha->idFicha}}" class="clickRow">
											<td value="{{$ficha->idFicha}}">
												{{$ficha->idFicha}}
											</td>
											<td>
												{{$ficha->tituloPaper}}
											</td>
											<td value="{{$ficha->idCategoria}}">
												{{$ficha->nomCategoria}}
											</td>
											<td value="{{$ficha->idTematica}}">
												{{$ficha->nomTematica}}
											</td>			
											<td>
												{{date('d/m/Y', strtotime($ficha->created_at))}}
											</td>
										</tr>
										@endforeach
										</tbody>
										</table>
								</div>
								
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-sm-4">
					<button type="button" id="cancelarTodo" class="btn btn-default btn-default">Cancelar</button>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('#tablaFichas').DataTable({
            "language": {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
							"sFirst":    "Primero",
							"sLast":     "Último",
							"sNext":     "Siguiente",
							"sPrevious": "Anterior"
							},
				"oAria": {
						"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
						"sSortDescending": ": Activar para ordenar la columna de manera descendente"
						}
            }
		});


      	$("#tablaFichas tbody" ).on('click', 'tr.clickRow',  function()
		{
			var id= $(this).children().eq(0).html();
            window.location.replace("EditarAdmin/" + id);   
		});

		$("#cancelarTodo").click(function(){
			window.location.href="{{URL::action('UsuariosCongresosController@establecerCongreso',array($idCongreso))}}";
		});


	});
</script>
@stop