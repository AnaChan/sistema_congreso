@section('content')

<div class="container">
	<div class="col-sm-10 col-sm-offset-1" style="margin-top: 50px;">
		<div class="panel panel-primary" style="border-color: #205081; border-radius: 2px; box-shadow: 0 5px 10px gray;">
			<div class="panel-heading" style="border-radius: 0px; background: #205081; border-color: #205081;">
					<div class="panel-title">Informaci&oacute;n de la ficha</strong></div>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<form role="form" class="form-horizontal">
							<fieldset>
								<legend>Agregar Autores</legend>
								<div class="form-group">
									<div class="alert alert-info fade in">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										Todos los autores deben encontrarse inscritos en el congreso. De la lista de usuarios inscritos, seleccione aquellos que son autores del presente art&iacute;culo.&nbsp;
									</div>
									<div class="col-sm-6">
										 <div class="well well-sm" style="text-aling:center">
											Autores para la ficha:*
										</div>
											<select multiple data-placeholder="Seleccione los autores" disabled style="width:100%"  class="chosen" id="usuarios" size="5">
										 		@foreach ($info['usuarios'] as $usuario)
										 		<option value="{{$usuario->idUsuario}}"  name="option1">{{$usuario->nombreUsuario}}&nbsp;{{$usuario->apelUsuario}}</option>
										 		@endforeach
										 		@foreach ($info['usuariosSel'] as $usuario)
										 		<option value="{{$usuario->idUsuario}}" selected  name="option1">{{$usuario->nombreUsuario}}&nbsp;{{$usuario->apelUsuario}}</option>
										 		@endforeach
											</select>
										
									</div>
									<div class="col-sm-6">
										<div class="well well-sm">
											Seleccione al autor responsable:*
										</div>
											<select disabled data-placeholder="Seleccione al autor responsable" class="chosen"  id="responsable">
										 	
											@foreach ($info['usuariosSel'] as $usuario)
											@if ($usuario->idUsuario == $info['responsable']->idUsuario)
										 		<option value="{{$usuario->idUsuario}}" selected  name="option1">{{$usuario->nombreUsuario}}&nbsp;{{$usuario->apelUsuario}}</option>
											@else
										 		<option value="{{$usuario->idUsuario}}" name="option1">{{$usuario->nombreUsuario}}&nbsp;{{$usuario->apelUsuario}}</option>

											@endif
									 		@endforeach
											</select>

									</div>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="panel panel-primary" style="border-color: #205081; border-radius: 2px; box-shadow: 0 5px 10px gray;">
			<div class="panel-heading" style="border-radius: 0px; background: #205081; border-color: #205081;">
				<div class="panel-title">Solicitud de Art&iacute;culo</strong></div>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<form role="form" class="form-horizontal">
							<fieldset>
								<legend>Art&iacute;culo</legend>
								<div class="form-group">
									<label class="col-sm-3 control-label">T&iacute;tulo:*&nbsp;</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" name="titulo" disabled   value="{{$info['ficha']->tituloPaper}}"></input>
									
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">S&iacute;ntesis:*&nbsp;</label>
									<div class="col-sm-9">
									
										<textarea name="sintesis" style="width:100%; resize: none;" disabled rows="8" >{{$info['ficha']->resumenPaper}}</textarea>
									
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Categor&iacute;a:*&nbsp;</label>
									<div class="col-sm-9">
										 		<select id="categoria" class="chosen-select" disabled   style="width:50%">
											
						 						@foreach ($info['categorias'] as $categoria)
						 						@if ($info['ficha']->idCategoria == $categoria->idCategoria)
						 							<option value="{{$categoria->idCategoria}}" selected>{{$categoria->nomCategoria}}</option>
										 		
						 						@else
						 							<option value="{{$categoria->idCategoria}}" >{{$categoria->nomCategoria}}</option>
										 		
										 		@endif
										 		@endforeach
										 	
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Tem&aacute;tica:*&nbsp;</label>
									<div class="col-sm-9">
											<select id="tematica" disabled  class="chosen-select" style="width:50%" >
											@foreach ($info['tematicas'] as $tematica)
											@if ($info['ficha']->idTematica == $tematica->idTematica)
												<option value="{{$tematica->idTematica}}" selected>{{$tematica->nomTematica}}</option>
											
											@else
												<option value="{{$tematica->idTematica}}" >{{$tematica->nomTematica}}</option>
											@endif
											@endforeach
									</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Palabras Clave:*&nbsp;</label>
									<div class="col-sm-9">
											<input type="text" class="form-control"  value="{{$info['ficha']->palabrasClaves}}" name="palabras" disabled></input>
										
									</div>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="panel panel-primary" style="border-color: #205081; border-radius: 2px; box-shadow: 0 5px 10px gray;">
			<div class="panel-heading" style="border-radius: 0px; background: #205081; border-color: #205081;">
				<div class="panel-title">Subida de Archivo</strong></div>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<form role="form"  id="formulario" name="formulario" enctype="multipart/form-data" action="{{URL::action('ManejoArchivosController@subirArchivo')}}" method="POST"  class="form-horizontal">
							<fieldset>
								<legend>Resumen</legend>
								
								@if($info['ficha']->rutaFicha == "" || $info['ficha']->rutaFicha == '')
											<div class="alert alert-info fade in">
												<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
												La ficha no contiene un archivo de resumen.
											</div>
										@else
								<div class="form-group">
									<div class="class=col-sm-9">
										<label class="col-sm-3 "><a target="_blank" href="{{ URL::action('ArchivoController@previsualizarArchivo', array($idCongreso,$info['ficha']->rutaFicha)) }}"><strong><span class="glyphicon glyphicon-search"></span> Vista Previa</strong></a></label><br>
									</div>
								</div>
								<div class="form-group">
									<div class="class=col-sm-9">
										<label class="col-sm-3 "><a href="{{ URL::action('ArchivoController@servirArchivo', array($idCongreso,$info['ficha']->rutaFicha)) }}"><strong><span class="glyphicon glyphicon-download-alt"></span> Descargar</strong></a></label>
									</div>
								</div>
					
								
								@endif

							</fieldset>
						</form>
					</div>
				</div>
				@if($info['paper'] == [])
				@else
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<form role="form"  id="formulario" name="formulario" enctype="multipart/form-data" action="{{URL::action('ManejoArchivosController@subirArchivo')}}" method="POST"  class="form-horizontal">
							<fieldset>
								<legend>Art&iacute;culo</legend>
								<div class="form-group">
									<label class="col-sm-3 control-label"><a href="{{ URL::action('ArchivoController@servirArchivo', array($idCongreso,$info['paper']->rutaPaper)) }}"><strong>Descargar</strong></a></label>
									
							</div>
								</div>
								
							</fieldset>
						</form>
					</div>
				</div>
				@endif
			</div>
			<div class="panel-footer">
				<div class="row">
					<div class="col-sm-4">
						<button type="button" id="cancelarTodo" class="btn btn-default btn-default">Regresar</button>
							
					</div>
				</div>
			</div>
		</div>

	</div>
	
<input type="hidden" id="det" value="{{$info['detalle']}}">
<input type="hidden" id="det2" value="{{$info['idcongreso']}}">

</div>
<script type="text/javascript">

	$(document).ready(function()
	{
		$("#tematica").chosen();
		$("#categoria").chosen();
		$("#usuarios").chosen({max_selected_options: 3});
		$("#usuarios").chosen({max_selected_options: 3});
		$("#responsable").chosen();
		
		var longMaxima={{$info['longitudMaxima']}};

	
		$("#cancelarTodo").click(function(){
			window.location.href="{{URL::action('UsuariosCongresosController@establecerCongreso',array($idCongreso))}}";
		});		  

		$("#file").fileinput(
		{
			showUpload: false,
			previewFileType: "pdf",
			browseClass: "btn btn-success",
			browseLabel: " Buscar",
			browseIcon: '<i class="glyphicon glyphicon-picture"></i>',
			removeClass: "btn btn-danger",
			removeLabel: " Eliminar",
			removeIcon: '<i class="glyphicon glyphicon-trash"></i>',
			uploadClass: "btn btn-info",
		});


		$('#usuarios').on('change', function(evt, params) {
   	 		if(params.deselected){
   	 			
   	 			$("#responsable option[value='" + params.deselected +"']").remove();
   	 			$("#responsable").trigger("chosen:updated");
   	 		}
   	 		else
   	 		{
  	 			obj= $("#usuarios option:selected");

				document.getElementById("responsable").options.length = 0;
		  		var select = document.getElementById("responsable");
		   		var items=$("#usuarios").val();
		   		for (var i=0; i<items.length; i++) {
		     		var option = document.createElement("option");
		     		option.text = $('#usuarios').children('option[value="'+items[i]+'"]').text();
		     		option.value = items[i];
		     		select.add(option);
		     		$("#responsable").trigger("chosen:updated");
   				};
   	 		};
  		});

		
		function asignar(obj,valor){
    		cmp = document.getElementById( obj );
    		cmp.value = valor;
		}


		
	});
</script>
@stop