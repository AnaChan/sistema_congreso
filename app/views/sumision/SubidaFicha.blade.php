@section('content')

<div class="container">
	<div class="col-sm-10 col-sm-offset-1" style="margin-top: 50px;">
		<div class="panel panel-primary" style="border-color: #205081; border-radius: 2px; box-shadow: 0 5px 10px gray;">
			<div class="panel-heading" style="border-radius: 0px; background: #205081; border-color: #205081;">
				@if($info['ficha'] == [])
					<div class="panel-title">Subir Ficha</strong></div>
				@else
					<div class="panel-title">Informaci&oacute;n de la ficha</strong></div>
				@endif
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<form role="form" class="form-horizontal">
							<fieldset>
								<legend>Agregar Autores</legend>
								<div class="form-group">
									<div class="alert alert-info fade in">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										Todos los autores deben encontrarse inscritos en el congreso. De la lista de usuarios inscritos, seleccione aquellos que son autores del presente art&iacute;culo.&nbsp;
									</div>
									<div class="col-sm-6">
										 <div class="well well-sm" style="text-aling:center">
											Autores para la ficha:*
										</div>
										@if ($info['ficha']  == [])
											<select multiple data-placeholder="Seleccione los autores" style="width:100%" class="chosen" id="usuarios" size="5">
										 		@foreach ($info['usuarios'] as $usuario)
										 		<option value="{{$usuario->idUsuario}}"  name="option1">{{$usuario->nombreUsuario}} {{$usuario->apelUsuario}}</option>
										 		@endforeach
											</select>
										@else
											<select multiple data-placeholder="Seleccione los autores" style="width:100%" class="chosen" id="usuarios" size="5" disabled>
										 		@foreach ($info['usuarios'] as $usuario)
										 		<option value="{{$usuario->idUsuario}}" selected>{{$usuario->nombreUsuario}}&nbsp;{{$usuario->apelUsuario}}</option>
										 		@endforeach
											</select>
										@endif
									</div>
									<div class="col-sm-6">
										<div class="well well-sm">
											Seleccione al autor responsable:*
										</div>
										@if ($info['ficha']  == [])
										<select  data-placeholder="Seleccione al autor responsable" class="chosen" id="responsable">
										</select>
										@else
											<select  data-placeholder="Seleccione al autor responsable" class="chosen" disabled id="responsable">
										 		<option value="{{$usuario->idUsuario}}" selected>{{$usuario->nombreUsuario}}&nbsp;{{$usuario->apelUsuario}}</option>								 	
											</select>

										@endif
									</div>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="panel panel-primary" style="border-color: #205081; border-radius: 2px; box-shadow: 0 5px 10px gray;">
			<div class="panel-heading" style="border-radius: 0px; background: #205081; border-color: #205081;">
				<div class="panel-title">Solicitud de Art&iacute;culo</strong></div>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<form role="form" class="form-horizontal">
							<fieldset>
								<legend>Art&iacute;culo</legend>
								<div class="form-group">
									<label class="col-sm-3 control-label">T&iacute;tulo:*&nbsp;</label>
									<div class="col-sm-9">
									@if ($info['ficha']  == [])
										<input type="text" class="form-control" name="titulo"></input>
									@else
										<input type="text" class="form-control" name="titulo"   value="{{$info['ficha']->tituloPaper}}" disabled></input>
									@endif
									</div>
								</div>
								<div class="alert alert-info fade in">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										Escriba una breve explicaci&oacute;n de el tema a subir
									</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">S&iacute;ntesis:*&nbsp;</label>
									
									<div class="col-sm-9">

									@if ($info['ficha']  == [])
										<textarea name="sintesis" style="width:100%; resize: none;" rows="8"></textarea>
										
									@else
										<textarea name="sintesis" style="width:100%; resize: none;" rows="8" disabled>{{$info['ficha']->resumenPaper}}</textarea>
									@endif
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Categor&iacute;a:*&nbsp;</label>
									<div class="col-sm-9">
											@if ($info['ficha']  == [])
												<select data-placeholder="Seleccione una categoría" id="categoria" class="chosen-select"  style="width:50%">
						 						@foreach ($info['categorias'] as $categoria)
						 						<option value="{{$categoria->idCategoria}}">{{$categoria->nomCategoria}}</option>
										 	@endforeach
										 	@else
										 		<select data-placeholder="Seleccione una categoría" id="categoria" class="chosen-select" disabled   style="width:50%">
											
						 						@foreach ($info['categorias'] as $categoria)
						 						<option value="{{$categoria->idCategoria}}" selected>{{$categoria->nomCategoria}}</option>
										 		@endforeach
										 	
										 	@endif
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Tem&aacute;tica:*&nbsp;</label>
									<div class="col-sm-9">
											@if ($info['ficha']  == [])
											<select id="tematica" data-placeholder="Seleccione una temática" class="chosen-select" style="width:50%" >
										
											@foreach ($info['tematicas'] as $tematica)
											<option value="{{$tematica->idTematica}}">{{$tematica->nomTematica}}</option>
											@endforeach
										@else
											<select id="tematica" data-placeholder="Seleccione una temática" disabled  class="chosen-select" style="width:50%" >
											@foreach ($info['tematicas'] as $tematica)
											<option value="{{$tematica->idTematica}}" selected>{{$tematica->nomTematica}}</option>
											@endforeach
										@endif
									</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Palabras Clave:*&nbsp;</label>
									<div class="col-sm-9">
										@if ($info['ficha']  == [])
											<input type="text" class="form-control" name="palabras"></input>
										@else
											<input type="text" class="form-control" disabled value="{{$info['ficha']->palabrasClaves}}" name="palabras"></input>
										@endif
									</div>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="panel panel-primary" style="border-color: #205081; border-radius: 2px; box-shadow: 0 5px 10px gray;">
			<div class="panel-heading" style="border-radius: 0px; background: #205081; border-color: #205081;">
				<div class="panel-title">Subida de Archivo</strong></div>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<form role="form"  id="formulario" name="formulario" enctype="multipart/form-data" action="{{URL::action('ManejoArchivosController@subirArchivo')}}" method="POST"  class="form-horizontal">
							<fieldset>
								@if ($info['ficha']  == [])
								<legend>Resumen</legend>
								<div class="alert alert-info fade in">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										Este documento es un resumen sobre la ponencia que se desea ingresar. No es el art&iacute;culo final de la ponencia 
								</div>
								<div class="alert alert-info fade in">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									
									{{$subidaPresentacion['ayuda']}}
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Resumen:* &nbsp;</label>
									<div class="col-sm-9">
											<input id="file" name="nombreArchivo" type="file" accept="pdf/*" >
											<input type="hidden" name="tipoArchivo" value="ficha"/>
											<input type="hidden" name="idCongreso" value="{{$info['idcongreso']}}"/>
											<input type="hidden" id="idFicha" name="idFicha" value=""/>
										
									</div>
								</div>
								@else
								<legend>Articulo</legend>
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									
									{{$subidaPresentacion['ayuda']}}
								</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">Documento:* &nbsp;</label>
										<div class="col-sm-9">
												<input id="file" name="nombreArchivo" type="file" accept="pdf/*" >
												<input type="hidden" name="tipoArchivo" value="paper"/>
												<input type="hidden" name="idCongreso" value="{{$info['idcongreso']}}"/>
												<input type="hidden" id="idFicha" name="idFicha" value="{{$info['idficha']}}"/>
												<input type="hidden" id="idPaper" name="idPaper" value=""/>
										</div>
									</div>		
								@endif
								
							</fieldset>
						</form>
					</div>
				</div>
			</div>
			<div class="panel-footer">
				<div class="row">
					<div class="col-sm-4">
						<button type="button" id="cancelarTodo" class="btn btn-default btn-default">Regresar</button>
						@if ($info['ficha']  == [])
							<button type="button" id="btnEnviar" class="btn btn-primary btn-default" value="Enviar Solicitud">Enviar Solicitud</button>
						@else
							<button type="button" id="btnEnviarPaper" class="btn btn-primary btn-default" value="Enviar Articulo">Enviar Art&iacute;culo</button>
						
						@endif
					</div>
				</div>
			</div>
		</div>

	</div>
	<input type="hidden" id="det" value="{{$info['detalle']}}">
	<input type="hidden" id="det2" value="{{$info['idcongreso']}}">

<input type="hidden" id="det" value="{{$info['detalle']}}">
<input type="hidden" id="det2" value="{{$info['idcongreso']}}">

</div>
<!--Ventana Modal para enviar solicitud para ser revisor -->

<div class="modal fade" id="nueva-solicitud-revisor-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" >Enviar solicitud para ser revisor del congreso.</h4>
      </div>
      <div class="modal-body">
		<div class="container" style="width:450px">
			<div class="row clearfix">
				<div class="col-md-4 column">
					<span style="font-weight:bold">Elija la Tem&aacute;tica</span>
				</div>
				<div class="col-md-8 column" id="divTematicas" > 
					<select class="chosen" id="listaTematicas" placeholder="Lista de temáticas" >
					</select>
				</div>
			</div>
			<br/>
     	</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" id="agregarSolicitud">Enviar Aplicaci&oacute;n</button>
			<button type="button" class="btn btn-primary" data-dismiss="modal" >Cancelar</button>
		</div>
    </div>
  </div>
</div>
</div>

<script type="text/javascript">
	$("#listaTematicas").chosen();
	$("#nueva-solicitud-revisor").click(function(){

		$.post("{{URL::action('CongresoController@listarTematicasDisponibles')}}", {idCongreso: "{{$idCongreso}}",idUsuario:"{{Auth::user()->idUsuario}}"})
		.done(function(data)
		{
			if(data.error){
				alertify.error(data.mensaje);
			}
			else{
				console.log(data.htmlVista);
				$("#divTematicas").html(data.htmlvista);
				$("#listaTematicas").chosen();
				$('#listaTematicas').trigger("chosen:updated");
				$('#nueva-solicitud-revisor-form').modal('show');
			}
		})
		.fail(function(data, status, jqXHR)
		{
			alertify.error("Error: No se puede obtener el listado de temáticas.");
		});
	});

	$("#agregarSolicitud").click(function(){
		alertify.confirm("Si envía la solicitud deberá esperar hasta que esta sea gestionada por el Administrador del congreso. <br>¿Está seguro que desea enviarla en este momento?", function (e) 
		{
			if (e) {
				enviarSolicitudRevisor();

			}
		});
	});

	function enviarSolicitudRevisor()
	{
		var idCongreso="{{$idCongreso}}";
		var idTematica="";
		var idPcTematica=$("#listaTematicas").val();
		var usuariosEnTabla = new Array();
		usuariosEnTabla.push("{{Auth::user()->idUsuario}}");
		$.post("{{URL::action('UsuariosCongresosController@actualizarRevisores')}}",
		 {idCongreso: "{{$idCongreso}}",
		idTematica: idTematica,
		idPcTematica:idPcTematica,
		listaUsuarios:JSON.stringify(usuariosEnTabla)
		})
		.done(function(data)
		{
			if(data.mensaje.indexOf("ERROR") != -1){
				alertify.error(data.mensaje);
			}
			else{
				alertify.success("Petición enviada existosamente, redireccionando...");
				window.location.href="{{URL::action('UsuariosCongresosController@establecerCongreso',array($idCongreso))}}";
			}
		})
		.fail(function(data, status, jqXHR)
		{
			alertify.error("Error en el servidor");
		});
	}

</script>
<script type="text/javascript">

	$(document).ready(function()
	{
		$("#tematica").chosen();
		$("#categoria").chosen();
		//$("#usuarios").chosen({search_contains: true});
		$("#usuarios").chosen({max_selected_options: 4});
		$("#usuarios").chosen({max_selected_options: 4});
		$("#usuarios").chosen({search_contains: true});
		$("#responsable").chosen();
		
		var longMaxima={{$info['longitudMaxima']}};

	
		$("#cancelarTodo").click(function(){
			window.location.href="{{URL::action('UsuariosCongresosController@establecerCongreso',array($idCongreso))}}";
		});		  

		$("#file").fileinput(
		{
			showUpload: false,
			showRemove: false,
			previewFileType: "pdf",
			browseClass: "btn btn-success",
			browseLabel: " Buscar",
			browseIcon: '<i class="glyphicon glyphicon-picture"></i>',
			removeClass: "btn btn-danger",
			removeLabel: " Eliminar",
			removeIcon: '<i class="glyphicon glyphicon-trash"></i>',
			uploadClass: "btn btn-info",
		});

		$("#file").change(function(){ 
			$( "#img-vistaprevia" ).css("display","none");
			$("[class='close fileinput-remove text-right']").hide();
		});



		$('#usuarios').on('change', function(evt, params) {
   	 		if(params.deselected){
   	 			
   	 			$("#responsable option[value='" + params.deselected +"']").remove();
   	 			$("#responsable").trigger("chosen:updated");
   	 		}
   	 		else
   	 		{
  	 			obj= $("#usuarios option:selected");

				document.getElementById("responsable").options.length = 0;
		  		var select = document.getElementById("responsable");
		   		var items=$("#usuarios").val();
		   		for (var i=0; i<items.length; i++) {
		     		var option = document.createElement("option");
		     		option.text = $('#usuarios').children('option[value="'+items[i]+'"]').text();
		     		option.value = items[i];
		     		select.add(option);
		     		$("#responsable").trigger("chosen:updated");
   				};
   	 		};
  		});

		$("#btnEnviarPaper").click(function()
		{
			var ruta = $("#file").val().replace(/C:\\fakepath\\/i, '');
			var ficha = $("#idFicha").val();
			
			var error = false;
			if(ruta.length == 0 )
			{
				alertify.error("Tiene que seleccionar un archivo.");
				$("#file").parent().removeClass('has-error').addClass('has-error');
				error = true;
				
				
			}
			else
			{
				$("#file").parent().removeClass('has-error');
			}
			
		 	if(!/({{$subidaPresentacion['extensiones']}})$/i.test(ruta)){
		 		error = true;
		 		alertify.error("La extension del archivo es invalida.");
				$("#file").parent().removeClass('has-error').addClass('has-error');
		 	}
		 	else
			{
				$("#file").parent().removeClass('has-error');
			}
			if(!error)
			{

				$.post("{{URL::action('SumissionController@guardarPaper')}}", {ruta:ruta,ficha:ficha})
				.done(function(data, status,jqXHR)
				{
					
					if(data.error)
					{
						alertify.error(data.mensaje);
						
					}
					else
					{
						alertify.success(data.mensaje);
						
						document.getElementById("idPaper").value  = data.paper;
						

						document.forms["formulario"].submit();
											
					}
				})
				.fail(function(data, status,jqXHR)
				{
					
					console.log("Server Returned " + status);
					alertify.error("Error de comunicación con el servidor.");
				});
			}
			else{
				
			}

		});

		function asignar(obj,valor){
    		cmp = document.getElementById( obj );
    		cmp.value = valor;
		}


		$("#btnEnviar").click(function()
		{

			var autores = $("#usuarios").val() || [];
			var titulo = $("[name=titulo]").val();
			var sintesis = $("[name=sintesis]").val();
			var tematica = $("#tematica").val();
			var categoria = $("#categoria").val();
			var palabras = $("[name=palabras]").val();
			var detalle = $("#det").val();
			var idCongreso = $("#det2").val();
			var ruta = $("#file").val().replace(/C:\\fakepath\\/i, '');
			var error = false;
			var resp = $("#responsable").val();
			


			if( autores.length == 0 )
			{
				alertify.error("El artículo debe poseer al menos un autor.");
				$("#usuarios").parent().removeClass('has-error').addClass('has-error');
				error = true;
			}
			else
			{
				$("#usuarios").parent().removeClass('has-error');
			}


			if( autores.length > 4 )
			{
				alertify.error("El artículo no debe poseer más de cuatro autores.");
				$("#usuarios").parent().removeClass('has-error').addClass('has-error');
				error = true;
			}
			else
			{
				$("#usuarios").parent().removeClass('has-error');
			}

			if(titulo.length < 1)
			{
				alertify.error("El título del artículo es obligatorio.");
				$("[name='titulo']").parent().removeClass('has-error').addClass('has-error');
				errors = true;
			}
			else
			{

				$("[name='titulo']").parent().removeClass('has-error');	
			}

			if(titulo.length > 250)
			{
				alertify.error("El título del artículo es demasiado extenso.");
				$("[name='titulo']").parent().removeClass('has-error').addClass('has-error');
				errors = true;
			}
			else
			{

				$("[name='titulo']").parent().removeClass('has-error');	
			}
			
			if(sintesis.length < 1){
				alertify.error("La síntesis del artículo es obligatoria.");
				$("[name='sintesis']").parent().removeClass('has-error').addClass('has-error');
				error = true;
			}else{
				$("[name='sintesis']").parent().removeClass('has-error');
			}

			if($("[name=sintesis]").val().trim().split(" ").length > longMaxima )
			{
				alertify.error("La longitud de la síntesis es incorrecta debe contener como máximo "+longMaxima+" palabras");
				$("[name=sintesis]").parent().removeClass('has-error').addClass('has-error');
				return;
			}
			else
			{
				$("[name=sintesis]").parent().removeClass('has-error');
			}

			if( tematica == null || tematica.length < 1)
			{
				alertify.error("No ha ingresado una temática válida.");
				$("#tematica").parent().removeClass('has-error').addClass('has-error');
				error = true;
			}
			else
			{
				$("#tematica").parent().removeClass('has-error');
			}
				
			
			if( categoria == null || categoria.length < 1)
			{

				alertify.error("No ha ingresado una categoría válida.");
				$("#categoria").parent().removeClass('has-error').addClass('has-error');
				error = true;
			}

			else
			{
				$("#categoria").parent().removeClass('has-error');
			}
			
			if(palabras.length < 1){
				alertify.error("Es obligatorio agregar las palabras clave.");
				$("[name='palabras']").parent().removeClass('has-error').addClass('has-error');
				error = true;
			}else{
				$("[name='palabras']").parent().removeClass('has-error');
			}

			if(palabras.length > 255){
				alertify.error("Las palabras clave han excedido el tamaño máximo.");
				$("[name='palabras']").parent().removeClass('has-error').addClass('has-error');
				error = true;
			}else{
				$("[name='palabras']").parent().removeClass('has-error');
			}

			
			if(ruta.length == 0 )
			{
				alertify.error("Tiene que seleccionar un archivo.");
				$("#file").parent().removeClass('has-error').addClass('has-error');
				error = true;
				
			}
			else
			{
				$("#file").parent().removeClass('has-error');
			}
		 	
		 	if(!/({{$subidaPresentacion['extensiones']}})$/i.test(ruta)){
		 		error = true;
		 		alertify.error("La extension del archivo es invalida.");
				$("#file").parent().removeClass('has-error').addClass('has-error');
		 	}
		 	else
			{
				$("#file").parent().removeClass('has-error');
			}
				    
		

			if(!error)
			{

				$.post("{{URL::action('SumissionController@guardarArticulo')}}", {titulo: titulo, autores: autores, sintesis: sintesis, tematica: tematica,categoria: categoria, palabras: palabras, detalle: detalle, resp:resp,ruta:ruta})
				.done(function(data, status,jqXHR)
				{
					
					if(data.error)
					{
						alertify.error(data.mensaje);
						
					}
					else
					{
						alertify.success(data.mensaje);
						
						$("#idFicha").val = data.ficha;
						asignar('idFicha',data.ficha);
						
						document.forms["formulario"].submit();
											
					}
				})
				.fail(function(data, status,jqXHR)
				{
					
					console.log("Server Returned " + status);
					alertify.error("Error de comunicación con el servidor.");
				});
			}
			else{
				
			}
		});

	});
</script>
@stop