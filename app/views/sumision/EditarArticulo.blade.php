@section('content')

<div class="container">
	<div class="col-sm-10 col-sm-offset-1" style="margin-top: 50px;">
		<div class="panel panel-primary" style="border-color: #205081; border-radius: 2px; box-shadow: 0 5px 10px gray;">
			<div class="panel-heading" style="border-radius: 0px; background: #205081; border-color: #205081;">
					<div class="panel-title">Informaci&oacute;n de la ficha</strong></div>
				
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<form role="form" class="form-horizontal">
							<fieldset>
								<legend>Agregar Autores</legend>
								<div class="form-group">
									<div class="alert alert-info fade in">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
										Toda la informaci&oacute;n es traida de la ficha y no puede ser editable, a excepci&oacute; de el documento.&nbsp;
									</div>
									<div class="col-sm-6">
										 <div class="well well-sm" style="text-aling:center">
											Autores para la ficha:*
										</div>
											<select disabled multiple data-placeholder="Seleccione los autores" style="width:100%" class="chosen-select" id="usuarios" size="5" >
										 		@foreach ($info['usuarios'] as $usuario)
										 		<option value="{{$usuario->idUsuario}}" >{{$usuario->nombreUsuario}}&nbsp;{{$usuario->apelUsuario}}</option>
										 		@endforeach
										 		@foreach ($info['usuariosSel'] as $usuario)
										 		<option value="{{$usuario->idUsuario}}" selected  name="option1">{{$usuario->nombreUsuario}}&nbsp;{{$usuario->apelUsuario}}</option>
										 		@endforeach
											</select>
										
									</div>
									<div class="col-sm-6">
										<div class="well well-sm">
											Seleccione al autor responsable:*
										</div>
											<select disabled  data-placeholder="Seleccione al autor responsable" class="chosen"  id="responsable">
										 	
											@foreach ($info['usuariosSel'] as $usuario)
											@if ($usuario->idUsuario == $info['responsable']->idUsuario)
										 		<option value="{{$usuario->idUsuario}}" selected  name="option1">{{$usuario->nombreUsuario}}&nbsp;{{$usuario->apelUsuario}}</option>
											@else
										 		<option value="{{$usuario->idUsuario}}" name="option1">{{$usuario->nombreUsuario}}&nbsp;{{$usuario->apelUsuario}}</option>

											@endif
									 		@endforeach
											</select>
									</div>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="panel panel-primary" style="border-color: #205081; border-radius: 2px; box-shadow: 0 5px 10px gray;">
			<div class="panel-heading" style="border-radius: 0px; background: #205081; border-color: #205081;">
				<div class="panel-title">Solicitud de Art&iacute;culo</strong></div>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<form role="form" class="form-horizontal">
							<fieldset>
								<legend>Art&iacute;culo</legend>
								<div class="form-group">
									<label class="col-sm-3 control-label">T&iacute;tulo:*&nbsp;</label>
									<div class="col-sm-9">
										<input type="text" disabled class="form-control" name="titulo"    value="{{$info['ficha']->tituloPaper}}"></input>
									
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">S&iacute;ntesis:*&nbsp;</label>
									<div class="col-sm-9">
									
										<textarea name="sintesis" disabled style="width:100%; resize: none;"  rows="8" >{{$info['ficha']->resumenPaper}}</textarea>
									
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Categor&iacute;a:*&nbsp;</label>
									<div class="col-sm-9">
										 		<select id="categoria" disabled class="chosen-select"    style="width:50%">
											
						 						@foreach ($info['categorias'] as $categoria)
						 						@if ($info['ficha']->idCategoria == $categoria->idCategoria)
						 							<option value="{{$categoria->idCategoria}}" selected>{{$categoria->nomCategoria}}</option>
										 		
						 						@else
						 							<option value="{{$categoria->idCategoria}}" >{{$categoria->nomCategoria}}</option>
										 		
										 		@endif
										 		@endforeach
										 	
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Tem&aacute;tica:*&nbsp;</label>
									<div class="col-sm-9">
											<select id="tematica" disabled   class="chosen-select" style="width:50%" >
											@foreach ($info['tematicas'] as $tematica)
											@if ($info['ficha']->idTematica == $tematica->idTematica)
												<option value="{{$tematica->idTematica}}" selected>{{$tematica->nomTematica}}</option>
											
											@else
												<option value="{{$tematica->idTematica}}" >{{$tematica->nomTematica}}</option>
											@endif
											@endforeach
									</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Palabras Clave:*&nbsp;</label>
									<div class="col-sm-9">
											<input type="text" class="form-control" disabled  value="{{$info['ficha']->palabrasClaves}}" name="palabras" ></input>
										
									</div>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="panel panel-primary" style="border-color: #205081; border-radius: 2px; box-shadow: 0 5px 10px gray;">
			<div class="panel-heading" style="border-radius: 0px; background: #205081; border-color: #205081;">
				<div class="panel-title">Subida de Archivo</strong></div>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<form role="form"  id="formulario" name="formulario" enctype="multipart/form-data" action="{{URL::action('ManejoArchivosController@subirArchivo')}}" method="POST"  class="form-horizontal">
							<fieldset>
								
								<legend>Art&iacute;culo</legend>
								@if($info['paper']->rutaPaper == "" || $info['paper']->rutaPaper == '')
											<div class="alert alert-info fade in">
												<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
												El art&iacute;culo no contiene un archivo, Para ingresar haga click sobre el boton buscar
											</div>
											@else
											<div class="alert alert-info fade in">
												<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
												Ya cuenta con un art&iacute;culo, si desea cambiarlo haga click sobre el bot&oacute;n cambiar.
											</div>
											@endif
								<div class="alert alert-info fade in">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									
									{{$subidaPresentacion['ayuda']}}
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Art&iacute;culo:* &nbsp;</label>
									<div class="col-sm-9">
										<a target="_blank" href="{{ URL::action('ArchivoController@previsualizarArchivo', array($idCongreso,$info['paper']->rutaPaper)) }}"><strong><span class="glyphicon glyphicon-search"></span> (Ver actual )</strong></a>
										<input id="file" name="nombreArchivo" type="file" accept="pdf/*" >
										<input type="hidden" name="tipoArchivo" value="paper"/>
										<input type="hidden" name="idCongreso" value="{{$info['idcongreso']}}"/>
										<input type="hidden" id="idFicha" name="idFicha" value="{{$info['idficha']}}"/>
										<input type="hidden" id="idPaper" name="idPaper" value="{{$info['paper']->idPaper}}"/>
									</div>
								</div>
								
							</fieldset>
						</form>
					</div>
				</div>
			</div>
			<div class="panel-footer">
				<div class="row">
					<div class="col-sm-4">
						<button type="button" id="cancelarTodo" class="btn btn-default btn-default">Regresar</button>
						
							<button type="button" id="btnEditar" class="btn btn-primary btn-default" value="Enviar Solicitud">Enviar Solicitud</button>
						
					</div>
				</div>
			</div>
		</div>

	</div>
	<input type="hidden" id="det" value="{{$info['detalle']}}">
	<input type="hidden" id="det2" value="{{$info['idcongreso']}}">
	<input type="hidden" id="cambio" value="N">
	<input type="hidden" id="rutaV" value="{{$info['paper']->rutaPaper}}"
</div>
<script type="text/javascript">

	$(document).ready(function()
	{
		$("#tematica").chosen();
		$("#categoria").chosen();
		$("#usuarios").chosen({max_selected_options: 4});
		$("#usuarios").chosen({max_selected_options: 4});
		$("#responsable").chosen();
		
		var longMaxima={{$info['longitudMaxima']}};

	
		$("#cancelarTodo").click(function(){
			window.location.href="{{URL::action('UsuariosCongresosController@establecerCongreso',array($info['idcongreso']))}}";
		});		  

		$("#file").fileinput(
		{
			showUpload: false,
			showRemove: false,
			previewFileType: "pdf",
			browseClass: "btn btn-success",
			browseLabel: " Cambiar",
			browseIcon: '<i class="glyphicon glyphicon-edit"></i>',
			removeClass: "btn btn-danger",
			removeLabel: " Eliminar",
			removeIcon: '<i class="glyphicon glyphicon-trash"></i>',
			uploadClass: "btn btn-info",
		});


  		 $("#file").change(function() {
        		asignar('cambio',"S");
        		
   		 });
   		 $("#file").change(function()
		{
			$("[class='close fileinput-remove text-right']").hide();
		});

		$("#file").change(function(){ 
			$( "#img-vistaprevia" ).css("display","none");
		});


		$('#usuarios').on('change', function(evt, params) {
   	 		if(params.deselected){
   	 			
   	 			$("#responsable option[value='" + params.deselected +"']").remove();
   	 			$("#responsable").trigger("chosen:updated");
   	 		}
   	 		else
   	 		{
  	 			obj= $("#usuarios option:selected");

				document.getElementById("responsable").options.length = 0;
		  		var select = document.getElementById("responsable");
		   		var items=$("#usuarios").val();
		   		for (var i=0; i<items.length; i++) {
		     		var option = document.createElement("option");
		     		option.text = $('#usuarios').children('option[value="'+items[i]+'"]').text();
		     		option.value = items[i];
		     		select.add(option);
		     		$("#responsable").trigger("chosen:updated");
   				};
   	 		};
  		});


		

		function asignar(obj,valor){
    		cmp = document.getElementById( obj );
    		cmp.value = valor;
		}


		$("#btnEditar").click(function()
		{

			var idCongreso = $("#det2").val();
			
			var error = false;
			var cambio = $("#cambio").val();
			var id = $("#idPaper").val();
			if(cambio == 'S' || cambio == "S"){
				var ruta = $("#file").val().replace(/C:\\fakepath\\/i, '');
			
				if(ruta.length == 0 )
				{
					alertify.error("Tiene que seleccionar un archivo.");
					$("#file").parent().removeClass('has-error').addClass('has-error');
					error = true;	
				}
				else
				{
					$("#file").parent().removeClass('has-error');
				}

			
			
		 		if(!/({{$subidaPresentacion['extensiones']}})$/i.test(ruta)){
		 			
		 			error = true;
		 			alertify.error("La extension del archivo es invalida.");
					$("#file").parent().removeClass('has-error').addClass('has-error');
			 	}
			 	else
				{
					$("#file").parent().removeClass('has-error');
				}
			}
			else{
					var ruta = $("#rutaV").val();
				}
			if(!error)
			{

				$.post("{{URL::action('SumissionController@editarPaper')}}", {ruta:ruta,id:id})
				.done(function(data, status,jqXHR)
				{
					
					if(data.error)
					{
						alertify.error(data.mensaje);
						
					}
					else
					{
						alertify.success(data.mensaje);
						
						
						
						if($("#cambio").val() == "S"){
							
							document.forms["formulario"].submit();
						}
											
					}
				})
				.fail(function(data, status,jqXHR)
				{
					
					console.log("Server Returned " + status);
					alertify.error("Error de comunicación con el servidor.");
				});
			}



			
		});

	});
</script>
@stop