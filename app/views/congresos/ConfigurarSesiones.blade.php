@section('content')
	<div class="">
	@if( $congresoOwner > 0 )
		<div class="row clearfix">
			<div class="col-md-12 column">
				<h3 class="text-center">
					Sesiones para congreso "{{ $nombreCongreso }}"
				</h3>
				<div class="alert alert-info fade in">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<strong>Importante</strong>
					<br/><br/>
					<strong>Sesi&oacute;n</strong>
					 Es la fecha con hora de inicio y  hora de finalizacion en la que pueden ser agendadas las ponencias para una tematica seleccionada
					<br/><br/>
					<strong>1) </strong>
					La modificación de fechas para sesiones debe manejarse con cuidado, considerando su uso en la pantalla de AGENDA e interacción con presentaciones programadas.
					<br/><br/>
					<strong>2) </strong> Si para un mismo día se crea más de una sesión empezando todas a la misma hora (Hora de Inicio), la Hora de Finalización <strong>deberá</strong> ser la misma. Esto para dar formato al momento de hacer la exportación de la agenda.
				</div>
			</div>
		</div>
		<div class="row clearfix">
			<div class="col-md-12 column">
				<a id="agregarSesion"  href="">Agregar Sesi&oacute;n</a>
				<br/><br/>
				<table id="tbl_sesionestable" name="tbl_sesionestable" class="table display">
					<thead>
						<tr>
							<th>Nombre</th>
							<th>Moderador</th>
							<th>Tem&aacute;tica</th>
							<th>Fecha</th>
							<th>Hora Inicio</th>
							<th>Hora Fin</th>
							<th>Duraci&oacute;n</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					@foreach ($sesionesCongreso as $objeto)
						<tr class="clickRow">
							<td id="sesionNombre_{{$objeto->idSesion}}" visible="false">
								{{$objeto->nombreSesion}}
							</td>
							<td id="sesionModerador_{{$objeto->idSesion}}" visible="false">
								{{$objeto->moderador}}
							</td>
							<td id="sesionTematica_{{$objeto->idSesion}}" visible="false">
								{{$objeto->nomTematica}}
							</td>
							<td id="sesionFecha_{{$objeto->idSesion}}" visible="false">
								{{date('d/m/Y', strtotime( $objeto->fecSesion ))}}
							</td>
							<td id="sesionInicio_{{$objeto->idSesion}}" visible="false">
								{{$objeto->horaInicio}}
							</td>
							<td id="sesionFin_{{$objeto->idSesion}}" visible="false">
								{{$objeto->horaFin}}
							</td>
							<td id="sesionDuracion_{{$objeto->idSesion}}" visible="false">
								{{$objeto->duracionSesion}}
							</td>
							<td><a id="{{$objeto->idSesion}}" class="modificar" href='congresoSesion' title="Modificar">Modificar</a></td>
							<td><a id="{{$objeto->idSesion}}" class="eliminar"  href='congresoSesion' title="Eliminar">Eliminar</a></td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
			<br><br>

			<div class="row clearfix container">
				<div class="col-md-6 column">
					<h3 id="ACME_LABEL" name="ACME_LABEL"></h3>
					<input type='hidden' name='gotourl' id='gotourl' value=""></input>
				</div>
			</div>
			
			<div class="row clearfix container">

				<div class="col-md-6 column">
					<input type='hidden' name='action' id='action' value=""></input>
					<input type='hidden' name='idRecord' id='idRecord' value=""></input>
					<br/>
					
					<div class="row clearfix">
						<div class="col-md-4 column">
							<label class="col-sm-3 control-label">Nombre: &nbsp;</label>
						</div>
						<div class="col-md-6 column">
							<input name="nomSesion" id="nomSesion" class="form-control" disabled='' type="text" ></input>
						</div>
					</div>
					<br/>

					<div class="row clearfix">
						<div class="col-md-4 column">
							<label class="col-sm-3 control-label">Moderador: &nbsp;</label>
						</div>
						<div class="col-md-6 column">
							<input name="moderador" id="moderador" class="form-control" disabled='' type="text" ></input>
						</div>
					</div>
					<br/>

					<div class="row clearfix">
						<div class="col-md-4 column">
							<label class="col-sm-3 control-label">Tem&aacute;tica: &nbsp;</label>
						</div>
						<div class="col-md-6 column">
							{{ Form::select('tematica', $tematicas ) }}
						</div>
					</div>
					<br/>

					<div class="row clearfix">
						<div class="col-md-4 column">
							<button type="button" id="performAction" name="performAction"  disabled=''  class="btn btn-primary btn-default">Guardar</button>
						</div>
						<div class="col-md-6 column">
						</div>
					</div>

					<br/>
				</div>

				<div class="col-md-6 column">
					<br/>

					<div class="row clearfix">
						<div class="col-md-4 column">
							<label class="col-sm-3 control-label">Fecha: &nbsp;</label>
						</div>
						<div class='input-group date col-md-6 column' id='fechaSS' name='fechaSS' data-date-format="DD/MM/YYYY">
							<input type='text' class="form-control" id='fechaS' name="fechaS" disabled=''>
							<span class="input-group-addon"><span class="glyphicon glyphicon-time"></span>
						</div>
					</div>
					<br/>

					<div class="row clearfix">
						<div class="col-md-4 column">
							<label class="col-sm-3 control-label">Hora Inicio: &nbsp;</label>
						</div>
						<div class="input-group date col-md-6 column">
							<input data-format="hh:mm" type="text" name="hinicio" id="hinicio" class="form-control" disabled=''></input>
						</div>
					</div>
					<br/>

					<div class="row clearfix">
						<div class="col-md-4 column">
							<label class="col-sm-3 control-label">Hora Fin: &nbsp;</label>
						</div>
						<div class="input-group date col-md-6 column">
							<input data-format="hh:mm:ss" type="text" name="hfin" id="hfin" class="form-control" disabled=''></input>
						</div>
					</div>
					<br/>
				</div>


			</div>
			
		</div>
	@else
		<div class="row clearfix">
			<div class="col-md-12 column">
				<h3 class="text-center text-error">
					No tiene permisos para editar informaci&oacute;n sobre este congreso.
				</h3>
			</div>
		</div>
	@endif

</div>


<script type="text/javascript">
	$(document).ready(function()
	{
		$("[name='tematica']").addClass('form-control');
		$("[name='tematica']").attr('disabled','');
		//$('#fechaSS').datetimepicker({pickTime: false});

		$('#tbl_sesionestable').dataTable( {
	        "language": {
	        	"lengthMenu"	: "Mostrar _MENU_ por p&aacute;gina",
	            "zeroRecords"	: "No hay coincidencias",
	            "info"			: "Mostrando p&aacute;gina _PAGE_ de _PAGES_",
	            "infoEmpty"		: "No hay records disponibles",
	            "infoFiltered"	: "(filtrado de un total de _MAX_ registros)",
	            "sSearch"		: "Buscar:",
	            "sLoadingRecords": "Cargando...",
				"oPaginate": {
							"sFirst":    "Primero",
							"sLast":     "&Uacute;ltimo",
							"sNext":     "Siguiente",
							"sPrevious": "Anterior"
							},
	        }
    	} );

		$("#tbl_sesionestable tbody" ).on('click', 'a.eliminar',  function()
		{
			var gotToURL = this.getAttribute('href');
			var recId	 = this.id;
			alertify.confirm("¿Está seguro que quiere borrar este record?", function (e) {
				if (e) {
					$.post("{{ URL::action('CongresoSesionesController@actualizarData') }}", {action:'DELETE', currentId : recId,idDC : {{$idCongreso}} })
						.done(function(data, status,jqXHR)
						{
							if(data.error)
							{
								alertify.error(data.mensaje);
							}
							else
							{
								alertify.success(data.mensaje);
								window.setTimeout(function()
								{
									window.location.replace("{{URL::action('CongresoSesionesController@getIndex',array($idCongreso) )}}");
								},
								900);
								
							}
						})
						.fail(function(data, status,jqXHR)
						{
							console.log("Server Returned " + status);
							alertify.error("Error de comunicación con el servidor, contacte a su adminstrador.");
						});
				}
				else {return false;}
			
			});
			return false;
		});
		
		$("#tbl_sesionestable tbody" ).on('click', 'a.modificar',  function()
		{
			document.getElementById('ACME_LABEL').innerHTML = 'Modificar';
			var objId = this.id;
			
			Sesion = document.getElementById('sesionNombre_'+objId).innerHTML;
			Sesion = Sesion.replace(/\n|\r/g, "").trim();
			document.getElementById('nomSesion').value = Sesion;
			document.getElementById('nomSesion').removeAttribute('disabled');
			document.getElementById('nomSesion').focus();

			moderador = document.getElementById('sesionModerador_'+objId).innerHTML;
			moderador = moderador.replace(/\n|\r/g, "").trim();
			document.getElementById('moderador').value = moderador;
			document.getElementById('moderador').removeAttribute('disabled');

			//**************************************
			lcNomTematica = document.getElementById('sesionTematica_'+objId).innerHTML;
			lcNomTematica = lcNomTematica.replace(/\n|\r/g, "").trim();
			lcNomTematica = lcNomTematica.toUpperCase();
			var sel = document.getElementsByName('tematica');
			var cardinalidad = sel[0].length;

			for(var opt = 0; opt < cardinalidad; opt++) {
			    if(sel[0][opt].text.toUpperCase() == lcNomTematica) {
			        sel[0][opt].selected = true;
			    }
			}
			$("[name='tematica']").removeAttr('disabled');
			//**************************************

			fechaSesion = document.getElementById('sesionFecha_'+objId).innerHTML;
			fechaSesion = fechaSesion.replace(/\n|\r/g, "").trim();
			document.getElementById('fechaS').value = fechaSesion;
			document.getElementById('fechaS').removeAttribute('disabled');
			$('#fechaSS').datetimepicker({pickTime: false});

			sesionInicio = document.getElementById('sesionInicio_'+objId).innerHTML;
			sesionInicio = sesionInicio.replace(/\n|\r/g, "").trim();
			document.getElementById('hinicio').value = sesionInicio;
			document.getElementById('hinicio').removeAttribute('disabled');
			$('#hinicio').datetimepicker({
				pickDate: false
			});
			

			sesionFin = document.getElementById('sesionFin_'+objId).innerHTML;
			sesionFin = sesionFin.replace(/\n|\r/g, "").trim();
			document.getElementById('hfin').value = sesionFin;
			document.getElementById('hfin').removeAttribute('disabled');
			$('#hfin').datetimepicker({
				pickDate: false
			});


			$("[name='performAction']").removeAttr('disabled');
			document.getElementById('action').value = 'UPDATE';
			document.getElementById('idRecord').value = objId;
			
			return false;
		});
		
		$("#agregarSesion").click(function()
		{
			@if( $sinTematicas )
				alertify.error('No existen Tem&aacute;ticas para este congreso. No se pueden agragar Sesiones')
			@else
				document.getElementById('ACME_LABEL').innerHTML = 'Agregar';
				document.getElementById('action').value = 'ADD';

				$("[name='performAction']").removeAttr('disabled');
				$("[name='nomSesion']").removeAttr('disabled');
				$("[name='nomSesion']").val('');
				document.getElementById('nomSesion').focus();

				$("[name='moderador']").removeAttr('disabled');
				$("[name='moderador']").val('');

				$("[name='tematica']").removeAttr('disabled');
				
				$("[name='fechaS']").removeAttr('disabled');
				$("[name='fechaS']").val('');
				$('#fechaSS').datetimepicker({pickTime: false});

				$("[name='hinicio']").removeAttr('disabled');
				$("[name='hinicio']").val('');
				$("[name='hfin']").removeAttr('disabled');
				$("[name='hfin']").val('');
				
				$('#hinicio').datetimepicker({
					pickDate: false
				});
				$('#hfin').datetimepicker({
					pickDate: false
				});
			@endif
			return false;
		});
		
		function formatDateMySQL(dateX) {
			return dateX.getFullYear() + '-' +
			(dateX.getMonth() < 9 ? '0' : '') + (dateX.getMonth()+1) + '-' +
			(dateX.getDate() < 10 ? '0' : '') + dateX.getDate()+' 00:00:00';
		}
		
		$("#performAction").click(function()
		{
			var psesion	= document.getElementById('nomSesion').value.trim();
			var pmoderador	= document.getElementById('moderador').value.trim();
			var ptempinicio	= $("[name='hinicio']").val();
			var ptempfin	= $("[name='hfin']").val();
			var ptempfecha	= $("[name='fechaS']").val();

			if(psesion == '')
			{
				alertify.error("Ingrese un Nombre de Sesi&oacute;n");
				$("[name='nomSesion']").parent().removeClass('has-error').addClass('has-error');
				return;
			}else{$("[name='nomSesion']").parent().removeClass('has-error');}
			if(pmoderador == '')
			{
				alertify.error("Ingrese un Moderador");
				$("[name='moderador']").parent().removeClass('has-error').addClass('has-error');
				return;
			}else{$("[name='moderador']").parent().removeClass('has-error');}
			if(ptempinicio == '')
			{
				alertify.error("Ingrese una Hora de Inicio ");
				$("[name='hinicio']").parent().removeClass('has-error').addClass('has-error');
				return;
			}else{$("[name='hinicio']").parent().removeClass('has-error');}
			if(ptempfin == '')
			{
				alertify.error("Ingrese una Hora de Fin");
				$("[name='hfin']").parent().removeClass('has-error').addClass('has-error');
				return;
			}else{$("[name='hfin']").parent().removeClass('has-error');}
			if(ptempfecha == '')
			{
				alertify.error("Ingrese una Fecha para la Sesi&oacute;n");
				$("[name='fechaS']").parent().removeClass('has-error').addClass('has-error');
				return;
			}else{$("[name='fechaS']").parent().removeClass('has-error');}

			var ptematica	= $("[name='tematica']").val();
			var pinicio	= hoursFormat24($("[name='hinicio']").val());
			var pfin	= hoursFormat24($("[name='hfin']").val());
			if(pfin<=pinicio)
			{
				alertify.error('Hora de Fin debe ser mayor a Hora de Inicio');
				return;
			}

			var fechaSesion = formatDateMySQL(new Date( $("[name=fechaS]").val().split("/").reverse().join("/")));

			var paction		= document.getElementById('action').value.trim();
			var pcurrentId	= document.getElementById('idRecord').value.trim();

			$.post("{{ URL::action('CongresoSesionesController@actualizarData') }}", {sesion: psesion, moderador : pmoderador, tematica:ptematica, fecha:fechaSesion, inicio:pinicio, fin:pfin, action:paction, currentId:pcurrentId ,idDC : {{$idCongreso}} })
				.done(function(data, status,jqXHR)
				{
					if(data.error)
					{
						alertify.error(data.mensaje);
					}
					else
					{
						alertify.success(data.mensaje);
						window.setTimeout(function()
						{
							window.location.replace("{{URL::action('CongresoSesionesController@getIndex',array($idCongreso) )}}");
						},
						900);
						
					}
				})
				.fail(function(data, status,jqXHR)
				{
					console.log("Server Returned " + status);
					alertify.error("Error de comunicación con el servidor, contacte a su adminstrador");
				});	
					
		});

		function hoursFormat24(pvalue)
		{
			var time = pvalue;
			var hours = Number(time.match(/^(\d+)/)[1]);
			var minutes = Number(time.match(/:(\d+)/)[1]);
			var AMPM = time.match(/\s(.*)$/)[1];
			if(AMPM == "PM" && hours<12) hours = hours+12;
			if(AMPM == "AM" && hours==12) hours = hours-12;
			var sHours = hours.toString();
			var sMinutes = minutes.toString();
			if(hours<10) sHours = "0" + sHours;
			if(minutes<10) sMinutes = "0" + sMinutes;
			return sHours + ":" + sMinutes;
		};
		
		
		
	});
</script>

@stop