@section('content')
<div class="container">
	<div class="col-sm-10 col-sm-offset-1" style="margin-top: 50px;">
		<div class="panel panel-primary" style="border-color: #205081; border-radius: 2px; box-shadow: 0 5px 10px gray;">
			<div class="panel-heading" style="border-radius: 0px; background: #205081; border-color: #205081;">
				<div class="panel-title">Configuraci&oacute;n del Congreso: <strong> {{$info_congreso['general'][0]->nomCongreso}}</strong></div>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<form role="form" class="form-horizontal">
							<fieldset>
								<legend>Informaci&oacute;n de Pre-Envio</legend>
								<div class="alert alert-info fade in">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									<strong>NOTA: </strong> Si desea agregar a la lista una categor&iacute;a, tem&aacute;tica o tipo de archivo, pongase en contacto con el <strong><a href="mailto:conuca@uca.edu.sv">Administrador del sistema (conuca@uca.edu.sv)</a></strong>.
								</div>	
								<div class="form-group">
									<label for="nomCongreso" class="col-sm-3 control-label">Categoria:&nbsp;</label>
									<div class="col-sm-9">
										<select multiple data-placeholder="Seleccione las categorías" class="chosen-choices" id="categoria" style="width:100%">
										@foreach($info_congreso['categorias'] as $categoria)
											<option {{$categoria->idCongreso!='NULL'?'selected':''}}  value="{{$categoria->idCategoria}}">{{$categoria->nomCategoria}}</option>
										 @endforeach
									</select>
									</div>
								</div>
								<div class="form-group">
									<label for="acronimo" class="col-sm-3 control-label">Tem&aacute;tica: &nbsp;</label>
									<div class="col-sm-9">
										<select multiple data-placeholder="Seleccione las Tem&aacute;ticas" class="chosen-choices" id="tematica" style="width:100%">
											@foreach($info_congreso['tematicas'] as $tematica)
												<option {{$tematica->idCongreso!='NULL'?'selected':''}} value="{{$tematica->idTematica}}">{{$tematica->nomTematica}}</option>
											 @endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label for="sitioWeb" class="col-sm-3 control-label">Tipos de Archivo (Subida):&nbsp;</label>
									<div class="col-sm-9">
										<select multiple data-placeholder="Seleccione los formatos" class="chosen-choices" id="tiposarchivo" style="width:100%;padding: .4em;margin-bottom: 12px;">
											@foreach($info_congreso['extensiones'] as $extension)
												<option {{$extension->idCongreso!='NULL'?'selected':''}} value="{{$extension->idExtensionDocumento}}">{{$extension->nomExtension}}</option>
											 @endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label for="ciudad" class="col-sm-3 control-label">Tama&ntilde;o M&aacute;xima de archivos (MB): &nbsp; </label>
									<div class="col-sm-9">
										<input type="number" onkeypress="return permitirSoloNumeros(event,this);" class="form-control" name="tammaxarchivo" value="{{$info_congreso['general'][0]->tamMaxArchivo}}" min="1" max="10"></input>
									</div>
								</div>
								<div class="form-group">
									<label for="pais" class="col-sm-3 control-label">Longitud M&aacute;xima Resumen <br>(en palabras): &nbsp; </label>
									<div class="col-sm-9">
										<input type="number" class="form-control" name="longmax" value="{{$info_congreso['general'][0]->longitudMaxResumenPaper}}" min="500"></input>
									</div>
								</div>
								<div class="form-group">
									<label for="pais" class="col-sm-3 control-label">Comit&eacute; del Congreso: &nbsp; </label>
									<div class="col-sm-9">
										<table class="table table-hover table-condensed" id="tablaPC">
											<thead>
											<tr>
												<th>
													Usuario
												</th>
												<th>
													Tem&aacute;tica
												</th>
												<th>
													Acci&oacute;n
												</th>
											</tr>
											</thead>
											<tbody>
											@foreach($info_congreso['comitePC'] as $pcmiembro)
											<tr id="{{$pcmiembro->idPcTematica}}">
												<td value="{{$pcmiembro->idUsuario}}">
													{{Usuario::Find($pcmiembro->idUsuario)->nombreUsuario. ' ' .Usuario::Find($pcmiembro->idUsuario)->apelUsuario . ' ('.Usuario::Find($pcmiembro->idUsuario)->emailUsuario.')'}}
												</td>
												<td value="{{$pcmiembro->idTematica}}">
													{{Tematica::Find($pcmiembro->idTematica)->nomTematica}}
												</td>
												<td value="{{$pcmiembro->idPcTematica}}">
													<button type="button" class="eliminarFilaDB btn btn-danger fileinput-remove fileinput-remove-button" style="font-size:5px;">
													<i class="glyphicon glyphicon-remove" style="font-size:10px;"></i>
													</button>
												</td>
											</tr>
											@endforeach
											</tbody>
											</table>
											<div id="pc-msg" class="alert alert-warning" style="display:{{$info_congreso['comitePC']==null?'block':'none'}}">
												<strong>NOTA: </strong>No se han agregado miembros al comite.
											</div>
											<button type="button" id="nuevo-pc" class="btn btn-success fileinput-remove fileinput-remove-button" style="font-size:5px;">
											<i class="glyphicon glyphicon-plus" style="font-size:10px;"></i>
											</button>
									</div>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
			<div class="panel-footer">
				<div class="row">
					<div class="col-sm-4">
						<button type="button" id="cancelarTodo" class="btn btn-default btn-default">Regresar</button>
						<button type="button" id="guardarTodo" class="btn btn-primary btn-default">Guardar Cambios</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Ventana Modal para nuevo miembro del comite (PC)-->
<div class="modal fade" id="nuevo-pc-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" >Agregar miembro al comit&eacute;</h4>
			</div>
			<div class="modal-body">
				<div class="container" style="width:450px">
					<div class="row clearfix">
						<div class="col-md-4 column">
							<span style="font-weight:bold">Usuario &nbsp; </span>
						</div>
						<div class="col-md-8 column" id="divUsuarios" > 
							<select data-placeholder="Seleccione una tematica" class="chosen" id="listaUsuarios">

							</select>
						</div>
					</div>
					<br/>
					<div class="row clearfix">
						<div class="col-md-4 column">
							<span style="font-weight:bold">Temática &nbsp; </span>
						</div>
						<div class="col-md-8 column" > 
							<select data-placeholder="Seleccione una tematica" class="chosen" id="listaTematicas">

							</select>
						</div>
					</div>
					<br/>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="button" class="btn btn-primary" id="agregarPCbtn">Agregar</button>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	$(document).ready(function() {
		$("#categoria").chosen();
		$("#tematica").chosen();
		$("#listaTematicas").chosen({disable_search_threshold: 10});
		$("#tiposarchivo").chosen();

		var categorias=new Array();
		var tematicas=new Array();
		var extensiones=new Array();
		var comiteUSR = new Array();
		var comiteTEM = new Array();	

		var elementosActuales=$('#tematica').val();


		function buscarID(id,array)
		{
			for (var i=0; i<array.length; i++){
				if(array[i]==id) return true;
			}
			return false;
		}

		function configurarEventos()
		{
			//event.preventDefault();
			$('.eliminarFila').unbind('click');
			$(".eliminarFila").click(function()
			{
				$(this).closest('tr').remove();

				if (document.getElementById('tablaPC').getElementsByTagName("tbody")[0].getElementsByTagName("tr").length==0)
					$( "#pc-msg" ).css("display","block");
				else
					$( "#pc-msg" ).css("display","none");
	
			});

			function eliminarDeArray(array,from, to) {
				var rest = array.slice((to || from) + 1 || array.length);
				array.length = from < 0 ? array.length + from : from;
				return array.push.apply(array, rest);
			};

			function eliminarRegistro(id,tabla,referencia,idUsr,idPcTematica)
			{
				var ref=referencia;
				$.post("{{URL::action('UsuariosCongresosController@eliminarRegistro')}}", {idRegistro: id,nomTabla:tabla,idUsr:idUsr,idDetalleCongreso: "{{$info_congreso['general'][0]->idDetalleCongreso}}",idPcTematica:idPcTematica})
				.done(function(data)
				{

					if(data.mensaje.indexOf("ERROR") != -1){
						alertify.error(data.mensaje);
					}
					else{
						alertify.success("Registro eliminado.");
						$(ref).closest('tr').remove();
						if (document.getElementById('tablaPC').getElementsByTagName("tbody")[0].getElementsByTagName("tr").length==0)
							$( "#pc-msg" ).css("display","block");
						else
							$( "#pc-msg" ).css("display","none");
					}
				})
				.fail(function(data, status, jqXHR)
				{
					console.log("Server Returned " + status);
					alertify.error("Error: No se pudo eliminar el registro");
				});
			}
			$('#categoria').unbind('change');
			$('#categoria').on('change', function(event, params) {
				event.preventDefault();
				if(params.deselected){
					var id=params.deselected
					if(!buscarID(id,categorias)){
						eliminarRegistro(id,'categoria','null','null','null')
					}
					var index = categorias.indexOf(id);
					if(index!= -1)  eliminarDeArray(categorias,index);
				}
				else
				{	
					var id=params.selected;
					categorias.push(params.selected);
				}
			});

			$('#tematica').unbind('change');
			$('#tematica').on('change', function(event, params) {
				event.preventDefault();
				if(params.deselected){
					var id=params.deselected;
					alertify.confirm("<strong>IMPORTANTE: ¿Esta seguro que desea eliminar la temática?</strong>"+
									"</br></br>Cualquier usuario (PC) relacionado con esta temática también será desvinculado"+
									"</br>del registro para este congreso.",
						function (e)
						{
							if (e) {
								var rows = document.getElementById("tablaPC").rows;
								for(var i = 1, ceiling = rows.length; i < ceiling; i++) {
									if(rows[i].cells[1].getAttribute("value")==id){
										if(!buscarID(id,tematicas)){
											eliminarRegistro(id,'tablaPC',rows[i],rows[i].cells[0].getAttribute("value"),'null');
										}
										else{
											$(rows[i]).closest('tr').remove();
											if (document.getElementById('tablaPC').getElementsByTagName("tbody")[0].getElementsByTagName("tr").length==0)
												$( "#pc-msg" ).css("display","block");
											else
												$( "#pc-msg" ).css("display","none");
										} 
									} 
								}
								
								if(!buscarID(id,tematicas)) eliminarRegistro(id,'tematica','null','null','null')
								var index = tematicas.indexOf(id);
								if(index!= -1) eliminarDeArray(tematicas,index);

							}else {
								$('#tematica').val(elementosActuales);
								$('#tematica').trigger("chosen:updated");
							}
						}
					);
				}
				else
				{	
					var id=params.selected;
					tematicas.push(params.selected);
					elementosActuales=$('#tematica').val();
				}
			});

			$('#tiposarchivo').unbind('change');
			$('#tiposarchivo').on('change', function(event, params) {
				event.preventDefault();
				if(params.deselected){
					var id=params.deselected
					if(!buscarID(id,extensiones)){
						eliminarRegistro(id,'tiposarchivo','null','null','null')
					}
					var index = extensiones.indexOf(id);
					if(index!= -1) eliminarDeArray(extensiones,index);
				}
				else
				{	
					var id=params.selected;
					extensiones.push(params.selected);
				}
			});

			$('.eliminarFilaDB').unbind('click');
			$(".eliminarFilaDB").click(function()
			{
				var ref=this;
				alertify.confirm("¿Esta seguro que desea eliminar el registro de manera permanente?", function (e) {
					if (e) {
						var table=$(ref).closest('table').attr('id');
						var idUsr=$(ref).closest('tr').children("td:nth-child(1)").attr('value');
						var id=$(ref).closest('tr').children("td:nth-child(2)").attr('value');
						var idPcTematica=$(ref).closest('tr').children("td:nth-child(3)").attr('value');
						eliminarRegistro(id,table,ref,idUsr,idPcTematica);
						
					}else {
						return;
					}
					e=null;
				});
			});
			return false;
		}

		configurarEventos();

		function cambiarClase(tabla)
		{

			var laTabla="#"+tabla+" [class= bg-success] td button";
			$(laTabla).removeClass('eliminarFila').addClass('eliminarFilaDB');

			laTabla="#"+tabla+" [class= bg-success]";
			$(laTabla).removeClass('bg-success');

			configurarEventos();
		}

		$("#nuevo-pc").click(function(){
			var items=$('#tematica').val();
			if(items==null){
				alertify.error("Debe agregar al menos una temática.");
				return;
			}
			var usuariosEnLista = new Array();
			var rows = document.getElementById("tablaPC").rows;
			for(var i = 1, ceiling = rows.length; i < ceiling; i++) {
				usuariosEnLista.push(rows[i].cells[0].getAttribute("value"));
			}
			
			var tematicasEnLista = new Array();
			var rows = document.getElementById("tablaPC").rows;
			for(var i = 1, ceiling = rows.length; i < ceiling; i++) {
				tematicasEnLista.push(rows[i].cells[1].getAttribute("value"));
			}

			document.getElementById("listaTematicas").options.length = 0;
			var select = document.getElementById("listaTematicas");
			var length = select.options.length;
			var items=$("#tematica").val();
			for (var i=0; i<items.length; i++) {
				if (!buscarID(items[i],tematicasEnLista)){
					var option = document.createElement("option");
					option.text = $('#tematica').children('option[value="'+items[i]+'"]').text();
					option.value = items[i];
					select.add(option);
				}
			}
			$(select).trigger("chosen:updated");

			if(document.getElementById("listaTematicas").options.length == 0){
				alertify.error("No hay tematicas disponibles para asignar.");
				return;
			}

			$.post("{{URL::action('UsuariosCongresosController@usuariosDisponiblesPC')}}", {idCongreso: "{{$info_congreso['general'][0]->idCongreso}}",idRepetidos: JSON.stringify(usuariosEnLista)})
			.done(function(data)
			{
				document.getElementById("listaUsuarios").options.length = 0;
				if(data==""){
					alertify.error("No hay usuarios disponibles.");
					return;
				}
				$("#divUsuarios").html(data);
				$("#listaUsuarios").chosen();
				$('#listaUsuarios').trigger("chosen:updated");

				$('#agregarPCbtn').unbind('click');
				$("#agregarPCbtn").click(function(){
					$( "#tablaPC tbody" ).append("<tr class='bg-success' data-toggle='tooltip' data-placement='left' title='Nuevo Elemento (Por Procesar)'>" +
						"<td value="+ $("#listaUsuarios option:selected").val()+">" + $("#listaUsuarios option:selected").text() +"</td>" +
						"<td value="+ $("#listaTematicas option:selected").val()+ ">" + $("#listaTematicas option:selected").text() + "</td>" +
						"<td>" + '<button type="button" class="eliminarFila btn btn-danger fileinput-remove fileinput-remove-button" style="font-size:5px;"> '+
									'<i class="glyphicon glyphicon-remove" style="font-size:10px;"></i></button>' +"</td>" +
					"</tr>" );

					configurarEventos();
					$('#nuevo-pc-form').modal('hide');
					$( "#pc-msg" ).css("display","none");
					alertify.success("El nuevo miembro del comité ha sido agregado.");
				});			

				$('#nuevo-pc-form').modal('show');
			})
			.fail(function(data, status, jqXHR)
			{
				console.log("Server Returned " + status);
				alertify.error("Error: No se puede obtener el listado de usuarios.");
			});
		});

		$("#cancelarTodo").click(function(){
			window.location.href="{{URL::action('UsuariosCongresosController@establecerCongreso',array($idCongreso))}}";
		});

		function guardarDatosCongresoPreEnvio()
		{
			var idCongreso="{{$info_congreso['general'][0]->idCongreso}}";
			var tammaxarchivo=$("[name=tammaxarchivo]").val();
			var longmax=$("[name=longmax]").val();
			//var mostrarNRev=document.getElementsByName("mostrarNRev-estado")[0].checked?1:0;

			$.post("{{URL::action('UsuariosCongresosController@actualizarCongresoPreEnvio')}}",
			 {idCongreso: "{{$info_congreso['general'][0]->idCongreso}}",
			tammaxarchivo: tammaxarchivo,
			longmax:longmax,
			mostrarNRev:0,
			categorias:JSON.stringify(categorias),
			tematicas:JSON.stringify(tematicas),
			extensiones:JSON.stringify(extensiones)
			})
			.done(function(data)
			{
				if(data.mensaje.indexOf("ERROR") != -1)
					alertify.error(data.mensaje);
				else
					guardarDatosRevisiones();
			})
			.fail(function(data, status, jqXHR)
			{
				alertify.error("Error en el servidor");
			});
		}

		function guardarDatosRevisiones()
		{

			comiteUSR = new Array();
			comiteTEM = new Array();
			var rows = document.getElementById("tablaPC").rows;

			for(var i = 1, ceiling = rows.length; i < ceiling; i++) {
				if(rows[i].getAttribute("class")=="bg-success"){
					comiteUSR.push(rows[i].cells[0].getAttribute("value"));
					comiteTEM.push(rows[i].cells[1].getAttribute("value"));
				}
			}
			if(rows.length==1)
			{
				window.location.href="{{URL::action('UsuariosCongresosController@establecerCongreso',array($idCongreso))}}";
				return;
			}
			
			$.post("{{URL::action('UsuariosCongresosController@actualizarCongresoComite')}}",
			 {idCongreso: "{{$info_congreso['general'][0]->idCongreso}}",
			comiteUSR:JSON.stringify(comiteUSR),
			comiteTEM:JSON.stringify(comiteTEM),
			})
			.done(function(data)
			{
				if(data.mensaje.indexOf("ERROR") != -1)
				{
					alertify.error(data.mensaje);
				}
				else
				{
					alertify.success("El congreso ha sido configurado correctamente.");
					cambiarClase("tablaPC");
					window.location.href="{{URL::action('UsuariosCongresosController@establecerCongreso',array($idCongreso))}}";
				}
			})
			.fail(function(data, status, jqXHR)
			{
				alertify.error("Error en el servidor");
				console.log(data.message);
			});

		}

		$("#guardarTodo").click(function(){
			guardarDatosCongresoPreEnvio();
			categorias=new Array();
			tematicas=new Array();
			extensiones=new Array();

		});
	});
</script>
@stop