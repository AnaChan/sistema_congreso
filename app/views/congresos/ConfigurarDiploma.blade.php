@section('content')
<div class="col-sm-10 col-sm-offset-1" style="margin-top: 50px;">
	<div class="panel panel-primary" style="border-color: #205081; border-radius: 2px; box-shadow: 0 5px 10px gray;">
		<div class="panel-heading" style="border-radius: 0px; background: #205081; border-color: #205081;">
			<div class="panel-title">Diseños para diplomas</div>
		</div>
		<form role="form"  id="subidaDiplomas" name="subidaDiplomas" enctype="multipart/form-data" action="{{URL::action('CrearAgendaController@guardarEstilosDiplomas')}}" method="POST"  class="form-horizontal">

		<input type="hidden" name="idCongreso" value="{{$idCongreso}}"/>
		<input type="hidden" name="totalControles" id="totalControles" value="3">

		<div class="panel-body">
			<div class="alert alert-info fade in">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<strong>1</strong> Los "diseños para diplomas" se basan en una <strong>imagen</strong> de cierta <strong>calidad</strong> y <strong>dimensión</strong>. Cada diseño ha sido programado para respetar los espacios definidos para nombre, títiulo y fecha. Se proporciona un ejemplo por cada estilo para tener una idea "visual" de los espacios reservados que se esperan en la imagen, para su correcta impresión al momento de general el diploma (PDF).
				<br/><br/>
				<strong>2</strong> Es su responsabilidad respetar los espacios reservados. Si la imagen es de mayor dimensión a la definida por el estilo, esta se ajustará al tamaño esperado.
				<br/><br/>
				<strong>3</strong> Para el nombre del ponente, solo se tomará el primer nombre y primer apellido.
				<br/><br/>
				<strong>4</strong> Al subir una nueva imagen, se reemplazará la anterior.
			</div>

			<div class="row">
				<div class="col-md-12 column"> <!--col-sm-10 col-sm-offset-1 -->
					<legend>Estilo 1 - Orientaci&oacute;n Horizontal</legend>
				</div>
				<div class="col-md-6 column"> <!--col-sm-10 col-sm-offset-1 -->
					<fieldset>
						<label class="control-label">Imagen en calidad 200 ppi&nbsp;&nbsp;</label></label><a href="{{URL::asset('diplomasEstilo')}}/Estilo1/200ppi/200.jpg" target="_blank"> (ejemplo) </a>
						</br>
						<label class="control-label">Dimensiones: 2200(ancho) x 1700(alto) Pixeles</label>
						</br></br>
						<table class="table">
							<thead>
							<tr>
								<label class="control-label">Nombre ponente</label></br>
								<label class="control-label">Fuente: Times, Tamaño: 60</label></br>
								X <span class="label label-default">100px</span>
								Y <span class="label label-default">400px</span>
								Ancho <span class="label label-default">2000px</span>
								Alto <span class="label label-default">400px</span></br></br>
							</tr>
							<tr>
								<label class="control-label">T&iacute;tulo ponencia</label></br>
								<label class="control-label">Fuente: Times, Tamaño: 30</label></br>
								X <span class="label label-default">100px</span>
								Y <span class="label label-default">880px</span>
								Ancho <span class="label label-default">2000px</span>
								Alto <span class="label label-default">260px</span></br></br>
							</tr>
							<tr>
								<label class="control-label">Fecha</label></br>
								<label class="control-label">Fuente: Times, Tamaño: 14</label></br>
								X <span class="label label-default">100px</span>
								Y <span class="label label-default">1190px</span>
								Ancho <span class="label label-default">2000px</span>
								Alto <span class="label label-default">60px</span>
							</tr>
							</thead>
						</table>
					</fieldset>
				</div>
				
				<div class="col-md-6 column">
					<input name="estilo1" id="estilo1" type="file" >
					@if( $estilo['1'] == "" )
						<strong>No tiene ning&uacute;n archivo asociado</strong>
					@else
						<span class="glyphicon glyphicon-picture"></span> <strong>Ya dispone de una imagen</strong>
						</br>
						<a href="{{URL::asset('/')}}{{$estilo['1']}}" target="_blank"><img src="{{URL::asset('/')}}{{$estilo['1']}}" width="200" height="150"></a>
					@endif
				</div>
			</div>


			</br></br>

			<div class="row">
				<div class="col-md-12 column"> <!--col-sm-10 col-sm-offset-1 -->
					<legend>Estilo 2 - Orientaci&oacute;n Vertical</legend>
				</div>
				<div class="col-md-6 column"> <!--col-sm-10 col-sm-offset-1 -->
					<fieldset>
						<label class="control-label">Imagen en calidad 200 ppi&nbsp;&nbsp;</label></label><a href="{{URL::asset('diplomasEstilo')}}/Estilo2/200ppi/200.jpg" target="_blank"> (ejemplo) </a>
						</br>
						<label class="control-label">Dimensiones: 1700(alto) x 2200(ancho) Pixeles</label>
						</br></br>
						<table class="table">
							<thead>
							<tr>
								<label class="control-label">Nombre ponente</label></br>
								<label class="control-label">Fuente: Times, Tamaño: 40</label></br>
								X <span class="label label-default">50px</span>
								Y <span class="label label-default">650px</span>
								Ancho <span class="label label-default">1600px</span>
								Alto <span class="label label-default">300px</span></br></br>
							</tr>
							<tr>
								<label class="control-label">T&iacute;tulo ponencia</label></br>
								<label class="control-label">Fuente: Times, Tamaño: 30</label></br>
								X <span class="label label-default">50px</span>
								Y <span class="label label-default">1030px</span>
								Ancho <span class="label label-default">1600px</span>
								Alto <span class="label label-default">200px</span></br></br>
							</tr>
							<tr>
								<label class="control-label">Fecha</label></br>
								<label class="control-label">Fuente: Times, Tamaño: 14</label></br>
								X <span class="label label-default">50px</span>
								Y <span class="label label-default">1350px</span>
								Ancho <span class="label label-default">1600px</span>
								Alto <span class="label label-default">100px</span>
							</tr>
							</thead>
						</table>
					</fieldset>
				</div>

				<div class="col-md-6 column">
					<input name="estilo2" id="estilo2" type="file" >
					@if( $estilo['2'] == "" )
						<strong>No tiene ning&uacute;n archivo asociado</strong>
					@else
						<span class="glyphicon glyphicon-picture"></span> <strong>Ya dispone de una imagen</strong>
						</br>
						<a href="{{URL::asset('/')}}{{$estilo['2']}}" target="_blank"><img src="{{URL::asset('/')}}{{$estilo['2']}}" width="150" height="200"></a>
					@endif
				</div>
			</div>

			</br></br>

			<div class="row">
				<div class="col-md-12 column"> <!--col-sm-10 col-sm-offset-1 -->
					<legend>Estilo 3 - Orientaci&oacute;n Horizontal</legend>
				</div>
				<div class="col-md-6 column"> <!--col-sm-10 col-sm-offset-1 -->
					<fieldset>
						<label class="control-label">Estilo a 200 ppi&nbsp;&nbsp;</label></label><a href="{{URL::asset('diplomasEstilo')}}/Estilo3/200ppi/200.jpg" target="_blank"> (ejemplo) </a>
						</br>
						<label class="control-label">Dimensiones: 2200(ancho) x 1700(alto) Pixeles</label>
						</br></br>
						<table class="table">
							<thead>
							<tr>
								<label class="control-label">Nombre ponente</label></br>
								<label class="control-label">Fuente: Times, Tamaño: 60</label></br>
								X <span class="label label-default">100px</span>
								Y <span class="label label-default">400px</span>
								Ancho <span class="label label-default">2000px</span>
								Alto <span class="label label-default">400px</span></br></br>
							</tr>
							<tr>
								<label class="control-label">T&iacute;tulo ponencia</label></br>
								<label class="control-label">Fuente: Times, Tamaño: 30</label></br>
								X <span class="label label-default">100px</span>
								Y <span class="label label-default">880px</span>
								Ancho <span class="label label-default">2000px</span>
								Alto <span class="label label-default">260px</span></br></br>
							</tr>
							<tr>
								<label class="control-label">Fecha</label></br>
								<label class="control-label">Fuente: Times, Tamaño: 14</label></br>
								X <span class="label label-default">100px</span>
								Y <span class="label label-default">1190px</span>
								Ancho <span class="label label-default">2000px</span>
								Alto <span class="label label-default">60px</span>
							</tr>
							</thead>
						</table>
					</fieldset>
				</div>

				<div class="col-md-6 column">
					<input name="estilo3" id="estilo3" type="file" >
					@if( $estilo['3'] == "" )
						<strong>No tiene ning&uacute;n archivo asociado</strong>
					@else
						<span class="glyphicon glyphicon-picture"></span> <strong>Ya dispone de una imagen</strong>
						</br>
						<a href="{{URL::asset('/')}}{{$estilo['3']}}" target="_blank"><img src="{{URL::asset('/')}}{{$estilo['3']}}" width="200" height="150"></a>
					@endif
				</div>
			</div>


		</div>
		</form>
		
		<div class="panel-footer">
			<div class="row">
				<div class="col-sm-4">
					<button type="button" id="subidaEstilos" class="btn btn-primary btn-default">Subir archivos</button>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function()
	{

		$("#estilo1").fileinput(
		{
			showUpload: false,
			showRemove: false,
			browseClass: "btn btn-success",
			browseLabel: " Buscar",
			browseIcon: '<i class="glyphicon glyphicon-picture"></i>',
			removeClass: "btn btn-danger",
			removeLabel: " Eliminar",
			removeIcon: '<i class="glyphicon glyphicon-trash"></i>',
			uploadClass: "btn btn-info",
		});
		$("#estilo2").fileinput(
		{
			showUpload: false,
			showRemove: false,
			browseClass: "btn btn-success",
			browseLabel: " Buscar",
			browseIcon: '<i class="glyphicon glyphicon-picture"></i>',
			removeClass: "btn btn-danger",
			removeLabel: " Eliminar",
			removeIcon: '<i class="glyphicon glyphicon-trash"></i>',
			uploadClass: "btn btn-info",
		});
		$("#estilo3").fileinput(
		{
			showUpload: false,
			showRemove: false,
			browseClass: "btn btn-success",
			browseLabel: " Buscar",
			browseIcon: '<i class="glyphicon glyphicon-picture"></i>',
			removeClass: "btn btn-danger",
			removeLabel: " Eliminar",
			removeIcon: '<i class="glyphicon glyphicon-trash"></i>',
			uploadClass: "btn btn-info",
		});

		//Quita el boton de Eliminar de la vista previa que es igual al "showRemove" para que no falle el guardar
		$("#estilo1").change(function()
		{
			$("[class='close fileinput-remove text-right']").hide();
		});

		$("#estilo2").change(function()
		{
			$("[class='close fileinput-remove text-right']").hide();
		});

		$("#estilo3").change(function()
		{
			$("[class='close fileinput-remove text-right']").hide();
		});

		$("#subidaEstilos").click(function()
		{
			var totalControls = parseInt($("#totalControles").val());
            var alto     = true;
            var mensajeUsuario = 'ALTO, ningún archivo elegido para subir';

            var ruta     = "";
            for (var i=1; i<=totalControls; i++) {
                ruta = $("#estilo"+i).val().replace(/C:\\fakepath\\/i, '');
                if(ruta != ""){
					alto = false;
                }
            }
            if(alto)
				alertify.error(mensajeUsuario);
            else
            {
            	//Fake ajax .. para dar una sensacion de progreso al usuario.
            	$('#cargando-img').removeClass('hidden');
				$("#fade").fadeToggle("slow");
				document.forms["subidaDiplomas"].submit();
            }
		});
	});
</script>
@stop