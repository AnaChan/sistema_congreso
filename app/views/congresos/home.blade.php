@section('content')
@if (Session::has('message'))
<br>
<div class="panel panel-warning fade in">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="padding:7px">×</button>
	<div class="panel-heading">
		<h3 class="panel-title">
			CONUCA - Mensaje de la aplicaci&oacute;n
		</h3>
	</div>
	<div class="panel-body">{{ Session::get('message') }}</div>
</div>
@endif
<div class="row">
	<div class="col-sm-7">
		<div class="panel panel-default" style="margin-top: 15px;">
			<div class="panel-body">
				<div class="page-header" style="margin-top: 0px !important;">
					<h3>Fechas Importantes</h3>
				</div>
				<table class="table table-striped">
					<thead>
						<tr>
							<th></th>
							<th>Descripci&oacute;n</th>
							<th>Fecha de Inicio:</th>
							<th>Fecha de Fin:</th>
						</tr>
					</thead>
					<tbody>
						@if (count($fechasImportantes) == 0)
							<tr>
								<td colspan="4">
									<div class="row">
										<div class="col-sm-8 col-sm-offset-2" style="text-align: center;">
											<h3><span class='label label-info'>No hay congresos para mostrar</span></h3>
										</div>
									</div>
								</td>
							</tr>
						@else
							@foreach ($fechasImportantes as $key => $fechaImportante)
								<tr>
									<td>{{$key +1}}</td>
									<td>{{$fechaImportante->nomTipoFecha}}</td>
									<td>{{date('d/m/Y', strtotime($fechaImportante->fecInicio))}}</td>
									<td>{{date('d/m/Y', strtotime($fechaImportante->fecFin))}}</td>
								</tr>
							@endforeach
						@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-sm-5">
		<div class="panel panel-default" style="margin-top: 15px;">
			<div class="panel-body">
				<div class="page-header" style="margin-top: 0px !important;">
					<h3>Informaci&oacute;n del congreso</h3>
				</div>
				@if ($nomRol =='Chair')	
				<ul class="list-group">
					<a class="list-group-item active">CONGRESO: {{$nombreCongreso}} <small><strong>{{$acronimoCongreso}}</strong></small></a>
					<li class="list-group-item" style="text-align:center">
						<ul class="list-group">
							<img src="{{$banner}}" style="height:63px; width:324px"  alt="banners/convacio.png"/>
						</ul>
					</li>
					<a href="#" class="list-group-item active">Usuarios inscritos<span class="badge">{{$personasInscritas}}</span></a>
					<li class="list-group-item">
						<ul class="list-group">
							<a href="#" class="list-group-item">Miembros del comit&eacute;<span class="badge">{{$pcInscritos}}</span></a>
							<a href="#" class="list-group-item">Revisores<span class="badge">{{$revisoresInscritos}}</span></a>
							<a href="#" class="list-group-item">Autores<span class="badge">{{$autoresInscritos}}</span></a>
						</ul>
					</li>
					<a href="#" class="list-group-item active" >Fichas inscritas<span class="badge">{{$fichasInscritas}}</span></a>
					<li class="list-group-item">
						<ul class="list-group">
							<a href="#" class="list-group-item">Fichas Aceptadas<span class="badge">{{$fichasAceptadas}}</span></a>
							<a href="#" class="list-group-item">Fichas Sin Revisar<span class="badge">{{$fichasSinRevisar}}</span></a>
						</ul>
					</li>
				</ul>
						</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default" style="margin-top: 15px;">
			<div class="panel-body">
				<div class="page-header" style="margin-top: 0px !important;">
					<h3>Fichas y art&iacute;culos</h3>
				</div>
				<table class="table table-striped" id="tablaResumen">
					<thead>
						<tr>
							<th>Id</th>
							<th>T&iacute;tulo</th>
							<th>Tematica</th>
							<th>Estado</th>
							<th>Autores</th>
							<th>Info</th>
							
						</tr>
					</thead>
					<tbody>
					@foreach ($listaArticulos as $elemento)
								<tr>
									<td>{{$elemento['idFicha']}}</td>
									<td>{{$elemento['tituloPaper']}}</td>
									<td>{{$elemento['Tematica']}}</td>
									<td>{{$elemento['nombreEstado']}}</td>
									<td>{{$elemento['autores']}}</td>
									<td><a target="_blank" href="{{URL::action('SumissionController@indexVisual',array('idCongreso'=>$idCongreso,'idFicha'=>$elemento['idFicha']))}}">Ver</a></td>
									
								</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
				@else
					<a class="list-group-item active">CONGRESO: {{$nombreCongreso}} <small><strong>{{$acronimoCongreso}}</strong></small></a>
					<li class="list-group-item" style="text-align:center">
						<ul class="list-group">
							<img src="{{$banner}}" style="height:63px; width:324px"  alt="banners/convacio.png"/>
						</ul>
					</li>
				@endif
			</div>
		</div>
	</div>
</div>
</div>

<!--Ventana Modal para enviar solicitud para ser revisor -->

<div class="modal fade" id="nueva-solicitud-revisor-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="padding-top: 12%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" >Enviar solicitud para ser revisor del congreso.</h4>
      </div>
      <div class="modal-body">
		<div class="container" style="width:450px">
			<div class="row">
				<div class="col-md-4 column">
					<span style="font-weight:bold">Elija la Tem&aacute;tica</span>
				</div>
				<div class="col-md-8 column" id="divTematicas" > 
					<select class="chosen" id="listaTematicas" placeholder="Lista de temáticas" >
					</select>
				</div>
			</div>
			<br/>
     	</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" id="agregarSolicitud">Enviar</button>
			<button type="button" class="btn btn-primary" data-dismiss="modal" >Cancelar</button>
		</div>
    </div>
  </div>
</div>

<script type="text/javascript">

		$('#tablaResumen').DataTable({
            "language": {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "No se encuentran archivos disponibles.",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
							"sFirst":    "Primero",
							"sLast":     "Último",
							"sNext":     "Siguiente",
							"sPrevious": "Anterior"
							},
				"oAria": {
						"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
						"sSortDescending": ": Activar para ordenar la columna de manera descendente"
						}
            }
		});

	$("#listaTematicas").chosen();
	$("#nueva-solicitud-revisor").click(function(){

		$.post("{{URL::action('CongresoController@listarTematicasDisponibles')}}", {idCongreso: "{{$idCongreso}}",idUsuario:"{{Auth::user()->idUsuario}}"})
		.done(function(data)
		{
			if(data.error){
				alertify.error(data.mensaje);
			}
			else{
				console.log(data.htmlVista);
				$("#divTematicas").html(data.htmlvista);
				$("#listaTematicas").chosen();
				$('#listaTematicas').trigger("chosen:updated");
				$('#nueva-solicitud-revisor-form').modal('show');
			}
		})
		.fail(function(data, status, jqXHR)
		{
			alertify.error("Error: No se puede obtener el listado de temáticas.");
		});
	});

	$("#agregarSolicitud").click(function(){
		alertify.confirm("Si envía la solicitud deberá esperar hasta que esta sea gestionada por el Administrador del congreso. <br>¿Está seguro que desea enviarla en este momento?", function (e) 
		{
			if (e) {
				enviarSolicitudRevisor();

			}
		});
	});

	function enviarSolicitudRevisor()
	{
		var idCongreso="{{$idCongreso}}";
		var idTematica="";
		var idPcTematica=$("#listaTematicas").val();
		var usuariosEnTabla = new Array();
		usuariosEnTabla.push("{{Auth::user()->idUsuario}}");
		$.post("{{URL::action('UsuariosCongresosController@actualizarRevisores')}}",
		 {idCongreso: "{{$idCongreso}}",
		idTematica: idTematica,
		idPcTematica:idPcTematica,
		solicitado:1,
		listaUsuarios:JSON.stringify(usuariosEnTabla)
		})
		.done(function(data)
		{
			if(data.mensaje.indexOf("ERROR") != -1){
				alertify.error(data.mensaje);
			}
			else{
				alertify.success("Petición enviada existosamente, redireccionando...");
				window.location.href="{{URL::action('UsuariosCongresosController@establecerCongreso',array($idCongreso))}}";
			}
		})
		.fail(function(data, status, jqXHR)
		{
			alertify.error("Error en el servidor");
		});
	}

</script>
@stop