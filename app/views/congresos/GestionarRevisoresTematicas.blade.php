@section('content')
<div class="container">
	<div class="col-sm-10 col-sm-offset-1" style="margin-top: 50px;">
	<div class="panel panel-primary" style="border-color: #205081; border-radius: 2px; box-shadow: 0 5px 10px gray;">
		<div class="panel-heading" style="border-radius: 0px; background: #205081; border-color: #205081;">
			<div class="panel-title">Configuraci&oacute;n del Congreso: <strong> {{Congreso::Where('idCongreso','=',$idCongreso)->first()->nomCongreso}} </strong></div>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1">
					<form role="form" class="form-horizontal">
						<fieldset>
							<legend>Listado de revisores solicitantes y propuestos por el comit&eacute; del congreso</legend>
							<div class="form-group">
								<div class="col-sm-12">
									<table class="table table-hover table-condensed" id="tablaRevisoresAP">
										<thead>
										<tr>
											<th>
												Usuario
											</th>
											<th>
												Estado
											</th>
											<th>
												Tem&aacute;ticas
											</th>
											<th>
												Acci&oacute;n
											</th>
										</tr>
										</thead>
										<tbody>
										@foreach($revisores as $revisor)
										<tr id="{{$revisor->idPcTematica}}">
											<td value="{{$revisor->idUsuario}}">
												{{Usuario::Find($revisor->idUsuario)->nombreUsuario. ' ' .Usuario::Find($revisor->idUsuario)->apelUsuario . ' ('.Usuario::Find($revisor->idUsuario)->emailUsuario.')'}}
											</td>
											<td value="{{$revisor->nombreEstado}}">
												{{$revisor->nombreEstado}}
											</td>
											<td value="{{$revisor->idTematica}}">
												{{$revisor->nomTematica}}
											</td>
											<td value="{{$revisor->idPcTematica}}">
												<button type="button" class="aprobarFilaDB btn btn-success fileinput-remove fileinput-remove-button" style="font-size:5px;">
												<i class="glyphicon glyphicon-ok" style="font-size:10px;"></i>
												</button>
												<button type="button" class="eliminarFilaDB btn btn-danger fileinput-remove fileinput-remove-button" style="font-size:5px;">
												<i class="glyphicon glyphicon-remove" style="font-size:10px;"></i>
												</button>
											</td>
										</tr>
										@endforeach
										</tbody>
										</table>
										<div id="revA-msg" class="alert alert-warning" style="display:{{$revisores==null?'block':'none'}}">
											<strong>NOTA: </strong>No hay registros que gestionar.
										</div>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-sm-4">
					
					<button type="button" id="cancelarTodo" class="btn btn-default btn-default">Cancelar</button>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		function configurarEventos()
		{
			//event.preventDefault();
			function eliminarDeArray(array,from, to) {
				var rest = array.slice((to || from) + 1 || array.length);
				array.length = from < 0 ? array.length + from : from;
				return array.push.apply(array, rest);
			};

			function eliminarRegistro(id,tabla,referencia,idTematica,idPcTematica)
			{
				var ref=referencia;
				var tablaC="revisoresGrupo";
				$.post("{{URL::action('UsuariosCongresosController@eliminarRegistro')}}", {idRegistro: id,nomTabla:tablaC,idCongreso:"{{$idCongreso}}",idPcTematica:idPcTematica,idTematica:idTematica})
				.done(function(data)
				{
					if(data.mensaje.indexOf("ERROR") != -1){
						alertify.error(data.mensaje);
					}
					else{
						alertify.success("Registro eliminado.");
						$(ref).closest('tr').remove();
						if (document.getElementById('tablaRevisoresAP').getElementsByTagName("tbody")[0].getElementsByTagName("tr").length==0)
							$( "#rev-msg" ).css("display","block");
						else
							$( "#rev-msg" ).css("display","none");
					}
				})
				.fail(function(data, status, jqXHR)
				{
					console.log("Server Returned " + status);
					alertify.error("Error: No se pudo aprobar el registro.");
				});
			}


			$('.aprobarFilaDB').unbind('click');
			$(".aprobarFilaDB").click(function()
			{
				var ref=this;
				var id=$(ref).closest('tr').children("td:nth-child(1)").attr('value');			
				var idTematica=$(ref).closest('tr').children("td:nth-child(3)").attr('value');
				var idPcTematica=$(ref).closest('tr').children("td:nth-child(4)").attr('value');			
				$.post("{{URL::action('UsuariosCongresosController@aprobarRegistroRevisor')}}", {idUsuario: id,idCongreso:"{{$idCongreso}}",idPcTematica:idPcTematica,idTematica:idTematica})
				.done(function(data)
				{
					if(data.mensaje.indexOf("ERROR") != -1){
						alertify.error(data.mensaje);
					}
					else{
						alertify.success("Registro aprobado");
						$(ref).closest('tr').remove();
						if (document.getElementById('tablaRevisoresAP').getElementsByTagName("tbody")[0].getElementsByTagName("tr").length==0)
							$( "#rev-msg" ).css("display","block");
						else
							$( "#rev-msg" ).css("display","none");
					}
				})
				.fail(function(data, status, jqXHR)
				{
					console.log("Server Returned " + status);
					alertify.error("Error: No se pudo eliminar el registro.");
				});
			});

			$('.eliminarFilaDB').unbind('click');
			$(".eliminarFilaDB").click(function()
			{
				var ref=this;
				alertify.confirm("<strong>IMPORTANTE: </strong>¿Esta seguro que desea rechazar este usuario como revisor del congreso?", function (e) {
					if (e) {
						var table=$(ref).closest('table').attr('id');
						var idTematica=$(ref).closest('tr').children("td:nth-child(3)").attr('value');
						var idPcTematica=$(ref).closest('tr').children("td:nth-child(4)").attr('value');
						var id=$(ref).closest('tr').children("td:nth-child(1)").attr('value');
						eliminarRegistro(id,"revisores",ref,idTematica,idPcTematica);
					}else {
						return;
					}
					e=null;
				});
			});
			return false;
		}

		configurarEventos();

		function cambiarClase(tabla)
		{

			var laTabla="#"+tabla+" [class= bg-success] td button";
			 console.log(laTabla);	
			$(laTabla).removeClass('eliminarFila').addClass('eliminarFilaDB');

			laTabla="#"+tabla+" [class= bg-success]";
			$(laTabla).removeClass('bg-success');

			configurarEventos();
		}

		$("#cancelarTodo").click(function(){
			window.location.href="{{URL::action('UsuariosCongresosController@establecerCongreso',array($idCongreso))}}";
		});


	});
</script>
@stop