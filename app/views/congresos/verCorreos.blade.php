@section('content')
<div class="col-sm-10 col-sm-offset-1" style="margin-top: 50px;">
	<div class="panel panel-primary" style="border-color: #205081; border-radius: 2px; box-shadow: 0 5px 10px gray;">
		<div class="panel-heading" style="border-radius: 0px; background: #205081; border-color: #205081;">
			<div class="panel-title">Informaci&oacute;n del mensaje</div>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1">
					@foreach($correos as $mail)
					<form role="form" class="form-horizontal" name="formCorreo" >
						<fieldset>
							<div class="form-group">
								<label for="destinatario" class="col-sm-3 control-label">Para:</label>
								<div class="col-sm-9">
									<input type="text" maxlength="100" class="form-control" id="destinatario" name="destinatario" value="{{$mail['destinatarios']}}" />
								</div>
							</div>
							<div class="form-group">
								<label for="fecha" class="col-sm-3 control-label">Fecha:</label>
								<div class="col-sm-9">
									<input type="text" maxlength="100" class="form-control" id="fecha" name="fecha" value="{{$mail['fecha']}}" />
								</div>
							</div>
							<div class="form-group">
								<label for="asunto" class="col-sm-3 control-label">Asunto:</label>
								<div class="col-sm-9">
									<input type="text" maxlength="100" class="form-control" id="asunto" name="asunto" value="{{$mail['asunto']}}" />
								</div>
							</div>
							<div class="form-group">
								<label for="mensaje" class="col-sm-3 control-label">Mensaje:</label>
								<div class="col-sm-9">
									<textarea class="form-control" id="mensaje" name="mensaje" rows="10" >{{$mail['mensaje']}}</textarea>
								</div>
							</div>
						</fieldset>
					</form>
					@endforeach
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-sm-4">
					<button type="button" id="cerrar" class="btn btn-primary btn-default" onclick="close_window();return false;">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function close_window() {
		close();
	}

	$(document).ready(function()
	{
		$("#destinatario").prop('disabled', true);
		$("#fecha").prop('disabled', true);
		$("#asunto").prop('disabled', true);
		$("#mensaje").prop('disabled', true);
	});
</script>
@stop