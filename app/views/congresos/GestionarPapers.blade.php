@section('content')
<style>
	.table td{
		cursor:pointer;
	}
</style>
<div class="container">
	<div class="col-sm-10 col-sm-offset-1" style="margin-top: 50px;">
	<div class="panel panel-primary" style="border-color: #205081; border-radius: 2px; box-shadow: 0 5px 10px gray;">
		<div class="panel-heading" style="border-radius: 0px; background: #205081; border-color: #205081;">
			<div class="panel-title">Configuraci&oacute;n del Congreso: <strong> {{Congreso::Where('idCongreso','=',$idCongreso)->first()->nomCongreso}} </strong></div>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1">
					<form role="form" class="form-horizontal">
						<fieldset>
							<legend>Listado de art&iacute;culos enviados por los autores</legend>
							<div class="form-group">
								<div class="col-sm-12">
									<table class="table table-hover table-condensed" id="tablaPapers">
										<thead>
										<tr>
											<th>
												#
											</th>
											<th>
												T&iacute;tulo
											</th>
											<th>
												Categor&iacute;a
											</th>
											<th>
												Tem&aacute;tica
											</th>
											<th>
												Fecha
											</th>
											<th>
												Aceptaci&oacute;n
											</th>
											<th>
												Revisiones
											</th>
											<th>
												Accion
											</th>
										</tr>
										</thead>
										<tbody>
										@foreach($papers as $paper)
										<tr id="{{$paper->idPaper}}" class="clickRowPaper">
											<td value="{{$paper->idPaper}}">
												{{$paper->idPaper}}
											</td>
											<td>
												{{$paper->tituloPaper}}
											</td>
											<td value="{{$paper->idCategoria}}">
												{{$paper->nomCategoria}}
											</td>
											<td value="{{$paper->idTematica}}">
												{{$paper->nomTematica}}
											</td>			
											<td>
												{{date('d/m/Y', strtotime($paper->created_at))}}
											</td>
											<td style="text-align: center;">
												{{$paper->porcentaje}}%
											</td>
											<td style="text-align: center;">
												{{$paper->revisadas}}/{{$paper->revisiones}}
											</td>
											<td>
												<a href="{{ URL::action('ArchivoController@servirArchivo', array($idCongreso,$paper->rutaPaper)) }}" target="_blank" class="verArchivo btn btn-info" style="font-size:5px;" data-toggle='tooltip' data-placement='left' title='Ver Archivo Enviado'>
												<i class="glyphicon glyphicon-file" style="font-size:10px;"></i>
												</a>
											</td>
										</tr>
										@endforeach
										</tbody>
										</table>
									<button type="button" id="rechazarRestantes" class="btn btn-default btn-danger ">
										<i class="glyphicon glyphicon-remove"></i> Rechazarlos
									</button>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-sm-4">
					<button type="button" id="cancelarTodo" class="btn btn-default btn-default">Cancelar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	history.pushState({},"","");
	$(document).ready(function() {
		$('#tablaPapers').DataTable({
            "language": {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
							"sFirst":    "Primero",
							"sLast":     "Último",
							"sNext":     "Siguiente",
							"sPrevious": "Anterior"
							},
				"oAria": {
						"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
						"sSortDescending": ": Activar para ordenar la columna de manera descendente"
						}
            }
		});

		$(".clickRowPaper").unbind();
		$("#tablaPapers tbody" ).on('click', 'tr.clickRowPaper',  function()
		{
			var id= $(this).children().eq(0).html();
			
            window.location.replace("DetallePaper/" + id);    
      	});

		$("#rechazarRestantes").click(function(){
			alertify.confirm("¿Está seguro que desea rechazar los artículos restantes?", function (e) 
			{
				if (e) {
					var idFP= new Array();
					var rows = document.getElementById("tablaPapers").rows;
					for(var i = 1, ceiling = rows.length; i < ceiling; i++) {
						idFP.push(rows[i].cells[0].getAttribute("value"));
					}
					$.post("{{URL::action('UsuariosCongresosController@rechazarTodasFP')}}",
					 {
						arrayRegistros: JSON.stringify(idFP),
						tabla:"paper",
						accion:"RECHAZADA"

					})
					.done(function(data)
					{
						if(!data.error){
							alertify.success("Se rechazaron "+idFP.length+" papers ");
							$('#tablaPapers').dataTable().fnClearTable();
						}
						else{
							alertify.error("Error: "+data.mensaje);
						}
					})
					.fail(function(data, status, jqXHR)
					{
						alertify.error("Error: "+data.mensaje);
					});
				}
			});
		});

		$("#cancelarTodo").click(function(){
			window.location.href="{{URL::action('UsuariosCongresosController@establecerCongreso',array($idCongreso))}}";
		});


	});
</script>
@stop