@section('content')
<div class="container">
	<div class="col-sm-10 col-sm-offset-1" style="margin-top: 50px;">
	<div class="panel panel-primary" style="border-color: #205081; border-radius: 2px; box-shadow: 0 5px 10px gray;">
		<div class="panel-heading" style="border-radius: 0px; background: #205081; border-color: #205081;">
			<div class="panel-title">Configuraci&oacute;n del Congreso: <strong>{{Congreso::Where('idCongreso','=',$idCongreso)->first()->nomCongreso}} </strong></div>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1">
					<form role="form" class="form-horizontal">
						<fieldset>
							<legend>Gesti&oacute;n de Revisores para la tem&aacute;tica:<strong> {{Tematica::Where('idTematica','=',$tematica[0]->idTematica)->first()->nomTematica}}</strong></legend>
							<div class="form-group">
								<div class="alert alert-info fade in">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									<strong>Nota</strong> Las invitaciones deben ser aprobadas por el Administrador del congreso antes que estas sean enviadas a los usuarios, se mostraran estados de Enviada/Rechazada/Pendiente en un flujo normal y el estado Solicitado cuando un usuario desee formar parte del equipo de revisores.
								</div>	
								<div class="col-sm-12">
									<table class="table table-hover table-condensed" id="tablaRevisores">
										<thead>
										<tr>
											<th>
												Usuario
											</th>
											<th>
												Estado
											</th>
											<th>
												Acci&oacute;n
											</th>
										</tr>
										</thead>
										<tbody>
										@foreach($revisores as $revisor)
										<tr id="{{$revisor->idPcRevisor}}">
											<td value="{{$revisor->idUsuario}}">
												{{Usuario::Find($revisor->idUsuario)->nombreUsuario. ' ' .Usuario::Find($revisor->idUsuario)->apelUsuario . ' ('.Usuario::Find($revisor->idUsuario)->emailUsuario.')'}}
											</td>
											<td value="{{$revisor->nombreEstado}}">
												{{EstadoURC::Find($revisor->idEstadoURC)->nombreEstado}}
											</td>
											<td>
												<button type="button" class="eliminarFilaDB btn btn-danger fileinput-remove fileinput-remove-button" style="font-size:5px;">
												<i class="glyphicon glyphicon-remove" style="font-size:10px;"></i>
												</button>
											</td>
										</tr>
										@endforeach
										</tbody>
										</table>
										<div id="rev-msg" class="alert alert-warning" style="display:{{$revisores==null?'block':'none'}}">
											<strong>NOTA: </strong>No se han agregado revisores a la tem&aacute;tica.
										</div>
										<button type="button" id="nuevo-revisor" class="btn btn-success fileinput-remove fileinput-remove-button" style="font-size:5px;">
										<i class="glyphicon glyphicon-plus" style="font-size:10px;"></i>
										</button>
								</div>							
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-sm-4">
					<button type="button" id="guardarTodo" class="btn btn-primary btn-default">Enviar Petici&oacute;n</button>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Ventana Modal para nuevo revisor-->
<div class="modal fade" id="nuevo-revisor-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" >Agregar Revisor a la tem&aacute;tica</h4>
			</div>
			<div class="modal-body">
				<div class="container" style="width:450px">
					<div class="row clearfix">
						<div class="col-md-4 column">
							<span style="font-weight:bold">Temática &nbsp; </span>
						</div>
						<div class="col-md-8 column" > 
							<input type="text" class="form-control" name="tematica" id="cajaTematica" value="{{Tematica::Where('idTematica','=',$tematica[0]->idTematica)->first()->nomTematica}}" disabled></input>
						</div>
					</div>
					<br/>
					<div class="row clearfix">
						<div class="col-md-4 column">
							<span style="font-weight:bold">Usuario &nbsp; </span>
						</div>
						<div class="col-md-8 column" id="divRevisores"> 
							<select data-placeholder="Seleccione una tematica" class="chosen" id="listaRevisores">

							</select>
						</div>
					</div>
					<br/>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="button" class="btn btn-primary" id="agregarRevisorbtn">Agregar</button>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	$(document).ready(function() {

		$("#listaRevisores").chosen();

		function buscarID(id,array)
		{
			for (var i=0; i<array.length; i++){
				if(array[i]==id) return true;
			}
			return false;
		}

		function configurarEventos()
		{
			//event.preventDefault();
			$('.eliminarFila').unbind('click');
			$(".eliminarFila").click(function()
			{
				$(this).closest('tr').remove();

				if (document.getElementById('tablaRevisores').getElementsByTagName("tbody")[0].getElementsByTagName("tr").length==0)
					$( "#rev-msg" ).css("display","block");
				else
					$( "#rev-msg" ).css("display","none");
	
			});

			function eliminarDeArray(array,from, to) {
				var rest = array.slice((to || from) + 1 || array.length);
				array.length = from < 0 ? array.length + from : from;
				return array.push.apply(array, rest);
			};

			function eliminarRegistro(id,tabla,referencia,idUsr)
			{
				var ref=referencia;
				$.post("{{URL::action('UsuariosCongresosController@eliminarRegistro')}}", {idRegistro: id,nomTabla:tabla,idCongreso:"{{$idCongreso}}",idPcTematica:"{{$tematica[0]->idPcTematica}}",idTematica:"{{$tematica[0]->idTematica}}"})
				.done(function(data)
				{
					if(data.mensaje.indexOf("ERROR") != -1){
						alertify.error(data.mensaje);
					}
					else{
						alertify.success("Registro eliminado.");
						$(ref).closest('tr').remove();
						if (document.getElementById('tablaRevisores').getElementsByTagName("tbody")[0].getElementsByTagName("tr").length==0)
							$( "#rev-msg" ).css("display","block");
						else
							$( "#rev-msg" ).css("display","none");
					}
				})
				.fail(function(data, status, jqXHR)
				{
					console.log("Server Returned " + status);
					alertify.error("Error: No se pudo eliminar el registro.");
				});
			}
	
			$('.eliminarFilaDB').unbind('click');
			$(".eliminarFilaDB").click(function()
			{
				var ref=this;
				alertify.confirm("¿Esta seguro que desea eliminar el registro de manera permanente?", function (e) {
					if (e) {
						var table=$(ref).closest('table').attr('id');
						var idUsr=$(ref).closest('tr').children("td:nth-child(1)").attr('value');
						var id=$(ref).closest('tr').children("td:nth-child(1)").attr('value');
						eliminarRegistro(id,"revisores",ref,idUsr);
					}else {
						return;
					}
					e=null;
				});
			});
			return false;
		}

		configurarEventos();

		function cambiarClase(tabla)
		{

			var laTabla="#"+tabla+" [class= bg-success] td button";
			 console.log(laTabla);	
			$(laTabla).removeClass('eliminarFila').addClass('eliminarFilaDB');

			laTabla="#"+tabla+" [class= bg-success]";
			$(laTabla).removeClass('bg-success');

			configurarEventos();
		}

		$("#nuevo-revisor").click(function(){
			var usuariosEnLista = new Array();
			var rows = document.getElementById("tablaRevisores").rows;
			for(var i = 1, ceiling = rows.length; i < ceiling; i++) {
				usuariosEnLista.push(rows[i].cells[0].getAttribute("value"));
			}

			$.post("{{URL::action('UsuariosCongresosController@usuariosDisponiblesRevisores')}}", {idCongreso: "{{$idCongreso}}",idPcRevisor:"{{$tematica[0]->idPcTematica}}",idTematica:"{{$tematica[0]->idTematica}}",idRepetidos: JSON.stringify(usuariosEnLista)})
			.done(function(data)
			{
				document.getElementById("listaRevisores").options.length = 0;
				if(data==""){
					alertify.error("No hay usuarios disponibles.");
					return;
				}
				$("#divRevisores").html(data);
				$("#listaRevisores").chosen();
				$('#listaRevisores').trigger("chosen:updated");

				$('#agregarRevisorbtn').unbind('click');
				$("#agregarRevisorbtn").click(function(){
					$( "#tablaRevisores tbody" ).append("<tr class='bg-success' data-toggle='tooltip' data-placement='left' title='Nuevo Elemento (Por Procesar)'>" +
						"<td value="+ $("#listaRevisores option:selected").val()+">" + $("#listaRevisores option:selected").text() +"</td>" +
						"<td value='POR'> POR APROBAR </td>" +
						"<td>" + '<button type="button" class="eliminarFila btn btn-danger fileinput-remove fileinput-remove-button" style="font-size:5px;"> '+
									'<i class="glyphicon glyphicon-remove" style="font-size:10px;"></i></button>' +"</td>" +
					"</tr>" );

					configurarEventos();
					$('#nuevo-revisor-form').modal('hide');
					$( "#rev-msg" ).css("display","none");
					alertify.success("Nuevo revisor agregado.");
				});			

				$('#nuevo-revisor-form').modal('show');
			})
			.fail(function(data, status, jqXHR)
			{
				console.log("Server Returned " + status);
				alertify.error("Error: No se puede obtener el listado de usuarios.");
			});
		});

		$("#cancelarTodo").click(function(){
			window.location.href="{{URL::action('UsuariosCongresosController@establecerCongreso',array($idCongreso))}}";
		});

		function guardarDatosRevisores()
		{
			var idCongreso="{{$idCongreso}}";
			var idTematica="{{$tematica[0]->idTematica}}";
			var idPcTematica="{{$tematica[0]->idPcTematica}}";
			var usuariosEnTabla = new Array();
			var rows = document.getElementById("tablaRevisores").rows;

			for(var i = 1, ceiling = rows.length; i < ceiling; i++) {
				if(rows[i].getAttribute("class")=="bg-success")
					usuariosEnTabla.push(rows[i].cells[0].getAttribute("value"));
			}

			if (usuariosEnTabla.length==0){
				alertify.success("Sin cambios.");
				return;
			}

			$.post("{{URL::action('UsuariosCongresosController@actualizarRevisores')}}",
			 {idCongreso: "{{$idCongreso}}",
			idTematica: idTematica,
			idPcTematica:idPcTematica,
			listaUsuarios:JSON.stringify(usuariosEnTabla)
			})
			.done(function(data)
			{
				if(data.mensaje.indexOf("ERROR") != -1){
					alertify.error(data.mensaje);
				}
				else{
					cambiarClase("tablaRevisores");
					alertify.success("Datos almacenados exitosamente.");
				}
			})
			.fail(function(data, status, jqXHR)
			{
				alertify.error("Error en el servidor");
			});
		}


		$("#guardarTodo").click(function(){
			guardarDatosRevisores();
		});
	});
</script>
@stop