@section('content')



<body>
<div class="col-md-10">
	<h2>Ejemplo de cargar archivo</h2>
</dic>


</br></br>

<div id="accordion">
  <h3>Cargar archivo</h3>
  <div>
    	<div id="mulitplefileuploader">Cargar</div>

  </div>
  <h3>Descripcion</h3>
  <div>
    <p>
    Ejemplo de como se vera y funcionara la carga de archivos en el sistema CONUCA
    </p>
  </div>
</div>
<input id="id" type="hidden" value="1"/>


<table id="list47"></table>
<div id="plist47"></div>


<table id="list48"></table>
<div id="plist48"></div>

 </div>

</body>
 
 <script type="text/javascript">
 //arodriguez
	$(document).ready(function()
	{	/*
			ESTO SE DEBE DE METER EN EL SUCCESS AL GUARDAR LA INFORMACION YA QUE SE DEBE DE CREAR EL REGISTRO PRIMERO
			accion : in :insert | up: update
			idElem: nombre del input file, este se le debe de poner aqui para que el pluggin lo genere
			id: id del row de la tabla
			donde: tabla donde se insertara la ruta
			{
				pa:paper
				pre: presentacion
				dc:detalle congreso
				axf: autorcficha

			}
			tipoArchivo: fo :foto|ot:otro
		*/
		var accion = "in", id = "32", idElem = "myfile", donde = "pre", tipoArchivo = "ot";

		//SINGLE FILE
		$("#mulitplefileuploader").uploadFile({
				url: "upload?fileName="+idElem+"&ac="+accion+"&i="+id+"&wh="+donde+"&ty="+tipoArchivo,
				method: "POST",
				allowedTypes:"pdf,doc,docx,txt,rtf,xls,xlsx,odt,ods,ppt,pps,odp,img,jpg,gif,png,rar,zip,jpeg",
				fileName: idElem,
				multiple: true,
				onSuccess:function(files,data,xhr)
				{
					$("#status").html("<font color='green'>Upload is success</font>");
					
				},
				onError: function(files,status,errMsg)
				{		
					$("#status").html("<font color='red'>Upload is Failed</font>");
				}				

		});
		
		
		//var mydata = <?php echo $usuariosData;?>;

		/*jQuery("#list47").jqGrid({
			data: mydata,
			datatype: "local",
			height: 150,
			rowNum: 10,
			rowList: [10,20,30],
		   	colNames:['Inv No','Date', 'Client', 'Amount','Tax','Total','Notes'],
		   	colModel:[
		   		{name:'id',index:'id', width:60, sorttype:"int"},
		   		{name:'invdate',index:'invdate', width:90, sorttype:"date", formatter:"date"},
		   		{name:'name',index:'name', width:100},
		   		{name:'amount',index:'amount', width:80, align:"right",sorttype:"float", formatter:"number"},
		   		{name:'tax',index:'tax', width:80, align:"right",sorttype:"float"},		
		   		{name:'total',index:'total', width:80,align:"right",sorttype:"float"},		
		   		{name:'note',index:'note', width:150, sortable:false}		
		   	],
		   	pager: "#plist47",
		   	viewrecords: true,
		   	caption: "Manipulating Array Data"
		});
		jQuery("#list47").jqGrid('navGrid','#plist47',{edit:false,add:false,del:false});
		
		//usando AJAX

		$.ajax({
		    url: "getGrid",
		    type: "POST",
		    data: {
		        	ACTION: "enviarNotificacion"
		    	  },
		    dataType: "JSON",
		    success: function (data) {
		        console.log(data);
		        //Creando el grid
		        jQuery("#list48").jqGrid({
				data: data,
				datatype: "local",
				height: 150,
				rowNum: 10,
				rowList: [10,20,30],
			   	colNames:['Inv No','Date', 'Client', 'Amount','Tax','Total','Notes'],
			   	colModel:[
					   		{name:'id',index:'id', width:60, sorttype:"int"},
					   		{name:'invdate',index:'invdate', width:90, sorttype:"date", formatter:"date"},
					   		{name:'name',index:'name', width:100},
					   		{name:'amount',index:'amount', width:80, align:"right",sorttype:"float", formatter:"number"},
					   		{name:'tax',index:'tax', width:80, align:"right",sorttype:"float"},		
					   		{name:'total',index:'total', width:80,align:"right",sorttype:"float"},		
					   		{name:'note',index:'note', width:150, sortable:false}		
			   			 ],
			   	pager: "#plist48",
			   	viewrecords: true,
			   	caption: "Manipulating Array Data"
			});

		    }
		    
		});
*/

		$( "#accordion" ).accordion({
			collapsible: true
		});

		$("#tableHeaderFloat a.delete").click(function()
		{
			var gotToURL = this.getAttribute('href');
			alertify.confirm("Seguro quiere borrar este record?", function (e) {
				if (e) {
					//Antonio ajax destroy
					//En el controller, llega a: function edit($id)
					$.ajax({
						url: gotToURL,
						type: "GET",
						data: { fakeParam : "" },
						dataType: "JSON",
						success: function (data) {
							console.log(data);
							var output;
							output = data.mensaje;
							alertify.success(output);
							window.location.reload(false); // from the cache
						},
						error: function(xhr, textStatus, error){
							alertify.error('Error en la comunicacion con el servidor');
							console.log(xhr.statusText);
							console.log(textStatus);
							console.log(error);
						}

					});
				} else {
					return false;
				}
			});
			return false;
		});
	});
</script>

@stop