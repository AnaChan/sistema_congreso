@section('content')
<div class="col-sm-10 col-sm-offset-1" style="margin-top: 50px;">
	<div class="panel panel-primary" style="border-color: #205081; border-radius: 2px; box-shadow: 0 5px 10px gray;">
		<div class="panel-heading" style="border-radius: 0px; background: #205081; border-color: #205081;">
			<div class="panel-title">Solicitud de Inscripci&oacute;n de Congreso - CONUCA</div>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1">
					<form role="form" class="form-horizontal">
						<fieldset>
							<legend>Informaci&oacute;n del congreso</legend>
							<div class="form-group">
								<label for="nomCongreso" class="col-sm-3 control-label">Nombre del congreso*:</label>
								<div class="col-sm-9">
									<input type="text" maxlength="50" class="form-control" id="nomCongreso" name="nomCongreso" placeholder="Ingrese el nombre del congreso" />
								</div>
							</div>
							<div class="form-group">
								<label for="acronimo" class="col-sm-3 control-label">Acr&oacute;nimo*:</label>
								<div class="col-sm-9">
									<input type="text" maxlength="15" class="form-control" id="acronimo" name="acronimo" placeholder="Ingrese el acrónimo del congreso" />
								</div>
							</div>
							<div class="form-group">
								<label for="sitioWeb" class="col-sm-3 control-label">Sitio Web*:</label>
								<div class="col-sm-9">
									<input type="text" maxlength="200" class="form-control" id="sitioWeb" name="sitioWeb" placeholder="Ingrese la dirección del sitio web" />
								</div>
							</div>
							<div class="form-group">
								<label for="ciudad" class="col-sm-3 control-label">Ciudad*:</label>
								<div class="col-sm-9">
									<input type="text" maxlength="25" class="form-control" id="ciudad"  name="ciudad" placeholder="Ingrese el nombre de la ciudad donde se realizara el congreso" />
								</div>
							</div>
							<div class="form-group">
								<label for="pais" class="col-sm-3 control-label">Pa&iacute;s*:</label>
								<div class="col-sm-9">
									<input type="text" maxlength="25" class="form-control" id="pais" name="pais" placeholder="Ingrese el nombre de la pais donde se realizara el congreso" />
								</div>
							</div>
							<div class="form-group">
								<label for="pais" class="col-sm-3 control-label">Fecha de Inicio*:</label>
								<div class="col-sm-9">
									<div class='input-group date span2' id='dpIniControl' data-date-format="DD/MM/YYYY">
										<input type='text' class="form-control" id='dpIni' name="fechaInicio" />
										<span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="pais" class="col-sm-3 control-label">Fecha de Fin*:</label>
								<div class="col-sm-9">
									<div class='input-group date span2' id='dpFinControl' data-date-format="DD/MM/YYYY">
										<input type='text' class="form-control" id='dpFin' name="fechaFin" />
										<span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="email" class="col-sm-3 control-label">Email*:</label>
								<div class="col-sm-9">
									<input type="text" maxlength="100" class="form-control" id="email" name="email" placeholder="Ingrese el correo donde desea recibir las notificaciones" />
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-sm-4">
					<button type="button" id="cancelarTodo" class="btn btn-default btn-default">Regresar</button>
					<button type="button" id="btnEnviar" class="btn btn-primary btn-default">Enviar Solicitud</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function()
	{
		var today = new Date();

		$('#dpIniControl').datetimepicker({
			pickTime: false
		});
		$('#dpFinControl').datetimepicker({
			pickTime: false
		});

		var nowTemp = new Date();
		var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

		function formatDate(date1) {
			return date1.getFullYear() + '-' +
			(date1.getMonth() < 9 ? '0' : '') + (date1.getMonth()+1) + '-' +
			(date1.getDate() < 10 ? '0' : '') + date1.getDate()+' 00:00:00';
		}

		$("#cancelarTodo").click(function(){
			window.location.href="{{URL::action('HomeController@inicio')}}";
		});

		$("#btnEnviar").click(function()
		{

			var nombreCongreso = $("[name='nomCongreso']").val();
			var acronimo = $("[name=acronimo]").val();
			var sitioWeb = $("[name=sitioWeb]").val();
			var ciudad = $("[name=ciudad]").val();
			var pais = $("[name=pais]").val();
			var email = $("[name=email]").val();
			var fechaInicio = $("[name=fechaInicio]").val();
			var fechaFin =$("[name=fechaFin]").val();
			var fechaRegex = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
			var correoRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			var sitioRegex=  new RegExp('^(https?:\\/\\/)?'+ // protocolo
										'((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // dominio
										'((\\d{1,3}\\.){3}\\d{1,3}))'+ // ip en formato ipv4
										'(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // puerto y ruta
										'(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string (si tiene)
										'(\\#[-a-z\\d_]*)?$','i'); 
			var error = false;


			if(nombreCongreso.length < 1)
			{
				alertify.error("El nombre del congreso es obligatorio.");
				$("[name='nomCongreso']").parent().removeClass('has-error').addClass('has-error');
				errors = true;
			}
			
			else
			{
				$("[name='nomCongreso']").parent().removeClass('has-error');	
			}

			if(acronimo.length < 1){
				alertify.error("El acrónimo del congreso es obligatorio.");
				$("[name='acronimo']").parent().removeClass('has-error').addClass('has-error');
				error = true;
			}else{
				$("[name='acronimo']").parent().removeClass('has-error');
			}

			if(  !sitioRegex.test(sitioWeb))
			{
				alertify.error("El sitio web es inválido");
				$("[name='sitioWeb']").parent().removeClass('has-error').addClass('has-error');
				errors = true;
			}
			else
			{
				$("[name='sitioWeb']").parent().removeClass('has-error');
			}

			if(ciudad.length < 1){
				alertify.error("La ciudad es obligatoria.");
				$("[name='ciudad']").parent().removeClass('has-error').addClass('has-error');
				error = true;
			}else{
				$("[name='ciudad']").parent().removeClass('has-error');
			}
			
			if(pais.length < 1){
				alertify.error("El país es obligatorio.");
				$("[name='pais']").parent().removeClass('has-error').addClass('has-error');
				error = true;
			}else{
				$("[name='pais']").parent().removeClass('has-error');
			}
			
			if(!fechaRegex.test(fechaInicio))
			{
				alertify.error("No ha ingresado una fecha de inicio válida.");
				$("[name='fechaInicio']").parent().removeClass('has-error').addClass('has-error');
				error = true;
			}
			else
			{
				$("[name='fechaInicio']").parent().removeClass('has-error');
			}


			if(!fechaRegex.test(fechaFin))
			{
				alertify.error("No ha ingresado una fecha de fin válida.");
				$("[name='fechaFin']").parent().removeClass('has-error').addClass('has-error');
				error = true;
			}
			else
			{
				$("[name='fechaFin']").parent().removeClass('has-error');
			}

			f1=formatDate(new Date($("#dpIni").val().split("/").reverse().join("/")));
			f2=formatDate(new Date($("#dpFin").val().split("/").reverse().join("/")));
			if(f1 > f2)
			{
				alertify.error("La fecha de fin debe de ser mayor o igual a la fecha de inicio.");
				$("[name='fechaFin']").parent().removeClass('has-error').addClass('has-error');
				error = true;	
			}
			else
			{
				$("[name='fechaFin']").parent().removeClass('has-error');	
			}

			if (!correoRegex.test(email)){
				alertify.error("La dirección de correo electrónico es inválida.");
				$("[name='email']").parent().removeClass('has-error').addClass('has-error');
				error = true;
			}else{
				$("[name='email']").parent().removeClass('has-error');
			}

			if(!error)
			{
				fechaInicio=formatDate(new Date($("#dpIni").val().split("/").reverse().join("/")));
				fechaFin=formatDate(new Date($("#dpFin").val().split("/").reverse().join("/")));
				$.post("{{URL::action('CongresoController@guardarSolicitud')}}", {nombreCongreso: nombreCongreso, acronimo: acronimo, sitioWeb: sitioWeb, ciudad: ciudad,pais: pais, fechaInicio: fechaInicio, fechaFin: fechaFin,email: email})
				.done(function(data, status,jqXHR)
				{
					var btn=this;
					btn.innerHTML='Enviar Solicitud';
					btn.disabled=false;
					if(data.error)
					{
						alertify.error(data.mensaje);
					}
					else
					{
						alertify.success(data.mensaje);
						window.setTimeout(function()
						{
							window.location.replace("{{URL::action('HomeController@inicio')}}");
						},
						2000);
					}
				})
				.fail(function(data, status,jqXHR)
				{
					var btn=this;
					btn.innerHTML='Enviar Solicitud';
					btn.disabled=false;
					console.log("Server Returned " + status);
					alertify.error("Error de comunicación con el servidor.");
				});
			}
		});

});
</script>
@stop