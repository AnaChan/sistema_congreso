@section('content')
@if (Session::has('message'))
<br>
<div class="panel panel-danger fade in">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="padding:7px">×</button>
	<div class="panel-heading">
		<h3 class="panel-title">
			CONUCA - ERROR Fatal
		</h3>
	</div>
	<div class="panel-body">{{ Session::get('message') }}</div>
</div>
@endif
<div class="row">
	<div class="col-md-9">
		@if (strcmp((count(Auth::user())!=0?EstadoUsuario::Where('idEstadoUsuario','=',Auth::user()->idEstadoUsuario)->first()->valorEstado:'null'),'ADM')!=0)
			<h3>Listado de Congresos </h3>
		@else
			<h3>Listado de congresos aprobados</h3>
		@endif
	</div>
	<div class="col-md-3" style="padding-top: 20px;">
			@if (strcmp((count(Auth::user())!=0?EstadoUsuario::Where('idEstadoUsuario','=',Auth::user()->idEstadoUsuario)->first()->valorEstado:'null'),'ADM')!=0)
				<span>Mostrar: </span>
				<select class="chosen" id="tipoFiltroCongreso" {{Auth::check()?:'disabled',''}}>
					<option {{$idUsuario==0?'selected':''}} value="1">Congresos Abiertos</option>
					<option value="2">Inscrito como Autor</option>
					<option value="3">Inscrito como Revisor</option>
					<option value="4">Inscrito como Administrador</option>
					<option value="5">Inscrito como Miembro del Comit&eacute;</option>
					<option value="6">Sin inscribir (Abiertos)</option>
					<option value="7">Congresos cerrados</option>
					<option value="8">Mis congresos sin configurar</option>
					<option {{$idUsuario!=0?'selected':''}}  value="9">Todos mis congresos (DESC)</option>
					<option value="10">Todos mis congresos (ASC)</option>
				</select>
			@endif
	</div>
</div>
<div class="row" id="contenedor"   style="padding-top: 15px;text-align:left">
	@if(count($congresos)==0)
	<div class="col-sm-4 col-sm-offset-4">
		<h3><span class='label label-info'>No hay congresos para mostrar</span></h3>
	</div>
	@else
		@foreach($congresos as $congreso)
			@if (strcmp((count(Auth::user())!=0?EstadoUsuario::Where('idEstadoUsuario','=',Auth::user()->idEstadoUsuario)->first()->valorEstado:'null'),'ADM')!=0)
				<div class="col-sm-6 col-md-4 ">
					<div class="thumbnail" >
						<a idclick="{{$congreso->idCongreso}}" class="thumbnail congresolink"  height="63" width="324" style="cursor:pointer;margin-bottom: 0px;"><img src="{{URL::asset('banners')}}/{{$congreso->banner}}" style="cursor:pointer;height:63px; width:324px" alt="banners/convacio.png"/></a>
						<a idclick="{{$congreso->idCongreso}}" class="btn btn-xs btn-primary congresolink entrarBTN" 
						   style="cursor:pointer;margin-bottom: 20 px;
						   			border-radius: 2px;-webkit-transition: all .2s ease-in-out;
						   			-o-transition: all .2s ease-in-out;transition: all .2s ease-in-out;width:100%">
						   			Haga <strong>click</strong> para ingresar
						</a>
						<div class="caption">
							<h4 style="width:100%;text-overflow:ellipsis;overflow:hidden;white-space: nowrap;" data-toggle="tooltip" data-placement="left" title="{{$congreso->nomCongreso}} - {{$congreso->acronimoCongreso}}">{{$congreso->nomCongreso}}<small> {{$congreso->acronimoCongreso}}</small></h4>
							<h5 style="width:100%;text-overflow:ellipsis;overflow:hidden;white-space: nowrap;" data-toggle="tooltip" data-placement="left" title="{{$congreso->webCongreso}}"><a target="_blank" href="{{$congreso->webCongreso}}">{{$congreso->webCongreso}}</a></h5>
						</div>
					</div>
				</div>
			@else
				<div class="col-sm-6 col-md-4 ">
					<div class="thumbnail file-caption-disabled">
						<a idclick="{{$congreso->idCongreso}}" class="thumbnail congresolink"  height="63" width="324" style="margin-bottom: 0px;"><img src="{{URL::asset('banners')}}/{{$congreso->banner}}" style="height:63px; width:324px" alt="banners/convacio.png"/></a>
						<div class="caption">
							<h4 style="width:100%;text-overflow:ellipsis;overflow:hidden;white-space: nowrap;" data-toggle="tooltip" data-placement="left" title="{{$congreso->nomCongreso}} - {{$congreso->acronimoCongreso}}">{{$congreso->nomCongreso}}<small> {{$congreso->acronimoCongreso}}</small></h4>
							<h5 style="width:100%;text-overflow:ellipsis;overflow:hidden;white-space: nowrap;" data-toggle="tooltip" data-placement="left" title="{{$congreso->webCongreso}}"><a target="_blank" href="{{$congreso->webCongreso}}">{{$congreso->webCongreso}}</a></h5>
							<h5 style="width:100%;text-overflow:ellipsis;overflow:hidden;white-space: nowrap;" data-toggle="tooltip" data-placement="left" title="Creado por: {{Usuario::Where('idUsuario',$congreso->idCreador)->first()->nombreUsuario.' '.Usuario::Where('idUsuario',$congreso->idCreador)->first()->apelUsuario}}">Creado por:<small> {{Usuario::Where('idUsuario',$congreso->idCreador)->first()->nombreUsuario.' '.Usuario::Where('idUsuario',$congreso->idCreador)->first()->apelUsuario}}</small></h5>
							<hr>
							<div class="row">
							<div class="col-sm-6">
								<a idclick="{{$congreso->idCongreso}}"  class="btn btn-xs btn-danger eliminarbtn"><span class="glyphicon glyphicon-trash"></span> Eliminar</a>
								@if(strcmp($congreso->comentarioCongreso,"CREA_SL-ELI_RQST-ADM-AS4P")==0)
									<a class="btn btn-xs btn-warning popover-dismiss" title="Mensaje" data-toggle="popover" title="" data-content="El creador ha solicitado la eliminación de este congreso."><span class="glyphicon glyphicon-envelope"></span> VER</a>
								@endif
							</div>
							<div class="col-sm-6">
								<span style="font-weight:bold"><small>Estado </small></span>
								<input idclick="{{$congreso->idCongreso}}" type="checkbox" name="check-estado" {{$congreso->valorEstado=='ACT'?'checked':''}}>
						  	</div>
						  </div>
						</div>

					</div>
				</div>
			@endif
	@endforeach
	@endif
</div>
<script type="x-tmpl-mustache" id="templateCongreso">
	<%#congresos%>
	<div class="col-sm-6 col-md-4">
		<div class="thumbnail">
			<a idclick="<%idCongreso%>" class="thumbnail congresolink" height="63" width="324" style="margin-bottom: 0px;">
				<img style="cursor:pointer;height:63px; width:324px" height="63" width="324" src='{{URL::asset("banners")}}/<%banner%>' />
			</a>
			<a idclick="<%idCongreso%>" class="btn btn-xs btn-primary congresolink entrarBTN" 
				style="cursor:pointer;margin-bottom: 20 px;
						border-radius: 2px;-webkit-transition: all .2s ease-in-out;
						-o-transition: all .2s ease-in-out;transition: all .2s ease-in-out;width:100%">
						Haga <strong>click</strong> para ingresar
			</a>
			<div class="caption">
				<h4 style="width:100%;text-overflow:ellipsis;overflow:hidden;white-space: nowrap;" data-toggle="tooltip" data-placement="left" title="<%nomCongreso%> - <%acronimoCongreso%>">
				<%nomCongreso%>
					<small><%acronimoCongreso%></small>
				</h4>
				<h5 style="width:100%;text-overflow:ellipsis;overflow:hidden;white-space: nowrap;" data-toggle="tooltip" data-placement="left" title="<%webCongreso%>">
					<a href="<%webCongreso%>"><%webCongreso%></a>
				</h5>
			</div>
		</div>
	</div>
	<%/congresos%>
</script>
<script type="x-tmpl-mustache" id="templateSinCongreso">
	<div class='col-sm-4 col-sm-offset-4'>
		<h3>
			<span class='label label-info'>No hay congresos para mostrar</span>
		</h3>
	</div>
</script>
<script type="text/javascript">
	$("#tipoFiltroCongreso").chosen({disable_search_threshold: 10});
	$(".popover-dismiss").popover({
    	container: 'body'
	});
	$("[name='check-estado']").bootstrapSwitch({size:'small',onColor:'success',offColor:'info',onText:'Activo',offText:'Inactivo'});

	$('input[name="check-estado"]').on('switchChange.bootstrapSwitch',
		function(event, state) {
			var estado="";
			if(state) estado="true";
			else estado="false";

			var idCongreso=$(this).attr("idclick");
			$.post(
				"{{URL::action('CongresoController@cambiarEstadoCongreso')}}",
				{idCongreso: idCongreso,estado:estado}
			)
			.done(function(data)
			{
				if(data.error)
					 alertify.error(data.mensaje);
				else
				{
					alertify.success(data.mensaje);
					window.location.replace("{{URL::action('HomeController@inicio')}}");
				}
			})
			.fail(function(data, status, jqXHR)
			{
				alertify.error("Error de comunicación con el servidor.");
			});
		}
	);
	function accionEliminarBTN(element)
	{
		var el=element;
		var idCongreso = $(el).attr("idclick");
		alertify.confirm("¿Está seguro que desea eliminar este congreso?", function (e) 
		{
			if (e) {
				$.post(
					"{{URL::action('CongresoController@eliminarCongreso')}}",
					{idCongreso: idCongreso}
				)
				.done(function(data)
				{
					if(data.error)
						 alertify.error(data.mensaje);
					else
					{
						alertify.success(data.mensaje);
						window.location.replace("{{URL::action('HomeController@inicio')}}");
					}
				})
				.fail(function(data, status, jqXHR)
				{
					alertify.error("Error de comunicación con el servidor.");
				});
			}
		});
	}

	if("{{strcmp((count(Auth::user())!=0?EstadoUsuario::Where('idEstadoUsuario','=',Auth::user()->idEstadoUsuario)->first()->valorEstado:'null'),'ADM')}}"!=0)
	{
		function accionBTN(e)
		{
			var el=e;
			var idCongreso = $(el).attr("idclick");
			$("body").addClass('modal-loading');
			$.post(
				"{{URL::action('HomeController@verificarExistente')}}",
				{idCongreso: idCongreso}
			)
			.done(function(data)
			{
				if(data.error) alertify.error(data.mensaje);
				else
				{
					if(data.existe){
						alertify.success(data.mensaje);
						window.location.replace(data.urlCongreso);
					}
					else{
						alertify.confirm("Usted no se encuentra inscrito actualmente en este congreso.<br>¿Desea inscribirse?", function (e) 
						{
							if (e) {
								$.post(
									"{{URL::action('HomeController@inscribirUsuario')}}",
									{idCongreso: idCongreso}
								)
								.done(function(data)
								{
									if(data.error) alertify.error(data.mensaje);
									else
									{
										alertify.success(data.mensaje);
										window.location.replace(data.urlCongreso);
									}
								})
								.fail(function(data, status, jqXHR)
								{
									console.log("Server Returned " + status);
									alertify.error("Error de comunicación con el servidor.");
								});
							}
						});
					}
				}
			})
			.fail(function(data, status, jqXHR)
			{
				console.log("Server Returned " + status);
				alertify.error("Error de comunicación con el servidor.");
			});
		}

		$(document).ready(function()
		{
			$("#tipoFiltroCongreso").change(function()
			{
				var filtroN = $("#tipoFiltroCongreso").val();
				$.post(
					"{{URL::action('HomeController@filtrar')}}",
					{filtro: filtroN}
				)
				.done(function(data)
				{
					if(data.congresos.length > 0)
					{
						$("#contenedor").html(Mustache.render($("#templateCongreso").html(), data));
					}
					else
					{
						$("#contenedor").html($("#templateSinCongreso").html());
					}
					$(".congresolink").click(function(event){
						accionBTN(this);
					});

				})
				.fail(function(data, status, jqXHR)
				{
					console.log("Server Returned " + status);
					alertify.error("Error de comunicación con el servidor.");
				});

			});

			$(".congresolink").click(function(event){
				accionBTN(this);
			});
		});
	}
	else{
		$(".eliminarbtn").click(function(event){
			accionEliminarBTN(this);
		});
		$('#tipoFiltroCongreso').prop('disabled', true).trigger("chosen:updated");
	}
</script>
@stop