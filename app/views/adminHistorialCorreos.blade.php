@section('content')

<div class="">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<h3 class="text-center">
				Historial de Correo
			</h3>
		</div>
	</div>
	
	<a href="{{URL::action('CorreosController@index')}}">Enviar Correo</a>
	<br><br>
	<div class="row clearfix">
		<div class="col-md-12 column">
			<table class="table display" id="tableHeaderFloat" name="tableHeaderFloat">
				<thead>
					<tr>
						<th style="width: 5%;" >#</th>
						<th style="width: 20%;" >Destinatario</th>
						<th style="width: 15%;" >Fecha</th>
						<th style="width: 15%;" >Asunto</th>
						<th style="width: 35%;" >Mensaje</th>
						<th style="width: 10%;" ></th>
					</tr>
				</thead>
				<tbody>
					
					@foreach($correos as $mail)
					<tr>

						<td>
							{{$mail['orden']}}
						</td>
						<td>
							@if(strlen($mail['destinatarios']) > 40)
								{{substr($mail['destinatarios'],0,40)}}...
							@else
								{{$mail['destinatarios']}}
							@endif
						</td>
						<td>
							@if(strlen($mail['fecha']) > 10)
								{{substr($mail['fecha'],0,10)}}...
							@else
								{{$mail['fecha']}}
							@endif
						</td>
						<td>
							@if(strlen($mail['asunto']) > 40)
								{{substr($mail['asunto'],0,40)}}...
							@else
								{{$mail['asunto']}}
							@endif
						</td>
						<td>
							@if(strlen($mail['mensaje']) > 60)
								{{substr($mail['mensaje'],0,60)}}...
							@else
								{{$mail['mensaje']}}
							@endif
						</td>
						<td><a title="Ver" href="{{URL::action('CorreosController@adminVerMail',array($mail['orden']) )}}" target="_blank" >Ver</a></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function()
	{
		$('#tableHeaderFloat').dataTable( {
	        "language": {
	        	"lengthMenu"	: "Mostrar _MENU_ mensajes por p&aacute;gina",
	            "zeroRecords"	: "No hay coincidencias",
	            "info"			: "Mostrando p&aacute;gina _PAGE_ de _PAGES_",
	            "infoEmpty"		: "No hay records disponibles",
	            "infoFiltered"	: "(filtrado de un total de _MAX_ registros)",
	            "sSearch"		: "Buscar:",
	            "sLoadingRecords": "Cargando...",
				"oPaginate": {
							"sFirst":    "Primero",
							"sLast":     "&Uacute;ltimo",
							"sNext":     "Siguiente",
							"sPrevious": "Anterior"
							},
	        }
    	} );
	});
</script>

@stop