@section('content')
<div class="col-sm-10 col-sm-offset-1" style="margin-top: 50px;">
	@if (Session::has('accessError'))
	<div class="has-error center" style="text-align: center;"><label class="control-label">{{trans('miscellaneous.accessError')}}</label></div>
	@endif
	<div class="panel panel-primary" style="border-color: #205081; border-radius: 2px; box-shadow: 0 5px 10px gray;">
		<div class="panel-heading" style="border-radius: 0px; background: #205081; border-color: #205081;">
			<div class="panel-title">{{trans('miscellaneous.inisesion')}} - CONUCA</div>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-sm-6">
					<div class="panel-body">
						<div class="row">
							<div class="col-sm-12">
								<span style="font-size: 147%;">{{trans('miscellaneous.credenciales')}}:</span>
							</div>
							<div class="col-sm-12" style="margin-top:25px;">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
									<input type="text" class="form-control" name="correo" placeholder="{{trans('miscellaneous.correo')}}">
								</div>
							</div>
							<div class="col-sm-12" style="margin-top:25px;">
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
									<input type="password" class="form-control" name="password" placeholder="{{trans('miscellaneous.password')}}">
								</div>
							</div>
							<div class="col-sm-12" style="margin-top:25px;"	>
								<div class="input-group">
									<div class="checkbox">
										<label>
											<input id="login-remember" type="checkbox" name="recordar"> {{trans('miscellaneous.recordardatos')}}
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>   
				<div class="col-sm-6">
					<div class="panel-body">
						<div align="justify" >
							<p>¿Eres nuevo en CONUCA?</p>
							<p><a href="{{{ URL::action('RegistroController@index')}}}" >Registrate en este enlace.</a></p>
							<p>¿Has olvidado tus credenciales?</p>
							<p><a href="{{{ URL::action('RemindersController@getRemind')}}}">Haz click aqu&iacute;.</a></p>
							<p>
								En CONUCA se encontrar&aacute;n todas las pautas necesarias que se deben llevar a cabo para la creaci&oacute;n de un nuevo evento, como as&iacute; tambi&eacute;n su administraci&oacute;n y utilizaci&oacute;n.
							</p>
							<p>
								El Sistema de Administraci&oacute;n de Congresos es un sistema que se utiliza para la administraci&oacute;n de los art&iacute;culos recibidos en un congreso y para la posterior evaluaci&oacute;n de los mismos.
							</p>
						</div>
					</div>
				</div> 
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-sm-4">
					<button class="btn btn-primary" id="login">{{trans('miscellaneous.ingresar')}}</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function()
	{
		$("#login").click(function()
		{

			var correo = $("[name='correo']").val();
			var correoRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			var password = $("[name='password']").val();
			var recordar = $("[name='recordar']").is(":checked");
			var errors = false;

			if(!correoRegex.test(correo))
			{
				alertify.error("No ha ingresado un correo válido.");
				$("[name='correo']").parent().removeClass('has-error').addClass('has-error');
				errors = true;
			}
			else
			{
				$("[name='correo']").parent().removeClass('has-error');
			}

			if(password.length < 7)
			{
				alertify.error("La contraseña debe de tener mas de 6 caracteres.");
				$("[name='password']").parent().removeClass('has-error').addClass('has-error');
				errors = true;
			}
			else
			{
				$("[name='password']").parent().removeClass('has-error');
			}

			if(!errors)
			{
				$.post("login", {correo: correo, password: password, recordar: recordar})
				.done(function(data, status, jqXHR)
				{
					if(data.error)
					{
						alertify.error(data.mensaje);
						$("[name='correo']").parent().removeClass('has-error').addClass('has-error');
						$("[name='password']").parent().removeClass('has-error').addClass('has-error');
					}
					else
					{
						window.location.replace('{{URL::action('HomeController@inicio')}}');
					}
				})
				.fail(function(data, status, jqXHR)
				{
					console.log("Server Returned " + status);
					alertify.error("Error de comunicación con el servidor.");
				});
			}
		});

		$(document).keypress(function(event)
		{
			if(event.which == 13) {
   				$("#login").click();
   			}
		});
	});
</script>

@stop