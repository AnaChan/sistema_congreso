@section('content')
<div class="col-sm-10 col-sm-offset-1" style="margin-top: 50px;">
	<div class="panel panel-primary" style="border-color: #205081; border-radius: 2px; box-shadow: 0 5px 10px gray;">
		<div class="panel-heading" style="border-radius: 0px; background: #205081; border-color: #205081;">
			<div class="panel-title">Reestablecer Contraseña - CONUCA</div>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1">
					<form role="form" class="form-horizontal">
						<fieldset>
							<legend>Informaci&oacute;n de usuario</legend>
							<div class="form-group">
								<label for="correo" class="col-sm-3 control-label">Correo Electr&oacute;nico*:</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="correo" placeholder="Ingrese su correo" />
								</div>
							</div>

							<legend style="padding-top: 10px">Eres un robot?</legend>
							<label class="col-sm-3 control-label">reCAPTCHA*:</label>
							<div class="col-sm-9">
								{{Form::captcha();}}
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-sm-4">
					<button class="btn btn-default"  onClick="location.href='{{{ URL::action('LoginController@index')}}}'" >Regresar al login</button>
					<button class="btn btn-primary" id="crear">Reestablecer</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){

		$('#crear').click(function() 
		{
			var correo = $('#correo').val();
			var correoRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			var recaptcha_response_field = $('#recaptcha_response_field').val();
			var recaptcha_challenge_field = $('#recaptcha_challenge_field').val();
			var errors = false;

			if(!correoRegex.test(correo))
			{
				alertify.error("No ha ingresado un correo válido.");
				$("#correo").parent().parent().removeClass('has-error').addClass('has-error');
				errors = true;
			}
			else
			{
				$("#correo").parent().parent().removeClass('has-error');
			}

			if(!errors)
			{
				$.post("{{{ URL::action('RemindersController@postRemind') }}}", {correo: correo, recaptcha_response_field: recaptcha_response_field, recaptcha_challenge_field: recaptcha_challenge_field}) 
				.done(function(data, status, jqXHR)
				{
					if(data.error)
					{
						alertify.error(data.mensaje);
						$('#recaptcha_reload').click();
						$('#recaptcha_response_field').val('');
					}
					else
					{
						alertify.success(data.mensaje);
						window.setTimeout(function()
						{
							window.location.replace("{{{ URL::action('LoginController@index')}}}");
						},
						2000);
					}
				})
				.fail(function(data, status, jqXHR)
				{
					console.log("Server Returned " + status);
					alertify.error("Error de comunicación con el servidor.");
					$('#recaptcha_reload').click();
					$('#recaptcha_response_field').val('');
				});
			}
		});

		$(document).keypress(function(event)
		{
			if(event.which == 13) {
   				$("#crear").click();
   			}
		});
	});
</script>
@stop