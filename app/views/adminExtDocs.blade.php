@section('content')
	 
	 <?php
	 $tituloPantalla	= 'Extensiones para Documentos';
	 $resourceLink		= 'adminextdocs';
	 $agregarLinkLabel	= 'Agregar Extensi&oacute;n';
	 
	 $leftDivACME1		= 'Nombre Extensi&oacute;n: &nbsp'; 
	 $leftDivACME2		= 'Extensi&oacute;n: &nbsp'; 

	 $HTMLControlNane1	= 'nombreExt';
	 $HTMLControlNane2	= 'extTipo';

	 ?>
	 
	<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<h3 class="text-center">
				{{$tituloPantalla}}
			</h3>
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-md-6 column">
			<a id="agregarCat"  href="{{URL::action('ExtDocsController@create')}}">{{$agregarLinkLabel}}</a>
			<br/><br/>
			<table id="tbl_categorias" name="tbl_categorias" class="table display">
				<thead>
					<tr>
						<th>Nombre Extensi&oacute;n</th>
						<th>Extensi&oacute;n</th>
						<th></th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					@foreach ($extensiones as $objeto)
						<tr class="clickRow">
							<td id="control1_{{$objeto->idExtensionDocumento}}" visible="false">
								{{$objeto->nomExtension}}
							</td>
							<td id="control2_{{$objeto->idExtensionDocumento}}" visible="false">
								{{$objeto->tipoExtension}}
							</td>
							<td><a id="{{$objeto->idExtensionDocumento}}" class="modificar" title="Modificar" href="{{URL::action('ExtDocsController@edit',array($objeto->idExtensionDocumento) )}}">Modificar</a></td>
							<td><a id="{{$objeto->idExtensionDocumento}}" class="eliminar" title="Eliminar" href="{{URL::action('ExtDocsController@edit',array($objeto->idExtensionDocumento) )}}">Eliminar</a></td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		
		<br/>
		
		<div class="col-md-5 column" style="box-shadow: 0 5px 8px gray;">
		
		<h3 id="lblCategoria" name="lblCategoria"></h3>
		<input type='hidden' name='gotourl' id='gotourl' value=""></input>
		<br/>
		
		<div class="row clearfix">
			<div class="col-md-4 column">
				<label class="col-sm-3 control-label">{{$leftDivACME1}}</label>
			</div>
			<div class="col-md-6 column">
				<input name="{{$HTMLControlNane1}}" id="{{$HTMLControlNane1}}" disabled='' class="form-control" type="text" ></input>
			</div>
			<div class="col-md-4 column"></div>
		</div>
		<br/>

		<div class="row clearfix">
			<div class="col-md-4 column">
				<label class="col-sm-3 control-label">{{$leftDivACME2}}</label>
			</div>
			<div class="col-md-6 column">
				<input name="{{$HTMLControlNane2}}" id="{{$HTMLControlNane2}}" disabled='' class="form-control" type="text" ></input>
			</div>
			<div class="col-md-4 column"></div>
		</div>
		<br/>
	
		<div class="row clearfix">
			<div class="col-md-6 column" style="text-aling:center">
				 <button type="button" id="performAction"  disabled=''  class="btn btn-primary btn-default">Guardar</button>
			</div>
		</div>
		<br/>
		</div>
		
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function()
	{
		$('#tbl_categorias').dataTable( {
	        "language": {
	        	"lengthMenu"	: "Mostrar _MENU_ por p&aacute;gina",
	            "zeroRecords"	: "No hay coincidencias",
	            "info"			: "Mostrando p&aacute;gina _PAGE_ de _PAGES_",
	            "infoEmpty"		: "No hay records disponibles",
	            "infoFiltered"	: "(filtrado de un total de _MAX_ registros)",
	            "sSearch"		: "Buscar:",
	            "sLoadingRecords": "Cargando...",
				"oPaginate": {
							"sFirst":    "Primero",
							"sLast":     "&Uacute;ltimo",
							"sNext":     "Siguiente",
							"sPrevious": "Anterior"
							},
	        }
    	} );

		$("#tbl_categorias tbody" ).on('click', 'a.eliminar',  function()
		{
			var gotToURL = this.getAttribute('href');
			alertify.confirm("Seguro quiere borrar este record?", function (e) {
				if (e) {
					$.post(gotToURL, { accion : "delete" })
					.done(function(data, status,jqXHR)
					{
						if(data.error)
						{
							alertify.error(data.mensaje);
						}
						else
						{
							alertify.success(data.mensaje);
							window.setTimeout(function()
							{
								window.location.replace("{{ URL::action('ExtDocsController@index')}}");
							},
							900);
						}
					})
					.fail(function(data, status,jqXHR)
					{
						console.log("Server Returned " + status);
						alertify.error("Error de comunicación con el servidor, contacte a su adminstrador");
					});
				} else {return false;}
			
			});
			return false;
		});
		
		$("#tbl_categorias tbody" ).on('click', 'a.modificar',  function()
		{
			document.getElementById('lblCategoria').innerHTML = 'Modificar';
			
			var catID = this.getAttribute('id');
			lcName = document.getElementById('control1_'+catID).innerHTML;
			lcName = lcName.replace(/\n|\r/g, "").trim();
		
			lcName2 = document.getElementById('control2_'+catID).innerHTML;
			lcName2 = lcName2.replace(/\n|\r/g, "").trim();

			document.getElementById('{{$HTMLControlNane1}}').value = lcName;
			document.getElementById('{{$HTMLControlNane2}}').value = lcName2;

			document.getElementById('gotourl').value = this.getAttribute('href');
			document.getElementById('performAction').removeAttribute('disabled');
			
			document.getElementById('{{$HTMLControlNane1}}').removeAttribute('disabled');
			document.getElementById('{{$HTMLControlNane2}}').removeAttribute('disabled');

			document.getElementById('{{$HTMLControlNane1}}').focus();
			return false;
		});
		
		$("#agregarCat").click(function()
		{
			document.getElementById('lblCategoria').innerHTML = 'Agregar';
			document.getElementById('gotourl').value = this.getAttribute('href');
			document.getElementById('performAction').removeAttribute('disabled');
			document.getElementById('{{$HTMLControlNane1}}').removeAttribute('disabled');
			document.getElementById('{{$HTMLControlNane2}}').removeAttribute('disabled');

			document.getElementById('{{$HTMLControlNane1}}').value = '' ;
			document.getElementById('{{$HTMLControlNane2}}').value = '' ;
			document.getElementById('{{$HTMLControlNane1}}').focus();
			return false;
		});
		
		
		$("#performAction").click(function()
		{
			var controlVal	= document.getElementById('{{$HTMLControlNane1}}').value.trim();
			var controlVal2	= document.getElementById('{{$HTMLControlNane2}}').value.trim();

			if (checkFieldsEmpty('{{$HTMLControlNane1}}','Ingrese un valor para Nombre Extensi&oacute;n'))
				return;
			if (checkFieldsEmpty('{{$HTMLControlNane2}}','Ingrese un valor para Extensi&oacute;n'))
				return;
				
			var gotToURL	= document.getElementById('gotourl').value;
			$.post(gotToURL, { accion : "edit", nomExtension : controlVal, extension : controlVal2 })
				.done(function(data, status,jqXHR)
				{
					if(data.error)
					{
						alertify.error(data.mensaje);
					}
					else
					{
						alertify.success(data.mensaje);
						window.setTimeout(function()
						{
							window.location.replace("{{ URL::action('ExtDocsController@index')}}");
						},
						900);
					}
				})
				.fail(function(data, status,jqXHR)
				{
					console.log("Server Returned " + status);
					alertify.error("Error de comunicación con el servidor, contacte a su adminstrador");
				});
		});
		
		function checkFieldsEmpty(idControl,mensajeStop)
		{
			var detener = false;
			var controlHTML	= $("[name='"+idControl+"']").val().trim();
			if(controlHTML == '')
			{
				alertify.error(mensajeStop);
				$("[name='"+idControl+"']").parent().removeClass('has-error').addClass('has-error');
				detener = true;
			}else{$("[name='"+idControl+"']").parent().removeClass('has-error');}
			return detener;
		};
		
	});
</script>

@stop