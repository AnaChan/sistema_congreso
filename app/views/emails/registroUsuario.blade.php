<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <!-- Facebook sharing information tags -->
        <meta property="og:title" content="*|MC:SUBJECT|*">
        
        <title>Registro de Usuario</title>
		
	</head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="-webkit-text-size-adjust: none;margin: 0;padding: 0;background-color: #FAFAFA;width: 100% !important;">
    	<center>
        	<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="backgroundTable" style="margin: 0;padding: 0;background-color: #FAFAFA;height: 100% !important;width: 100% !important;">
            	<tr>
                	<td align="center" valign="top" style="padding-top: 20px;border-collapse: collapse;">
                    	<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer" style="border: 1px solid #DDDDDD;background-color: #FFFFFF;">
                        	<tr>
                            	<td align="center" valign="top" style="border-collapse: collapse;">
                                    <!-- // Begin Template Header \\ -->
                                	<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader" style="background-color: #FFFFFF;border-bottom: 0;">
                                        <tr>
                                            <td class="headerContent" style="border-collapse: collapse;color: #202020;font-family: Arial;font-size: 34px;font-weight: bold;line-height: 100%;padding: 0;text-align: center;vertical-align: middle;">
                                            
                                            	<!-- // Begin Module: Standard Header Image \\ -->
                                            	<img src="{{$banner_email}}" style="width:600px;height:150px;min-height:150px;max-height:150px;border: 0;line-height: 100%;outline: none;text-decoration: none;" id="headerImage campaign-icon">
                                            	<!-- // End Module: Standard Header Image \\ -->
                                            
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // End Template Header \\ -->
                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top" style="border-collapse: collapse;">
                                    <!-- // Begin Template Body \\ -->
                                	<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody">
                                    	<tr>
                                            <td valign="top" style="border-collapse: collapse;">
                                
                                                <!-- // Begin Module: Standard Content \\ -->
                                                <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td valign="top" class="bodyContent" style="border-collapse: collapse;background-color: #FFFFFF;">
                                                            <div mc:edit="std_content00" style="color: #505050;font-family: Arial;font-size: 14px;line-height: 150%;text-align: left;">
                                                                <h1 class="h1" style="color: #202020;display: block;font-family: Arial;font-size: 34px;font-weight: bold;line-height: 100%;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;">Bienvenido a {{{$nombreSistema}}}</h1>
                                                                <br>
                                                                <strong>Confirmar registro:</strong> Este correo fue registrado con la siguiente informaci&oacute;n en nuestro sistema. Si tu no has ingresado esta informaci&oacute;n puedes eliminar el registro con el siguiente link <a href="{{{$URLEliminar}}}" target="_blank" style="color: #336699;font-weight: normal;text-decoration: underline;">Eliminar Usuario</a>
                                                            </div>
														</td>
                                                    </tr>
                                                    <tr>
                                                    	<td valign="top" style="padding-top: 0;padding-bottom: 0;border-collapse: collapse;">
                                                          <table border="0" cellpadding="10" cellspacing="0" width="100%" class="templateDataTable" style="background-color: #FFFFFF;border: 1px solid #DDDDDD;">
                                                              <tr>
                                                                  <th scope="col" valign="top" width="50%" class="dataTableHeading" mc:edit="data_table_heading00" style="background-color: #D8E2EA;color: #336699;font-family: Helvetica;font-size: 14px;font-weight: bold;line-height: 150%;text-align: left;">
                                                                    CORREO
                                                                  </th>
                                                                  <th scope="col" valign="top" width="25%" class="dataTableHeading" mc:edit="data_table_heading01" style="background-color: #D8E2EA;color: #336699;font-family: Helvetica;font-size: 14px;font-weight: bold;line-height: 150%;text-align: left;">
                                                                    NOMBRES
                                                                  </th>
                                                                  <th scope="col" valign="top" width="25%" class="dataTableHeading" mc:edit="data_table_heading02" style="background-color: #D8E2EA;color: #336699;font-family: Helvetica;font-size: 14px;font-weight: bold;line-height: 150%;text-align: left;">
                                                                    APELLIDOS
                                                                  </th>
                                                              </tr>
                                                              <tr mc:repeatable>
                                                                  <td valign="top" class="dataTableContent" mc:edit="data_table_content00" style="border-collapse: collapse;border-top: 1px solid #DDDDDD;border-bottom: 0;color: #202020;font-family: Helvetica;font-size: 12px;font-weight: bold;line-height: 150%;text-align: left;">
                                                                    {{{$correo}}}
                                                                  </td>
                                                                  <td valign="top" class="dataTableContent" mc:edit="data_table_content01" style="border-collapse: collapse;border-top: 1px solid #DDDDDD;border-bottom: 0;color: #202020;font-family: Helvetica;font-size: 12px;font-weight: bold;line-height: 150%;text-align: left;">
                                                                    {{{$nombres}}}
                                                                  </td>
                                                                  <td valign="top" class="dataTableContent" mc:edit="data_table_content02" style="border-collapse: collapse;border-top: 1px solid #DDDDDD;border-bottom: 0;color: #202020;font-family: Helvetica;font-size: 12px;font-weight: bold;line-height: 150%;text-align: left;">
                                                                    {{{$apellidos}}}
                                                                  </td>
                                                              </tr>
                                                          </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" class="bodyContent" style="border-collapse: collapse;background-color: #FFFFFF;">
                                                            <div mc:edit="std_content01" style="color: #505050;font-family: Arial;font-size: 14px;line-height: 150%;text-align: left;">
                                                                Si tu has realizado este registro a nuestro sistemas solo debes acceder al siguiente link <a href="{{{$URLConfirmar}}}" target="_blank" style="color: #336699;font-weight: normal;text-decoration: underline;">Confirmar Usuario</a> para confirmar tu usuario o puedes utilizar el boton de confirmacion.
                                                            </div>
														</td>
                                                    </tr>
                                                    <tr>
                                                    	<td align="center" valign="top" style="padding-top: 0;border-collapse: collapse;">
                                                        	<table border="0" cellpadding="15" cellspacing="0" class="templateButton" style="-moz-border-radius: 3px;-webkit-border-radius: 3px;background-color: #336699;border: 0;border-radius: 3px;color: #FFFFFF;font-family: Arial;font-size: 15px;font-weight: bold;letter-spacing: -.5px;line-height: 100%;text-align: center;text-decoration: none;border-collapse: separate !important;">
                                                            	<tr>
                                                                	<td valign="middle" class="templateButtonContent" style="border-collapse: collapse;">
                                                                    	<div mc:edit="std_content02">
                                                                        	<a href="{{{$URLConfirmar}}}" target="_blank" style="color: #FFFFFF;font-family: Arial;font-size: 15px;font-weight: bold;letter-spacing: -.5px;line-height: 100%;text-align: center;text-decoration: none;">Confirmar Usuario</a>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <!-- // End Module: Standard Content \\ -->
                                                
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // End Template Body \\ -->
                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top" style="border-collapse: collapse;">
                                    <!-- // Begin Template Footer \\ -->
                                	<table border="0" cellpadding="10" cellspacing="0" width="600" id="templateFooter" style="background-color: #FFFFFF;border-top: 0;">
                                    	<tr>
                                        	<td valign="top" class="footerContent" style="border-collapse: collapse;">
                                            
                                                <!-- // Begin Module: Transactional Footer \\ -->
                                                <table border="0" cellpadding="10" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td valign="top" style="border-collapse: collapse;">
                                                            <div mc:edit="std_footer" style="color: #707070;font-family: Arial;font-size: 12px;line-height: 125%;text-align: center;">
																<em>{{{$anio}}} {{{$nombreSistema}}}</em>
																<br>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <!-- // End Module: Transactional Footer \\ -->
                                            
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // End Template Footer \\ -->
                                </td>
                            </tr>
                        </table>
                        <br>
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>