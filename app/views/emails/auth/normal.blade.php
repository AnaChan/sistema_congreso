<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<img  src= "{{$banner_email}}" class="img-rounded" />
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-md-12 column">
			<h2>
				Estimado, {{$usuario}}
			</h2>
			<p>
				{{$mensaje}}
			</p>
			<p>
				<a class="btn" href="#">Ir al sitio »</a>
			</p>
			<p>
				Atentamente,
				Administrador CONUCA
			</p>
		</div>
	</div>
</div>