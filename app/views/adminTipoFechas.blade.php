@section('content')

	 <?php
	 $tituloPantalla	= 'Tipos de Fecha';
	 $resourceLink		= 'admintipofechas';
	 $agregarLinkLabel	= 'Agregar Tipo Fecha';
	 $leftDivACME		= 'Tipo Fecha: &nbsp'; 
	 $HTMLControlNane	= 'nombreFecha'; 
	 ?>
	 
	<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<h3 class="text-center">
				{{$tituloPantalla}}
			</h3>
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-md-6 column">
			<a id="agregarCat"  href="{{URL::action('TipoFechaController@create')}}">{{$agregarLinkLabel}}</a>
			<br/><br/>
			<table id="tbl_categorias" name="tbl_categorias" class="table display">
				<thead>
					<tr>
						<th>
							Tipo Fecha
						</th>
						<th></th>
						@if(false)
						<th></th>
						@endif
					</tr>
				</thead>
				<tbody>
					@foreach ($tiposF as $objeto)
						<tr class="clickRow">
							<td id="catName_{{$objeto->idTipoFecha}}" visible="false">
								{{$objeto->nomTipoFecha}}
							</td>
							<td><a id="{{$objeto->idTipoFecha}}" class="modificar" title="Modificar" href="{{URL::action('TipoFechaController@edit',array($objeto->idTipoFecha) )}}">Modificar</a></td>
							@if(false)
							<td><a id="{{$objeto->idTipoFecha}}" class="eliminar" title="Eliminar" href="{{URL::action('TipoFechaController@edit',array($objeto->idTipoFecha) )}}">Eliminar</a></td>
							@endif
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		
		<br/>
		
		<div class="col-md-5 column" style="box-shadow: 0 5px 8px gray;">
		
		<h3 id="lblCategoria" name="lblCategoria"></h3>
		<input type='hidden' name='gotourl' id='gotourl' value=""></input>
		<br/>
		
		<div class="row clearfix">
			<div class="col-md-4 column">
				<label class="col-sm-3 control-label">{{$leftDivACME}}</label>
			</div>
			<div class="col-md-6 column">
				<input name="{{$HTMLControlNane}}" id="{{$HTMLControlNane}}" disabled='' class="form-control" type="text" ></input>
			</div>
			<div class="col-md-4 column"></div>
		</div>
		<br/>
	
		<div class="row clearfix">
			<div class="col-md-6 column" style="text-aling:center">
				 <button type="button" id="performAction"  disabled=''  class="btn btn-primary btn-default">Guardar</button>
			</div>
		</div>
		<br/>
		</div>
		
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function()
	{
		$('#tbl_categorias').dataTable( {
	        "language": {
	        	"lengthMenu"	: "Mostrar _MENU_ por p&aacute;gina",
	            "zeroRecords"	: "No hay coincidencias",
	            "info"			: "Mostrando p&aacute;gina _PAGE_ de _PAGES_",
	            "infoEmpty"		: "No hay records disponibles",
	            "infoFiltered"	: "(filtrado de un total de _MAX_ registros)",
	            "sSearch"		: "Buscar:",
	            "sLoadingRecords": "Cargando...",
				"oPaginate": {
							"sFirst":    "Primero",
							"sLast":     "&Uacute;ltimo",
							"sNext":     "Siguiente",
							"sPrevious": "Anterior"
							},
	        }
    	} );

		
		$("#tbl_categorias tbody" ).on('click', 'a.eliminar',  function(){
			var gotToURL = this.getAttribute('href');
			alertify.confirm("¿Está seguro que quiere borrar este record?", function (e) {
				if (e) {
					$.post(gotToURL, { accion : "delete" })
					.done(function(data, status,jqXHR)
					{
						if(data.error)
						{
							alertify.error(data.mensaje);
						}
						else
						{
							alertify.success(data.mensaje);
							window.setTimeout(function()
							{
								window.location.replace("{{ URL::action('TipoFechaController@index')}}");
							},
							900);
						}
					})
					.fail(function(data, status,jqXHR)
					{
						console.log("Server Returned " + status);
						alertify.error("Error de comunicación con el servidor, contacte a su adminstrador");
					});
				} else {return false;}
			
			});
			return false;
		});
		
		
		$("#tbl_categorias tbody" ).on('click', 'a.modificar',  function(){
			document.getElementById('lblCategoria').innerHTML = 'Modificar';
			
			var catID = this.getAttribute('id');
			lcName = document.getElementById('catName_'+catID).innerHTML;
			lcName = lcName.replace(/\n|\r/g, "").trim();
			document.getElementById('{{$HTMLControlNane}}').value = lcName;
			document.getElementById('gotourl').value = this.getAttribute('href');
			document.getElementById('performAction').removeAttribute('disabled');
			document.getElementById('{{$HTMLControlNane}}').removeAttribute('disabled');
			document.getElementById('{{$HTMLControlNane}}').focus();
			return false;
		});
		
		$("#agregarCat").click(function()
		{
			document.getElementById('lblCategoria').innerHTML = 'Agregar';
			document.getElementById('gotourl').value = this.getAttribute('href'); //controller.create, get verb
			document.getElementById('performAction').removeAttribute('disabled');
			document.getElementById('{{$HTMLControlNane}}').removeAttribute('disabled');
			document.getElementById('{{$HTMLControlNane}}').value = '' ;
			document.getElementById('{{$HTMLControlNane}}').focus();
			return false;
		});
		
		
		$("#performAction").click(function()
		{
			var controlVal	= document.getElementById('{{$HTMLControlNane}}').value.trim();
			var gotToURL	= document.getElementById('gotourl').value;
			if (checkFieldsEmpty('{{$HTMLControlNane}}','Ingrese un valor para Tipo Fecha'))
				return;

			$.post(gotToURL, { accion : "edit", nomTipoFecha : controlVal })
				.done(function(data, status,jqXHR)
				{
					if(data.error)
					{
						alertify.error(data.mensaje);
					}
					else
					{
						alertify.success(data.mensaje);
						window.setTimeout(function()
						{
							window.location.replace("{{ URL::action('TipoFechaController@index')}}");
						},
						900);
					}
				})
				.fail(function(data, status,jqXHR)
				{
					console.log("Server Returned " + status);
					alertify.error("Error de comunicación con el servidor, contacte a su adminstrador");
				});
		});
		function checkFieldsEmpty(idControl,mensajeStop)
		{
			var detener = false;
			var controlHTML	= $("[name='"+idControl+"']").val().trim();
			if(controlHTML == '')
			{
				alertify.error(mensajeStop);
				$("[name='"+idControl+"']").parent().removeClass('has-error').addClass('has-error');
				detener = true;
			}else{$("[name='"+idControl+"']").parent().removeClass('has-error');}
			return detener;
		};
	});
</script>

@stop