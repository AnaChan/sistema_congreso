@section('content')

<div class="col-sm-10 col-sm-offset-1" style="margin-top: 50px;">
	<div class="panel panel-primary" style="border-color: #205081; border-radius: 2px; box-shadow: 0 5px 10px gray;">
		<div class="panel-heading" style="border-radius: 0px; background: #205081; border-color: #205081;">
			<div class="panel-title">Edici&oacute;n de Perfil</div>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1">
					<?php $Usuario = Usuario::find(Auth::user()->idUsuario);  ?>
					{{ Form::model($Usuario,  ['role' => 'form' , 'class' => 'form-horizontal','id' => 'formEdicion','method' => 'PATCH' , 'route' => ['editprofile.update', Auth::user()->idUsuario ]]) }}
					<fieldset>
						<div class="form-group">
							<label for="nombreUsuario" class="col-sm-3 control-label">Nombres*:</label>
							<div class="col-sm-9">
								<input type="text" maxlength="200" class="form-control" id="nombreUsuario" name="nombreUsuario" value='{{$Usuario->nombreUsuario}}'/>
							</div>
						</div>
						<div class="form-group">
							<label for="apelUsuario" class="col-sm-3 control-label">Apellidos*:</label>
							<div class="col-sm-9">
								<input type="text" maxlength="200" class="form-control" id="apelUsuario" name="apelUsuario" value='{{$Usuario->apelUsuario}}'/>
							</div>
						</div>
						<div class="form-group">
							<label for="passwordUsuario" class="col-sm-3 control-label">Contrase&ntilde;a*:</label>
							<div class="col-sm-9">
								<span>No cambiar contrase&ntilde;a </span>{{ Form::checkbox('nocambiar', '1', false); }}
								<input type="password" maxlength="20" class="form-control" id="passwordUsuario" name="passwordUsuario" />
							</div>
						</div>
						<div class="form-group">
							<label for="confirmPassword" class="col-sm-3 control-label">Confirmar Contrase&ntilde;a*:</label>
							<div class="col-sm-9">
								<input type="password" maxlength="20" class="form-control" id="confirmPassword" name="confirmPassword" />
							</div>
						</div>
						<div class="form-group">
							<label for="emailUsuario" class="col-sm-3 control-label">Correo Electr&oacute;nico*:</label>
							<div class="col-sm-9">
								<input type="text" maxlength="100" class="form-control" id="emailUsuario"  name="emailUsuario" value='{{$Usuario->emailUsuario}}'/>
							</div>
						</div>
						<div class="form-group">
							<label for="resumenUsuario" class="col-sm-3 control-label">Resumen:</label>
							<div class="col-sm-9">
								{{ Form::textarea('resumenUsuario') }}
							</div>
						</div>
						<div class="form-group">
							<label for="areasinteres" class="col-sm-3 control-label">&Aacute;reas de Inter&eacute;s:</label>
							<div class="col-sm-9">
								<select multiple data-placeholder="áreas" style="width:100%" class="chosen-choices" id="misAreas" name="misAreas" size="5">
									@foreach($otrasAreas as $otra)
										<option value="{{ $otra->idTematica }}">{{ $otra->nomTematica }}</option>
									@endforeach
									@foreach($areas1 as $area)
										<option selected='selected' value="{{ $area->idTematica }}">{{ $area->nomTematica }}</option>
									@endforeach
								</select>
							</div>
							<input type='hidden' name='listaMisSelecciones' id='listaMisSelecciones' />
						</div>
					</fieldset>
					</form>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-sm-4">
					<button type="button" id="performAction"  class="btn btn-primary btn-default">Actualizar Informaci&oacute;n</button>
				</div>
			</div>
		</div>
	</div>
</div>


		
<script type="text/javascript">
	$(document).ready(function()
	{
		$("#misAreas").chosen();
		var checkMessagesServer = $("[name='errmessage']").val();
		if(checkMessagesServer && checkMessagesServer  != '')
		{	
			if(checkMessagesServer.indexOf('actualizada') == -1)
			{
				$("[name='emailUsuario']").parent().removeClass('has-error').addClass('has-error');
				alertify.error(checkMessagesServer);
			}
			else
				alertify.success(checkMessagesServer);
		}
		$("[name='resumenUsuario']").addClass('form-control'); 
		
		
		$("#performAction").click(function()
		{
			$("[name='nombreUsuario']").parent().removeClass('has-error');
			$("[name='apelUsuario']").parent().removeClass('has-error');
			$("[name='passwordUsuario']").parent().removeClass('has-error');
			$("[name='confirmPassword']").parent().removeClass('has-error');
			$("[name='emailUsuario']").parent().removeClass('has-error');
			
			var errors = false;
			var password = $("[name='passwordUsuario']").val();
			var passwordConfirm = $("[name='confirmPassword']").val();
			
			if(password.length < 7 && !$("[name='nocambiar']").is(":checked"))
			{
				alertify.error("La contrase&ntilde;a debe de tener mas de 6 caracteres!");
				$("[name='passwordUsuario']").parent().removeClass('has-error').addClass('has-error');
				errors = true;
			}
			
			if(password != passwordConfirm && !$("[name='nocambiar']").is(":checked"))
			{
				alertify.error("Contrase&ntilde;as no coinciden");
				$("[name='passwordUsuario']").parent().removeClass('has-error').addClass('has-error');
				$("[name='confirmPassword']").parent().removeClass('has-error').addClass('has-error');
				$("[name='passwordUsuario']").val('');
				$("[name='confirmPassword']").val('');
				errors = true;
			}
			
			var correo = $("[name='emailUsuario']").val();
			var correoRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if(!correoRegex.test(correo))
			{
				alertify.error("Correo invalido!");
				$("[name='emailUsuario']").parent().removeClass('has-error').addClass('has-error');
				errors = true;
			}
			
			if(!errors)
			{
				var misSelecciones = '';
				for (var i=0 ; i < document.getElementById('misAreas').options.length ; i++)
				{
					if(document.getElementById('misAreas').options[i].selected)
						misSelecciones += document.getElementById('misAreas').options[i].value + ',';
				}
				misSelecciones = misSelecciones.substring(0,misSelecciones.length-1);
				$("[name='listaMisSelecciones']").val(misSelecciones);
				alertify.success('Actualizando Informaci&oacute;n...');
				$('#formEdicion').submit();
			}
		});
	});
</script>

@stop