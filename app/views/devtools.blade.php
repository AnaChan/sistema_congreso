@section('content')
<div class="container">
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<fieldset>
					<div class="row clearfix">
						<label for="nomCongreso" class="col-sm-3 control-label">SQL:</label>
						<div class="col-sm-9">
							{{$sql}}
						</div>
					</div><br>
					<div class="row clearfix">
						<label for="nomCongreso" class="col-sm-3 control-label">JSON return</label>
						<div id='jsondiv' name='jsondiv' class="col-sm-7">
							@if($error)
								{{$mensaje}}
							@else
								{{json_encode($cursor,JSON_PRETTY_PRINT)}}
							@endif
						</div>
						@if(!$error)
						<div class="col-sm-1">
							<button type="button" id="estilizar" class="btn btn-default btn-default glyphicon glyphicon-wrench"> Switch</button>
						</div>
						@endif
					</div><br>
					<div class="row clearfix" style='max-width:1800px;overflow-x:scroll;overflow-y: hidden;'>
						<div id='jsontable' name='jsontable' class="col-sm-10">
						</div>
					</div>
				</fieldset>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {

		$("#estilizar").click(function(){
			workingObject = $("#jsondiv").html();
			JsonObject = JSON.parse(workingObject);
			$('#jsontable').html('');
			$('#jsontable').append(CreateTableView(JsonObject,'table table-striped',true)).fadeIn();
		});
	
		function CreateTableView(objArray, theme, enableHeader) {
			if (theme === undefined) {
			    theme = 'mediumTable'; //default theme
			}

			if (enableHeader === undefined) {
			    enableHeader = true; //default enable headers
			}

			var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;

			var str = '<table class="' + theme + '">';
			 
			// table head
			if (enableHeader) {
			    str += '<thead><tr>';
			    for (var index in array[0]) {
			        str += '<th scope="col">' + index + '</th>';
			    }
			    str += '</tr></thead>';
			}
			 
			// table body
			str += '<tbody>';
			for (var i = 0; i < array.length; i++) {
			    str += (i % 2 == 0) ? '<tr class="alt">' : '<tr>';
			    for (var index in array[i]) {
			        str += '<td>' + array[i][index] + '</td>';
			    }
			    str += '</tr>';
			}
			str += '</tbody>'
			str += '</table>';
			return str;
		}

	});
</script>
@stop