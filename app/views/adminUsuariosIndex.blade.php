@section('content')

<div class="">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<h3 class="text-center">
				Administraci&oacute;n de Usuarios
			</h3>
		</div>
	</div>
	
	<a href="{{URL::action('AdminusuariosController@create')}}">Agregar Usuario</a>
	<br><br>
	<div class="row clearfix">
		<div class="col-md-12 column">
			<table class="table display" id="tableHeaderFloat" name="tableHeaderFloat">
				<thead>
					<tr>
						<th>Nombres</th>
						<th>Apellidos</th>
						<th>Email</th>
						<th>Estado</th>
						<th>Creado</th>
						<th>Modificado</th>
						<th>&Uacute;ltimo acceso</th>
						<th></th>
						@if(false)
						<th></th>
						@endif
					</tr>
				</thead>
				<tbody>
					
					@foreach($usuarios as $usuario)
					<tr>

						<td> {{ $usuario->nombreUsuario }} </td>
						<td> {{ $usuario->apelUsuario }} </td>
						<td> {{ $usuario->emailUsuario }} </td>
						<?php $estadoUsuario 	= EstadoUsuario::find($usuario->idEstadoUsuario); ?>
						<td> {{ $estadoUsuario->nombreEstado }} </td>
						<td> {{ strftime("%m/%d/%Y", strtotime($usuario->fecCreaUsuario))  }} </td>
						<td> {{ ($usuario->fecModUsuario == '0000-00-00 00:00:00')? '  /  /' :  strftime("%m/%d/%Y", strtotime($usuario->fecModUsuario))  }} </td>
						
						
						@if($usuario->ultimoAcceso == '0000-00-00 00:00:00')
							<td></td>
						@else
							<td> {{strftime("%m/%d/%Y", strtotime($usuario->ultimoAcceso))}} {{date("h:i",strtotime($usuario->ultimoAcceso))}} {{date("A",strtotime($usuario->ultimoAcceso))}}</td>
						@endif
						
						<td><a title="Modificar" href="{{URL::action('AdminusuariosController@edit',array($usuario->idUsuario) )}}">Modificar</a></td>
						@if(false)
						<td><a class="delete" title="Eliminar" href="{{URL::action('AdminusuariosController@destroy',array($usuario->idUsuario) )}}">Eliminar</a></td>
						@endif
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div> 
 
 <script type="text/javascript">
	$(document).ready(function()
	{
		$('#tableHeaderFloat').dataTable( {
	        "language": {
	        	"lengthMenu"	: "Mostrar _MENU_ usuarios por p&aacute;gina",
	            "zeroRecords"	: "No hay coincidencias",
	            "info"			: "Mostrando p&aacute;gina _PAGE_ de _PAGES_",
	            "infoEmpty"		: "No hay records disponibles",
	            "infoFiltered"	: "(filtrado de un total de _MAX_ registros)",
	            "sSearch"		: "Buscar:",
	            "sLoadingRecords": "Cargando...",
				"oPaginate": {
							"sFirst":    "Primero",
							"sLast":     "&Uacute;ltimo",
							"sNext":     "Siguiente",
							"sPrevious": "Anterior"
							},
	        }
    	} );

		$("#tableHeaderFloat tbody" ).on('click', 'a.delete',  function()
		{
			var gotToURL = this.getAttribute('href');
			alertify.confirm("¿Está seguro que quiere borrar este record?", function (e) {
				if (e) {
					$.post(gotToURL, {fakeParam: ""})
					.done(function(data, status,jqXHR)
					{
						if(data.error)
						{
							alertify.error(data.mensaje);
						}
						else
						{
							alertify.success(data.mensaje);
							window.setTimeout(function()
							{
								window.location.replace("{{ URL::action('AdminusuariosController@index')}}");
							},
							900);
							
						}
					})
					.fail(function(data, status,jqXHR)
					{
						console.log("Server Returned " + status);
						alertify.error("Error de comunicación con el servidor, contacte a su adminstrador");
					});
				} else {
					return false;
				}
			});
			return false;
		});
		
	});
</script>

@stop