	@section('content')
<div class="col-sm-12" style="margin-top: 50px;">
	<div class="panel panel-primary" style="border-color: #205081; border-radius: 2px; box-shadow: 0 5px 10px gray;">
		<div class="panel-heading" style="border-radius: 0px; background: #205081; border-color: #205081;">
			<div class="panel-title">Aprobaci&oacute;n o Rechazo de Congreso</div>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-7 column">
					<div class="row">
						<div class="col-md-4 column">
							<span style="font-weight:bold">Nombre del Congreso: </span>
						</div>
						<div class="col-md-8 column">
							 <span >{{$congreso->nomCongreso}}</span>
						</div>
					</div>
					<br/>
					<div class="row">
						<div class="col-md-4 column">
							 <span style="font-weight:bold">Acr&oacute;nimo del Congreso: </span>
						</div>
						<div class="col-md-8 column">
							 <span>{{$congreso->acronimoCongreso}}</span>
						</div>
					</div>
					<br/>
					<div class="row">
						<div class="col-md-4 column">
							 <span style="font-weight:bold">Sitio Web:</span>
						</div>
						<div class="col-md-8 column">
							<h5><a target="_blank" href="{{$congreso->webCongreso}}">{{$congreso->webCongreso}}</a></h5>
						</div>
					</div>
					<br/>
					<div class="row">
						<div class="col-md-4 column">
							 <span style="font-weight:bold">Ciudad: </span>
						</div>
						<div class="col-md-8 column">
							 <span>{{$congreso->ciudad}}</span>
						</div>
					</div>
					<br/>
					<div class="row">
						<div class="col-md-4 column">
							 <span style="font-weight:bold">Pa&iacute;s: </span>
						</div>
						<div class="col-md-8 column">
							 <span>{{$congreso->pais}}</span>
						</div>
					</div>
					<br/>
					<div class="row">
						<div class="col-md-4 column">
							 <span style="font-weight:bold">Fecha de Inicio: </span>
						</div>
						<div class="col-md-8 column">
							 <span>{{$congreso->fecIniCongreso}}</span>
						</div>
					</div>
					<br/>
					<div class="row">
						<div class="col-md-4 column">
							 <span style="font-weight:bold">Fecha de fin: </span>
						</div>
						<div class="col-md-8 column">
							 <span>{{$congreso->fecFinCongreso}}</span>
						</div>
					</div>
					<br/>
					<div class="row">
						<div class="col-md-4 column">
							 <span style="font-weight:bold">Email de contacto:</span>
						</div>
						<div class="col-md-8 column">
							 <span>{{$congreso->emailOrgCongreso}}</span>
						</div>
					</div>
					<br/>
					<div class="row">
						<div class="col-md-4 column">
							 <span style="font-weight:bold">Usuario Solicitante: </span>
						</div>
						<div class="col-md-8 column">
							 <span>{{Usuario::Find($congreso->idCreador)->nombreUsuario . ' ' . Usuario::Find($congreso->idCreador)->apelUsuario}}</span>
						</div>
					</div>
				</div>
				<div class="col-md-5 column">
					<div class="row clearfix">
						<div class="col-md-6 column">
							 <span style="font-weight:bold">Comentarios: </span>
						</div>
						<div class="col-md-6 column">
						</div>
					</div>
					<br/>
					<div class="row text-center">
						<div class="col-md-12 column">
							<textarea name="comentarios" style="width:85%; resize: none;" placeholder="El comentario es obligatorio solamente si se rechaza el congreso" rows="8"></textarea>
						</div>
					</div>
					<br/>
					<div class="row text-center">
						<div class="col-md-4 column">
							 <button id="btnAprobar"  type="button" class="btn btn-primary">Aprobar</button>
						</div>
						<div class="col-md-4 column">
							 <button id="btnRechazar" type="button" class="btn btn-danger">Rechazar</button>
						</div>
						<div class="col-md-4 column">
							 <button type="button" id="cancelarTodo" class="btn btn-default">Cancelar</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<input type="hidden" name="correo" value="{{$congreso->emailOrgCongreso}}">
		<input type="hidden" name="id" value={{$congreso->idCongreso}}>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function()
	{

		$("#btnAprobar").click(function()
		{
			var id = $("[name='id']").val();
			var comentarios = $("[name='comentarios']").val();
			var correo = $("[name='correo']").val();
			$.post("{{URL::action('CongresoController@modificarSolicitud')}}", {comentarios: comentarios, estado: 'aprobada', id: id,correo:correo})
			.done(function(data, status,jqXHR)
			{
				if(data.error)
				{
					alertify.error(data.mensaje);
				}
				else
				{
					alertify.success(data.mensaje);
					window.location.replace("{{URL::action('CongresoController@MostrarSolicitudesCongreso')}}");					
				}
			})
			.fail(function(data, status,jqXHR)
			{
				console.log("Server Returned " + status);
				alertify.error("Error de comunicación con el servidor.");
			});
		});

		$("#cancelarTodo").click(function(){
			window.location.href="{{URL::action('HomeController@inicio')}}";
		});

		$("#btnRechazar").click(function()
		{
			var id = $("[name='id']").val();

			var comentarios = $("[name='comentarios']").val();
			var correo = $("[name='correo']").val();

			if(comentarios.length<10 || comentarios.length>250){
				alertify.error("El comentario es obligatorio y debe tener como mínimo 10 caracteres y menos de 250");
				$("[name='comentarios']").parent().removeClass('has-error').addClass('has-error');
				return;
			}
			else
			{
				$("[name='comentarios']").parent().removeClass('has-error');
			}


			$.post("{{URL::action('CongresoController@modificarSolicitud')}}", {comentarios: comentarios, estado: 'rechazada', id: id,correo:correo})
			.done(function(data, status,jqXHR)
			{
				if(data.error)
				{
					alertify.error(data.mensaje);
				}
				else
				{
					alertify.success(data.mensaje);
					window.location.replace("{{URL::action('CongresoController@MostrarSolicitudesCongreso')}}");
				}
			})
			.fail(function(data, status,jqXHR)
			{
				console.log("Server Returned " + status);
				alertify.error("Error de comunicación con el servidor.");
			});
		});

	});
</script>
@stop