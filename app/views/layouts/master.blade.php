<!doctype html>

<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>CONUCA</title>
		<meta name="description" content="CONUCA">
		<meta name="author" content="UCA">

		<link href="{{ URL::asset('css/chosen.css') }}" rel="stylesheet" type="text/css">
		<link href="{{ URL::asset('css/fileinput.min.css') }}" rel="stylesheet" type="text/css">
		<link href="{{ URL::asset('css/bootstrap-datetimepicker.css') }}" rel="stylesheet" type="text/css">
		<link href="{{ URL::asset('css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css">
		<link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
		<link href="{{ URL::asset('css/alertify.core.css') }}" rel="stylesheet" type="text/css">
		<link href="{{ URL::asset('css/alertify.bootstrap.css') }}" rel="stylesheet" type="text/css">
		<link href="{{ URL::asset('css/site.css') }}" rel="stylesheet" type="text/css" >
		<link href="{{ URL::asset('css/bootstrap-slider.min.css') }}" rel="stylesheet" type="text/css">

		<!--<script src="//cdnjs.cloudflare.com/ajax/libs/less.js/1.7.0/less.min.js"></script>-->
		<script src="{{ URL::asset('js/jquery.min.js') }}"></script>
		<script src="{{ URL::asset('lib/jquery-ui.custom.min.js') }}"></script>

		<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
		<script src="{{ URL::asset('js/alertify.min.js') }}"></script>
		<script src="{{ URL::asset('js/moment.min.js') }}"></script>
		<script src="{{ URL::asset('js/Mustache.min.js') }}"></script>
		<script src="{{ URL::asset('js/bootstrap-datetimepicker.min.js') }}"></script>
		<script src="{{ URL::asset('js/chosen.jquery.min.js') }}"></script>
		<script src="{{ URL::asset('js/fileinput.min.js') }}"></script>
		<script src="{{ URL::asset('js/bootstrap-switch.min.js') }}"></script>

		<!-- pdf.js-->
		<script src="{{ URL::asset('js/compatibility.js') }}"></script>
		<script src="{{ URL::asset('js/pdf.js') }}"></script>

		<!--Archivos js y css para el dataTable-->
		<script src="{{ URL::asset('js/jquery.dataTables.min.js') }}"></script>
		<link href="{{{ URL::asset('css/jquery.dataTables.min.css') }}}" rel="stylesheet">
		<!--<link href="{{{ URL::asset('css/jquery.dataTables_themeroller.min.css') }}}" rel="stylesheet">-->

		<!--Archivos js y css para multiselect-->
		<script src="{{ URL::asset('js/jquery.multi-select.js') }}"></script>
		<script src="{{ URL::asset('js/jquery.quicksearch.js') }}"></script>
		<link href="{{{ URL::asset('css/multi-select.css') }}}" rel="stylesheet">


		<!--Archivos js y css para el calendario-->
		<link href="{{ URL::asset('fullcalendar/fullcalendar.css') }}" rel='stylesheet' />
		<!--<link href='fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />-->
		<!--<script src="{{ URL::asset('lib/jquery.min.js')}}"></script>
		<script src="{{ URL::asset('lib/jquery-ui.custom.min.js')}}"></script>-->
		<script src="{{ URL::asset('fullcalendar/fullcalendar.min.js')}}"></script>


		<!--Archivos js y css para el themeroller-->
		<script src="{{{ URL::asset('js/jquery-ui-1.10.4.custom.min.js') }}}"></script>
		<script src="{{{ URL::asset('js/i18n/jquery.ui.datepicker-es.js') }}}"></script>
		<link href="{{{ URL::asset('css/custom-theme/jquery-ui-1.10.4.custom.css') }}}" rel="stylesheet">

		<!--Archivos JQGRID plugin-->
		<link href="{{{ URL::asset('css/ui.jqgrid.css') }}}" rel="stylesheet">
		<script src="{{{ URL::asset('js/jquery.jqGrid.min.js') }}}"></script>
		<script src="{{{ URL::asset('js/i18n/grid.locale-es.js') }}}"></script>

		<!--Archivos fileupload plugin-->
		<script src="{{{ URL::asset('js/ajaxfileupload.js') }}}"></script>
		<link href="{{{ URL::asset('css/uploadfile.css') }}}" rel="stylesheet">
		<script src="{{{ URL::asset('js/jquery.uploadfile.min.js') }}}"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	</head>

	<body>
		<div id="fade" class="modal fondocarga"  style="z-index: 10000;height:100%;width:100%;background-color: rgba(0, 0, 0, 0.2);padding:0;margin:0;border:0;position:fixed;overflow:hidden;left:0px;top:0px;display: table; vertical-align: middle;">
			<div class="cont" style="display: table-cell; vertical-align: middle;text-align:center">
				<button class="btn btn-lg btn-info" disabled><span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Cargando...</button>
			</div>
		</div>
		<div class="container-fluid" style="max-width: 1800px;">
			<div class="row barraPrincipal" style="min-height: 80px; border-color: #205081; box-shadow: 0 5px 10px gray;">
				<div class="col-md-6 titulo">
					<a href="{{URL::action('HomeController@inicio')}}" ><img src="{{URL::asset('imagenes/logoconuca_white.png')}}"/></a>
				</div>
				@if (Auth::check())
				<div class="col-md-4 col-md-offset-2" style="padding-top: 9px;">
					<div class="well wellUser">
						<div class="row" style="margin: 0px;">
							<div class="input-group input-group-sm">	
								<input type="text" class="form-control" value="{{{Auth::user()->emailUsuario}}}" style="cursor: default; background-color: white; font-weight: bold;" readonly="readonly"/>
								<span class="input-group-btn">
									@if ($mostrar_rol_seteado)
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" disabled><strong>{{($nombre_rol == 'PC')?'Miembro del comit&eacute;': (($nombre_rol == 'Chair')?'Administrador del Congreso':$nombre_rol)}}</strong></button>
									@endif
									@if ($nombre_rol =='ADM'))
										<button type="button" class="btn btn-default dropdown-toggle" data-placement="bottom" 
												data-toggle="tooltip" data-placement="left" title="Hola! Te informamos que esta cuenta no puede ser modificada" disabled>
											<strong>Administrador del Sistema </strong><span class="glyphicon glyphicon-lock"></span>
										</button>
									@endif
									<button id="logout" class="btn btn-primary" type="button" data-toggle="tooltip" data-placement="left" title="Cerrar sesión"><span class="glyphicon glyphicon-off"></span></button>
									@if ($nombre_rol !='ADM')
										<a class="btn btn-primary" tittle='Perfil de Usuario' href="{{ URL::asset('editprofile') }}" data-toggle="tooltip" data-placement="left" title="Modificar perfil"><span class="glyphicon glyphicon-user"></span></a>
										<a class="btn btn-primary" tittle='Perfil de Usuario' href="{{URL::asset('manuales/manual_usuario.pdf')}}"  target="_blank" data-toggle="tooltip" data-placement="left" title="Manual de Usuario"><span class="glyphicon glyphicon-question-sign"></span></a>
									@else
										<a class="btn btn-primary" tittle='Perfil de Usuario' href="{{URL::asset('manuales/manual_admin.pdf')}}"  target="_blank" data-toggle="tooltip" data-placement="left" title="Manual de Administrador"><span class="glyphicon glyphicon-question-sign"></span></a>
									@endif
									<button type="button" data-toggle="dropdown" class="btn btn-primary dropdown-toggle">{{trans('miscellaneous.lenguaje')}}<span class="caret"></span></button>
									@if (Session::get('idioma') == 'es')
										<ul class="dropdown-menu">
											<li><a onClick="cambiarIdioma('en');" href="#">{{trans('miscellaneous.idiomaingles')}}</a></li>
											<li><a onClick="cambiarIdioma('es');"href="#">✔ {{trans('miscellaneous.idiomaespanol')}}</a></li>
										</ul>
									@endif
									@if (Session::get('idioma') == 'en')
										<ul class="dropdown-menu">
											<li><a onClick="cambiarIdioma('en');" href="#">✔ {{trans('miscellaneous.idiomaingles')}}</a></li>
											<li><a onClick="cambiarIdioma('es');" href="#">{{trans('miscellaneous.idiomaespanol')}}</a></li>
										</ul>
									@else
										<!-- DEFAULT option-->
										<ul class="dropdown-menu">
											<li><a onClick="cambiarIdioma('en');" href="#">{{trans('miscellaneous.idiomaingles')}}</a></li>
											<li><a onClick="cambiarIdioma('es');"href="#">✔ {{trans('miscellaneous.idiomaespanol')}}</a></li>
										</ul>
									@endif
								</span>
							</div>
						</div>
					</div>
				</div>
				@else
				<div class="col-md-1 col-md-offset-5" style="padding-top: 9px;">
						<div class="row" style="margin: 0px;">
							<div class="input-group input-group-sm">
								<button type="button" data-toggle="dropdown" class="btn btn-primary dropdown-toggle">{{trans('miscellaneous.lenguaje')}}<span class="caret"></span></button>
								@if (Session::get('idioma') == 'es')
									<ul class="dropdown-menu">
										<li><a onClick="cambiarIdioma('en');" href="#">{{trans('miscellaneous.idiomaingles')}}</a></li>
										<li><a onClick="cambiarIdioma('es');" href="#">✔ {{trans('miscellaneous.idiomaespanol')}}</a></li>
									</ul>
								@endif
								@if (Session::get('idioma') == 'en')
									<ul class="dropdown-menu">
										<li><a onClick="cambiarIdioma('en');" href="#">✔ {{trans('miscellaneous.idiomaingles')}}</a></li>
										<li><a onClick="cambiarIdioma('es');" href="#">{{trans('miscellaneous.idiomaespanol')}}</a></li>
									</ul>
								@else
									<!-- DEFAULT option-->
									<ul class="dropdown-menu">
										<li><a onClick="cambiarIdioma('en');" href="#">{{trans('miscellaneous.idiomaingles')}}</a></li>
										<li><a onClick="cambiarIdioma('es');"href="#">✔ {{trans('miscellaneous.idiomaespanol')}}</a></li>
									</ul>
								@endif
							</div>
						</div>
					</div>
				@endif
			</div>
		</div>
		<div class="container" style="border: solid #dddddd 1px;border-top: solid #dddddd 0px;padding-top: 10px; min-height: 613px;">
			@if ($mostrar_barra)
				@if ($nombre_rol == 'Chair')
					@include('layouts.barraChair')
				@elseif ($nombre_rol == 'Revisor')
					@include('layouts.barraRevisor')
				@elseif ($nombre_rol == 'Autor')
					@include('layouts.barraAutor')
				@elseif ($nombre_rol == 'PC')
					@include('layouts.barraPC')
				@elseif ($nombre_rol == 'ADM')
					@include('layouts.barraAdmin')
				@else
					@include('layouts.barraUsuario')
				@endif
			@endif
			<div class="row">
				<div class="container">
					@yield('content','')
				</div>
			</div>
		</div>
		<style>
			.divider-vertical {
			height: 40px;
			margin: 0 9px;
			border-left: 1px solid #F2F2F2;
			border-right: 1px solid #FFF;
			}
			.glyphicon-refresh-animate {
			    -animation: spin .7s infinite linear;
			    -webkit-animation: spin2 .7s infinite linear;
			}

			@-webkit-keyframes spin2 {
			    from { -webkit-transform: rotate(0deg);}
			    to { -webkit-transform: rotate(360deg);}
			}

			@keyframes spin {
			    from { transform: scale(1) rotate(0deg);}
			    to { transform: scale(1) rotate(360deg);}
			}
		</style>
		<script src="{{ URL::asset('js/bootstrap-slider.js') }}"></script>
		<script type="text/javascript">
			function permitirSoloLetras(e, t) {
				try {
					if (window.event) {
						var codigoCar = window.event.keyCode;
					}
					else if (e) {
						var codigoCar = e.which;
					}
					else { return true; }
					return ((codigoCar > 64 && codigoCar < 91) ||
							(codigoCar > 96 && codigoCar < 123 ) ||
							codigoCar ==241 || codigoCar==209
							|| codigoCar==32)
				}
				catch (err) {
					alertify.error(err.Description);
				}
			}
		
			function permitirSoloNumeros(e, t) {
				try {
					if (window.event) {
						var codigoCar = window.event.keyCode;
					}
					else if (e) {
						var codigoCar = e.which;
					}
					else {
						return true; 
					}
					return (codigoCar > 47 && codigoCar < 58)
			}
				catch (err) {
					alert(err.Description);
				}
			}
			function cambiarIdioma(idiomaCaption)
			{
				$.post("{{URL::action('DeveloperController@cambiarIdioma')}}", {idioma : idiomaCaption })
				.done(function(data, status,jqXHR)
				{
					if(data.error)
					{
						alertify.error(data.mensaje);
					}
					else
					{
						window.location.reload();
					}
				})
				.fail(function(data, status,jqXHR)
				{
					alertify.error("Error de comunicación con el servidor, contacte a su adminstrador");
				});
			}

			$(document).ready(function()
			{
				$('#seteadoAutor').click(function () {
					$.get("{{URL::action('CambiarRolController@autor', array($idCongreso))}}",{})
					.done(function (data, status, jqXHR){
						alertify.success(data.mensaje);
						window.location.replace("{{URL::action('UsuariosCongresosController@establecerCongreso',array($idCongreso))}}");
					})
					.fail(function(data, status,jqXHR)
					{
						console.log("Server Returned " + status);
						alertify.error("Error de comunicación con el servidor.");
					});
				});

				$('#seteadoRevisor').click(function () {
					$.get("{{URL::action('CambiarRolController@revisor', array($idCongreso))}}",{})
					.done(function (data, status, jqXHR){
						alertify.success(data.mensaje);
						window.location.replace("{{URL::action('UsuariosCongresosController@establecerCongreso',array($idCongreso))}}");
					})
					.fail(function(data, status,jqXHR)
					{
						console.log("Server Returned " + status);
						alertify.error("Error de comunicación con el servidor.");
					});
				});

				$('#seteadoPC').click(function () {
					$.get("{{URL::action('CambiarRolController@pc', array($idCongreso))}}",{})
					.done(function (data, status, jqXHR){
						alertify.success(data.mensaje);
						window.location.replace("{{URL::action('UsuariosCongresosController@establecerCongreso',array($idCongreso))}}");
					})
					.fail(function(data, status,jqXHR)
					{
						console.log("Server Returned " + status);
						alertify.error("Error de comunicación con el servidor.");
					});
				});

				$('#seteadoChair').click(function () {
					$.get("{{URL::action('CambiarRolController@chair', array($idCongreso))}}",{})
					.done(function (data, status, jqXHR){
						alertify.success(data.mensaje);
						window.location.replace("{{URL::action('UsuariosCongresosController@establecerCongreso',array($idCongreso))}}");
					})
					.fail(function(data, status,jqXHR)
					{
						console.log("Server Returned " + status);
						alertify.error("Error de comunicación con el servidor.");
					});
				});

				$( document ).ajaxStart(function()
				{
					$('#cargando-img').removeClass('hidden');
					$("#fade").fadeToggle("slow");
				});

				$( document ).ajaxComplete(function(evento, jqXHR, data) 
				{
					$('#cargando-img').removeClass('hidden').addClass('hidden');
					$("#fade").hide();
				});

				$(document).ajaxSuccess(function(evento, jqXHR, data)
				{
					if(typeof jqXHR.responseJSON != 'undefined' && jqXHR.responseJSON.error && jqXHR.responseJSON.errorAuthJSON) window.location.replace('{{URL::action("LoginController@index")}}');
				});

				$("#fade").hide();

				$("#logout").click(function()
				{
					$.ajax(
					{
						type: "DELETE",
						url: "{{ URL::action('LoginController@hacerLogout') }}"
					})
					.done(function(data, status, jqXHR)
					{
						alertify.success("Se ha cerrado la sesión correctamente");
						window.location.replace("{{ URL::action('HomeController@inicio')}}");
					})
					.fail(function(data, status, jqXHR)
					{
						console.log("Server Returned " + status);
						alertify.error("Error de comunicación con el servidor.");
					});
				});
				$('#cargando-img').removeClass('hidden').addClass('hidden');
				$("#fade").hide();
			});

		</script>
	</body>
	<div style="text-align: center;height:70px;margin-top:20px;">
		© 2015. {{trans('miscellaneous.copyright')}}
	</div>
</html>