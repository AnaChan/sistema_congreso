<div class="row" style="border-bottom: 1px solid #dddddd;">
	<div class="col-md-9">
		<ul class="nav nav-tabs" style="border: 0px;">
			<li ><a href="{{URL::action('HomeController@inicio')}}">
					<span class="glyphicon glyphicon-home"></span>
				</a>
			</li>
			<li class="divider-vertical"></li>
			<li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">Congresos<span class="caret"></span></a>
				<ul class="dropdown-menu" role="menu">
					<li><a href="{{URL::action('CongresoController@MostrarSolicitudesCongreso')}}">Gestionar Solicitudes</a></li>
				</ul>
			</li>
			<li class="divider-vertical"></li>
			<li >
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">Usuarios<span class="caret"></span></a>
				<ul class="dropdown-menu" role="menu">
					<li><a href="{{URL::action('AdminusuariosController@index')}}">Ver Usuarios</a></li>
					<li><a href="{{URL::action('AdminusuariosController@create')}}">Nuevo Usuario</a></li>
				</ul>
			</li>
			<li class="divider-vertical"></li>
			<li >
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">Opciones<span class="caret"></span></a>
				<ul class="dropdown-menu" role="menu">
					<li><a href="{{ URL::action('AreasInvestigacionController@index') }}">Areas de Investigaci&oacute;n</a></li>
					<li><a href="{{ URL::action('CategoriasController@index') }}">Categor&iacute;as</a></li>
					<li><a href="{{ URL::action('ExtDocsController@index') }}">Extensi&oacute;n de Documentos</a></li>
					<li><a href="{{ URL::action('TematicaController@index') }}">Tem&aacute;tica</a></li>
					<li><a href="{{ URL::action('TipoFechaController@index') }}">Tipos de Fechas</a></li>
				</ul>
			</li>
			<li class="divider-vertical"></li>
			<li >
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">Mensajes<span class="caret"></span></a>
				<ul class="dropdown-menu" role="menu">
					<li><a href="{{ URL::action('CorreosController@adminIndex') }}">Enviar Correo</a></li>
					<li><a href="{{ URL::action('CorreosController@adminMostrarMail') }}">Ver Historial</a></li>
				</ul>
			</li>
			<li class="divider-vertical"></li>
			<li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">Ayuda<span class="caret"></span></a>
				<ul class="dropdown-menu" role="menu">
					<li><a href="{{URL::asset('manuales/manual_admin.pdf')}}"  target="_blank">Manual de ayuda</a></li>
					<li><a href="{{URL::action('CongresoController@acercade')}}">Acerca De</a></li>
				</ul>
			</li>
		</ul>
	</div>
	@if($mostrar_buscar)
	<div class="col-md-4">
		<div class="input-group">
			<input type="text" class="form-control" placeholder="Buscar...">
			<span class="input-group-btn">
				<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search" ></span></button>
			</span>	
		</div>
	</div>
	@endif
</div>
