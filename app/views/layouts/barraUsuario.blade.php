<div class="row"  style="border-bottom: 1px solid #dddddd;">
	<div class="col-md-8">
		<ul class="nav nav-tabs" style="border: 0px;">
			<li ><a href="{{URL::action('HomeController@inicio')}}">
					<span class="glyphicon glyphicon-home"></span>
				</a>
			</li>
			<li class="divider-vertical"></li>
			<li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">Congresos<span class="caret"></span></a>
				<ul class="dropdown-menu" role="menu">
					<li><a href="{{URL::action('CongresoController@index')}}">Inscribir Congreso</a></li>
				</ul>
			</li>
			<li class="divider-vertical"></li>
			@if(Auth::guest())
				<li ><a href="{{URL::action('LoginController@index')}}">Iniciar Sesi&oacute;n</a></li>
				<li class="divider-vertical"></li>
			@endif
			<li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">Ayuda<span class="caret"></span></a>
				<ul class="dropdown-menu" role="menu">
					<li><a href="{{URL::asset('manuales/manual_usuario.pdf')}}"  target="_blank">Manual de ayuda</a></li>
					<li><a href="{{URL::action('CongresoController@acercade')}}">Acerca De</a></li>
				</ul>
			</li>
		</ul>
	</div>
	@if($mostrar_buscar)
	<div class="col-md-4">
		<div class="input-group">
			<input type="text" class="form-control" placeholder="Buscar...">
			<span class="input-group-btn">
				<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search" ></span></button>
			</span>	
		</div>
	</div>
	@endif
</div>