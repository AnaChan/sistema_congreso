@if (Auth::check())
<div class="row"  style="border-bottom: 1px solid #dddddd;">
	<div class="col-md-9">
		<ul class="nav nav-tabs" style="border: 0px;">
			<li ><a href="{{URL::action('HomeController@inicio')}}">
					<span class="glyphicon glyphicon-home"></span>
				</a>
			</li>
			<li class="divider-vertical"></li>
			<li ><a href="{{URL::action('UsuariosCongresosController@establecerCongreso',array($idCongreso))}}">Inicio Congreso</a></li>
			<li class="divider-vertical"></li>
			<li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">Someter a Revisi&oacute;n<span class="caret"></span></a>
				<ul class="dropdown-menu" role="menu">
					<li><a href="{{URL::action('SumissionController@index', array($idCongreso))}}">Subir Ficha</a></li>
					<li><a href="{{URL::action('SumissionController@indexFichaLista', array($idCongreso))}}">Subir Art&iacute;culo</a></li>
					<li><a href="{{URL::action('SumissionController@listarFichas', array($idCongreso))}}">Editar Ficha</a></li>
					<li><a href="{{URL::action('SumissionController@listarArticulosEditar', array($idCongreso))}}">Editar Articulo</a></li>
					<li><a href="{{URL::action('SumissionController@indexPresentaciones', array($idCongreso))}}">Mis Presentaciones</a></li>
					<li><a href="{{URL::action('SumissionController@indexFuentes', array($idCongreso))}}">Archivos Fuentes</a></li>
				</ul>
			</li>
			<li class="divider-vertical"></li>
			<li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">Revisi&oacute;n<span class="caret"></span></a>
				<ul class="dropdown-menu" role="menu">
					<li><a href="{{ URL::action('RevisarFichaController@listarFichas', array($idCongreso))}}">Ficha</a></li>

					<li><a href="{{ URL::action('RevisarPaperController@listarPaper', array($idCongreso))}}">Art&iacute;culo</a></li>

				</ul>
			</li>
		</ul>
	</div>
	@if ($mostrar_buscar)
	<div class="col-md-4">
		<div class="input-group">
			<input type="text" class="form-control" placeholder="Buscar...">
			<span class="input-group-btn">
				<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search" ></span></button>
			</span>	
		</div>
	</div>
	@endif
</div>

@endif
