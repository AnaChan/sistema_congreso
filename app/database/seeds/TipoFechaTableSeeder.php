<?php
class TipoFechaTableSeeder extends Seeder {

	public function run()
	{
		DB::table('tipo_fecha')->delete();
		TipoFecha::create(['nomTipoFecha' => 'Llamado a ponencias','valorTipoFecha' => 'Llamado a ponencias']);
		TipoFecha::create(['nomTipoFecha' => 'Recepción de Artículos','valorTipoFecha' => 'Recepción de Artículos']);
		TipoFecha::create(['nomTipoFecha' => 'Confirmación de aceptación de ponencias','valorTipoFecha' => 'Confirmación de aceptación de ponencias']);
		TipoFecha::create(['nomTipoFecha' => 'Confirmación de aceptación de Artículos','valorTipoFecha' => 'Confirmación de aceptación de Artículos']);
		TipoFecha::create(['nomTipoFecha' => 'Recepción de presentación y documentos de apoyo','valorTipoFecha' => 'Recepción de presentación y documentos de apoyo']);
		TipoFecha::create(['nomTipoFecha' => 'Realización del Congreso','valorTipoFecha' => 'Realización del Congreso']);
		
	}

}
