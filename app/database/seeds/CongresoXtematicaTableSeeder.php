<?php
// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class CongresoXTematicaTableSeeder extends Seeder 
{

	public function run()
	{
		DB::table('congreso_x_tematica')->delete();
		$faker = Faker::create('es_MX');
		$faker->seed(1234);
		
		/*$debug = Config::get('app.debug');
		if($debug)
		{
			CongresoXTematica::create
			([
				'idCongreso' => 3,
				'idTematica' => 1
			]);

			CongresoXTematica::create
			([
				'idCongreso' => 3,
				'idTematica' => 2
			]);
		}
		*/
	}

}