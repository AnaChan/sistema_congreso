<?php
class RolTableSeeder extends Seeder {

	public function run()
	{
		DB::table('rol')->delete();
		Rol::create(['nomRol' => 'Chair']);
		Rol::create(['nomRol' => 'Revisor']);
		Rol::create(['nomRol' => 'Autor']);
		Rol::create(['nomRol' => 'PC']);
		
	}

}