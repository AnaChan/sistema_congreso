<?php
class ExtensionDocumentoTableSeeder extends Seeder {

	public function run()
	{
		DB::table('extension_documento')->delete();
		ExtensionDocumento::create(['nomExtension' => 'PDF','tipoExtension' => '.pdf']);
		ExtensionDocumento::create(['nomExtension' => 'PPT','tipoExtension' => '.ppt']);
		ExtensionDocumento::create(['nomExtension' => 'DOC','tipoExtension' => '.doc']);
		ExtensionDocumento::create(['nomExtension' => 'DOCX','tipoExtension' => '.docx']);
		ExtensionDocumento::create(['nomExtension' => 'XLS','tipoExtension' => '.xls']);
		ExtensionDocumento::create(['nomExtension' => 'XML','tipoExtension' => '.xml']);
		
	}

}
