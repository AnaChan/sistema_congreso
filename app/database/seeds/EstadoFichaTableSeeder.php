<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class EstadoFichaTableSeeder extends Seeder {

	public function run()
	{
		DB::table('estado_ficha')->delete();
		
		EstadoFicha::create
		([
			'nombreEstado' 	=> 	'PENDIENTE',
			'valorEstado'	=>	'PEN'
		]);
		EstadoFicha::create
		([
			'nombreEstado' 	=> 	'APROBADA',
			'valorEstado'	=>	'APR'
		]);
		EstadoFicha::create
		([
			'nombreEstado' 	=> 	'RECHAZADA',
			'valorEstado'	=>	'REC'
		]);		
	}
}

