<?php
class UsuariosTableSeeder extends Seeder {
	public function run()
	{
		DB::table('usuarios')->delete();
		/*Creación del Usuario Administrador*/
		Usuario::create([
			'nombreUsuario' => 'Administrador',
			'apelUsuario' => 'CONUCA',
			'passwordUsuario' => Hash::make('cualquiera'),
			'emailUsuario' => 'conuca@uca.edu.sv',
			'idEstadoUsuario' => EstadoUsuario::Where('valorEstado','=','ADM')->first()->idEstadoUsuario,
			'fecModUsuario' => date('Y-m-d H:i:s')
			]);
	}
}