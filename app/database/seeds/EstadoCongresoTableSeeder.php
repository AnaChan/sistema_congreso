<?php
class EstadoCongresoTableSeeder extends Seeder {

	public function run()
	{
		DB::table('estado_congreso')->delete();
		EstadoCongreso::create(['nombreEstado' => 'INSCRITO','valorEstado' => 'INS','descripcion'=>'']);
		EstadoCongreso::create(['nombreEstado' => 'SIN CONFIGURAR','valorEstado' => 'SNC','descripcion'=>'']);
		EstadoCongreso::create(['nombreEstado' => 'RECHAZADO','valorEstado' => 'REC','descripcion'=>'']);
		EstadoCongreso::create(['nombreEstado' => 'ACTIVO','valorEstado' => 'ACT','descripcion'=>'']);
		EstadoCongreso::create(['nombreEstado' => 'CERRADO','valorEstado' => 'CER','descripcion'=>'']);

	}

}
