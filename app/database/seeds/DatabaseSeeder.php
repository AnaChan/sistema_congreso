<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		$this->call('EstadoUsuarioTableSeeder');
		$this->call('EstadoCongresoTableSeeder');
		$this->call('EstadoURCTableSeeder');
		$this->call('RolTableSeeder');
		$this->call('ExtensionDocumentoTableSeeder');
		$this->call('TipoFechaTableSeeder');
		$this->call('CategoriaTableSeeder');
		$this->call('ExtensionDocumentoTableSeeder');
		$this->call('EstadoPaperTableSeeder');
		$this->call('EstadoFichaTableSeeder');
		$this->call('EstadoPresentacionTableSeeder');
		$this->call('AreaInvestigacionTableSeeder');
		$this->call('AgendaTableSeeder');
		$this->call('UsuariosTableSeeder');		

		//arodriguez
		$debug = Config::get('app.debug');
		if($debug)
		{
			$this->call('CongresoTableSeeder');
			$this->call('UsuariosTableSeeder');
			$this->call('DetalleCongresoTableSeeder');
			$this->call('TematicaTableSeeder');
			$this->call('SesionTableSeeder');
			$this->call('FichaTableSeeder');
			$this->call('PresentacionTableSeeder');
			$this->call('AutorXFichaTableSeeder');
			$this->call('ActividadTableSeeder');
			$this->call('UsuariorolXCongresoTableSeeder');
			$this->call('PcXTematicaTableSeeder');
			$this->call('AulaTableSeeder');
		}

	}
}
