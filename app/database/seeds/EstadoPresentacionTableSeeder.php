<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class EstadoPresentacionTableSeeder extends Seeder {

	public function run()
	{
		DB::table('estado_presentacion')->delete();
		$faker = Faker::create('es_MX');
		$faker->seed(1234);

		//DB::table('estado_presentacion')->truncate();
		
		foreach(range(1, 10) as $index)
		{
			EstadoPresentacion::create
			([
				'nomEstado' => $faker->word
			]);
		}
	}

}

