<?php
// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class DetalleCongresoXExtensionTableSeeder extends Seeder 
{

	public function run()
	{
		DB::table('detallecongreso_x_extension')->delete();
		$faker = Faker::create('es_MX');
		$faker->seed(1234);
		
		
			DetalleCongresoXExtension::create
			([
				'idDetalleCongreso' => 3,
				'idExtensionDocumento' => 1
			]);
		
	}

}