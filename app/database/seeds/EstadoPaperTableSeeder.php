<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class EstadoPaperTableSeeder extends Seeder {

	public function run()
	{
		DB::table('estado_paper')->delete();
		
		
			EstadoPaper::create
			([
				'nombreEstado' => 'PENDIENTE',
				'valorEstado'	=>	'PEN'
			]);
			EstadoPaper::create
			([
				'nombreEstado' => 'APROBADA',
				'valorEstado'	=>	'APR'
			]);
			EstadoPaper::create
			([
				'nombreEstado' => 'RECHAZADA',
				'valorEstado'	=>	'REC'
			]);		
	}

}