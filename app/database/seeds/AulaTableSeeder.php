<?php
// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;
//arodriguez.05.19

class AulaTableSeeder extends Seeder
 {

	public function run()
	{
		DB::table('aula')->delete();
		$faker = Faker::create('es_MX');
		$faker->seed(1234);

		$debug = Config::get('app.debug');
		if($debug)
		{

			Aula::create
			([
				//'idTematica' => 6,
				'idDetalleCongreso' => 3,
				'nomAula' => 'A-10',
				'tipoAula' => 'Aula Grande'
			]);

			Aula::create
			([
				//'idTematica' => 6,
				'idDetalleCongreso' => 3,
				'nomAula' => 'A-11',
				'tipoAula' => 'Aula Grande'
			]);
			Aula::create
			([
				//'idTematica' => 6,
				'idDetalleCongreso' => 3,
				'nomAula' => 'B-12',
				'tipoAula' => 'Aula Grande'
			]);
		}
		
	}

}
