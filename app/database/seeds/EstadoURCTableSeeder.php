<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class EstadoURCTableSeeder extends Seeder {

	public function run()
	{
		DB::table('estado_urc')->delete();
		
		
			EstadoURC::create
			([
				'nombreEstado'	=> 	'INACTIVO',
				'valorEstado'	=>	'INA'
			]);
			EstadoURC::create
			([
				'nombreEstado'	=>	'ACTIVO',
				'valorEstado'	=>	'ACT'
			]);
			EstadoURC::create
			([
				'nombreEstado'	=>	'INVITADO',
				'valorEstado'	=>	'INV'
			]);		
			EstadoURC::create
			([
				'nombreEstado' 	=>	'POR APROBAR',
				'valorEstado'	=>	'POR'
			]);
			EstadoURC::create
			([
				'nombreEstado' 	=>	'SOLICITADO',
				'valorEstado'	=>	'SOL'
			]);
	}

}

