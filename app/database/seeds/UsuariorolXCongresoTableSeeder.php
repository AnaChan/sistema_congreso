<?php
// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class UsuariorolXCongresoTableSeeder extends Seeder 
{

	public function run()
	{
		DB::table('usuariorol_x_congreso')->delete();

		UsuariorolXCongreso::create
			([
				'idUsuario' => 2,
				'idRol' => 1,
				'idCongreso' => 3,
				'idEstadoURC' => 1,
			]);

		$debug = Config::get('app.debug');
		if($debug)
		{
		
			//arodriguez
			UsuariorolXCongreso::create
				([
					'idUsuario' => 3,
					'idRol' => 4,
					'idCongreso' => 3,
					'idEstadoURC' => 1,
				]);

			//arodriguez
			UsuariorolXCongreso::create
				([
					'idUsuario' => 4,
					'idRol' => 4,
					'idCongreso' => 3,
				'idEstadoURC' => 1,
			]);
		}
	}

}

