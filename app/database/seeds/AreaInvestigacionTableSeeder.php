<?php
//arodriguez.05.19
// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class AreaInvestigacionTableSeeder extends Seeder {

	public function run()
	{
		DB::table('area_investigacion')->delete();
		$faker = Faker::create('es_MX');
		$faker->seed(1234);

		//foreach(range(1, 15) as $index)
		//{
			AreaInvestigacion::create
			([
				'nomAreaInvestigacion' => 'Electronica',
				'fecCreaAreaInvestigacion' => date('Y-m-d H:i:s'),
				'fecModAreaInvestigacion' => date('Y-m-d H:i:s')
			]);
			
			AreaInvestigacion::create
			([
				'nomAreaInvestigacion' => 'Informatica',
				'fecCreaAreaInvestigacion' => date('Y-m-d H:i:s'),
				'fecModAreaInvestigacion' => date('Y-m-d H:i:s')
			]);

			AreaInvestigacion::create
			([
				'nomAreaInvestigacion' => 'Ciencias Ambientales',
				'fecCreaAreaInvestigacion' => date('Y-m-d H:i:s'),
				'fecModAreaInvestigacion' => date('Y-m-d H:i:s')
			]);
		//}
	}

}