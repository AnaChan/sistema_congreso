<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class SesionTableSeeder extends Seeder 
{

	public function run()
	{
		DB::table('sesion')->delete();
		$faker = Faker::create('es_MX');
		$faker->seed(1234);

		//DB::table('usuarios')->truncate();
		
		Sesion::create
			([
				'idSesion' => 1,
				'fecSesion' => date('Y-m-d H:i:s'),
				'moderador' => "Moderador",
				'duracionSesion' => rand(5,10),
				'horaInicio' => "09:00:00",
				'horaFin' => "12:00:00",
				'idTematica' => 0,
				'idCongreso' => 0,
				'nombreSesion' => 'Sesion 1'
			]);
/*
		//sesiones de prueba
			Sesion::create
			([
				'idSesion' => 2,
				'fecSesion' => date('Y-m-d H:i:s'),
				'moderador' => "Moderador",
				'duracionSesion' => rand(1,10),
				'horaInicio' =>"07:00:00",
				'horaFin' => "15:00:00",
				'idTematica' => Tematica::all()->random()->idTematica,
				'idCongreso' => 3,
				'nombreSesion' => 'Sesion 1'
			]);

			Sesion::create
			([
				'idSesion' => 3,
				'fecSesion' => date('Y-m-d H:i:s'),
				'moderador' => "Moderador",
				'duracionSesion' => rand(1,10),
				'horaInicio' => rand(1,23),
				'horaFin' => rand(1,23),
				'idTematica' => Tematica::all()->random()->idTematica,
				'idCongreso' => 3,
				'nombreSesion' => 'Sesion 1'
			]);

		*/
	}

}