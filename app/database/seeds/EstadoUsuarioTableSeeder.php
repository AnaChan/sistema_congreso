<?php
class EstadoUsuarioTableSeeder extends Seeder {

	public function run()
	{
		EstadoUsuario::create(['nombreEstado' => 'ACTIVO', 'valorEstado' => 'ACT']);
		EstadoUsuario::create(['nombreEstado' => 'ADMIN', 'valorEstado' => 'ADM']);
		EstadoUsuario::create(['nombreEstado' => 'CONFIRMAR', 'valorEstado' => 'CON']);
	}

}
