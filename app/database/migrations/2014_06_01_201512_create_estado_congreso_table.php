<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEstadoCongresoTable extends Migration {

	public function up()
	{
		Schema::create('estado_congreso', function(Blueprint $table) {
			$table->increments('idEstadoCongreso');
			$table->string('nombreEstado');
			$table->string('valorEstado');
			$table->string('descripcion');
			$table->timestamp('fecCreacionEstadoCongreso');
			$table->timestamp('fecModEstadoCongreso');
		});
	}

	public function down()
	{
		Schema::drop('estado_congreso');
	}
}