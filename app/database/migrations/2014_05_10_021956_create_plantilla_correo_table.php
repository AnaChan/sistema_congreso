<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlantillaCorreoTable extends Migration {

	public function up()
	{
		Schema::create('plantilla_correo', function(Blueprint $table) {
			$table->increments('idPlantillaCorreo');
			$table->string('nomPlantilla');
			$table->integer('idDetalleCongreso')->unsigned();
			$table->text('cuerpoPlantillaCorreo');
		});
	}

	public function down()
	{
		Schema::drop('plantilla_correo');
	}
}