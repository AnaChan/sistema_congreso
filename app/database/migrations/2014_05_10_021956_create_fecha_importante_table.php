<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFechaImportanteTable extends Migration {

	public function up()
	{
		Schema::create('fecha_importante', function(Blueprint $table) {
			$table->increments('idFechaImportante');
			$table->timestamp('fecInicio');
			$table->timestamp('fecFin');
			$table->integer('idTipoFecha')->unsigned();
			$table->integer('idDetalleCongreso')->unsigned();
			$table->timestamps();
			$table->unique(array('idTipoFecha', 'idDetalleCongreso'));
		});
	}

	public function down()
	{
		Schema::drop('fecha_importante');
	}
}