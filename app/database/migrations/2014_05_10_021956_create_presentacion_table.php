<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePresentacionTable extends Migration {

	public function up()
	{
		Schema::create('presentacion', function(Blueprint $table) {
			$table->increments('idPresentacion');
			$table->string('rutaPresentacion');
			$table->integer('tamPresentacion');
			$table->timestamp('created_at');
			$table->timestamp('updated_at');
			$table->integer('idAgenda')->unsigned();
			$table->integer('idSesion')->unsigned();
			$table->integer('idFicha')->unsigned();
			$table->integer('idEstadoPresentacion')->unsigned();

		});
	}

	public function down()
	{
		Schema::drop('presentacion');
	}
}