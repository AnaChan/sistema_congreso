<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEstadoPresentacionTable extends Migration {

	public function up()
	{
		Schema::create('estado_presentacion', function(Blueprint $table) {
			$table->increments('idEstadoPresentacion');
			$table->string('nomEstado');
			$table->timestamp('created_at');
			$table->timestamp('updated_at');

		});
	}

	public function down()
	{
		Schema::drop('estado_presentacion');
	}
}