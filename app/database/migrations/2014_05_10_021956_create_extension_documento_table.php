<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExtensionDocumentoTable extends Migration {

	public function up()
	{
		Schema::create('extension_documento', function(Blueprint $table) {
			$table->increments('idExtensionDocumento');
			$table->string('nomExtension');
			$table->string('tipoExtension');
		});
	}

	public function down()
	{
		Schema::drop('extension_documento');
	}
}