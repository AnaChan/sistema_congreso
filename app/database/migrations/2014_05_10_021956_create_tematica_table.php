<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTematicaTable extends Migration {

	public function up()
	{
		Schema::create('tematica', function(Blueprint $table) {
			$table->increments('idTematica');
			$table->string('nomTematica');
			$table->integer('idDetalleCongreso')->unsigned();
			$table->integer('idAreaInvestigacion')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('tematica');
	}
}