<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCongresoTable extends Migration {

	public function up()
	{
		Schema::create('congreso', function(Blueprint $table) {
			$table->increments('idCongreso');
			$table->string('nomCongreso');
			$table->string('acronimoCongreso');
			$table->string('webCongreso')->nullable();
			$table->string('ciudad');
			$table->string('pais');
			$table->timestamp('fecIniCongreso');
			$table->timestamp('fecFinCongreso');
			$table->timestamp('fecModCongreso');
			$table->timestamp('fecCreaCongreso')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->string('emailOrgCongreso');
			$table->text('comentarioCongreso')->nullable();
			$table->integer('idEstadoCongreso')->unsigned();
			$table->integer('idCreador')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('congreso');
	}
}