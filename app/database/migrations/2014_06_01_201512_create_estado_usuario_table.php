<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEstadoUsuarioTable extends Migration {

	public function up()
	{
		Schema::create('estado_usuario', function(Blueprint $table) {
			$table->increments('idEstadoUsuario');
			$table->string('nombreEstado')->nullable();
			$table->string('valorEstado')->nullable();
			$table->string('descripcion')->nullable();
			$table->timestamp('fecCreacionEstadoUsuario')->nullable();
			$table->timestamp('fecModEstadoUsuario')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('estado_usuario');
	}
}