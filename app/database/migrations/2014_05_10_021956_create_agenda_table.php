<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAgendaTable extends Migration {

	public function up()
	{
		Schema::create('agenda', function(Blueprint $table) {
			$table->increments('idAgenda');
			$table->string('titulo');
			$table->timestamp('fecha');
			$table->time('horaInicio');
			$table->time('horaFin');
			$table->string('aulaLabel');
			$table->boolean('esActividad');
			$table->string('idElementoHtml');
			$table->integer('idAula')->unsigned();
			$table->integer('idCongreso');
			$table->integer('idTematica');
			$table->integer('idSesion');
			$table->timestamp('created_at');
			$table->timestamp('updated_at');
			//true nose puede agendar nada mas para la fecha, hora establecida 
		});
	}

	public function down()
	{
		Schema::drop('actividad');
	}
}