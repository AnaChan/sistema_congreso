<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCriterioEvaluacionTable extends Migration {

	public function up()
	{
		Schema::create('criterio_evaluacion', function(Blueprint $table) {
			$table->increments('idCriterioEvaluacion');
			$table->string('nombreCriterio');
			$table->boolean('criterioGrupal');
			$table->integer('idDetalleCongreso')->unsigned();
			$table->integer('idCriterioEvaluacionPadre')->unsigned()->nullable();
			$table->string('indicadorFicha');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('criterio_evaluacion');
	}
}