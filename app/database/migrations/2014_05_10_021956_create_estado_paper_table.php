<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEstadoPaperTable extends Migration {

	public function up()
	{
		Schema::create('estado_paper', function(Blueprint $table) {
			$table->increments('idEstadoPaper');
			$table->string('nombreEstado');
			$table->string('valorEstado');
			$table->timestamp('created_at');
			$table->timestamp('updated_at');

		});
	}

	public function down()
	{
		Schema::drop('estado_paper');
	}
}