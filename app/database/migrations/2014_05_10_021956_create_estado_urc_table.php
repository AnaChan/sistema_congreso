<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEstadoURCTable extends Migration {

	public function up()
	{
		Schema::create('estado_urc', function(Blueprint $table) {
			$table->increments('idEstadoURC');
			$table->string('nombreEstado');
			$table->string('valorEstado', 3);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('estado_urc');
	}
}