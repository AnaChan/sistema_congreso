<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsuariorolXCongresoTable extends Migration {

	public function up()
	{
		Schema::create('usuariorol_x_congreso', function(Blueprint $table) {
			$table->increments('idUsuariorolXCongreso');
			$table->integer('idUsuario')->unsigned();
			$table->integer('idRol')->unsigned();
			$table->integer('idCongreso')->unsigned();
			$table->integer('idEstadoURC')->unsigned();
			$table->timestamps();
			$table->unique(array('idUsuario','idRol','idCongreso'));
		});
	}

	public function down()
	{
		Schema::drop('usuariorol_x_congreso');
	}
}