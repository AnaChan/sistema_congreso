<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActividadTable extends Migration {

	public function up()
	{
		Schema::create('actividad', function(Blueprint $table) {
			$table->increments('idActividad');
			$table->string('responsableAct');
			$table->string('titulo');
			$table->timestamp('created_at');
			$table->timestamp('updated_at');
			$table->integer('idAgenda')->unsigned();
			$table->integer('idAula')->unsigned();
			$table->integer('idCongreso')->unsigned();
			$table->boolean('agendado');
		});
	}

	public function down()
	{
		Schema::drop('actividad');
	}
}