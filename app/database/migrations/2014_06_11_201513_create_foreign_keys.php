<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForeignKeys extends Migration 
{
	
	public function up()
	{
		Schema::table('detalle_congreso', function(Blueprint $table) {
			$table->foreign('idCongreso')->references('idCongreso')->on('congreso')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('revision_ficha', function(Blueprint $table) {
			$table->foreign('idUsuario')->references('idUsuario')->on('usuarios')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('revision_ficha', function(Blueprint $table) {
			$table->foreign('idFicha')->references('idFicha')->on('ficha')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('ficha', function(Blueprint $table) {
			$table->foreign('idDetalleCongreso')->references('idDetalleCongreso')->on('detalle_congreso')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('autor_x_ficha', function(Blueprint $table) {
			$table->foreign('idUsuario')->references('idUsuario')->on('usuarios')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('autor_x_ficha', function(Blueprint $table) {
			$table->foreign('idFicha')->references('idFicha')->on('ficha')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('detallecongreso_x_categoria', function(Blueprint $table) {
			$table->foreign('idDetalleCongreso')->references('idDetalleCongreso')->on('detalle_congreso')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('detallecongreso_x_categoria', function(Blueprint $table) {
			$table->foreign('idCategoria')->references('idCategoria')->on('categoria')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('tematica', function(Blueprint $table) {
			$table->foreign('idAreaInvestigacion')->references('idAreaInvestigacion')->on('area_investigacion')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('tematica_x_areainvestigacion', function(Blueprint $table) {
			$table->foreign('idAreaInvestigacion')->references('idAreaInvestigacion')->on('area_investigacion')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('tematica_x_areainvestigacion', function(Blueprint $table) {
			$table->foreign('idTematica')->references('idTematica')->on('tematica')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('usuario_x_areainvestigacion', function(Blueprint $table) {
			$table->foreign('idUsuario')->references('idUsuario')->on('usuarios')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('usuario_x_areainvestigacion', function(Blueprint $table) {
			$table->foreign('idAreaInvestigacion')->references('idAreaInvestigacion')->on('area_investigacion')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('usuariorol_x_congreso', function(Blueprint $table) {
			$table->foreign('idRol')->references('idRol')->on('rol')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('usuariorol_x_congreso', function(Blueprint $table) {
			$table->foreign('idCongreso')->references('idCongreso')->on('congreso')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('usuariorol_x_congreso', function(Blueprint $table) {
			$table->foreign('idEstadoURC')->references('idEstadoURC')->on('estado_urc')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('usuariorol_x_congreso', function(Blueprint $table) {
			$table->foreign('idUsuario')->references('idUsuario')->on('usuarios')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('fecha_importante', function(Blueprint $table) {
			$table->foreign('idTipoFecha')->references('idTipoFecha')->on('tipo_fecha')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('fecha_importante', function(Blueprint $table) {
			$table->foreign('idDetalleCongreso')->references('idDetalleCongreso')->on('detalle_congreso')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('revision_paper', function(Blueprint $table) {
			$table->foreign('idPaper')->references('idPaper')->on('paper')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('revision_paper', function(Blueprint $table) {
			$table->foreign('idUsuario')->references('idUsuario')->on('usuarios')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('revision_x_criterio', function(Blueprint $table) {
			$table->foreign('idRevision')->references('idRevisionPaper')->on('revision_paper')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('revision_x_criterio', function(Blueprint $table) {
			$table->foreign('idCriterio')->references('idCriterioEvaluacion')->on('criterio_evaluacion')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('area_x_ficha', function(Blueprint $table) {
			$table->foreign('idAreaInvestigacion')->references('idAreaInvestigacion')->on('area_investigacion')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('area_x_ficha', function(Blueprint $table) {
			$table->foreign('idFicha')->references('idFicha')->on('ficha')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('presentacion', function(Blueprint $table) {
			$table->foreign('idFicha')->references('idFicha')->on('ficha')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		//Schema::table('presentacion', function(Blueprint $table) {
		//	$table->foreign('idSesion')->references('idSesion')->on('sesion')
		//				->onDelete('cascade')
		//				->onUpdate('restrict');
		//});
		Schema::table('paper', function(Blueprint $table) {
			$table->foreign('idFicha')->references('idFicha')->on('ficha')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('paper', function(Blueprint $table) {
			$table->foreign('idEstadoPaper')->references('idEstadoPaper')->on('estado_paper')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('aula', function(Blueprint $table) {
			$table->foreign('idDetalleCongreso')->references('idDetalleCongreso')->on('detalle_congreso')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('sesion', function(Blueprint $table) {
			$table->foreign('idTematica')->references('idTematica')->on('tematica')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		//TODO
		Schema::table('actividad', function(Blueprint $table) {
			$table->foreign('idCongreso')->references('idCongreso')->on('congreso')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		//TODO
		Schema::table('actividad', function(Blueprint $table) {
			$table->foreign('idAgenda')->references('idAgenda')->on('agenda')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('tipo_sumision', function(Blueprint $table) {
			$table->foreign('idDetalleCongreso')->references('idDetalleCongreso')->on('detalle_congreso')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('puntaje', function(Blueprint $table) {
			$table
			->foreign('idCriterioEvaluacion')
			->references('idCriterioEvaluacion')
			->on('criterio_evaluacion')
			->onDelete('cascade')
			->onUpdate('restrict');
		});
		Schema::table('detallecongreso_x_extension', function(Blueprint $table) {
			$table->foreign('idDetalleCongreso')->references('idDetalleCongreso')->on('detalle_congreso')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('detallecongreso_x_extension', function(Blueprint $table) {
			$table->foreign('idExtensionDocumento')->references('idExtensionDocumento')->on('extension_documento')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('congreso_x_tematica', function(Blueprint $table) {
			$table->foreign('idCongreso')->references('idCongreso')->on('congreso')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('congreso_x_tematica', function(Blueprint $table) {
			$table->foreign('idTematica')->references('idTematica')->on('tematica')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('congreso', function(Blueprint $table) {
			$table->foreign('idCreador')->references('idUsuario')->on('usuarios')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('plantilla_correo', function(Blueprint $table) {
			$table->foreign('idDetalleCongreso')->references('idDetalleCongreso')->on('detalle_congreso')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('criterio_evaluacion', function(Blueprint $table) {
			$table
			->foreign('idDetalleCongreso')
			->references('idDetalleCongreso')
			->on('detalle_congreso')
			->onDelete('cascade')
			->onUpdate('restrict');
			
			$table
			->foreign('idCriterioEvaluacionPadre')
			->references('idCriterioEvaluacion')
			->on('criterio_evaluacion')
			->onDelete('cascade')
			->onUpdate('restrict');
		});
		Schema::table('revision_ficha_x_criterio', function(Blueprint $table) {
			$table
			->foreign('idRevisionFicha')
			->references('idRevisionFicha')
			->on('revision_ficha')
			->onDelete('cascade')
			->onUpdate('restrict');

			$table
			->foreign('idPuntaje')
			->references('idPuntaje')
			->on('puntaje')
			->onDelete('cascade')
			->onUpdate('restrict');
		});

		Schema::table('revision_paper_x_criterio', function(Blueprint $table) {
			$table
			->foreign('idRevisionPaper')
			->references('idRevisionPaper')
			->on('revision_paper')
			->onDelete('cascade')
			->onUpdate('restrict');

			$table
			->foreign('idPuntaje')
			->references('idPuntaje')
			->on('puntaje')
			->onDelete('cascade')
			->onUpdate('restrict');
		});

		Schema::table('usuarios', function(Blueprint $table) {
			$table->foreign('idEstadoUsuario')->references('idEstadoUsuario')->on('estado_usuario')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('congreso', function(Blueprint $table) {
			$table->foreign('idEstadoCongreso')->references('idEstadoCongreso')->on('estado_congreso')
						->onDelete('cascade')
						->onUpdate('restrict');
		});

				//arodriguez
		//Schema::table('agenda', function(Blueprint $table) {
			//$table->foreign('idAula')->references('idAula')->on('aula')
						//->onDelete('restrict')
						//->onUpdate('restrict');
		//});
		Schema::table('ficha', function(Blueprint $table) {
			$table->foreign('idTematica')->references('idTematica')->on('tematica')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('ficha', function(Blueprint $table) {
			$table->foreign('idEstadoFicha')->references('idEstadoFicha')->on('estado_ficha')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('ficha', function(Blueprint $table) {
			$table->foreign('idCategoria')->references('idCategoria')->on('categoria')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('presentacion', function(Blueprint $table) {
			$table->foreign('idAgenda')->references('idAgenda')->on('agenda')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('presentacion', function(Blueprint $table) {
			$table->foreign('idEstadoPresentacion')->references('idEstadoPresentacion')->on('estado_presentacion')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		/*
		Schema::table('actividad', function(Blueprint $table) {
			$table->foreign('idAgenda')->references('idAgenda')->on('agenda')
						->onDelete('restrict')
						->onUpdate('restrict');
		});*/
		Schema::table('area_interes', function(Blueprint $table) {
			$table->foreign('idUsuario')->references('idUsuario')->on('usuarios')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('area_interes', function(Blueprint $table) {
			$table->foreign('idTematica')->references('idTematica')->on('tematica')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('pc_x_tematica', function(Blueprint $table) {
			$table->foreign('idTematica')->references('idTematica')->on('tematica')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('pc_x_tematica', function(Blueprint $table) {
			$table->foreign('idUsuariorolXCongreso')->references('idUsuariorolXCongreso')->on('usuariorol_x_congreso')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('pc_x_revisor_area', function(Blueprint $table) {
			$table->foreign('idPcRevisor')->references('idPcTematica')->on('pc_x_tematica')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
		Schema::table('pc_x_revisor_area', function(Blueprint $table) {
			$table->foreign('idTematica')->references('idTematica')->on('tematica')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('pc_x_revisor_area', function(Blueprint $table) {
			$table->foreign('idUsuario')->references('idUsuario')->on('usuarios')
						->onDelete('cascade')
						->onUpdate('restrict');
		});
	
	}

	public function down()
	{
		Schema::table('detalle_congreso', function(Blueprint $table) {
			$table->dropForeign('detalle_congreso_idCongreso_foreign');
		});
		Schema::table('revision_ficha', function(Blueprint $table) {
			$table->dropForeign('revision_ficha_idUsuario_foreign');
		});
		Schema::table('revision_ficha', function(Blueprint $table) {
			$table->dropForeign('revision_ficha_idFicha_foreign');
		});
		Schema::table('ficha', function(Blueprint $table) {
			$table->dropForeign('ficha_idDetalleCongreso_foreign');
		});
		Schema::table('autor_x_ficha', function(Blueprint $table) {
			$table->dropForeign('autor_x_ficha_idUsuario_foreign');
		});
		Schema::table('autor_x_ficha', function(Blueprint $table) {
			$table->dropForeign('autor_x_ficha_idFicha_foreign');
		});
		Schema::table('detallecongreso_x_categoria', function(Blueprint $table) {
			$table->dropForeign('detallecongreso_x_categoria_idDetalleCongreso_foreign');
		});
		Schema::table('detallecongreso_x_categoria', function(Blueprint $table) {
			$table->dropForeign('detallecongreso_x_categoria_idCategoria_foreign');
		});
		Schema::table('tematica', function(Blueprint $table) {
			$table->dropForeign('tematica_idAreaInvestigacion_foreign');
		});
		Schema::table('tematica_x_areainvestigacion', function(Blueprint $table) {
			$table->dropForeign('tematica_x_areainvestigacion_idAreaInvestigacion_foreign');
		});
		Schema::table('tematica_x_areainvestigacion', function(Blueprint $table) {
			$table->dropForeign('tematica_x_areainvestigacion_idTematica_foreign');
		});
		Schema::table('usuario_x_areainvestigacion', function(Blueprint $table) {
			$table->dropForeign('usuario_x_areainvestigacion_idUsuario_foreign');
		});
		Schema::table('usuario_x_areainvestigacion', function(Blueprint $table) {
			$table->dropForeign('usuario_x_areainvestigacion_idAreaInvestigacion_foreign');
		});
		Schema::table('usuariorol_x_congreso', function(Blueprint $table) {
			$table->dropForeign('usuariorol_x_congreso_idRol_foreign');
		});
		Schema::table('usuariorol_x_congreso', function(Blueprint $table) {
			$table->dropForeign('usuariorol_x_congreso_idCongreso_foreign');
		});
		Schema::table('usuariorol_x_congreso', function(Blueprint $table) {
			$table->dropForeign('usuariorol_x_congreso_idEstadoURC_foreign');
		});
		Schema::table('fecha_importante', function(Blueprint $table) {
			$table->dropForeign('fecha_importante_idTipoFecha_foreign');
		});
		Schema::table('fecha_importante', function(Blueprint $table) {
			$table->dropForeign('fecha_importante_idDetalleCongreso_foreign');
		});
		Schema::table('revision_paper', function(Blueprint $table) {
			$table->dropForeign('revision_paper_idPaper_foreign');
		});
		Schema::table('revision_paper', function(Blueprint $table) {
			$table->dropForeign('revision_paper_idUsuario_foreign');
		});
		Schema::table('revision_x_criterio', function(Blueprint $table) {
			$table->dropForeign('revision_x_criterio_idRevision_foreign');
		});
		Schema::table('revision_x_criterio', function(Blueprint $table) {
			$table->dropForeign('revision_x_criterio_idCriterio_foreign');
		});
		Schema::table('area_x_ficha', function(Blueprint $table) {
			$table->dropForeign('area_x_ficha_idAreaInvestigacion_foreign');
		});
		Schema::table('area_x_ficha', function(Blueprint $table) {
			$table->dropForeign('area_x_ficha_idFicha_foreign');
		});
		Schema::table('presentacion', function(Blueprint $table) {
			$table->dropForeign('presentacion_idFicha_foreign');
		});
		Schema::table('presentacion', function(Blueprint $table) {
			$table->dropForeign('presentacion_idSesion_foreign');
		});
		Schema::table('paper', function(Blueprint $table) {
			$table->dropForeign('paper_idFicha_foreign');
		});
		Schema::table('paper', function(Blueprint $table) {
			$table->dropForeign('paper_idEstadoPaper_foreign');
		});
		Schema::table('aula', function(Blueprint $table) {
			$table->dropForeign('aula_idDetalleCongreso_foreign');
		});
		Schema::table('sesion', function(Blueprint $table) {
			$table->dropForeign('sesion_idTematica_foreign');
		});
		//TODO
		Schema::table('actividad', function(Blueprint $table) {
			$table->dropForeign('actividad_idCongreso_foreign');
		});
		//TODO
		Schema::table('actividad', function(Blueprint $table) {
			$table->dropForeign('actividad_idAgenda_foreign');
		});
		Schema::table('tipo_sumision', function(Blueprint $table) {
			$table->dropForeign('tipo_sumision_idDetalleCongreso_foreign');
		});
		Schema::table('puntaje', function(Blueprint $table) {
			$table->dropForeign('puntaje_idDetalleCongreso_foreign');
		});
		Schema::table('detallecongreso_x_extension', function(Blueprint $table) {
			$table->dropForeign('detallecongreso_x_extension_idDetalleCongreso_foreign');
		});
		Schema::table('detallecongreso_x_extension', function(Blueprint $table) {
			$table->dropForeign('detallecongreso_x_extension_idExtensionDocumento_foreign');
		});
		Schema::table('congreso_x_tematica', function(Blueprint $table) {
			$table->dropForeign('congreso_x_tematica_idCongreso_foreign');
		});
		Schema::table('congreso_x_tematica', function(Blueprint $table) {
			$table->dropForeign('congreso_x_tematica_idTematica_foreign');
		});
		Schema::table('congreso', function(Blueprint $table) {
			$table->dropForeign('congreso_idCreador_foreign');
		});
		Schema::table('plantilla_correo', function(Blueprint $table) {
			$table->dropForeign('plantilla_correo_idDetalleCongreso_foreign');
		});
		Schema::table('criterio_evaluacion', function(Blueprint $table) {
			$table->dropForeign('criterio_evaluacion_idDetalleCongreso_foreign');
		});
		Schema::table('revision_ficha_x_criterio', function(Blueprint $table) {
			$table->dropForeign('revision_ficha_x_criterio_idRevisionFicha_foreign');
		});
		Schema::table('revision_ficha_x_criterio', function(Blueprint $table) {
			$table->dropForeign('revision_ficha_x_criterio_idCriterioEvaluacion_foreign');
		});
		Schema::table('revision_ficha_x_criterio', function(Blueprint $table) {
			$table->dropForeign('revision_ficha_x_criterio_idPuntaje_foreign');
		});		
		Schema::table('usuarios', function(Blueprint $table) {
			$table->dropForeign('usuarios_idEstadoUsuario_foreign');
		});
		Schema::table('congreso', function(Blueprint $table) {
			$table->dropForeign('congreso_idEstadoCongreso_foreign');
		});

		//arodriguez
		Schema::table('agenda', function(Blueprint $table) {
			$table->dropForeign('agenda_idAula_foreign');
		});
		Schema::table('ficha', function(Blueprint $table) {
					$table->dropForeign('ficha_idTematica_foreign');
		});
		Schema::table('ficha', function(Blueprint $table) {
					$table->dropForeign('ficha_idEstadoFicha_foreign');
		});
		Schema::table('ficha', function(Blueprint $table) {
					$table->dropForeign('ficha_idCategoria_foreign');
		});
		Schema::table('presentacion', function(Blueprint $table) {
			$table->dropForeign('presentacion_idAgenda_foreign');
		});
		Schema::table('presentacion', function(Blueprint $table) {
			$table->dropForeign('presentacion_idEstadoPresentacion_foreign');
		});
		/*
		Schema::table('actividad', function(Blueprint $table) {
			$table->dropForeign('actividad_idAgenda_foreign');
		});*/

		Schema::table('area_interes', function(Blueprint $table) {
			$table->dropForeign('area_interes_idUsuario_foreign');
		});

		Schema::table('area_interes', function(Blueprint $table) {
			$table->dropForeign('area_interes_idTematica_foreign');
		});

		Schema::table('pc_x_tematica', function(Blueprint $table) {
			$table->dropForeign('pc_x_tematica_idTematica_foreign');
		});

		Schema::table('pc_x_tematica', function(Blueprint $table) {
			$table->dropForeign('pc_x_tematica_idUsuariorolXCongreso_foreign');
		});

		Schema::table('pc_x_revisor_area', function(Blueprint $table) {
			$table->dropForeign('pc_x_revisor_area_idPcRevisor_foreign');
		});

		Schema::table('pc_x_revisor_area', function(Blueprint $table) {
			$table->dropForeign('pc_x_revisor_area_idTematica_foreign');
		});

		Schema::table('pc_x_revisor_area', function(Blueprint $table) {
			$table->dropForeign('pc_x_revisor_area_idUsuario_foreign');
		});
	}
}