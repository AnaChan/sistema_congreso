<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaperTable extends Migration {

	public function up()
	{
		Schema::create('paper', function(Blueprint $table) {
			$table->increments('idPaper');
			$table->integer('idFicha')->unsigned();
			$table->timestamp('created_at');
			$table->timestamp('updated_at');
			$table->string('rutaPaper');
			$table->integer('tamPaper');
			$table->integer('idEstadoPaper')->unsigned();
			$table->string('rutaFuentes');
		});
	}

	public function down()
	{
		Schema::drop('paper');
	}
}