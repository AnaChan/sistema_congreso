<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAutorXFichaTable extends Migration {

	public function up()
	{
		Schema::create('autor_x_ficha', function(Blueprint $table) {
			$table->increments('idFichaXAutor');
			$table->integer('idUsuario')->unsigned();
			$table->integer('idFicha')->unsigned();
			$table->boolean('responsable');
			$table->string('rutaDiploma');
			$table->timestamps();
			$table->unique(array('idUsuario', 'idFicha'));
		});
	}

	public function down()
	{
		Schema::drop('autor_x_ficha');
	}
}