<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRevisionXCriterioTable extends Migration {

	public function up()
	{
		Schema::create('revision_x_criterio', function(Blueprint $table) {
			$table->increments('idRevisionXCriterio');
			$table->integer('idRevision')->unsigned();
			$table->integer('idCriterio')->unsigned();
			$table->integer('valorAsignado');
			$table->integer('idPuntaje')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('revision_x_criterio');
	}
}