<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsuariosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 *
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('usuarios', function(Blueprint $table) {
			$table->increments('idUsuario');
			$table->string('nombreUsuario');
			$table->string('apelUsuario');
			$table->string('passwordUsuario', 60);
			$table->string('emailUsuario')->unique();
			$table->string('codConfirmarUsuario')->nullable();
			$table->integer('idEstadoUsuario')->unsigned();
			$table->string('recordarUsuario')->nullable();
			$table->timestamp('fecResUsuario')->nullable();
			$table->timestamp('fecModUsuario');
			$table->timestamp('fecCreaUsuario')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('ultimoAcceso');
			$table->text('resumenUsuario')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('usuarios');
	}

}
