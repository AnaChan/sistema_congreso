<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTipoSumisionTable extends Migration {

	public function up()
	{
		Schema::create('tipo_sumision', function(Blueprint $table) {
			$table->increments('idTipoSumision');
			$table->string('nomTipoSumision');
			$table->integer('idDetalleCongreso')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('tipo_sumision');
	}
}