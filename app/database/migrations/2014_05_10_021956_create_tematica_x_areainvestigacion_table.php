<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTematicaXAreainvestigacionTable extends Migration {

	public function up()
	{
		Schema::create('tematica_x_areainvestigacion', function(Blueprint $table) {
			$table->increments('idTematicaXAreainvestigacion');
			$table->integer('idTematica')->unsigned();
			$table->integer('idAreaInvestigacion')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('tematica_x_areainvestigacion');
	}
}