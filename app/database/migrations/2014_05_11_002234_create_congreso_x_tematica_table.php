<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCongresoXTematicaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('congreso_x_tematica', function(Blueprint $table) {
			$table->increments('idCongresoXtematica');
			$table->integer('idCongreso')->unsigned();
			$table->integer('idTematica')->unsigned();
			$table->timestamps();
			$table->unique(array('idCongreso', 'idTematica'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('congreso_x_tematica');
	}

}
