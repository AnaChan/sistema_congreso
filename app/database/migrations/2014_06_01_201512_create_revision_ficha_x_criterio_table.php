<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRevisionFichaXCriterioTable extends Migration {

	public function up()
	{
		Schema::create('revision_ficha_x_criterio', function(Blueprint $table) {
			$table->increments('idRevisionFichaXCriterio');
			$table->integer('idRevisionFicha')->unsigned();
			$table->integer('idPuntaje')->unsigned();
			$table->timestamps();
			$table->unique(array('idRevisionFicha', 'idPuntaje'));
		});
	}

	public function down()
	{
		Schema::drop('revision_ficha_x_criterio');
	}
}