<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAreaInvestigacionTable extends Migration {

	public function up()
	{
		Schema::create('area_investigacion', function(Blueprint $table) {
			$table->increments('idAreaInvestigacion');
			$table->string('nomAreaInvestigacion');
			$table->timestamp('fecCreaAreaInvestigacion')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('fecModAreaInvestigacion');
		});
	}

	public function down()
	{
		Schema::drop('area_investigacion');
	}
}