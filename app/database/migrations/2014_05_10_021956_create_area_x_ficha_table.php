<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAreaXFichaTable extends Migration {

	public function up()
	{
		Schema::create('area_x_ficha', function(Blueprint $table) {
			$table->increments('idAreaXFicha');
			$table->integer('idAreaInvestigacion')->unsigned();
			$table->integer('idFicha')->unsigned();
			$table->timestamps();
			$table->unique(array('idAreaInvestigacion', 'idFicha'));
		});
	}

	public function down()
	{
		Schema::drop('area_x_ficha');
	}
}