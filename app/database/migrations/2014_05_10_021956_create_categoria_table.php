<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoriaTable extends Migration {

	public function up()
	{
		Schema::create('categoria', function(Blueprint $table) {
			$table->increments('idCategoria');
			$table->string('nomCategoria');
		});
	}

	public function down()
	{
		Schema::drop('categoria');
	}
}