<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRevisionPaperXCriterioTable extends Migration {

	public function up()
	{
		Schema::create('revision_paper_x_criterio', function(Blueprint $table) {
			$table->increments('idRevisionPaperXCriterio');
			$table->integer('idRevisionPaper')->unsigned();
			$table->integer('idPuntaje')->unsigned();
			$table->timestamps();
			$table->unique(array('idRevisionPaper', 'idPuntaje'));
		});
	}

	public function down()
	{
		Schema::drop('revision_paper_x_criterio');
	}
}