<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePuntajeTable extends Migration {

	public function up()
	{
		Schema::create('puntaje', function(Blueprint $table) {
			$table->increments('idPuntaje');
			$table->string('nomPuntaje');
			$table->integer('valorPuntaje');
			$table->integer('idCriterioEvaluacion')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('puntaje');
	}
}