<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRevisionPaperTable extends Migration {

	public function up()
	{
		Schema::create('revision_paper', function(Blueprint $table) {
			$table->increments('idRevisionPaper');
			$table->integer('idPaper')->unsigned();
			$table->integer('idUsuario')->unsigned();
			$table->string('comentario');
			$table->timestamps();
			$table->unique(array('idPaper', 'idUsuario'));
		});
	}

	public function down()
	{
		Schema::drop('revision_paper');
	}
}