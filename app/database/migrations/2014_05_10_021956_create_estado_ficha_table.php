<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEstadoFichaTable extends Migration {

	public function up()
	{
		Schema::create('estado_ficha', function(Blueprint $table) {
			$table->increments('idEstadoFicha');
			$table->string('nombreEstado');
			$table->string('valorEstado');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('estado_ficha');
	}
}