<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAreaInteresTable extends Migration {

	public function up()
	{
		//Es la tabla del perfil de revisor
		Schema::create('area_interes', function(Blueprint $table) {
			$table->increments('idAreaInteres');
			$table->integer('idUsuario')->unsigned();
			$table->integer('idTematica')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('area_interes');
	}
	
}