<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTipoFechaTable extends Migration {

	public function up()
	{
		Schema::create('tipo_fecha', function(Blueprint $table) {
			$table->increments('idTipoFecha');
			$table->string('nomTipoFecha');
			$table->string('valorTipoFecha');
		});
	}

	public function down()
	{
		Schema::drop('tipo_fecha');
	}
}